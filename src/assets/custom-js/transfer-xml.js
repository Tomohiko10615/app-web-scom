

function envioXml(valorEnvio) {

	var xml = valorEnvio;

	var xml = xml;
	xmlDoc = $.parseXML( xml ),
	$xml = $( xmlDoc ),

	/* DATOS_COMUNES_PROCESOS_TDC CABECERA BUSCAR VALOR INICIO*/

	$VALOR_NIVEL_OPERATIVO_3 = $xml.find( "VALOR_NIVEL_OPERATIVO_3" );
	$PARAMETRO_NIVEL_OPERATIVO_3 = $xml.find( "PARAMETRO_NIVEL_OPERATIVO_3" );
	$FECHA_Y_HORA_DE_CREACION_DE_LA_ORDEN = $xml.find( "FECHA_Y_HORA_DE_CREACION_DE_LA_ORDEN" );
	$OBSERVACIONES = $xml.find( "OBSERVACIONES" ); //campo nuevo
	$ZONA = $xml.find( "ZONA" );
	$CENTRO_OPERATIVO = $xml.find( "CENTRO_OPERATIVO" ); //campo nuevo
	$TIPO_DE_RED = $xml.find( "TIPO_DE_RED" ); //campo nuevo
	$SECTOR = $xml.find( "SECTOR" ); // campo nuevo
	$CODIGO_CABINA_SECUNDARIA = $xml.find( "CODIGO_CABINA_SECUNDARIA" );

	/* DATOS_COMUNES_PROCESOS_TDC CABECERABUSCAR VALOR FIN*/

	/* SUMINSTROS TABLA BUSCAR VALOR INICIO*/
	$CODIGO_CLIENTE= $xml.find( "CODIGO_CLIENTE");
	$NOMBRE_Y_APELLIDO_CLIENTE= $xml.find( "NOMBRE_Y_APELLIDO_CLIENTE");
	$CODIGO_TIPO_CLIENTE= $xml.find( "CODIGO_TIPO_CLIENTE");
	$RUTA_DE_LECTURA= $xml.find( "RUTA_DE_LECTURA");
	$TARIFA_EXISTENTE= $xml.find( "TARIFA_EXISTENTE");
	$LATITUD_CLIENTE= $xml.find( "LATITUD_CLIENTE");
	$LONGITUD_CLIENTE= $xml.find( "LONGITUD_CLIENTE" );
	$NUMERO_VIA_DIRECCION= $xml.find( "NUMERO_VIA_DIRECCION");
	$ID_COD_MUNICIPIO= $xml.find( "ID_COD_MUNICIPIO"); // nuevo campo
	$PROVINCIA= $xml.find( "PROVINCIA"); // nuevo campo
	$CODIGO_POSTAL= $xml.find( "CODIGO_POSTAL" ); // nuevo campo
	$TEXTO_DIRECCION= $xml.find( "TEXTO_DIRECCION"); // nuevo campo
	$SUCURSAL= $xml.find( "SUCURSAL"); // nuevo campo
	$LOCALIDAD= $xml.find( "LOCALIDAD" ); // nuevo campo
	/* SUMINSTROS TABLA BUSCAR VALOR FIN*/


	/* DATOS_COMUNES_PROCESOS_TDC CABECERA SETTEAR VALOR A DOM INICIO*/
	$( "#VALOR_NIVEL_OPERATIVO_3" ).append( $VALOR_NIVEL_OPERATIVO_3.text() );
	$( "#PARAMETRO_NIVEL_OPERATIVO_3" ).append( $PARAMETRO_NIVEL_OPERATIVO_3.text() );
	$( "#FECHA_Y_HORA_DE_CREACION_DE_LA_ORDEN" ).append( $FECHA_Y_HORA_DE_CREACION_DE_LA_ORDEN.text() );
	$( "#OBSERVACIONES" ).append( $OBSERVACIONES.text() );
	$( "#ZONA" ).append( $ZONA.text() );
	$( "#CENTRO_OPERATIVO" ).append( $CENTRO_OPERATIVO.text() );
	$( "#TIPO_DE_RED" ).append( $TIPO_DE_RED.text() );
	$( "#SECTOR" ).append( $SECTOR.text() );
	$( "#CODIGO_CABINA_SECUNDARIA" ).append( $CODIGO_CABINA_SECUNDARIA.text() );
	/* DATOS_COMUNES_PROCESOS_TDC CABECERA SETTEAR VALOR A DOM FIN*/

	/* SUMINSTROS TABLA SETTEAR VALOR A DOM INICIO*/
	$( "#CODIGO_CLIENTE" ).append( $CODIGO_CLIENTE.text() );
	$( "#NOMBRE_Y_APELLIDO_CLIENTE" ).append( $NOMBRE_Y_APELLIDO_CLIENTE.text() );
	$( "#CODIGO_TIPO_CLIENTE" ).append( $CODIGO_TIPO_CLIENTE.text() );
	$( "#RUTA_DE_LECTURA" ).append( $RUTA_DE_LECTURA.text() );
	$( "#TARIFA_EXISTENTE" ).append( $TARIFA_EXISTENTE.text() );
	$( "#LATITUD_CLIENTE" ).append( $LATITUD_CLIENTE.text() );
	$( "#LONGITUD_CLIENTE" ).append( $LONGITUD_CLIENTE.text() );
	$( "#NUMERO_VIA_DIRECCION" ).append( $NUMERO_VIA_DIRECCION.text() );
	$( "#ID_COD_MUNICIPIO" ).append( $ID_COD_MUNICIPIO.text() );
	$( "#PROVINCIA" ).append( $PROVINCIA.text() );
	$( "#CODIGO_POSTAL" ).append( $CODIGO_POSTAL.text() );
	$( "#TEXTO_DIRECCION" ).append( $TEXTO_DIRECCION.text() );
	$( "#SUCURSAL" ).append( $SUCURSAL.text() );
	$( "#LOCALIDAD" ).append( $LOCALIDAD.text() );
	/* SUMINSTROS TABLA SETTEAR VALOR A DOM FIN*/
  

/*-----------------------------------------TABLA MEDIDORES------------------------------------------INICIO--- */

var tbMedidores="";
	tbMedidores += "<style> table { width: 100%; border: 0; } th { padding: 5px; background-color: rgba(0,0,0,.02); color:rgba(0,0,0,.38); text-align: left; font-size: 12px;} td { padding: 5px; } ";
	tbMedidores += ".card-information__title { font-weight: bolder; font-size: 14px; padding: 5px 0 10px 0; }</style>";

try {
	

	var medidors = $xml.find("MEDIDORS");
	var itemsMedidors =medidors[0].getElementsByTagName("item");
	var divMedidors;
	

	if(itemsMedidors.length>0) {	
		
		
		tbMedidores += " <div class='card-information__title'>MEDIDORES</div> <table id='tbMedidors'>";
		tbMedidores += "<tr><th>NUMERO_MEDIDOR</th><th>MARCA_MEDIDOR</th><th>MODELO_MEDIDOR</th><th>TIPO_MEDIDA_CONEXION</th><th>TIPOLOGIA_MEDICION</th></tr>";
	}

/* 
	var tblecturas = "<table width=100%><tr><th>TIPO_LECTURA</th><th>VALOR_LECTURA</th><th>TIPO_HORARIO_LECTURA</th></tr>";
 */

	for(var i = 0; i<itemsMedidors.length; i++) {

		var itemMedidors =medidors[0].getElementsByTagName("item")[i];

		if(itemMedidors.parentElement.nodeName	== "MEDIDORS") {
		var numeroMedidor = itemMedidors.getElementsByTagName("NUMERO_MEDIDOR")[0] == undefined ? "" : itemMedidors.getElementsByTagName("NUMERO_MEDIDOR")[0].textContent;
		var marcaMedidor = itemMedidors.getElementsByTagName("MARCA_MEDIDOR")[0] == undefined ? "" : itemMedidors.getElementsByTagName("MARCA_MEDIDOR")[0].textContent;
		var modeloMedidor = itemMedidors.getElementsByTagName("MODELO_MEDIDOR")[0] == undefined ? "" : itemMedidors.getElementsByTagName("MODELO_MEDIDOR")[0].textContent; 
		var tipoMedidaConexion = itemMedidors.getElementsByTagName("TIPO_MEDIDA_CONEXION")[0] == undefined ? "" : itemMedidors.getElementsByTagName("TIPO_MEDIDA_CONEXION")[0].textContent; 
		var tipologiaMedicion = itemMedidors.getElementsByTagName("TIPOLOGIA_MEDICION")[0] == undefined ? "" : itemMedidors.getElementsByTagName("TIPOLOGIA_MEDICION")[0].textContent; 
		
		//var itemsLecturas =itemMedidors.getElementsByTagName("item"); 
		
/* 		for(var j = 0; j<itemsLecturas.length; j++) {
			var itemLecturas=itemMedidors.getElementsByTagName("item")[i];
			var tipoLectura=itemLecturas.getElementsByTagName("TIPO_LECTURA")[0].textContent;
			var valorLectura=itemLecturas.getElementsByTagName("VALOR_LECTURA")[0].textContent;
			var tipoHorarioLectura=itemLecturas.getElementsByTagName("TIPO_HORARIO_LECTURA")[0].textContent;
			
			
			tblecturas += "<tr><td>";
			tblecturas += tipoLectura;
			tblecturas += "</td><td>";
			tblecturas += valorLectura;
			tblecturas += "</td><td>";
			tblecturas += tipoHorarioLectura;
			tblecturas += "</td></tr>";
		} */

			//tblecturas += "</table>";
			tbMedidores += "<tr><td><p>";
			tbMedidores += numeroMedidor;
			tbMedidores += "</p></td><td>";
			tbMedidores += marcaMedidor;
			tbMedidores += "</td><td>";
			tbMedidores += modeloMedidor;
			tbMedidores += "</td><td>";
			tbMedidores += tipoMedidaConexion;
			tbMedidores += "</td><td>";
			tbMedidores += tipologiaMedicion;
			tbMedidores += "</td></tr>";
/* 			tbMedidores += "<tr><td colspan='6'><b>LECTURAS</b></td>";
			tbMedidores += "<tr><td colspan='6'>";
			tbMedidores += tblecturas;
			tbMedidores += "</td></tr>";
			tbMedidores += "<tr><td colspan='6' style='height:10px;' class='td-comb'>";
			tbMedidores += "</td></tr>"; */
		}
	}
	tbMedidores += "</table>";
	
} catch (error) {
	tbMedidores="<b>MEDIRORES</b><table><tr><td>No hay información</td></tr></table>";
}
$( "#divMedidors" ).append(tbMedidores);


/*-----------------------------------------TABLA MEDIDORES------------------------------------------FIN--- */

/*-----------------------------------------TABLA ANEXOS------------------------------------------INICIO--- */

try
{
	var ANEXOS = $xml.find("ANEXOS");
	var itemsAnexos =ANEXOS[0].getElementsByTagName("item");
	var tbAnexos="";

	if(itemsAnexos.length>0)
	 {	
		tbAnexos += "<b>ANEXOS</b><table id='tbAnexos'>";
		tbAnexos += "<tr><th>NOMBRE_ANEXO</th><th>RUTA_ANEXO</th><th>OBLIGATORIO_EN_CAMPO</th></tr>";
	 }
	 else 
	 {
		$( "#divAnexos" ).innerHtml("No hay información");
	 }
	 
	 for(var i = 0; i<itemsAnexos.length; i++)
	{
	 var itemAnexos =ANEXOS[0].getElementsByTagName("item")[i];
	 if(itemAnexos.parentElement.nodeName	== "ANEXOS")
	 {
		 var NOMBRE_ANEXO = itemAnexos.getElementsByTagName("NOMBRE_ANEXO")[0] == undefined ? "" : itemAnexos.getElementsByTagName("NOMBRE_ANEXO")[0].textContent;
		 var RUTA_ANEXO = itemAnexos.getElementsByTagName("RUTA_ANEXO")[0] == undefined ? "" : itemAnexos.getElementsByTagName("RUTA_ANEXO")[0].textContent;
		 var OBLIGATORIO_EN_CAMPO = itemAnexos.getElementsByTagName("OBLIGATORIO_EN_CAMPO")[0] == undefined ? "" : itemAnexos.getElementsByTagName("OBLIGATORIO_EN_CAMPO")[0].textContent; 
		
		
		tbAnexos += "<tr><td><p>";
		tbAnexos += NOMBRE_ANEXO;
		tbAnexos += "</p></td><td>";
		tbAnexos += RUTA_ANEXO;
		tbAnexos += "</td><td>";
		tbAnexos += OBLIGATORIO_EN_CAMPO;
		tbAnexos += "</td></tr>";	
	 }
	}
	tbAnexos += "</table>";
} catch (exc)
	{
		tbAnexos="<b>ANEXOS</b><table><tr><td>No hay información</td></tr></table>";
	}

$( "#divAnexos").append(tbAnexos);

/*-----------------------------------------TABLA ANEXOS------------------------------------------FIN--- */

/*-----------------------------------------TABLA CONSUMOS------------------------------------------INCIO--- */
 
/*  try
	{
		var CONSUMOS = $xml.find("CONSUMOS");
		var itemsConsumos =CONSUMOS[0].getElementsByTagName("item");
		var tbConsumos="";

		if(itemsConsumos.length>0)
		 {	
			tbConsumos += "<b>CONSUMOS</b><table border='0' class='table-style' id='tbConsumos'>";
			tbConsumos += "<tr><th>TIPO_CONSUMO</th><th>TIPO_HORARIO_CONSUMO</th><th>VALOR_CONSUMO</th><th>ANOMALIA_DE_LECTURA</th><th>FECHA_CONSUMO</th><th>NUMERO_CUENTA</th></tr>";
		 }
		 else 
		 {
			$( "#divConsumos" ).innerHtml("No hay información");
		 }
		 
		 for(var i = 0; i<itemsConsumos.length; i++)
		{
		 var itemConsumos =CONSUMOS[0].getElementsByTagName("item")[i];
		 if(itemConsumos.parentElement.nodeName	== "CONSUMOS")
		 {
			var TIPO_CONSUMO = itemConsumos.getElementsByTagName("TIPO_CONSUMO")[0].textContent;
			var TIPO_HORARIO_CONSUMO = itemConsumos.getElementsByTagName("TIPO_HORARIO_CONSUMO")[0].textContent;
			var VALOR_CONSUMO = itemConsumos.getElementsByTagName("VALOR_CONSUMO")[0].textContent; 
			var ANOMALIA_DE_LECTURA = itemConsumos.getElementsByTagName("ANOMALIA_DE_LECTURA")[0].textContent; 
			var FECHA_CONSUMO = itemConsumos.getElementsByTagName("FECHA_CONSUMO")[0].textContent;
			var NUMERO_CUENTA = itemConsumos.getElementsByTagName("NUMERO_CUENTA")[0].textContent; 
			
			tbConsumos += "<tr><td><p>";
			tbConsumos += TIPO_CONSUMO;
			tbConsumos += "</p></td><td>";
			tbConsumos += TIPO_HORARIO_CONSUMO;
			tbConsumos += "</td><td>";
			tbConsumos += VALOR_CONSUMO;
			tbConsumos += "</td><td>";
			tbConsumos += ANOMALIA_DE_LECTURA;
			tbConsumos += "</td><td>";
			tbConsumos += FECHA_CONSUMO;
			tbConsumos += "</td><td>";
			tbConsumos += NUMERO_CUENTA;
			tbConsumos += "</td></tr>";	
		 }
		}
		tbConsumos += "</table>";
	}
		catch (exc)
		{
			tbConsumos="<b>CONSUMOS</b><table><tr><td>No hay información</td></tr></table>";
		}

 $( "#divConsumos").append(tbConsumos); */
 
 /*-----------------------------------------TABLA CONSUMOS------------------------------------------FIN--- */
 
 
 
 //////////////////////////////GESTION_IMPAGOS_SCR
 var GESTION_IMPAGOS_SCR = $xml.find("GESTION_IMPAGOS_SCR");		

 if(GESTION_IMPAGOS_SCR.length>0) {
 try {

	
		var xmlText = GESTION_IMPAGOS_SCR[0]?.innerHTML;
		try {

			var beautifiedXmlText = new XmlBeautify().beautify("<GESTION_IMPAGOS_SCR>"+xmlText+"</GESTION_IMPAGOS_SCR>", 
				{
					indent: "  ",  //indent pattern like white spaces
					useSelfClosingElement: true //true:use self-closing element when empty element.
				});
		} catch (error) {
			beautifiedXmlText="";
		}
		

		var tbGESTION_IMPAGOS_SCR="<br/>";
		var tbLISTADO_FACTURA="";
		
		tbGESTION_IMPAGOS_SCR += "<div id='tbGESTION_IMPAGOS_SCR'>";
		tbGESTION_IMPAGOS_SCR += "<div class='card-information__title'>GESTION_IMPAGOS_SCR</div>";
		tbGESTION_IMPAGOS_SCR += "<textarea style='flex-shrink: 0;resize: none;width: 970px;height: 300px'>";
		tbGESTION_IMPAGOS_SCR += beautifiedXmlText;
		tbGESTION_IMPAGOS_SCR += "</textarea></div>";

			/* tbGESTION_IMPAGOS_SCR += "<b>GESTION_IMPAGOS_SCR</b><table border='0' class='table-style' id='tbGESTION_IMPAGOS_SCR'>";
			tbGESTION_IMPAGOS_SCR += "<tr><th>CONTRATISTA</th><th>ESTADO_CONEXION</th><th>FASE</th><th>TIPO_DE_CONEXION</th><th>TIPO_DE_ORDEN</th>";
			tbGESTION_IMPAGOS_SCR += "<th>SEGMENTO</th><th>REPROGRAMADA</th><th>SUBTIPO_DE_CORTE</th><th>SUBTIPO_DE_ORDEN</th><th>TIPO_ZONA</th>";
			tbGESTION_IMPAGOS_SCR += "<th>RADIOFRECUENCIA</th><th>BLUETOOTH</th><th>REFACTURACION</th><th>MOTIVO_DE_LA_RECONEXION</th><th>TIPO_DE_CORTE_Y_RECO</th>";
			tbGESTION_IMPAGOS_SCR += "<th>PUNTO_DE_CORTE_Y_RECO</th><th>ULTIMA_ACCION_EFECTIVA</th><th>SECTOR</th><th>MOTIVO_DEL_CORTE</th><th>DEUDAS_ALTAS</th>";
			tbGESTION_IMPAGOS_SCR += "<th>EM_TUS_MANOS</th><th>VALOR_PRODUCTO_EN_TUS_MANOS</th><th>CAPACIDAD_DE_PROTECCION_DEL_EMPALME</th><th>TIPO_DE_CAJA_MEDICION</th><th>TIPO_DE_CAJA_TOMA</th>";
			tbGESTION_IMPAGOS_SCR += "<th>XML_EXTENSION_PARAMS</th><th>VALOR_RECONEXION</th><th>CLIENTE_VITAL</th><th>NUMERO_DISPLAY_MEDIDOR</th><th>ID_DISPLAY_MEDIDOR</th>";			
			tbGESTION_IMPAGOS_SCR += "<th>MARCA_DISPLAY_MEDIDOR</th><th>MODELO_DISPLAY_MEDIDOR</th><th>LECTURA_DISPLAY_MEDIDOR</th><th>TIPO_DE_RECO</th><th>CODIGO_DE_BARRAS</th>";
			tbGESTION_IMPAGOS_SCR += "<th>FECHA_GENERACION</th><th>FLAG_TIENE_CONVENIO</th><th>TIPO_TENSION</th><th>CODIGO_ZONA_PELIGROSA</th><th>RANKING</th>";
			tbGESTION_IMPAGOS_SCR += "<th>DEUDA_CORTE</th><th>TIPO_DEUDA_CORTE</th><th>PROCESO_DE_CORTE</th><th>CODIGO_COLOR_CAJA_TEXTO</th><th>CODIGO_PLAN_DE_TERRENO</th>";
			tbGESTION_IMPAGOS_SCR += "<th>NUMERO_TRANSFORMADOR_DISTRIBUCION</th><th>ACCION</th><th>TIPO_ACOMETIDA</th></tr>";
		 
			var CONTRATISTA = $xml.find("CONTRATISTA").text();
			var ESTADO_CONEXION = $xml.find("ESTADO_CONEXION").text();
			var FASE = $xml.find("FASE").text(); 
			var TIPO_DE_CONEXION = $xml.find("TIPO_DE_CONEXION").text(); 
			var TIPO_DE_ORDEN = $xml.find("TIPO_DE_ORDEN").text();
			var SEGMENTO = $xml.find("SEGMENTO").text();
			var REPROGRAMADA = $xml.find("REPROGRAMADA").text();
			var SUBTIPO_DE_CORTE = $xml.find("SUBTIPO_DE_CORTE").text();
			var SUBTIPO_DE_ORDEN = $xml.find("SUBTIPO_DE_ORDEN").text();
			var TIPO_ZONA = $xml.find("TIPO_ZONA").text();
			var RADIOFRECUENCIA = $xml.find("RADIOFRECUENCIA").text();
			var BLUETOOTH = $xml.find("BLUETOOTH").text();
			var REFACTURACION = $xml.find("REFACTURACION").text();
			var MOTIVO_DE_LA_RECONEXION = $xml.find("MOTIVO_DE_LA_RECONEXION").text();
			var TIPO_DE_CORTE_Y_RECO = $xml.find("TIPO_DE_CORTE_Y_RECO").text();
			var PUNTO_DE_CORTE_Y_RECO = $xml.find("PUNTO_DE_CORTE_Y_RECO").text();
			var ULTIMA_ACCION_EFECTIVA = $xml.find("ULTIMA_ACCION_EFECTIVA").text();
			var SECTOR = $xml.find("SECTOR").text();
			var MOTIVO_DEL_CORTE = $xml.find("MOTIVO_DEL_CORTE").text();
			var DEUDAS_ALTAS = $xml.find("DEUDAS_ALTAS").text();
			var EM_TUS_MANOS = $xml.find("EM_TUS_MANOS").text();
			var VALOR_PRODUCTO_EN_TUS_MANOS = $xml.find("VALOR_PRODUCTO_EN_TUS_MANOS").text();
			var CAPACIDAD_DE_PROTECCION_DEL_EMPALME = $xml.find("CAPACIDAD_DE_PROTECCION_DEL_EMPALME").text();
			var TIPO_DE_CAJA_MEDICION = $xml.find("TIPO_DE_CAJA_MEDICION").text();
			var TIPO_DE_CAJA_TOMA = $xml.find("TIPO_DE_CAJA_TOMA").text();
			var XML_EXTENSION_PARAMS = $xml.find("XML_EXTENSION_PARAMS").text();
			var VALOR_RECONEXION = $xml.find("VALOR_RECONEXION").text();
			var CLIENTE_VITAL = $xml.find("CLIENTE_VITAL").text();
			var NUMERO_DISPLAY_MEDIDOR = $xml.find("NUMERO_DISPLAY_MEDIDOR").text();
			var ID_DISPLAY_MEDIDOR = $xml.find("ID_DISPLAY_MEDIDOR").text();
			var MARCA_DISPLAY_MEDIDOR = $xml.find("MARCA_DISPLAY_MEDIDOR").text();
			var MODELO_DISPLAY_MEDIDOR = $xml.find("MODELO_DISPLAY_MEDIDOR").text();
			var LECTURA_DISPLAY_MEDIDOR = $xml.find("LECTURA_DISPLAY_MEDIDOR").text();
			var TIPO_DE_RECO = $xml.find("TIPO_DE_RECO").text();
			var CODIGO_DE_BARRAS = $xml.find("CODIGO_DE_BARRAS").text();
			var FECHA_GENERACION = $xml.find("FECHA_GENERACION").text();
			var FLAG_TIENE_CONVENIO = $xml.find("FLAG_TIENE_CONVENIO").text();
			var TIPO_TENSION = $xml.find("TIPO_TENSION").text();
			var CODIGO_ZONA_PELIGROSA = $xml.find("CODIGO_ZONA_PELIGROSA").text();
			var RANKING = $xml.find("RANKING").text();
			var DEUDA_CORTE = $xml.find("DEUDA_CORTE").text();
			var TIPO_DEUDA_CORTE = $xml.find("TIPO_DEUDA_CORTE").text();
			var PROCESO_DE_CORTE = $xml.find("PROCESO_DE_CORTE").text();
			var CODIGO_COLOR_CAJA_TEXTO = $xml.find("CODIGO_COLOR_CAJA_TEXTO").text();
			var CODIGO_PLAN_DE_TERRENO = $xml.find("CODIGO_PLAN_DE_TERRENO").text();
			var NUMERO_TRANSFORMADOR_DISTRIBUCION = $xml.find("NUMERO_TRANSFORMADOR_DISTRIBUCION").text();
			var ACCION = $xml.find("ACCION").text();
			var TIPO_ACOMETIDA = $xml.find("TIPO_ACOMETIDA").text();
			
			
			tbGESTION_IMPAGOS_SCR += "<tr><td><p>";
			tbGESTION_IMPAGOS_SCR += CONTRATISTA;
			tbGESTION_IMPAGOS_SCR += "</p></td><td>";
			tbGESTION_IMPAGOS_SCR += ESTADO_CONEXION;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += FASE;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += TIPO_DE_CONEXION;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += TIPO_DE_ORDEN;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += SEGMENTO;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += REPROGRAMADA;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += SUBTIPO_DE_CORTE;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += SUBTIPO_DE_ORDEN;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += TIPO_ZONA;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += RADIOFRECUENCIA;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += BLUETOOTH;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += REFACTURACION;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += MOTIVO_DE_LA_RECONEXION;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += TIPO_DE_CORTE_Y_RECO;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += PUNTO_DE_CORTE_Y_RECO;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += ULTIMA_ACCION_EFECTIVA;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += SECTOR;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += MOTIVO_DEL_CORTE;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += DEUDAS_ALTAS;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += EM_TUS_MANOS;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += VALOR_PRODUCTO_EN_TUS_MANOS;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += CAPACIDAD_DE_PROTECCION_DEL_EMPALME;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += TIPO_DE_CAJA_MEDICION;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += TIPO_DE_CAJA_TOMA;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += XML_EXTENSION_PARAMS;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += VALOR_RECONEXION;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += CLIENTE_VITAL;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += NUMERO_DISPLAY_MEDIDOR;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += ID_DISPLAY_MEDIDOR;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += MARCA_DISPLAY_MEDIDOR;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += MODELO_DISPLAY_MEDIDOR;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += LECTURA_DISPLAY_MEDIDOR;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += TIPO_DE_RECO;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += CODIGO_DE_BARRAS;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += FECHA_GENERACION;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += FLAG_TIENE_CONVENIO;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += TIPO_TENSION;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += CODIGO_ZONA_PELIGROSA;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += RANKING;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += DEUDA_CORTE;
			tbGESTION_IMPAGOS_SCR += "</td><td>";			
			tbGESTION_IMPAGOS_SCR += TIPO_DEUDA_CORTE;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += PROCESO_DE_CORTE;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += CODIGO_COLOR_CAJA_TEXTO;			
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += CODIGO_PLAN_DE_TERRENO;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += NUMERO_TRANSFORMADOR_DISTRIBUCION;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += ACCION;
			tbGESTION_IMPAGOS_SCR += "</td><td>";
			tbGESTION_IMPAGOS_SCR += TIPO_ACOMETIDA;
			
			tbGESTION_IMPAGOS_SCR += "</td></tr><tr><td colspan=48>";	 */
		 //}
		//}
		
	/* 	try
		{
		
			var LISTADO_FACTURA = $xml.find("LISTADO_FACTURA");
			var itemsLISTADO_FACTURA =LISTADO_FACTURA[0].getElementsByTagName("item");
			if(itemsLISTADO_FACTURA.length>0)
			{
				tbLISTADO_FACTURA="<br/><b>LISTADO_FACTURA</b><table>";
				tbLISTADO_FACTURA += "<tr><th>MES_ANO</th><th>INDICATIVO</th><th>VALOR_DE_LA_FACTURA</th><th>TIPO_DE_FACTURA</th></tr>";
			}
			
			for(var i = 0; i<itemsLISTADO_FACTURA.length; i++)
			{
				var itemListadoFactura =LISTADO_FACTURA[0].getElementsByTagName("item")[i];
				 if(itemListadoFactura.parentElement.nodeName == "LISTADO_FACTURA")
				 {
					 var mesAno = itemListadoFactura.getElementsByTagName("MES_ANO")[0].textContent;
					 var indicativo = itemListadoFactura.getElementsByTagName("INDICATIVO")[0].textContent;
					 var valorFactura = itemListadoFactura.getElementsByTagName("VALOR_DE_LA_FACTURA")[0].textContent;
					 var tipoFactura = itemListadoFactura.getElementsByTagName("TIPO_DE_FACTURA")[0].textContent;
				 }
				 
				 tbLISTADO_FACTURA += "<tr><td>";
				 tbLISTADO_FACTURA += mesAno;
				 tbLISTADO_FACTURA += "</td><td>";
				 tbLISTADO_FACTURA += indicativo;
				 tbLISTADO_FACTURA += "</td><td>";
				 tbLISTADO_FACTURA += valorFactura;
				 tbLISTADO_FACTURA += "</td><td>";
				 tbLISTADO_FACTURA += tipoFactura;
				 tbLISTADO_FACTURA += "</td></tr>";
			}
			
			if(itemsLISTADO_FACTURA.length>0)
			{
				tbLISTADO_FACTURA += "</table>";
			}			
			
			
		}
		catch (exc)
		{
			tbLISTADO_FACTURA="<br/><b>LISTADO_FACTURA</b><table><tr><td>No hay información</td></tr></table>";
		}
		
		tbGESTION_IMPAGOS_SCR += tbLISTADO_FACTURA;
		tbGESTION_IMPAGOS_SCR += "</td></tr></table><br/>"; */
		
	}
	catch (exc)
	{
		tbGESTION_IMPAGOS_SCR="<b>GESTION_IMPAGOS_SCR</b><table><tr><td>No hay información</td></tr></table>";
	}
	}
	else
	{
		tbGESTION_IMPAGOS_SCR="<b>GESTION_IMPAGOS_SCR</b><table><tr><td>No hay información</td></tr></table>";
	}

 $( "#divGESTION_IMPAGOS_SCR").append(tbGESTION_IMPAGOS_SCR);
 
 
 //////////////////////////////GESTION_NUEVAS_CONEXIONES


 var GESTION_NUEVAS_CONEXIONES = $xml.find("GESTION_NUEVAS_CONEXIONES");		

 if(GESTION_NUEVAS_CONEXIONES.length>0) {
 try {

	
		var xmlText = GESTION_NUEVAS_CONEXIONES[0]?.innerHTML;
		try {

			var beautifiedXmlText = new XmlBeautify().beautify("<GESTION_NUEVAS_CONEXIONES>"+xmlText+"</GESTION_NUEVAS_CONEXIONES>", 
				{
					indent: "  ",  //indent pattern like white spaces
					useSelfClosingElement: true //true:use self-closing element when empty element.
				});
		} catch (error) {
			beautifiedXmlText="";
		}
		

		var tbGESTION_NUEVAS_CONEXIONES="<br/>"
		
		tbGESTION_NUEVAS_CONEXIONES += "<div id='tbGESTION_NUEVAS_CONEXIONES'>";
		tbGESTION_NUEVAS_CONEXIONES += "<div class='card-information__title' style='font-weight: bolder'>GESTION_NUEVAS_CONEXIONES</div>";
		tbGESTION_NUEVAS_CONEXIONES += "<textarea style='flex-shrink: 0;resize: none;width: 970px;height: 300px'>";
		tbGESTION_NUEVAS_CONEXIONES += beautifiedXmlText;
		tbGESTION_NUEVAS_CONEXIONES += "</textarea></div>";
		
	}
	catch (exc) {
		tbGESTION_NUEVAS_CONEXIONES="<b>GESTION_NUEVAS_CONEXIONES</b><table><tr><td>No hay información</td></tr></table>";
	}
	}
	else {
		tbGESTION_NUEVAS_CONEXIONES="<b>GESTION_NUEVAS_CONEXIONES</b><table><tr><td>No hay información</td></tr></table>";
	}

 $( "#divGESTION_NUEVAS_CONEXIONES").append(tbGESTION_NUEVAS_CONEXIONES);
/*  try
	{
		var GESTION_NUEVAS_CONEXIONES = $xml.find("GESTION_NUEVAS_CONEXIONES");		
		if(GESTION_NUEVAS_CONEXIONES.length>0)
		{
		var tbGESTION_NUEVAS_CONEXIONES="<br/>";
		var tbNUMERO_ORDEN_DE_SERVICIO="";
		var tbVALADIC="";
		var tbCOTIZACIONS="";
		var tbPRODUCTOSNC="";
		var tbOBSERVACIONCC="";
		var tbCUENTAS="";
		var tbMEDIDOR="";
		var tbLECTURAS="";
		var tbTRANSFORMADOR="";
		var LISTA_CODIGO_MEDIDA="";
		var tbTRANSFORMADOR="";
		
		tbGESTION_NUEVAS_CONEXIONES += "<b>GESTION_NUEVAS_CONEXIONES</b><table border='0' class='table-style' id='tbGESTION_NUEVAS_CONEXIONES'>";
		tbGESTION_NUEVAS_CONEXIONES += "<tr><th>LLAVE</th><th>USUARIO_CREADOR</th><th>TIPO_DOCUMENTO_IDENTIDAD</th><th>NUMERO_DOCUMENTO_IDENTIDAD</th><th>MEDIDOR_VECINO_IZQ</th>";
		tbGESTION_NUEVAS_CONEXIONES += "<th>MEDIDOR_VECINO_DER</th><th>SUBCLASE</th><th>NO_CLIENTE_VECINO_IZQ</th><th>NO_CLIENTE_VECINO_DER</th><th>EMAIL_DE_CONTACTO_CON_EL_CLIENTE</th>";
		tbGESTION_NUEVAS_CONEXIONES += "<th>FASE</th><th>TRANSFORMADOR</th><th>TIPO_DE_EMPALME</th><th>CAPACIDAD_CONTRATADA</th><th>TIPO_PROTECCION_DEL_EMPALME</th>";
		tbGESTION_NUEVAS_CONEXIONES += "<th>TIPO_PROTECCION_DEL_EMPALME_EXISTENTE</th><th>CODIGO_PUNTO_INSPECCIONADO</th><th>TIPO_DE_CAJA_MEDICION</th><th>TIPO_DE_CAJA_TOMA</th><th>8</th>";
		tbGESTION_NUEVAS_CONEXIONES += "<th>CAPACIDAD_DEL_TRANSFORMADOR</th><th>SET</th><th>ALIMENTADOR</th><th>MOTIVO_DE_VENTA</th><th>SUBTIPO_DE_ORDEN</th>";
		tbGESTION_NUEVAS_CONEXIONES += "<th>PROMEDIO_DE_CONSUMO</th><th>TRABAJO</th><th>TIPO_SOLICITUD</th><th>NUMERO_SOLICITUD</th><th>OBRA</th>";
		tbGESTION_NUEVAS_CONEXIONES += "<th>TIPO_OBRA_CIVIL</th><th>CORRECCION</th><th>TIPO</th><th>TIPO_DE_CONEXION</th><th>COMPLEJIDAD_DE_LA_TAREA</th>";
		tbGESTION_NUEVAS_CONEXIONES += "<th>ATENCION_DE_EMERGENCIA</th><th>TIPO_DE_SERVICIO_TEMPORAL</th><th>TIPO_DE_VERIFICACION_COMERCIAL</th><th>TIPO_DE_INSTALACION</th><th>CODIGO_DE_LA_ORDEN</th>";
		tbGESTION_NUEVAS_CONEXIONES += "<th>DESCRIPCION_TIPO_SERVICIO</th><th>TASA_DE_INTERES</th><th>VALOR_UF</th><th>TARIFA_SOLICITADA</th><th>NRO_DE_DEFICIENCIA_OSINERGMIN</th>";
		tbGESTION_NUEVAS_CONEXIONES += "<th>CODIGO_DE_TIPIFICACION_DE_OSINERGMIN</th><th>DESCRIPCION_DE_TIPIFICACION_DE_OSINERGMIN</th><th>POTENCIA_REQUERIDA</th><th>TIPO_DE_ARRANQUE</th><th>FLAG_REFORMA</th>";
		tbGESTION_NUEVAS_CONEXIONES += "<th>REP_DE_PAVIMENTOS</th><th>DESCRICAO_ETAPA</th><th>DESCRICAO_ETAPA_TIPO_SERVICIO</th><th>DESCRICAO_RETORNO</th><th>TEMA</th>";
		tbGESTION_NUEVAS_CONEXIONES += "<th>MOTIVO</th><th>VALOR_TOTAL_MATERIAL</th><th>VALOR_TOTAL_MANO_DE_OBRA</th><th>VALOR_TOTAL_OFERTA_COMERCIALE</th><th>DEFICIENCIA</th>";
		tbGESTION_NUEVAS_CONEXIONES += "<th>CAPACIDAD_DE_PROTECCION_DEL_EMPALME</th><th>CLASE</th><th>FECHA_PAGO</th><th>CAPACIDAD_DEL_EMPALME</th><th>XML_EXTENSION_PARAMS</th>";
		tbGESTION_NUEVAS_CONEXIONES += "<th>TIPO_MEDIDA_CONEXION</th><th>IVA</th><th>TASA_INTERES_CONNEXION</th><th>VALOR_DEL_KILOWATT</th><th>CONCENTRADOR_PRIMARIO</th>";
		tbGESTION_NUEVAS_CONEXIONES += "<th>CONCENTRADOR_SECONDARIO</th><th>POSICION_MEDIDOR_CONCENTRADOR_PS1</th><th>POSICION_MEDIDOR_CONCENTRADOR_PS2</th><th>POSICION_MEDIDOR_CONCENTRADOR_PS3</th><th>NUMERO_DISPLAY</th>";
		tbGESTION_NUEVAS_CONEXIONES += "<th>ID_DISPLAY</th><th>LECTURA_DISPLAY</th><th>MARCA_DISPLAY</th><th>MODELO_DISPLAY</th><th>SHUNT1</th>";
		tbGESTION_NUEVAS_CONEXIONES += "<th>SHUNT2</th><th>SHUNT3</th><th>NUMERO_SERIE_CS</th><th>NUMERO_GRUPO</th><th>CODIGO_AGRUPAMIENTO</th>";
		tbGESTION_NUEVAS_CONEXIONES += "<th>FAIXA_DE_CARGA</th><th>VENTA_DE_PADRAO</th><th>CENTRO_OPERATIVO_ACT</th><th>DV_RUT</th><th>CONSTANTE</th>";
		tbGESTION_NUEVAS_CONEXIONES += "<th>METROS_PARA_CALCULO_DURACION</th><th>DATO_TECNICO_PRC</th><th>RESTRICCION_DE_CONVENIOS</th><th>CANTIDAD_SUMINISTRO</th><th>CAMBIA_MEDIDOR</th>";
		tbGESTION_NUEVAS_CONEXIONES += "<th>GRUPO_PRODUCTO</th><th>TIPO_TENSION</th><th>ID_RELACION</th><th>NOMBRE_TD</th><th>PROPIEDAD_DEL_MEDIDOR</th>";
		tbGESTION_NUEVAS_CONEXIONES += "<th>TIPO_RED_D</th><th>CENTRO_DE_SERVICIO</th><th>TIPO_DE_OBRA</th><th>TIPOLOGIA_MEDICION</th>";
		tbGESTION_NUEVAS_CONEXIONES += "</tr>";
		
		
		var LLAVE = $xml.find("LLAVE").text();
		var USUARIO_CREADOR = $xml.find("USUARIO_CREADOR").text();
		var TIPO_DOCUMENTO_IDENTIDAD = $xml.find("TIPO_DOCUMENTO_IDENTIDAD").text();
		var NUMERO_DOCUMENTO_IDENTIDAD = $xml.find("NUMERO_DOCUMENTO_IDENTIDAD").text();
		var MEDIDOR_VECINO_IZQ = $xml.find("MEDIDOR_VECINO_IZQ").text();
		var MEDIDOR_VECINO_DER = $xml.find("MEDIDOR_VECINO_DER").text();
		var SUBCLASE = $xml.find("SUBCLASE").text();
		var NO_CLIENTE_VECINO_IZQ = $xml.find("NO_CLIENTE_VECINO_IZQ").text();
		var NO_CLIENTE_VECINO_DER = $xml.find("NO_CLIENTE_VECINO_DER").text();
		var EMAIL_DE_CONTACTO_CON_EL_CLIENTE = $xml.find("EMAIL_DE_CONTACTO_CON_EL_CLIENTE").text();
		var FASE = $xml.find("FASE").text();
		var TRANSFORMADOR = $xml.find("TRANSFORMADOR").text();
		var TIPO_DE_EMPALME = $xml.find("TIPO_DE_EMPALME").text();
		var CAPACIDAD_CONTRATADA = $xml.find("CAPACIDAD_CONTRATADA").text();
		var TIPO_PROTECCION_DEL_EMPALME = $xml.find("TIPO_PROTECCION_DEL_EMPALME").text();
		var TIPO_PROTECCION_DEL_EMPALME_EXISTENTE = $xml.find("TIPO_PROTECCION_DEL_EMPALME_EXISTENTE").text();
		var CODIGO_PUNTO_INSPECCIONADO = $xml.find("CODIGO_PUNTO_INSPECCIONADO").text();
		var TIPO_DE_CAJA_MEDICION = $xml.find("TIPO_DE_CAJA_MEDICION").text();
		var TIPO_DE_CAJA_TOMA = $xml.find("TIPO_DE_CAJA_TOMA").text();
		var TIPO_ACOMETIDA = $xml.find("TIPO_ACOMETIDA").text();
		var CAPACIDAD_DEL_TRANSFORMADOR = $xml.find("CAPACIDAD_DEL_TRANSFORMADOR").text();
		var SET = $xml.find("SET").text();
		var ALIMENTADOR = $xml.find("ALIMENTADOR").text();
		var MOTIVO_DE_VENTA = $xml.find("MOTIVO_DE_VENTA").text();
		var SUBTIPO_DE_ORDEN = $xml.find("SUBTIPO_DE_ORDEN").text();
		var PROMEDIO_DE_CONSUMO = $xml.find("PROMEDIO_DE_CONSUMO").text();
		var TRABAJO = $xml.find("TRABAJO").text();
		var TIPO_SOLICITUD = $xml.find("TIPO_SOLICITUD").text();
		var NUMERO_SOLICITUD = $xml.find("NUMERO_SOLICITUD").text();
		var OBRA = $xml.find("OBRA").text();
		var TIPO_OBRA_CIVIL = $xml.find("TIPO_OBRA_CIVIL").text();
		var CORRECCION = $xml.find("CORRECCION").text();
		var TIPO = $xml.find("TIPO").text();
		var TIPO_DE_CONEXION = $xml.find("TIPO_DE_CONEXION").text();
		var COMPLEJIDAD_DE_LA_TAREA = $xml.find("COMPLEJIDAD_DE_LA_TAREA").text();
		var ATENCION_DE_EMERGENCIA = $xml.find("ATENCION_DE_EMERGENCIA").text();
		var TIPO_DE_SERVICIO_TEMPORAL = $xml.find("TIPO_DE_SERVICIO_TEMPORAL").text();
		var TIPO_DE_VERIFICACION_COMERCIAL = $xml.find("TIPO_DE_VERIFICACION_COMERCIAL").text();
		var TIPO_DE_INSTALACION = $xml.find("TIPO_DE_INSTALACION").text();
		var CODIGO_DE_LA_ORDEN = $xml.find("CODIGO_DE_LA_ORDEN").text();
		var DESCRIPCION_TIPO_SERVICIO = $xml.find("DESCRIPCION_TIPO_SERVICIO").text();
		var TASA_DE_INTERES = $xml.find("TASA_DE_INTERES").text();
		var VALOR_UF = $xml.find("VALOR_UF").text();
		var TARIFA_SOLICITADA = $xml.find("TARIFA_SOLICITADA").text();
		var NRO_DE_DEFICIENCIA_OSINERGMIN = $xml.find("NRO_DE_DEFICIENCIA_OSINERGMIN").text();
		var CODIGO_DE_TIPIFICACION_DE_OSINERGMIN = $xml.find("CODIGO_DE_TIPIFICACION_DE_OSINERGMIN").text();
		var DESCRIPCION_DE_TIPIFICACION_DE_OSINERGMIN = $xml.find("DESCRIPCION_DE_TIPIFICACION_DE_OSINERGMIN").text();
		var POTENCIA_REQUERIDA = $xml.find("POTENCIA_REQUERIDA").text();
		var TIPO_DE_ARRANQUE = $xml.find("TIPO_DE_ARRANQUE").text();
		var FLAG_REFORMA = $xml.find("FLAG_REFORMA").text();
		var REP_DE_PAVIMENTOS = $xml.find("REP_DE_PAVIMENTOS").text();
		var DESCRICAO_ETAPA = $xml.find("DESCRICAO_ETAPA").text();
		var DESCRICAO_ETAPA_TIPO_SERVICIO = $xml.find("DESCRICAO_ETAPA_TIPO_SERVICIO").text();
		var DESCRICAO_RETORNO = $xml.find("DESCRICAO_RETORNO").text();
		var TEMA = $xml.find("TEMA").text();
		var MOTIVO = $xml.find("MOTIVO").text();
		var VALOR_TOTAL_MATERIAL = $xml.find("VALOR_TOTAL_MATERIAL").text();
		var VALOR_TOTAL_MANO_DE_OBRA = $xml.find("VALOR_TOTAL_MANO_DE_OBRA").text();
		var VALOR_TOTAL_OFERTA_COMERCIALE = $xml.find("VALOR_TOTAL_OFERTA_COMERCIALE").text();
		var DEFICIENCIA = $xml.find("DEFICIENCIA").text();
		var CAPACIDAD_DE_PROTECCION_DEL_EMPALME = $xml.find("CAPACIDAD_DE_PROTECCION_DEL_EMPALME").text();
		var CLASE = $xml.find("CLASE").text();
		var FECHA_PAGO = $xml.find("FECHA_PAGO").text();
		var CAPACIDAD_DEL_EMPALME = $xml.find("CAPACIDAD_DEL_EMPALME").text();
		var XML_EXTENSION_PARAMS = $xml.find("XML_EXTENSION_PARAMS").text();
		var TIPO_MEDIDA_CONEXION = $xml.find("TIPO_MEDIDA_CONEXION").text();
		var IVA = $xml.find("IVA").text();
		var TASA_INTERES_CONNEXION = $xml.find("TASA_INTERES_CONNEXION").text();
		var VALOR_DEL_KILOWATT = $xml.find("VALOR_DEL_KILOWATT").text();
		var CONCENTRADOR_PRIMARIO = $xml.find("CONCENTRADOR_PRIMARIO").text();
		var CONCENTRADOR_SECONDARIO = $xml.find("CONCENTRADOR_SECONDARIO").text();
		var POSICION_MEDIDOR_CONCENTRADOR_PS1 = $xml.find("POSICION_MEDIDOR_CONCENTRADOR_PS1").text();
		var POSICION_MEDIDOR_CONCENTRADOR_PS2 = $xml.find("POSICION_MEDIDOR_CONCENTRADOR_PS2").text();
		var POSICION_MEDIDOR_CONCENTRADOR_PS3 = $xml.find("POSICION_MEDIDOR_CONCENTRADOR_PS3").text();
		var NUMERO_DISPLAY = $xml.find("NUMERO_DISPLAY").text();
		var ID_DISPLAY = $xml.find("ID_DISPLAY").text();
		var LECTURA_DISPLAY = $xml.find("LECTURA_DISPLAY").text();
		var MARCA_DISPLAY = $xml.find("MARCA_DISPLAY").text();
		var MODELO_DISPLAY = $xml.find("MODELO_DISPLAY").text();
		var SHUNT1 = $xml.find("SHUNT1").text();
		var SHUNT2 = $xml.find("SHUNT2").text();
		var SHUNT3 = $xml.find("SHUNT3").text();
		var NUMERO_SERIE_CS = $xml.find("NUMERO_SERIE_CS").text();
		var NUMERO_GRUPO = $xml.find("NUMERO_GRUPO").text();
		var CODIGO_AGRUPAMIENTO = $xml.find("CODIGO_AGRUPAMIENTO").text();
		var FAIXA_DE_CARGA = $xml.find("FAIXA_DE_CARGA").text();
		var VENTA_DE_PADRAO = $xml.find("VENTA_DE_PADRAO").text();
		var CENTRO_OPERATIVO_ACT = $xml.find("CENTRO_OPERATIVO_ACT").text();
		var DV_RUT = $xml.find("DV_RUT").text();
		var CONSTANTE = $xml.find("CONSTANTE").text();
		var METROS_PARA_CALCULO_DURACION = $xml.find("METROS_PARA_CALCULO_DURACION").text();
		var DATO_TECNICO_PRC = $xml.find("DATO_TECNICO_PRC").text();
		var RESTRICCION_DE_CONVENIOS = $xml.find("RESTRICCION_DE_CONVENIOS").text();
		var CANTIDAD_SUMINISTRO = $xml.find("CANTIDAD_SUMINISTRO").text();
		var CAMBIA_MEDIDOR = $xml.find("CAMBIA_MEDIDOR").text();
		var GRUPO_PRODUCTO = $xml.find("GRUPO_PRODUCTO").text();
		var TIPO_TENSION = $xml.find("TIPO_TENSION").text();
		var ID_RELACION = $xml.find("ID_RELACION").text();
		var NOMBRE_TD = $xml.find("NOMBRE_TD").text();
		var PROPIEDAD_DEL_MEDIDOR = $xml.find("PROPIEDAD_DEL_MEDIDOR").text();
		var TIPO_RED_D = $xml.find("TIPO_RED_D").text();
		var CENTRO_DE_SERVICIO = $xml.find("CENTRO_DE_SERVICIO").text();
		var TIPO_DE_OBRA = $xml.find("TIPO_DE_OBRA").text();
		var TIPOLOGIA_MEDICION = $xml.find("TIPOLOGIA_MEDICION").text();
		
		
		tbGESTION_NUEVAS_CONEXIONES += "<tr><td><p>";
		tbGESTION_NUEVAS_CONEXIONES += LLAVE;
		tbGESTION_NUEVAS_CONEXIONES += "</p></td><td>";
		tbGESTION_NUEVAS_CONEXIONES += USUARIO_CREADOR;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TIPO_DOCUMENTO_IDENTIDAD;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += NUMERO_DOCUMENTO_IDENTIDAD;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += MEDIDOR_VECINO_IZQ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += MEDIDOR_VECINO_DER ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += SUBCLASE ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += NO_CLIENTE_VECINO_IZQ ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += NO_CLIENTE_VECINO_DER ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += EMAIL_DE_CONTACTO_CON_EL_CLIENTE ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += FASE ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TRANSFORMADOR ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TIPO_DE_EMPALME ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += CAPACIDAD_CONTRATADA ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TIPO_PROTECCION_DEL_EMPALME ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TIPO_PROTECCION_DEL_EMPALME_EXISTENTE ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += CODIGO_PUNTO_INSPECCIONADO ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TIPO_DE_CAJA_MEDICION ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TIPO_DE_CAJA_TOMA ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TIPO_ACOMETIDA ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += CAPACIDAD_DEL_TRANSFORMADOR ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += SET ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += ALIMENTADOR ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += MOTIVO_DE_VENTA ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += SUBTIPO_DE_ORDEN ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += PROMEDIO_DE_CONSUMO ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TRABAJO ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TIPO_SOLICITUD ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += NUMERO_SOLICITUD ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += OBRA ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TIPO_OBRA_CIVIL ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += CORRECCION ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TIPO ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TIPO_DE_CONEXION ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += COMPLEJIDAD_DE_LA_TAREA ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += ATENCION_DE_EMERGENCIA ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TIPO_DE_SERVICIO_TEMPORAL ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TIPO_DE_VERIFICACION_COMERCIAL ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TIPO_DE_INSTALACION ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += CODIGO_DE_LA_ORDEN ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += DESCRIPCION_TIPO_SERVICIO ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TASA_DE_INTERES ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += VALOR_UF ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TARIFA_SOLICITADA ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += NRO_DE_DEFICIENCIA_OSINERGMIN ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += CODIGO_DE_TIPIFICACION_DE_OSINERGMIN ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += DESCRIPCION_DE_TIPIFICACION_DE_OSINERGMIN ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += POTENCIA_REQUERIDA ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TIPO_DE_ARRANQUE ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += FLAG_REFORMA ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += REP_DE_PAVIMENTOS ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += DESCRICAO_ETAPA ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += DESCRICAO_ETAPA_TIPO_SERVICIO ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += DESCRICAO_RETORNO ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TEMA ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += MOTIVO ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += VALOR_TOTAL_MATERIAL ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += VALOR_TOTAL_MANO_DE_OBRA ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += VALOR_TOTAL_OFERTA_COMERCIALE ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += DEFICIENCIA ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += CAPACIDAD_DE_PROTECCION_DEL_EMPALME ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += CLASE ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += FECHA_PAGO ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += CAPACIDAD_DEL_EMPALME ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += XML_EXTENSION_PARAMS ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TIPO_MEDIDA_CONEXION ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += IVA ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TASA_INTERES_CONNEXION ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += VALOR_DEL_KILOWATT ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += CONCENTRADOR_PRIMARIO ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += CONCENTRADOR_SECONDARIO ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += POSICION_MEDIDOR_CONCENTRADOR_PS1 ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += POSICION_MEDIDOR_CONCENTRADOR_PS2 ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += POSICION_MEDIDOR_CONCENTRADOR_PS3 ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += NUMERO_DISPLAY ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += ID_DISPLAY ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += LECTURA_DISPLAY ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += MARCA_DISPLAY ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += MODELO_DISPLAY ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += SHUNT1 ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += SHUNT2 ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += SHUNT3 ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += NUMERO_SERIE_CS ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += NUMERO_GRUPO ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += CODIGO_AGRUPAMIENTO ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += FAIXA_DE_CARGA ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += VENTA_DE_PADRAO ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += CENTRO_OPERATIVO_ACT ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += DV_RUT ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += CONSTANTE ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += METROS_PARA_CALCULO_DURACION ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += DATO_TECNICO_PRC ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += RESTRICCION_DE_CONVENIOS ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += CANTIDAD_SUMINISTRO ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += CAMBIA_MEDIDOR ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += GRUPO_PRODUCTO ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TIPO_TENSION ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += ID_RELACION ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += NOMBRE_TD ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += PROPIEDAD_DEL_MEDIDOR ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TIPO_RED_D ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += CENTRO_DE_SERVICIO ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TIPO_DE_OBRA ;
		tbGESTION_NUEVAS_CONEXIONES += "</td><td>";
		tbGESTION_NUEVAS_CONEXIONES += TIPOLOGIA_MEDICION ;
		tbGESTION_NUEVAS_CONEXIONES += "</td></tr><tr><td colspan=104>hola";	
		tbGESTION_NUEVAS_CONEXIONES += "</td>";
		}
		else
		{
			tbGESTION_NUEVAS_CONEXIONES="<b>GESTION_NUEVAS_CONEXIONES</b><table><tr><td>No hay información</td></tr></table>";
		}
	}
	catch (exc)
	{
		tbGESTION_NUEVAS_CONEXIONES="<b>GESTION_NUEVAS_CONEXIONES</b><table><tr><td>No hay información</td></tr></table>";
	}
	
	$( "#divGESTION_NUEVAS_CONEXIONES").append(tbGESTION_NUEVAS_CONEXIONES); */
 
 //////////////////////////////GESTION_PERDIDAS_INSPECCIONES

 	var GESTION_PERDIDAS_INSPECCIONES = $xml.find("GESTION_PERDIDAS_INSPECCIONES");		

	if(GESTION_PERDIDAS_INSPECCIONES.length>0) {

		try {

			var xmlText = GESTION_PERDIDAS_INSPECCIONES[0]?.innerHTML;

			try {

				var beautifiedXmlText = new XmlBeautify().beautify("<GESTION_PERDIDAS_INSPECCIONES>"+xmlText+"</GESTION_PERDIDAS_INSPECCIONES>", 
					{
						indent: "  ",  //indent pattern like white spaces
						useSelfClosingElement: true //true:use self-closing element when empty element.
					});
			} catch (error) {
				beautifiedXmlText="";
			}


			var tbGESTION_PERDIDAS_INSPECCIONES="<br/>";
			console.log(beautifiedXmlText);

			tbGESTION_PERDIDAS_INSPECCIONES += "<div id='GESTION_PERDIDAS_INSPECCIONES'>";
			tbGESTION_PERDIDAS_INSPECCIONES += "<div class='card-information__title'>GESTION_PERDIDAS_INSPECCIONES</div>";
			tbGESTION_PERDIDAS_INSPECCIONES += "<textarea style='flex-shrink: 0;resize: none;width: 970px;height: 300px'>";
			tbGESTION_PERDIDAS_INSPECCIONES += beautifiedXmlText;
			tbGESTION_PERDIDAS_INSPECCIONES += "</textarea></div>";

		} catch (exc) {

			tbGESTION_PERDIDAS_INSPECCIONES="<b>GESTION_PERDIDAS_INSPECCIONES</b><table><tr><td>No hay información</td></tr></table>";
		}
	} else {
		
		tbGESTION_PERDIDAS_INSPECCIONES="<b>GESTION_PERDIDAS_INSPECCIONES</b><table><tr><td>No hay información</td></tr></table>";
	}

 $( "#divGESTION_PERDIDAS_INSPECCIONES").append(tbGESTION_PERDIDAS_INSPECCIONES);
 
 
 //////////////////////////////GESTION_PERDIDAS_NORMALIZACIONES
 var GESTION_PERDIDAS_NORMALIZACIONES = $xml.find("GESTION_PERDIDAS_NORMALIZACIONES");		

 if(GESTION_PERDIDAS_NORMALIZACIONES.length>0) {
 try {

	
		var xmlText = GESTION_PERDIDAS_NORMALIZACIONES[0]?.innerHTML;
		try {

			var beautifiedXmlText = new XmlBeautify().beautify("<GESTION_PERDIDAS_NORMALIZACIONES>"+xmlText+"</GESTION_PERDIDAS_NORMALIZACIONES>", 
				{
					indent: "  ",  //indent pattern like white spaces
					useSelfClosingElement: true //true:use self-closing element when empty element.
				});
		} catch (error) {
			beautifiedXmlText="";
		}
		

		var tbGESTION_PERDIDAS_NORMALIZACIONES="<br/>";
		var tbLISTADO_FACTURA="";
		console.log(beautifiedXmlText);
		
		tbGESTION_PERDIDAS_NORMALIZACIONES += "<div id='tbGESTION_PERDIDAS_NORMALIZACIONES'>";
		tbGESTION_PERDIDAS_NORMALIZACIONES += "<div class='card-information__title'>GESTION_PERDIDAS_NORMALIZACIONES</div>";
		tbGESTION_PERDIDAS_NORMALIZACIONES += "<textarea style='flex-shrink: 0;resize: none;width: 970px;height: 300px'>";
		tbGESTION_PERDIDAS_NORMALIZACIONES += beautifiedXmlText;
		tbGESTION_PERDIDAS_NORMALIZACIONES += "</textarea></div>";
		
	}
	catch (exc)
	{
		tbGESTION_PERDIDAS_NORMALIZACIONES="<b>GESTION_PERDIDAS_NORMALIZACIONES</b><table><tr><td>No hay información</td></tr></table>";
	}
	}
	else
	{
		tbGESTION_PERDIDAS_NORMALIZACIONES="<b>GESTION_PERDIDAS_NORMALIZACIONES</b><table><tr><td>No hay información</td></tr></table>";
	}

 $( "#divGESTION_PERDIDAS_NORMALIZACIONES").append(tbGESTION_PERDIDAS_NORMALIZACIONES);
 
 //////////////////////////////GESTION_IMPAGOS_COBRANZA
 var GESTION_IMPAGOS_COBRANZA = $xml.find("GESTION_IMPAGOS_COBRANZA");		

 if(GESTION_IMPAGOS_COBRANZA.length>0) {
 try {

	
		var xmlText = GESTION_IMPAGOS_COBRANZA[0]?.innerHTML;
		try {

			var beautifiedXmlText = new XmlBeautify().beautify("<GESTION_IMPAGOS_COBRANZA>"+xmlText+"</GESTION_IMPAGOS_COBRANZA>", 
				{
					indent: "  ",  //indent pattern like white spaces
					useSelfClosingElement: true //true:use self-closing element when empty element.
				});
		} catch (error) {
			beautifiedXmlText="";
		}
		

		var tbGESTION_IMPAGOS_COBRANZA="<br/>";
		var tbLISTADO_FACTURA="";
		console.log(beautifiedXmlText);
		
		tbGESTION_IMPAGOS_COBRANZA += "<div id='tbGESTION_IMPAGOS_COBRANZA'>";
		tbGESTION_IMPAGOS_COBRANZA += "<div class='card-information__title'>GESTION_IMPAGOS_COBRANZA</div>";
		tbGESTION_IMPAGOS_COBRANZA += "<textarea style='flex-shrink: 0;resize: none;width: 970px;height: 300px'>";
		tbGESTION_IMPAGOS_COBRANZA += beautifiedXmlText;
		tbGESTION_IMPAGOS_COBRANZA += "</textarea></div>";
		
	}
	catch (exc)
	{
		tbGESTION_IMPAGOS_COBRANZA="<b>GESTION_IMPAGOS_COBRANZA</b><table><tr><td>No hay información</td></tr></table>";
	}
	}
	else
	{
		tbGESTION_IMPAGOS_COBRANZA="<b>GESTION_IMPAGOS_COBRANZA</b><table><tr><td>No hay información</td></tr></table>";
	}

 $( "#divGESTION_IMPAGOS_COBRANZA").append(tbGESTION_IMPAGOS_COBRANZA);
}




function recepcionXml(valorRecepcion) {

	var xml = valorRecepcion;
	xmlDoc = $.parseXML(xml),
	$xml = $(xmlDoc),
	// Se busca el valor en el XML y se guarda en la variable
	$Codigo_Resultado = $xml.find("Codigo_Resultado");
	$Codigo_Causal_Resultado = $xml.find("Codigo_Causal_Resultado");
	$Codigo_Nota_Codificada = $xml.find("Codigo_Nota_Codificada");
	$Inicio_TdC = $xml.find("Inicio_TdC");
	$Fin_TdC = $xml.find("Fin_TdC");
	$Duracion_Ejecucion = $xml.find("Duracion_Ejecucion");
	$Matricula_Usuario = $xml.find("Matricula_Usuario");
	$Apellido_Usuario = $xml.find("Apellido_Usuario");
	$Nombre_Usuario = $xml.find("Nombre_Usuario");
	$Placa_Vehiculo = $xml.find("Placa_Vehiculo");
	$Codigo_Cuadrilla = $xml.find("Codigo_Cuadrilla");
	$latitud_empalme = $xml.find("latitud_empalme");
	$latitud_medidor = $xml.find("latitud_medidor");
	$longitud_empalme = $xml.find("longitud_empalme");
	$longitud_medidor = $xml.find("longitud_medidor");
//INICIO - REQ # 5 - JEGALARZA
$Fin_Operacion = $xml.find("Fin_Operacion");
$Notas_Operacion = $xml.find("Notas_Operacion");
$nro_informe_inspeccion = $xml.find("nro_informe_inspeccion");
$Codigo_Contratista = $xml.find("Codigo_Contratista");
$Tipo_Inspeccion = $xml.find("tipoInspeccion");
$Motivo_Inspeccion = $xml.find("motivoInspeccion");
$Identificador_Suministro = $xml.find("identificadorSuministro");
$Trabajo_coordinado = $xml.find("Trabajo_coordinado");
$Aprueba_Inspeccion = $xml.find("Aprueba_Inspeccion");
$Fecha_de_Notificacion_de_Aviso_intervencion = $xml.find("Fecha_de_Notificacion_de_Aviso_intervencion");
$Registro_de_cumplimiento_establecido_Art_171_RLCE = $xml.find("Registro_de_cumplimiento_establecido_Art_171_RLCE");
$Registro_de_cumplimiento_del_numeral_71_Norma_Reintegros_Recuperos = $xml.find("Registro_de_cumplimiento_del_numeral_71_Norma_Reintegros_Recuperos");
$reg_eval_general_de_la_conexion_electrica = $xml.find("reg_eval_general_de_la_conexion_electrica");
$reg_eval_general_del_sistema_medicion = $xml.find("reg_eval_general_del_sistema_medicion");
$Fecha_Propuesta_de_inicio_intervencion_Aviso_intervencion = $xml.find("Fecha_Propuesta_de_inicio_intervencion_Aviso_intervencion");
$Hora_Propuesta_de_inicio_intervencion_Aviso_Previo_intervencion = $xml.find("Hora_Propuesta_de_inicio_intervencion_Aviso_Previo_intervencion");
$Fecha_de_recepcion_del_Aviso_Previo_intervencion = $xml.find("Fecha_de_recepcion_del_Aviso_Previo_intervencion");
$Hora_de_recepcion_del_Aviso_Previo_intervencion = $xml.find("Hora_de_recepcion_del_Aviso_Previo_intervencion");
$Prueba_Vacio_AD = $xml.find("Prueba_Vacio_AD");
$Sticker_de_contraste = $xml.find("Sticker_de_contraste");
$Sin_mica_Mica_Rota = $xml.find("Sin_mica_Mica_Rota");
$Caja_sin_tapa_Tapa_mal_estado = $xml.find("Caja_sin_tapa_Tapa_mal_estado");
$Sin_cerradura_cerradura_en_mal_estado = $xml.find("Sin_cerradura_cerradura_en_mal_estado");
$estadoConexion = $xml.find("estadoConexion");
$Cliente_permite_inventario_carga = $xml.find("Cliente_permite_inventario_carga");
$cortado_art_90 = $xml.find("cortado_art_90");
$ESTADO_CAJA_PORTAMEDIDOR = $xml.find("ESTADO_CAJA_PORTAMEDIDOR");	
$TOMA_CON_REJA = $xml.find("TOMA_CON_REJA");
$CAJA_CON_REJA = $xml.find("CAJA_CON_REJA");

//FIN - REQ # 5 - JEGALARZA

 
 
	// Se envia el valor guradada en la variable al div para mostrarlo
	$("#Codigo_Resultado_RECEPCION").append($Codigo_Resultado.text());
	$("#Codigo_Causal_Resultado_RECEPCION").append($Codigo_Causal_Resultado.text());
	$("#Codigo_Nota_Codificada_RECEPCION").append($Codigo_Nota_Codificada.text());
	$("#Inicio_TdC_RECEPCION").append($Inicio_TdC.text());
	$("#Fin_TdC_RECEPCION").append($Fin_TdC.text());
	$("#Duracion_Ejecucion_RECEPCION").append($Duracion_Ejecucion.text());
	$("#Matricula_Usuario_RECEPCION").append($Matricula_Usuario.text());
	$("#Apellido_Usuario_RECEPCION").append($Apellido_Usuario.text());
	$("#Nombre_Usuario_RECEPCION").append($Nombre_Usuario.text());
	$("#Placa_Vehiculo_RECEPCION").append($Placa_Vehiculo.text());
	$("#Codigo_Cuadrilla_RECEPCION").append($Codigo_Cuadrilla.text());
	$("#latitud_empalme_RECEPCION").append($latitud_empalme.text());
	$("#latitud_medidor_RECEPCION").append($latitud_medidor.text());
	$("#longitud_empalme_RECEPCION").append($longitud_empalme.text());
	$("#longitud_medidor_RECEPCION").append($longitud_medidor.text());
//INICIO - REQ # 5 - JEGALARZA
$("#Fin_Operacion_RECEPCION").append($Fin_Operacion.text());
$("#Notas_Operacion_RECEPCION").append($Notas_Operacion.text());
$("#nro_informe_inspeccion_RECEPCION").append($nro_informe_inspeccion.text());
$("#Codigo_Contratista_RECEPCION").append($Codigo_Contratista.text());
$("#Tipo_Inspeccion_RECEPCION").append($Tipo_Inspeccion.text());
$("#Motivo_Inspeccion_RECEPCION").append($Motivo_Inspeccion.text());
$("#Identificador_Suministro_RECEPCION").append($Identificador_Suministro.text());
$("#Trabajo_coordinado_RECEPCION").append($Trabajo_coordinado.text());
$("#Aprueba_Inspeccion_RECEPCION").append($Aprueba_Inspeccion.text());
$("#Fecha_de_Notificacion_de_Aviso_intervencion_RECEPCION").append($Fecha_de_Notificacion_de_Aviso_intervencion.text());
$("#Registro_de_cumplimiento_establecido_Art_171_RLCE_RECEPCION").append($Registro_de_cumplimiento_establecido_Art_171_RLCE.text());
$("#Registro_de_cumplimiento_del_numeral_71_Norma_Reintegros_Recuperos_RECEPCION").append($Registro_de_cumplimiento_del_numeral_71_Norma_Reintegros_Recuperos.text());
$("#reg_eval_general_de_la_conexion_electrica_RECEPCION").append($reg_eval_general_de_la_conexion_electrica.text());
$("#reg_eval_general_del_sistema_medicion_RECEPCION").append($reg_eval_general_del_sistema_medicion.text());
$("#Fecha_Propuesta_de_inicio_intervencion_Aviso_intervencion_RECEPCION").append($Fecha_Propuesta_de_inicio_intervencion_Aviso_intervencion.text());
$("#Hora_Propuesta_de_inicio_intervencion_Aviso_Previo_intervencion_RECEPCION").append($Hora_Propuesta_de_inicio_intervencion_Aviso_Previo_intervencion.text());	
$("#Fecha_de_recepcion_del_Aviso_Previo_intervencion_RECEPCION").append($Fecha_de_recepcion_del_Aviso_Previo_intervencion.text());
$("#Hora_de_recepcion_del_Aviso_Previo_intervencion_RECEPCION").append($Hora_de_recepcion_del_Aviso_Previo_intervencion.text());
$("#Prueba_Vacio_AD_RECEPCION").append($Prueba_Vacio_AD.text());
$("#Sticker_de_contraste_RECEPCION").append($Sticker_de_contraste.text());
$("#Sin_mica_Mica_Rota_RECEPCION").append($Sin_mica_Mica_Rota.text());
$("#Caja_sin_tapa_Tapa_mal_estado_RECEPCION").append($Caja_sin_tapa_Tapa_mal_estado.text());
$("#Sin_cerradura_cerradura_en_mal_estado_RECEPCION").append($Sin_cerradura_cerradura_en_mal_estado.text());
$("#estadoConexion_RECEPCION").append($estadoConexion.text());
$("#Cliente_permite_inventario_carga_RECEPCION").append($Cliente_permite_inventario_carga.text());
$("#cortado_art_90_RECEPCION").append($cortado_art_90.text());
$("#ESTADO_CAJA_PORTAMEDIDOR_RECEPCION").append($ESTADO_CAJA_PORTAMEDIDOR.text());
$("#TOMA_CON_REJA_RECEPCION").append($TOMA_CON_REJA.text());
$("#CAJA_CON_REJA_RECEPCION").append($CAJA_CON_REJA.text());
//FIN - REQ # 5 - JEGALARZA






/*-----------------------------------------TABLA MEDIDORES------------------------------------------INICIO--- */

var tbMedidores="";
	tbMedidores += "<style> table { width: 100%; border: 0; } th { padding: 5px; background-color: rgba(0,0,0,.02); color:rgba(0,0,0,.38); text-align: left; font-size: 12px;} td { padding: 5px; } ";
	tbMedidores += ".card-information__title { font-weight: bolder; font-size: 14px; padding: 5px 0 10px 0; }</style>";

try {
	

	var medidors = $xml.find("medidores");
	
	var itemsMedidors =medidors[0].getElementsByTagName("item");
	

	if(itemsMedidors.length>0) {	
		
		
		tbMedidores += " <div class='card-information__title'>MEDIDORES</div> <table id='tbMedidors'>";
		tbMedidores += "<tr><th>ACCION MEDIDOR</th><th>NUMERO MEDIDOR</th><th>MARCA MEDIDOR</th><th>MODELO MEDIDOR</th><th>FECHA INSTALACION</th>";
		tbMedidores += "<th>TECNOLOGIA MEDIDOR</th><th>TIPO MEDICION</th><th>TIPO MEDIDOR</th></tr>";
	}

	for(var i = 0; i<itemsMedidors.length; i++) {

		var itemMedidors =medidors[0].getElementsByTagName("item")[i];
		
		if(itemMedidors.parentElement.nodeName	== "medidores") {
			
			var accionMedidor = itemMedidors.getElementsByTagName("accionMedidor")[0] == undefined ? "" : itemMedidors.getElementsByTagName("accionMedidor")[0].textContent;
			var numeroMedidor = itemMedidors.getElementsByTagName("numeroMedidor")[0] == undefined ? "" : itemMedidors.getElementsByTagName("numeroMedidor")[0].textContent;
			var marcaMedidor = itemMedidors.getElementsByTagName("marcaMedidor")[0] == undefined ? "" : itemMedidors.getElementsByTagName("marcaMedidor")[0].textContent; 
			var modeloMedidor = itemMedidors.getElementsByTagName("modeloMedidor")[0] == undefined ? "" : itemMedidors.getElementsByTagName("modeloMedidor")[0].textContent; 
			var tecnologiaMedidor = itemMedidors.getElementsByTagName("tecnologiaMedidor")[0] == undefined ? "" : itemMedidors.getElementsByTagName("tecnologiaMedidor")[0].textContent; 
			var tipoMedidor = itemMedidors.getElementsByTagName("tipoMedidor")[0] == undefined ? "" : itemMedidors.getElementsByTagName("tipoMedidor")[0].textContent;
			var tipoMedicion = itemMedidors?.getElementsByTagName("tipoMedicion")[0] == undefined ? "" : itemMedidors.getElementsByTagName("tipoMedicion")[0].textContent ;
			var fechaInstalacion = itemMedidors.getElementsByTagName("fechaInstalacion")[0] == undefined ? "" : itemMedidors.getElementsByTagName("fechaInstalacion")[0].textContent;

		
			tbMedidores += "<tr><td><p>";
			tbMedidores += accionMedidor;
			tbMedidores += "</p></td><td>";
			tbMedidores += numeroMedidor;
			tbMedidores += "</td><td>";
			tbMedidores += marcaMedidor;
			tbMedidores += "</td><td>";
			tbMedidores += modeloMedidor;
			tbMedidores += "</td><td>";
			tbMedidores += fechaInstalacion;
			tbMedidores += "</td><td>";
			tbMedidores += tecnologiaMedidor;
			tbMedidores += "</td><td>";
			tbMedidores += tipoMedicion;
			tbMedidores += "</td><td>";
			tbMedidores += tipoMedidor;
			tbMedidores += "</td></tr>";
		}
	}
	tbMedidores += "</table>";
	
} catch (error) {
	tbMedidores="<b>MEDIRORES</b><table><tr><td>No hay información</td></tr></table>";
}
$( "#divMedidors_RECEPCION" ).append(tbMedidores);


/*-----------------------------------------TABLA MEDIDORES------------------------------------------FIN--- */



/*-----------------------------------------TABLA PRUEBA_CARGA------------------------------------------INICIO--- */
var tbPruebaCarga="";
tbPruebaCarga += "<style> table { width: 100%; border: 0; } th { padding: 5px; background-color: rgba(0,0,0,.02); color:rgba(0,0,0,.38); text-align: left; font-size: 12px;} td { padding: 5px; } ";
tbPruebaCarga += ".card-information__title { font-weight: bolder; font-size: 14px; padding: 5px 0 10px 0; }</style>";
try {
	var PRUEBACARGA = $xml.find("pruebaCarga");
	var itemPrueba =PRUEBACARGA[0].getElementsByTagName("item");
	
	if(itemPrueba.length>0)
	{	
		tbPruebaCarga += "<div class='card-information__title'>PRUEBA_CARGA</div><table id='tbPruebaCarga'>";
		tbPruebaCarga += "<tr><th>Pruebas_Carga_en_el_primario</th><th>Fase</th></tr>";
	}
	else 
	{
		$( "#divPruebaCarga" ).innerHtml("No hay información");
	}
	
	for(var ite = 0; ite<itemPrueba.length; ite++)
	{
	var itemPrueba2 =PRUEBACARGA[0].getElementsByTagName("item")[ite];
	if(itemPrueba2.parentElement.nodeName	== "pruebaCarga")
	{
		var v_Pruebas_Carga_en_el_primario = itemPrueba2.getElementsByTagName("Pruebas_Carga_en_el_primario")[0] == undefined ? "" :  itemPrueba2.getElementsByTagName("Pruebas_Carga_en_el_primario")[0].textContent;
		var v_fase = itemPrueba2.getElementsByTagName("fase")[0] == undefined ? "": itemPrueba2.getElementsByTagName("fase")[0].textContent;
		
		
		tbPruebaCarga += "<tr><td><p>";
		tbPruebaCarga += v_Pruebas_Carga_en_el_primario;
		tbPruebaCarga += "</p></td><td>";
		tbPruebaCarga += v_fase;
		tbPruebaCarga += "</td></tr>";	
	}
	}
	tbPruebaCarga += "</table>";
} catch (exc2) {
	tbPruebaCarga="<b>PRUEBA_CARGA</b><table><tr><td>No hay información</td></tr></table>";
}

$( "#divPruebaCarga_RECEPCION").append(tbPruebaCarga);

/*-----------------------------------------TABLA PRUEBA_CARGA------------------------------------------FIN--- */


/*-----------------------------------------TABLA ANEXOS------------------------------------------INICIO--- */
	var tbAnexos="";
	tbAnexos += "<style> table { width: 100%; border: 0; } th { padding: 5px; background-color: rgba(0,0,0,.02); color:rgba(0,0,0,.38); text-align: left; font-size: 12px;} td { padding: 5px; } ";
	tbAnexos += ".card-information__title { font-weight: bolder; font-size: 14px; padding: 5px 0 10px 0; }</style>";
	try {
		var ANEXOS = $xml.find("anexos");
		var itemsAnexos =ANEXOS[0].getElementsByTagName("item");

		if(itemsAnexos.length>0)
		{	
			tbAnexos += "<div class='card-information__title'>ANEXOS</div><table id='tbAnexos'>";
			tbAnexos += "<tr><th>Nombre_Anexo</th><th>Tipo_anexo</th></tr>";
		}
		else 
		{
			$( "#divAnexos" ).innerHtml("No hay información");
		}
		
		for(var i = 0; i<itemsAnexos.length; i++)
		{
		var itemAnexos =ANEXOS[0].getElementsByTagName("item")[i];
		if(itemAnexos.parentElement.nodeName	== "anexos")
		{
			var Nombre_Anexo = itemAnexos.getElementsByTagName("Nombre_Anexo")[0] == undefined ? "" :  itemAnexos.getElementsByTagName("Nombre_Anexo")[0].textContent;
			var Tipo_anexo = itemAnexos.getElementsByTagName("Tipo_anexo")[0] == undefined ? "": itemAnexos.getElementsByTagName("Tipo_anexo")[0].textContent;
			
			
			tbAnexos += "<tr><td><p>";
			tbAnexos += Nombre_Anexo;
			tbAnexos += "</p></td><td>";
			tbAnexos += Tipo_anexo;
			tbAnexos += "</td></tr>";	
		}
		}
		tbAnexos += "</table>";
	} catch (exc) {
		tbAnexos="<b>ANEXOS</b><table><tr><td>No hay información</td></tr></table>";
	}

	$( "#divAnexos_RECEPCION").append(tbAnexos);

/*-----------------------------------------TABLA ANEXOS------------------------------------------FIN--- */

//////////////////////////////GESTION_IMPAGOS_COBRANZA
	var GESTION_IMPAGOS_COBRANZA = $xml.find("datosGestionImpagosCobranza");		

	if(GESTION_IMPAGOS_COBRANZA.length>0) {
	try {

	
		var xmlText = GESTION_IMPAGOS_COBRANZA[0]?.innerHTML;
		try {

			var beautifiedXmlText = new XmlBeautify().beautify("<datosGestionImpagosCobranza >"+xmlText+"</datosGestionImpagosCobranza >", 
				{
					indent: "  ",  //indent pattern like white spaces
					useSelfClosingElement: true //true:use self-closing element when empty element.
				});
		} catch (error) {
			beautifiedXmlText="";
		}
		

		var tbGESTION_IMPAGOS_COBRANZA="<br/>";
		var tbLISTADO_FACTURA="";
		console.log(beautifiedXmlText);
		
		tbGESTION_IMPAGOS_COBRANZA += "<div id='tbGESTION_IMPAGOS_COBRANZA'>";
		tbGESTION_IMPAGOS_COBRANZA += "<div class='card-information__title'>GESTION_IMPAGOS_COBRANZA</div>";
		tbGESTION_IMPAGOS_COBRANZA += "<textarea style='flex-shrink: 0;resize: none;width: 970px;height: 300px'>";
		tbGESTION_IMPAGOS_COBRANZA += beautifiedXmlText;
		tbGESTION_IMPAGOS_COBRANZA += "</textarea></div>";
		
	}
	catch (exc)
	{
		tbGESTION_IMPAGOS_COBRANZA="<b>GESTION_IMPAGOS_COBRANZA</b><table><tr><td>No hay información</td></tr></table>";
	}
	}
	else
	{
		tbGESTION_IMPAGOS_COBRANZA="<b>GESTION_IMPAGOS_COBRANZA</b><table><tr><td>No hay información</td></tr></table>";
	}

	$( "#divGESTION_IMPAGOS_COBRANZA_RECEPCION").append(tbGESTION_IMPAGOS_COBRANZA);


	 //////////////////////////////GESTION_IMPAGOS_SCR
	 var GESTION_IMPAGOS_SCR = $xml.find("datosGestionImpagosSCR ");		

	 if(GESTION_IMPAGOS_SCR.length>0) {
	 try {
	
		
			var xmlText = GESTION_IMPAGOS_SCR[0]?.innerHTML;
			try {
	
				var beautifiedXmlText = new XmlBeautify().beautify("<datosGestionImpagosSCR >"+xmlText+"</datosGestionImpagosSCR >", 
					{
						indent: "  ",  //indent pattern like white spaces
						useSelfClosingElement: true //true:use self-closing element when empty element.
					});
			} catch (error) {
				beautifiedXmlText="";
			}
			
	
			var tbGESTION_IMPAGOS_SCR="<br/>";
			var tbLISTADO_FACTURA="";
			console.log(beautifiedXmlText);
			
			tbGESTION_IMPAGOS_SCR += "<div id='tbGESTION_IMPAGOS_SCR'>";
			tbGESTION_IMPAGOS_SCR += "<div class='card-information__title'>GESTION_IMPAGOS_SCR</div>";
			tbGESTION_IMPAGOS_SCR += "<textarea style='flex-shrink: 0;resize: none;width: 970px;height: 300px'>";
			tbGESTION_IMPAGOS_SCR += beautifiedXmlText;
			tbGESTION_IMPAGOS_SCR += "</textarea></div>";
			
		}
		catch (exc)
		{
			tbGESTION_IMPAGOS_SCR="<b>GESTION_IMPAGOS_SCR</b><table><tr><td>No hay información</td></tr></table>";
		}
		}
		else
		{
			tbGESTION_IMPAGOS_SCR="<b>GESTION_IMPAGOS_SCR</b><table><tr><td>No hay información</td></tr></table>";
		}
	
	 $( "#divGESTION_IMPAGOS_SCR_RECEPCION").append(tbGESTION_IMPAGOS_SCR);
	 

	 
 //////////////////////////////GESTION_NUEVAS_CONEXIONES


 var GESTION_NUEVAS_CONEXIONES = $xml.find("datosGestionNuevasConexiones ");		

 if(GESTION_NUEVAS_CONEXIONES.length>0) {
 try {

	
		var xmlText = GESTION_NUEVAS_CONEXIONES[0]?.innerHTML;
		try {

			var beautifiedXmlText = new XmlBeautify().beautify("<datosGestionNuevasConexiones>"+xmlText+"</datosGestionNuevasConexiones>", 
				{
					indent: "  ",  //indent pattern like white spaces
					useSelfClosingElement: true //true:use self-closing element when empty element.
				});
		} catch (error) {
			beautifiedXmlText="";
		}
		

		var tbGESTION_NUEVAS_CONEXIONES="<br/>"
		
		tbGESTION_NUEVAS_CONEXIONES += "<div id='tbGESTION_NUEVAS_CONEXIONES'>";
		tbGESTION_NUEVAS_CONEXIONES += "<div class='card-information__title'>GESTION_NUEVAS_CONEXIONES</div>";
		tbGESTION_NUEVAS_CONEXIONES += "<textarea style='flex-shrink: 0;resize: none;width: 970px;height: 300px'>";
		tbGESTION_NUEVAS_CONEXIONES += beautifiedXmlText;
		tbGESTION_NUEVAS_CONEXIONES += "</textarea></div>";
		
	}
	catch (exc) {
		tbGESTION_NUEVAS_CONEXIONES="<b>GESTION_NUEVAS_CONEXIONES</b><table><tr><td>No hay información</td></tr></table>";
	}
	}
	else {
		tbGESTION_NUEVAS_CONEXIONES="<b>GESTION_NUEVAS_CONEXIONES</b><table><tr><td>No hay información</td></tr></table>";
	}

 $( "#divGESTION_NUEVAS_CONEXIONES_RECEPCION").append(tbGESTION_NUEVAS_CONEXIONES);

 
 //////////////////////////////GESTION_PERDIDAS_INSPECCIONES

 	var GESTION_PERDIDAS_INSPECCIONES = $xml.find("datosGestionPerdidasInspecciones ");		

	if(GESTION_PERDIDAS_INSPECCIONES.length>0) {

		try {

			var xmlText = GESTION_PERDIDAS_INSPECCIONES[0]?.innerHTML;

			try {

				var beautifiedXmlText = new XmlBeautify().beautify("<datosGestionPerdidasInspecciones >"+xmlText+"</datosGestionPerdidasInspecciones >", 
					{
						indent: "  ",  //indent pattern like white spaces
						useSelfClosingElement: true //true:use self-closing element when empty element.
					});
			} catch (error) {
				beautifiedXmlText="";
			}


			var tbGESTION_PERDIDAS_INSPECCIONES="<br/>";
			console.log(beautifiedXmlText);

			tbGESTION_PERDIDAS_INSPECCIONES += "<div id='GESTION_PERDIDAS_INSPECCIONES'>";
			tbGESTION_PERDIDAS_INSPECCIONES += "<div class='card-information__title'>GESTION_PERDIDAS_INSPECCIONES</div>";
			tbGESTION_PERDIDAS_INSPECCIONES += "<textarea style='flex-shrink: 0;resize: none;width: 970px;height: 300px'>";
			tbGESTION_PERDIDAS_INSPECCIONES += beautifiedXmlText;
			tbGESTION_PERDIDAS_INSPECCIONES += "</textarea></div>";

		} catch (exc) {

			tbGESTION_PERDIDAS_INSPECCIONES="<b>GESTION_PERDIDAS_INSPECCIONES</b><table><tr><td>No hay información</td></tr></table>";
		}
	} else {
		
		tbGESTION_PERDIDAS_INSPECCIONES="<b>GESTION_PERDIDAS_INSPECCIONES</b><table><tr><td>No hay información</td></tr></table>";
	}

 $( "#divGESTION_PERDIDAS_INSPECCIONES_RECEPCION").append(tbGESTION_PERDIDAS_INSPECCIONES);
 
 
 //////////////////////////////GESTION_PERDIDAS_NORMALIZACIONES
 var GESTION_PERDIDAS_NORMALIZACIONES = $xml.find("datosGestionPerdidasNormalizaciones ");		

 if(GESTION_PERDIDAS_NORMALIZACIONES.length>0) {
 try {

	
		var xmlText = GESTION_PERDIDAS_NORMALIZACIONES[0]?.innerHTML;
		try {

			var beautifiedXmlText = new XmlBeautify().beautify("<datosGestionPerdidasNormalizaciones >"+xmlText+"</datosGestionPerdidasNormalizaciones >", 
				{
					indent: "  ",  //indent pattern like white spaces
					useSelfClosingElement: true //true:use self-closing element when empty element.
				});
		} catch (error) {
			beautifiedXmlText="";
		}
		

		var tbGESTION_PERDIDAS_NORMALIZACIONES="<br/>";
		var tbLISTADO_FACTURA="";
		console.log(beautifiedXmlText);
		
		tbGESTION_PERDIDAS_NORMALIZACIONES += "<div id='tbGESTION_PERDIDAS_NORMALIZACIONES'>";
		tbGESTION_PERDIDAS_NORMALIZACIONES += "<div class='card-information__title'>GESTION_PERDIDAS_NORMALIZACIONES</div>";
		tbGESTION_PERDIDAS_NORMALIZACIONES += "<textarea style='flex-shrink: 0;resize: none;width: 970px;height: 300px'>";
		tbGESTION_PERDIDAS_NORMALIZACIONES += beautifiedXmlText;
		tbGESTION_PERDIDAS_NORMALIZACIONES += "</textarea></div>";
		
	}
	catch (exc)
	{
		tbGESTION_PERDIDAS_NORMALIZACIONES="<b>GESTION_PERDIDAS_NORMALIZACIONES</b><table><tr><td>No hay información</td></tr></table>";
	}
	}
	else
	{
		tbGESTION_PERDIDAS_NORMALIZACIONES="<b>GESTION_PERDIDAS_NORMALIZACIONES</b><table><tr><td>No hay información</td></tr></table>";
	}

 $( "#divGESTION_PERDIDAS_NORMALIZACIONES_RECEPCION").append(tbGESTION_PERDIDAS_NORMALIZACIONES);
 
}
  
 /* for(var i = 0; i<itemsRecursos.length; i++)
{
 var itemRecursos =recursos[0].getElementsByTagName("item")[i];
 if(itemRecursos.parentElement.nodeName	== "recursos")
 {
	 var Nombre_Operador = itemRecursos.getElementsByTagName("Nombre_Operador")[0].textContent;
	 var Apellido_Operador = itemRecursos.getElementsByTagName("Apellido_Operador")[0].textContent;
	 var Matricula_Operador = itemRecursos.getElementsByTagName("Matricula_Operador")[0].textContent;
	 var Tiempo_de_Trabajo = itemRecursos.getElementsByTagName("Tiempo_de_Trabajo")[0].textContent;
	 var Cuadrilla_de_pertenencia = itemRecursos.getElementsByTagName("Cuadrilla_de_pertenencia")[0].textContent;
	
	
	
	tbRecursos += "<tr><td><p>";
	tbRecursos += Nombre_Operador;
	tbRecursos += "</p></td><td>";
	tbRecursos += Apellido_Operador;
	tbRecursos += "</td><td>";
	tbRecursos += Matricula_Operador;
	tbRecursos += "</td><td>";
	tbRecursos += Tiempo_de_Trabajo;
	tbRecursos += "</td><td>";
	tbRecursos += Cuadrilla_de_pertenencia;
	tbRecursos += "</td></tr>";	
 }
}
 $( "#tbRecursos" ).append(tbRecursos);
} */



/* var operaciones = $xml.find("operaciones");
var anexos = $xml.find("anexos");
var recursos = $xml.find("recursos");

var itemsOperaciones =operaciones[0].getElementsByTagName("item");
var itemsAnexos =anexos[0].getElementsByTagName("item");
var itemsRecursos =recursos[0].getElementsByTagName("item");

var tbOperaciones = "<tr><th>Codigo_Operacion</th><th>Codigo_Resultado</th><th>Codigo_Causal_Resultado</th><th>Codigo_Nota_Codificada</th><th>Inicio_Operacion</th><th>Fin_Operacion</th><th>Duracion_Operacion</th><th>Notas_Operacion</th></tr>";
var tbAnexos = "<tr><th>Tipo_anexo</th><th>Nombre_Anexo</th><th>Ruta_Anexo</th></tr>";
var tbRecursos = "<tr><th>Nombre_Operador</th><th>Apellido_Operador</th><th>Matricula_Operador</th><th>Tiempo_de_Trabajo</th><th>Cuadrilla_de_pertenencia</th></tr>";



   for(var i = 0; i<itemsOperaciones.length; i++) {
	   
		   var itemOperaciones =operaciones[0].getElementsByTagName("item")[i];
		   
	   if(itemOperaciones.parentElement.nodeName	== "operaciones") {

		   var Codigo_Operacion = itemOperaciones.getElementsByTagName("Codigo_Operacion")[0].textContent;
		   var Codigo_Resultado = itemOperaciones.getElementsByTagName("Codigo_Resultado")[0].textContent;
		   var Codigo_Causal_Resultado = itemOperaciones.getElementsByTagName("Codigo_Causal_Resultado")[0].textContent; 
		   var Codigo_Nota_Codificada = itemOperaciones.getElementsByTagName("Codigo_Nota_Codificada")[0].textContent; 
		   var Inicio_Operacion = itemOperaciones.getElementsByTagName("Inicio_Operacion")[0].textContent; 
		   var Fin_Operacion = itemOperaciones.getElementsByTagName("Fin_Operacion")[0].textContent; 
		   var Duracion_Operacion = itemOperaciones.getElementsByTagName("Duracion_Operacion")[0].textContent; 
		   var Notas_Operacion = itemOperaciones.getElementsByTagName("Notas_Operacion")[0].textContent; 
		   
		   
		   tbOperaciones += "<tr><td><p>";
		   tbOperaciones += Codigo_Operacion;
		   tbOperaciones += "</p></td><td>";
		   tbOperaciones += Codigo_Resultado;
		   tbOperaciones += "</td><td>";
		   tbOperaciones += Codigo_Causal_Resultado;
		   tbOperaciones += "</td><td>";
		   tbOperaciones += Codigo_Nota_Codificada;
		   tbOperaciones += "</td><td>";
		   tbOperaciones += Inicio_Operacion;
		   tbOperaciones += "</td><td>";
		   tbOperaciones += Fin_Operacion;
		   tbOperaciones += "</td><td>";
		   tbOperaciones += Duracion_Operacion;
		   tbOperaciones += "</td><td>";
		   tbOperaciones += Notas_Operacion;
		   tbOperaciones += "</td></tr>";	
	   }
   }
	$( "#tbOperaciones" ).append(tbOperaciones); */

