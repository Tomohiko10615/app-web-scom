import { Component, Inject, Injectable } from '@angular/core';
import { Event, Router, NavigationStart, NavigationEnd } from '@angular/router';
import { PlatformLocation } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';
import { BnNgIdleService } from 'bn-ng-idle';
import { AuthService } from './core/service/auth.service';
import { Observable, Subject } from "rxjs";/**/

@Injectable({/** */
  providedIn: 'root'
})
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  currentUrl: string;
  
  /*2023.10 atributo miDato agregado, utilizado para consultar el estado de la sesion y con ello cerrar modales en caso se tengan abiertos*/
  private miDato$:Subject<String>;/** */

  constructor(
    public _router: Router,
    location: PlatformLocation,
    private spinner: NgxSpinnerService,
    private bnIdle: BnNgIdleService,
    private authService: AuthService

  ) {
    
    this.miDato$ = new Subject();/** */

    this._router.events.subscribe((routerEvent: Event) => {
      if (routerEvent instanceof NavigationStart) {
        this.spinner.show();
        location.onPopState(() => {
          window.location.reload();
        });
        this.currentUrl = routerEvent.url.substring(
          routerEvent.url.lastIndexOf('/') + 1
        );
      }
      if (routerEvent instanceof NavigationEnd) {
        this.spinner.hide();
      }
      window.scrollTo(0, 0);


    });
    
    this.bnIdle.startWatching(600).subscribe((res) => {
      if(res) {
          console.log("session out, bye.",new Date());
          this.authService.logout();
          this._router.navigate(['/']);

          this.miDato$.next("SESSION_ENDED");/** */
      }
    })

  }

  /*2023.10 metodo getSessionInfo agregado, utilizado para consultar el estado de la sesion y con ello cerrar modales en caso se tengan abiertos*/
  getSessionInfo():Observable<String>{/** */
    return this.miDato$.asObservable();
  }

}
