import { NgModule } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";
import { Page404Component } from './authentication/page404/page404.component';
import { AuthGuard } from './core/guard/auth.guard';
import { AuthLayoutComponent } from './layout/app-layout/auth-layout/auth-layout.component';
import { MainLayoutComponent } from './layout/app-layout/main-layout/main-layout.component';

const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: '/authentication/signin', pathMatch: 'full'},
      {
        path: 'gestion-ordenes',
        loadChildren: () =>
          import('./modules/gestion-ordenes/gestion-ordenes.module').then((m) => m.GestionOrdenesModule)
      },
      {
        path: 'gestion-medidores',
        loadChildren: () =>
          import('./modules/gestion-medidores/gestion-medidores.module').then((m) => m.GestionMedidoresModule)
      },
      
     /*  {
        path: 'extra-pages',
        loadChildren: () =>
          import('./extra-pages/extra-pages.module').then(
            (m) => m.ExtraPagesModule
          )
      }, */
    ]
  },
  {
    path: 'authentication',
    component: AuthLayoutComponent,
    loadChildren: () =>
      import('./authentication/authentication.module').then(
        (m) => m.AuthenticationModule
      )
  },
  { path: '**', component: Page404Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
