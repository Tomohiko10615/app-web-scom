import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { tap } from 'rxjs';
import { UsuarioService } from 'src/app/core/service/usuario.service';
import { OrdenMantenimientoService } from '../../../services/orden-mantenimiento.service';
import { Irregularidad } from '../../../models/irregularidad.model';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-orden-mantenimiento-crear',
  templateUrl: './orden-mantenimiento-crear.component.html',
  styleUrls: ['./orden-mantenimiento-crear.component.sass']
})
export class OrdenMantenimientoCrearComponent implements OnInit {
  breadscrums = [
    {
      title: 'Creación de Orden de Mantenimiento',
      items: ['Home'],
      active: 'Creación de Orden de Mantenimiento'
    }
  ];

  tipoResponsable = [
    {
      id: 'Area',
      descripcion: 'Area'
    },
    {
      id: 'Rol',
      descripcion: 'Rol'
    },
    {
      id: 'Usuario',
      descripcion: 'Usuario'
    }
  ]

  displayedColumns2 = [
    'id',
    'codigo',
    'tarea',
    'tipoTarea',
    'cambio',
    'contraste',
    'estado',
    'valor',
    'actions'
  ];

  form: FormGroup;
  form2: FormGroup;
  form3: FormGroup;
  form4: FormGroup;

  nroServicio: string;
  nroCuenta: string;
  cliente: string;
  direccion: string;
  distrito: string;
  rutaLectura: string;
  tipoAcometida: string;
  documento: string;

  trabajos: any[];
  prioridades: any[];
  tareas: any[];
  tipos: any[];
  responsables: any[] = [];
  irregularidades: MatTableDataSource<Irregularidad> = new MatTableDataSource();
  
  dataIrregularidad = new Array();

  idUrl: number;
  orden: any;
  idOrden: number = 0;

  botonBuscar: boolean = true;
  botonSubmit: string = "Guardar";
  botonGuardar: boolean = false;

  nroOrden: string = "";
  constructor(private fb: FormBuilder, 
              private ordenMantenimiento: OrdenMantenimientoService,
              private usuarioService : UsuarioService,
              private route : ActivatedRoute,
              private router: Router,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.idUrl = this.route.snapshot.params['id'];
    
    this.initForm();    

    if(this.idUrl) {
      this.getData(this.idUrl) 
      this.breadscrums = [
        {
          title: 'Edición de Orden de Mantenimiento',
          items: ['Home'],
          active: 'Edición de Orden de Mantenimiento'
        }
      ];
      this.botonBuscar = false;
      this.botonSubmit = "Actualizar";
    }

    this.getTrabajos();
    this.getPrioridades();
    this.getTipos();
  }

  initForm() {
    this.form = this.fb.group({
      nroCuenta: [{ value: !this.orden ? '' : this.orden.nroServicio, disabled: !this.orden ? false : true}, [Validators.required, Validators.pattern('[0-9]*')]]
    })
    this.form2 = this.fb.group({
      idTrabajo: [(!this.orden) ? '' : this.orden.idTrabajo, Validators.required],
      idPrioridad: [(!this.orden) ? '' : this.orden.idPrioridad, Validators.required],
      idTipoResponsable: [(!this.orden) ? '' : this.orden.tipoResponsable, Validators.required],
      idResponsable: [(!this.orden) ? '' : this.orden.idResponsable, Validators.required]
    })
    this.form3 = this.fb.group({
      observaciones: [(!this.orden) ? '' : this.orden.observacion, [Validators.pattern("[a-zA-Z0-9 ]*")]]
    })
    this.form4 = this.fb.group({
      idTipo: [{ value: '', disabled: false}, ''],
      idTarea: [{ value: '', disabled: false}, '']
    })
  }

  getData(id: number) {
    this.ordenMantenimiento.getOrdenById(id, 0).pipe(
      tap(response => {
        this.orden = response;
        this.getResponsable(response.tipoResponsable)
        this.initForm();
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.textoDireccion
        this.distrito = response.distrito
        this.rutaLectura = response.codRuta
        this.idOrden = response.idOrden
        this.documento = response.documento
        this.tipoAcometida = response.tipoAcometida
        this.nroOrden = "Nro de Orden: " + response.nroOrden
        response.ormTareas.forEach(element => {
          const dataAnormalidad = 
                        { 
                          'idTarea': element.idTarea,
                          'codigo' : element.codigo,
                          'tarea': element.tarea,
                          'tipoTarea': element.tipoTarea,
                          'cambio': element.cambio,
                          'contraste': element.contraste,
                          'estado': element.estado,
                          'valor': element.valor,
                        };
          this.dataIrregularidad.push(dataAnormalidad);
          this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);  
        });
      })
    ).subscribe();
  }

  buscarCliente() {
    const nroCuenta = this.form.value.nroCuenta
    this.ordenMantenimiento.getNroCuenta(nroCuenta).pipe(
      tap(response => {
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.textoDireccion
        this.distrito = response.distrito
        this.rutaLectura = response.codRuta
        this.documento = response.documento
        this.tipoAcometida = response.tipoAcometida        
      })
    ).subscribe();
  }

  getTrabajos() {
    this.ordenMantenimiento.getTrabajos().pipe(
      tap(response => {
        this.trabajos = response
      })
    ).subscribe();
  }

  getPrioridades() {
    this.ordenMantenimiento.getPrioridades().pipe(
      tap(response => {
        this.prioridades = response
      })
    ).subscribe();
  }

  getTipos() {
    this.ordenMantenimiento.getTipos().pipe(
      tap(response => {
        this.tipos = response
      })
    ).subscribe();
  }

  getOrmTareas(idTipo: number) {
    this.ordenMantenimiento.getOrmTareas(idTipo).pipe(
      tap(response => {
        this.tareas = response
      })
    ).subscribe();
  }

  getResponsable(event: any) {
    this.ordenMantenimiento.getResponsable(event).pipe(
      tap(response => {
        console.log(response)
        this.responsables = response
      })
    ).subscribe();
  }

  getTareas() {
    const tarea = this.form4.value.idTarea.split('-');
    const resultado = this.dataIrregularidad.filter(e => e.tarea.includes(tarea[2]));
    if(this.form4.value.idTarea == '') {
      alert('Debe seleccionar una tarea.')
    }else {
      if(resultado.length > 0) {
        alert('La tarea seleccionada se encuentra agregado.')
      }else {
        const dataAnormalidad = 
                            { 
                              'idTarea': tarea[0],
                              'codigo' : tarea[1],
                              'tarea': tarea[2],
                              'tipoTarea': tarea[3],
                              'cambio': tarea[4],
                              'contraste': tarea[5],
                              'estado': tarea[6],
                              'valor': tarea[7]
                            };
        this.dataIrregularidad.push(dataAnormalidad);
        this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
      }
    }
    
  }

  remove(i: number, irregularidad: any) {
    this.dataIrregularidad.splice(i, 1);
    this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
  }

  store() {
    const form = 
                {
                  "idUsuario": this.usuarioService.usuarioStorage?.id,
                  "idOrden": this.idOrden,
                  "tareas": this.dataIrregularidad,
                  ...this.form.value,
                  ...this.form2.value,
                  ...this.form3.value,
                }
   // const nroCuentaForm = this.form.get('nroCuenta').value;
    this.ordenMantenimiento.postOrdenMantenimiento(form).pipe(
      tap((response) => !!response),
      tap(() => {this.botonGuardar = true}),
      tap((response) => this.nroOrden = response.nroOrden),
      tap((response) => this.snackBar.open(`Registro exitoso. Nro Orden: ${response.nroOrden} Nro Cuenta: ${this.nroCuenta}`, 'CERRAR')),
      // tap(() => this.router.navigate(['/gestion-ordenes/orden-trabajo']))
    ).subscribe();
  }

}
