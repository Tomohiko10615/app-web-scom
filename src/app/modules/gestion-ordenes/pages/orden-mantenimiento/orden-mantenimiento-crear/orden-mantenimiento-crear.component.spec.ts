import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenMantenimientoCrearComponent } from './orden-mantenimiento-crear.component';

describe('OrdenMantenimientoCrearComponent', () => {
  let component: OrdenMantenimientoCrearComponent;
  let fixture: ComponentFixture<OrdenMantenimientoCrearComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenMantenimientoCrearComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenMantenimientoCrearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
