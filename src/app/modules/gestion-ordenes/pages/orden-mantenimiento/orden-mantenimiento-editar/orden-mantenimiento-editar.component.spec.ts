import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenMantenimientoEditarComponent } from './orden-mantenimiento-editar.component';

describe('OrdenMantenimientoEditarComponent', () => {
  let component: OrdenMantenimientoEditarComponent;
  let fixture: ComponentFixture<OrdenMantenimientoEditarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenMantenimientoEditarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenMantenimientoEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
