import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenMantenimientoVerComponent } from './orden-mantenimiento-ver.component';

describe('OrdenMantenimientoVerComponent', () => {
  let component: OrdenMantenimientoVerComponent;
  let fixture: ComponentFixture<OrdenMantenimientoVerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenMantenimientoVerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenMantenimientoVerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
