import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { tap } from 'rxjs';
import { UsuarioService } from 'src/app/core/service/usuario.service';
import { Irregularidad } from '../../../models/irregularidad.model';
import { OrdenInspeccionNotificadaService } from '../../../services/orden-inspeccion-notificada.service';
import { OrdenMantenimientoService } from '../../../services/orden-mantenimiento.service';
import beautify from "xml-beautifier";
import { TransferXmlService } from '../../../services/transfer-xml.service';
import { MatTabChangeEvent } from '@angular/material/tabs';

//Se declara funciones creadas en src/assets/custom-js/transfer-xml.js
declare function envioXml(valorEnvio): any;
declare function recepcionXml(message): any;

@Component({
  selector: 'app-orden-mantenimiento-ver',
  templateUrl: './orden-mantenimiento-ver.component.html',
  styleUrls: ['./orden-mantenimiento-ver.component.scss']
})
export class OrdenMantenimientoVerComponent implements OnInit {
  breadscrums = [
    {
      title: 'Ver de Orden de Mantenimiento',
      items: ['Home'],
      active: 'Ver de Orden de Mantenimiento'
    }
  ];

  tipoResponsable = [
    {
      id: 'Usuario',
      descripcion: 'Usuario'
    },
    {
      id: 'Area',
      descripcion: 'Area'
    },
    {
      id: 'Rol',
      descripcion: 'Rol'
    }
  ]

  displayedColumns2 = [
    'id',
    'codigo',
    'tarea',
    'tipoTarea'
  ];

  form: FormGroup;

  nroServicio: string;
  nroCuenta: string;
  cliente: string;
  direccion: string;
  distrito: string;
  rutaLectura: string;
  tipoAcometida: string;
  documento: string;
  estadoWkf: string;

  trabajos: any[];
  prioridades: any[];
  tareas: any[];
  tipos: any[];
  responsables: any[] = [];
  irregularidades: MatTableDataSource<Irregularidad> = new MatTableDataSource();
  
  dataIrregularidad = new Array();

  idUrl: number;
  orden: any;
  idOrden: number = 0;

  botonBuscar: boolean = true;
  botonSubmit: string = "Guardar";

 /*  xmlEnvio: any;
  xmlRecepcion: any; */
  xml: any 
  ordenTransfer: any = [];

  @ViewChild('tabGroup') tabGroup;
  xmlEnvio: string ='';
  xmlRecepcion: string='';
  activeXmlEnvio: boolean = false;
  activexmlRecepcion: boolean = false;

  constructor(private fb: FormBuilder, 
              private ordenMantenimiento: OrdenMantenimientoService,
              private ordenInspeccionService: OrdenInspeccionNotificadaService,
              private usuarioService : UsuarioService,
              private route : ActivatedRoute,
              private router: Router,
              private transferXml: TransferXmlService) { }

  ngOnInit(): void {
    this.idUrl = this.route.snapshot.params['id'];
    
    this.initForm();    

    if(this.idUrl) {
      this.getData(this.idUrl) 
      this.breadscrums = [
        {
          title: 'Ver de Orden de Mantenimiento',
          items: ['Home'],
          active: 'Ver de Orden de Mantenimiento'
        }
      ];
      this.botonBuscar = false;
      this.botonSubmit = "Actualizar";
    }

    this.getTrabajos();
    this.getPrioridades();
    this.getTipos();
/*     this.getXMLEnvio();
    this.getXMLRecepcion(); */
    this.getOrdenTransfer();
  }

  initForm() {
    this.form = this.fb.group({
      nroCuenta: [{ value: !this.orden ? '' : this.orden.nroServicio, disabled: true}, Validators.required],
      idTrabajo: [{ value: !this.orden ? '' : this.orden.idTrabajo, disabled: true}, Validators.required],
      idPrioridad: [{ value: !this.orden ? '' : this.orden.idPrioridad, disabled: true}, Validators.required],
      idTipoResponsable: [{ value: !this.orden ? '' : this.orden.tipoResponsable, disabled: true}, Validators.required],
      idResponsable: [{ value: !this.orden ? '' : this.orden.idResponsable, disabled: true}, Validators.required],
      observaciones: [{ value: !this.orden ? '' : this.orden.observacion, disabled: true}, Validators.required]
    })
  }


  tabChanged(tabChangeEvent: MatTabChangeEvent): void {

    if(tabChangeEvent.tab.textLabel=== 'DATOS ENVIO FORCEBEAT'){
      if(this.activeXmlEnvio == false) {
        this.obtenerDatosXmlEnvio();
        
      }
    }

    if(tabChangeEvent.tab.textLabel=== 'DATOS RECEPCIÓN FORCEBEAT'){
      if(this.activexmlRecepcion == false) {
        this.obtenerDatosXmlRecepcion();
       
      }
    }
  }


  obtenerDatosXmlEnvio(): void {
    
    this.transferXml.obtenerXmlEnvio(this.idUrl).pipe(
      tap((response: any) => {
        this.xmlEnvio = response.xml.replaceAll("\n", "");
        envioXml(this.xmlEnvio);
        this.activeXmlEnvio= true;
      })
    ).subscribe();
 
    }
  
    obtenerDatosXmlRecepcion(): void {
    
      this.transferXml.obtenerXmlRecepcion(this.idUrl).pipe(
        tap((response: any) => {
          this.xmlRecepcion = response.xml.replaceAll("\n", "");
          recepcionXml(this.xmlRecepcion);
          this.activexmlRecepcion= true;
        })
      ).subscribe();
   
      }


  getData(id: number) {
    this.ordenMantenimiento.getOrdenById(id, 0).pipe(
      tap(response => {
        this.orden = response;
        this.getResponsable(response.tipoResponsable)
        this.initForm();
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.textoDireccion
        this.distrito = response.distrito
        this.rutaLectura = response.codRuta
        this.orden = response.idOrden
        this.documento = response.documento
        this.tipoAcometida = response.tipoAcometida
        this.direccion = response.direccion
        this.rutaLectura = response.rutaLectura
        this.estadoWkf = response.estadoWkf
        response.ormTareas.forEach(element => {
          const dataAnormalidad = 
                        { 
                          'idTarea': element.idTarea,
                          'codigo' : element.codigo,
                          'tarea': element.tarea,
                          'tipoTarea': element.tipoTarea
                        };
          this.dataIrregularidad.push(dataAnormalidad);
          this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);  
        });
      })
    ).subscribe();
  }

  buscarCliente() {
    const nroCuenta = this.form.value.nroCuenta
    this.ordenMantenimiento.getNroCuenta(nroCuenta).pipe(
      tap(response => {
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.direccion
        this.distrito = response.distrito
        this.rutaLectura = response.codRuta
        this.documento = response.documento
        this.tipoAcometida = response.tipoAcometida        
      })
    ).subscribe();
  }

  getTrabajos() {
    this.ordenMantenimiento.getTrabajos().pipe(
      tap(response => {
        this.trabajos = response
      })
    ).subscribe();
  }

  getPrioridades() {
    this.ordenMantenimiento.getPrioridades().pipe(
      tap(response => {
        this.prioridades = response
      })
    ).subscribe();
  }

  getTipos() {
    this.ordenMantenimiento.getTipos().pipe(
      tap(response => {
        this.tipos = response
      })
    ).subscribe();
  }

  getOrmTareas(idTipo: number) {
    this.ordenMantenimiento.getOrmTareas(idTipo).pipe(
      tap(response => {
        this.tareas = response
      })
    ).subscribe();
  }

  getResponsable(event: any) {
    this.ordenMantenimiento.getResponsable(event).pipe(
      tap(response => {
        console.log(response)
        this.responsables = response
      })
    ).subscribe();
  }

  getTareas() {
    const tarea = this.form.value.idTarea.split('-');
    
    const dataAnormalidad = 
                        { 
                          'idTarea': tarea[0],
                          'codigo' : tarea[1],
                          'tarea': tarea[2],
                          'tipoTarea': tarea[3]
                        };
    this.dataIrregularidad.push(dataAnormalidad);
    this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
  }

  remove(i: number, irregularidad: any) {
    this.dataIrregularidad.splice(i, 1);
    this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
  }

  store() {
    const form = 
                {
                  "idUsuario": this.usuarioService.usuarioStorage?.id,
                  "idOrden": this.orden,
                  "tareas": this.dataIrregularidad,
                  ...this.form.value,
                }
    this.ordenMantenimiento.postOrdenMantenimiento(form).pipe(
      tap((response) => !!response),
      tap(() => this.router.navigate(['/gestion-ordenes/orden-trabajo']))
    ).subscribe();
  }

  getXMLEnvio() {
    this.ordenInspeccionService.getXML(this.idUrl, 'ENVIO').pipe(
      tap(response => {
        if(response != null) {
          this.xmlEnvio = beautify(response.xml)
        }else {
          this.xmlEnvio = ""
        }
      })
    ).subscribe();
  }

  getXMLRecepcion() {
    this.ordenInspeccionService.getXML(this.idUrl, 'RECEPCION').pipe(
      tap(response => {
        if(response != null) {
          this.xmlRecepcion = beautify(response.xml)
        }else {
          this.xmlRecepcion = ""
        }
      })
    ).subscribe();
  }

  getOrdenTransfer() {
    this.ordenInspeccionService.getOrdenTransfer(this.idUrl).pipe(
      tap(response => {
        if(response != null) {
          this.ordenTransfer = response[0]
        }else {
          this.ordenTransfer = []
        }
      })
    ).subscribe();
  }

}
