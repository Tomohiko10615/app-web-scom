import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenMantenimientoComponent } from './orden-mantenimiento.component';

describe('OrdenMantenimientoComponent', () => {
  let component: OrdenMantenimientoComponent;
  let fixture: ComponentFixture<OrdenMantenimientoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenMantenimientoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenMantenimientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
