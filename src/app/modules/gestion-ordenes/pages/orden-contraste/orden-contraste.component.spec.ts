import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenContrasteComponent } from './orden-contraste.component';

describe('OrdenContrasteComponent', () => {
  let component: OrdenContrasteComponent;
  let fixture: ComponentFixture<OrdenContrasteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenContrasteComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenContrasteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
