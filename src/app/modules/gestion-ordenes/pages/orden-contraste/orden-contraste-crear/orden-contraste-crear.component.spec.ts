import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenContrasteCrearComponent } from './orden-contraste-crear.component';

describe('OrdenContrasteCrearComponent', () => {
  let component: OrdenContrasteCrearComponent;
  let fixture: ComponentFixture<OrdenContrasteCrearComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenContrasteCrearComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenContrasteCrearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
