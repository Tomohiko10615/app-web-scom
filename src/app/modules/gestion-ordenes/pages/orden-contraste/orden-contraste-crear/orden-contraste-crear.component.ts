import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { tap } from 'rxjs';
import { UsuarioService } from 'src/app/core/service/usuario.service';
import { Irregularidad } from '../../../models/irregularidad.model';
import { OrdenInspeccionNotificada } from '../../../models/orden-inspeccion-notificada.model';
import { Tarea } from '../../../models/tarea.model';
import { OrdenContrasteService } from '../../../services/orden-contraste.service';

@Component({
  selector: 'app-orden-contraste-crear',
  templateUrl: './orden-contraste-crear.component.html',
  styleUrls: ['./orden-contraste-crear.component.sass']
})
export class OrdenContrasteCrearComponent implements OnInit {

  breadscrums = [
    {
      title: 'Creación de Orden de Contraste',
      items: ['Home'],
      active: 'Creación de Orden de Contraste'
    }
  ];

  filterToggle = false;

  displayedColumns = [
    'select',
    'nroOrden',
    'fechaCreacion',
    'contratista',
    'tipoInspeccion'
  ];

  displayedColumns2 = [
    'id',
    'codigo',
    'descripcion',
    'generaCNR',
    'generaOrdenNormalizacion',
    'causal',
    'actions'
  ];

  displayedColumns3 = [
    'select',
    'codAnormalidad',
    'codTarea',
    'tarea'
  ];

  ordenInspeccionNotificadas: MatTableDataSource<OrdenInspeccionNotificada> = new MatTableDataSource();
  irregularidades: MatTableDataSource<Irregularidad> = new MatTableDataSource();
  tareas: MatTableDataSource<Tarea> = new MatTableDataSource();
  
  form: FormGroup;
  form2: FormGroup;
  form3: FormGroup;

  nroServicio: string;
  nroCuenta: string;
  cliente: string;
  direccion: string;
  distrito: string;
  rutaLectura: string;

  marca: string;
  modelo: string;
  nroMedidor: string;
  estado: string;

  anormalidades: any[];
  subtipos: any[];
  motivos: any[];

  dataIrregularidad = new Array();
  dataTarea = new Array();

  idOrden: number = 0;
  idOrdenContraste: number = 0;
  idOrdenInspeccion: number = 0;

  idUrl: number;
  orden: any;
  botonBuscar: boolean = true;
  botonSubmit: string = "Guardar";
  botonActivar: boolean = true;
  botonGuardar: boolean = false;

  nroOrden: string = "";

  constructor(private fb: FormBuilder, 
              private ordenContrasteService: OrdenContrasteService,
              private usuarioService : UsuarioService,
              private route : ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.idUrl = this.route.snapshot.params['id'];

    this.initForm();
    this.getAnormalidades();
    this.getMotivo();

    if(this.idUrl) {
      this.getData();
      this.breadscrums = [
        {
          title: 'Edición de Orden de Contraste',
          items: ['Home'],
          active: 'Edición de Orden de Contraste'
        }
      ];
      this.botonBuscar = false;
      this.botonSubmit = "Actualizar";
    }
  }

  getData() {
    this.ordenContrasteService.getOrden(this.idUrl, 0).pipe(
      tap(response => {
        this.orden = response;
        this.initForm();
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.direccion
        this.distrito = response.distrito
        this.rutaLectura = response.rutaLectura
        this.idOrdenInspeccion = response.idOrdenInspeccion
        this.idOrden = response.idOrden
        this.idOrdenContraste = response.idOrdenContraste
        this.form.value.nroServicio = response.nroServicio
        this.marca = response.codMarca
        this.modelo = response.codModelo
        this.nroMedidor = response.nroComponente
        this.estado = response.estadoMedidor
        this.nroOrden = response.nroOrden
        this.buscarCliente();
        this.check(response.idOrdenInspeccion);
      })
    ).subscribe();
  }

  initForm() {
    this.form = this.fb.group({
      nroServicio: [{ value: !this.orden ? '' : this.orden.nroServicio, disabled: !this.orden ? false : true}, [Validators.required, Validators.pattern('[0-9]*')]]
    })
    this.form2 = this.fb.group({
      tareaEstado: [ { value: 'N'}, ''],
      idAnormalidad: []
    })
    this.form3 = this.fb.group({
      idMotivo: [{ value: !this.orden ? '' : this.orden.idMotivo, disabled: false}, Validators.required],
      observaciones: [{ value: !this.orden ? '' : this.orden.observacion, disabled: false}, [Validators.required, Validators.pattern("[a-zA-Z0-9 ]*")]],
    })
  }

  buscarCliente() {
    this.irregularidades = new MatTableDataSource<Irregularidad>();
    this.tareas = new MatTableDataSource<Tarea>();
    const nroServicio = this.form.value.nroServicio
    this.ordenContrasteService.getNroServicio(nroServicio).pipe(
      tap(response => {
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.direccion
        this.distrito = response.distrito
        this.rutaLectura = response.rutaLectura
        this.marca = response.codMarca
        this.modelo = response.codModelo
        this.nroMedidor = response.nroComponente
        this.estado = response.estadoMedidor
        this.ordenInspeccionNotificadas = new MatTableDataSource<OrdenInspeccionNotificada>(response.inspecciones);
        if(response.inspecciones.length == 0) {
          this.botonActivar = false
        }else {
          this.botonActivar = true
        }
      })
    ).subscribe();
  }

  getAnormalidades() {
    this.ordenContrasteService.getAnormalidades().pipe(
      tap(response => {
        this.anormalidades = response
      })
    ).subscribe();
  }

  getSubTipos() {
    this.ordenContrasteService.getSubTipos().pipe(
      tap(response => {
        this.subtipos = response
      })
    ).subscribe();
  }

  check(id: number) {
    this.idOrdenInspeccion = id;
    this.irregularidades = new MatTableDataSource<Irregularidad>();
    this.tareas = new MatTableDataSource<Tarea>();
    this.dataTarea = [];
    this.dataIrregularidad = [];
    this.ordenContrasteService.getOrden(id, 1).pipe(
      tap(response => {
        response.anormalidades.forEach((element: any) => {
          const dataAnormalidad = 
                        { 
                          'id': element.id,
                          'codigo' : element.codigo,
                          'descripcion': element.descripcion,
                          'generaCNR': element.generaCNR,
                          'generaOrdenNormalizacion': element.generaOrdenNormalizacion,
                          'causal': element.causal
                        };
          this.dataIrregularidad.push(dataAnormalidad);
        });
        this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
        response.tareas.forEach((element: any) => {
          const dataTareas = 
                        { 
                          'id': element.id,
                          'idTarea' : element.idTarea,
                          'idAnormalidad': element.idAnormalidad,
                          'tarea': element.tarea,
                          'codAnormalidad': element.codAnormalidad,
                          'codTarea': element.codTarea,
                          'tareaEstado': element.tareaEstado
                        };
          this.dataTarea.push(dataTareas);
        });
        this.tareas = new MatTableDataSource<Tarea>(this.dataTarea);
      })
    ).subscribe();
  }

  noRealizado(element: any, i: number) {
    this.dataTarea.splice(i, 1);
    this.dataTarea.push(this.getDataTareas(element, 'N'));
  }

  realizado(element: any, i: number) {
    this.dataTarea.splice(i, 1);
    this.dataTarea.push(this.getDataTareas(element, 'S'));
  }

  getDataTareas(element: any, estado: string) {
    const tarea = 
                { 
                  'id': element.id,
                  'idTarea' : element.idTarea,
                  'idAnormalidad': element.idAnormalidad,
                  'tarea': element.desTarea,
                  'codAnormalidad': element.codAnormalidad,
                  'codTarea': element.codTarea,
                  'tareaEstado': estado
                };
    return  tarea;
  }

  getTareas() {
    const medida = this.form2.value.idAnormalidad.split('-');
    const resultado = this.dataIrregularidad.filter(e => e.descripcion.includes(medida[2]));
    if(resultado.length > 0) {
      alert('La irregularidad seleccionada se encuentra agregado.')
    }else {
      const dataAnormalidad = 
                          { 
                            'id': medida[0],
                            'codigo' : medida[1],
                            'descripcion': medida[2],
                            'generaCNR': medida[3],
                            'generaOrdenNormalizacion': medida[4],
                            'causal': medida[5]
                          };
      this.dataIrregularidad.push(dataAnormalidad);
      this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
      this.ordenContrasteService.getTareas(medida[0]).pipe(
        tap(response => {
          response.forEach((element: any) => {
            const dataTareas = 
                          { 
                            'id': element.id,
                            'idTarea' : element.idTarea,
                            'idAnormalidad': element.idAnormalidad,
                            'tarea': element.desTarea,
                            'codAnormalidad': element.codAnormalidad,
                            'codTarea': element.codTarea,
                            'tareaEstado': 'N'
                          };
            this.dataTarea.push(dataTareas);
          });
          this.tareas = new MatTableDataSource<Tarea>(this.dataTarea);
        })
      ).subscribe();
    }
  }

  getMotivo() {
    this.ordenContrasteService.getMotivo().pipe(
      tap(response => {
        this.motivos = response
      })
    ).subscribe();
  }

  remove(i: number, irregularidad: any) {
    this.dataIrregularidad.splice(i, 1);
    this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
    this.dataTarea = this.dataTarea.filter(response => response?.codAnormalidad !== irregularidad.codigo)
    this.tareas = new MatTableDataSource<Tarea>(this.dataTarea);
  }

  store() {
    if(this.dataIrregularidad.length == 0) {
      alert('Debe seleccionar una orden de inspección notificada.')
    }else {
      const form = 
                  {
                    "idUsuario": this.usuarioService.usuarioStorage?.id,
                    "idOrden": this.idOrden,
                    "idOrdenContraste": this.idOrdenContraste,
                    "idOrdenInspeccion": this.idOrdenInspeccion,
                    "irregularidades": this.dataTarea,
                    ...this.form.value,
                    ...this.form2.value,
                    ...this.form3.value
                  }
      this.ordenContrasteService.postOrdenContraste(form).pipe(
        tap((response) => !!response),
        tap(() => {this.botonGuardar = true}),
        tap((response) => this.nroOrden = response.nroOrden),
        // tap(()=>{this.router.navigate(['/gestion-ordenes/orden-trabajo']);})
      ).subscribe();
    }
  }

}