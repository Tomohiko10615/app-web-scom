import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { tap } from 'rxjs';
import { UsuarioService } from 'src/app/core/service/usuario.service';
import { Irregularidad } from '../../../models/irregularidad.model';
import { OrdenInspeccionNotificada } from '../../../models/orden-inspeccion-notificada.model';
import { Tarea } from '../../../models/tarea.model';
import { OrdenContrasteService } from '../../../services/orden-contraste.service';
import { OrdenInspeccionNotificadaService } from '../../../services/orden-inspeccion-notificada.service';
import beautify from "xml-beautifier";
import { TransferXmlService } from '../../../services/transfer-xml.service';
import { MatTabChangeEvent } from '@angular/material/tabs';

//Se declara funciones creadas en src/assets/custom-js/transfer-xml.js
declare function envioXml(valorEnvio): any;
declare function recepcionXml(message): any;

@Component({
  selector: 'app-orden-contraste-ver',
  templateUrl: './orden-contraste-ver.component.html',
  styleUrls: ['./orden-contraste-ver.component.scss']
})
export class OrdenContrasteVerComponent implements OnInit {

  breadscrums = [
    {
      title: 'Creación de Orden de Contraste',
      items: ['Home'],
      active: 'Creación de Orden de Contraste'
    }
  ];

  filterToggle = false;

  displayedColumns = [
    'select',
    'nro',
    'nroOrden',
    'fechaCreacion',
    'contratista',
    'tipoInspeccion'
  ];

  displayedColumns2 = [
    'id',
    'codigo',
    'descripcion',
    'generaCNR',
    'generaOrdenNormalizacion',
    'causal'
  ];

  displayedColumns3 = [
    'select',
    'codAnormalidad',
    'codTarea',
    'tarea'
  ];


  displayedColumnsPruebaAlta = [
    'pruebaAlta01',
    'pruebaAlta02',
    'pruebaAlta03'
  ]

  displayedColumnsPruebaBaja = [
    'pruebaBaja01',
    'pruebaBaja02',
    'pruebaBaja03'
  ]

  displayedColumnsPruebaAislamiento = [
    'pruebaAislamientoR',
    'pruebaAislamientoS',
    'pruebaAislamientoT'
  ]

  displayedColumnsPruebaNominal = [
    'pruebaNominal01',
    'pruebaNominal02',
    'pruebaNominal03'
  ]

  ordenInspeccionNotificadas: MatTableDataSource<OrdenInspeccionNotificada> = new MatTableDataSource();
  irregularidades: MatTableDataSource<Irregularidad> = new MatTableDataSource();
  tareas: MatTableDataSource<Tarea> = new MatTableDataSource();
  
  form: FormGroup;

  nroServicio: string;
  nroCuenta: string;
  cliente: string;
  direccion: string;
  distrito: string;
  rutaLectura: string;

  marca: string;
  modelo: string;
  nroMedidor: string;
  estado: string;

  estadoWkf: string;

  anormalidades: any[];
  subtipos: any[];
  motivos: any[];

  dataIrregularidad = new Array();
  dataTarea = new Array();

  idOrden: number = 0;
  idOrdenContraste: number = 0;
  idOrdenInspeccion: number = 0;

  idUrl: number;
  orden: any;
  botonBuscar: boolean = true;
  botonSubmit: string = "Guardar";

  xmlEnvio: any;
  xmlRecepcion: any;
  activeXmlEnvio: boolean = false;
  activexmlRecepcion: boolean = false;
  xml: any 
  ordenTransfer: any = [];

  nroOrden: string = "";

  medContrastePruebasAlta: any;
  medContrastePruebasBaja: any;
  medContrastePruebasNominal: any;
  medContrastePruebasAislamiento: any;

  inicioCreacion: string = "SC4J";

  constructor(private fb: FormBuilder, 
              private ordenContrasteService: OrdenContrasteService,
              private ordenInspeccionService: OrdenInspeccionNotificadaService,
              private usuarioService : UsuarioService,
              private route : ActivatedRoute,
              private router: Router,
              private transferXml: TransferXmlService) { }

  ngOnInit() {
    this.idUrl = this.route.snapshot.params['id'];
    
    this.initForm();
    this.getAnormalidades();
    this.getMotivo();
  /*   this.getXMLEnvio();
    this.getXMLRecepcion(); */
    this.getOrdenTransfer();

    if(this.idUrl) {
      this.getData();
      this.breadscrums = [
        {
          title: 'Ver de Orden de Contraste',
          items: ['Home'],
          active: 'Ver de Orden de Contraste'
        }
      ];
      this.botonBuscar = false;
      this.botonSubmit = "Actualizar";
    }
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {

    if(tabChangeEvent.tab.textLabel=== 'DATOS ENVIO FORCEBEAT'){
      if(this.activeXmlEnvio == false) {
        this.obtenerDatosXmlEnvio();
        
      }
    }

    if(tabChangeEvent.tab.textLabel=== 'DATOS RECEPCIÓN FORCEBEAT'){
      if(this.activexmlRecepcion == false) {
        this.obtenerDatosXmlRecepcion();
       
      }
    }
  }


  obtenerDatosXmlEnvio(): void {
    
    this.transferXml.obtenerXmlEnvio(this.idUrl).pipe(
      tap((response: any) => {
        this.xmlEnvio = response.xml.replaceAll("\n", "");
        envioXml(this.xmlEnvio);
        this.activeXmlEnvio= true;
      })
    ).subscribe();
 
    }
  
    obtenerDatosXmlRecepcion(): void {
    
      this.transferXml.obtenerXmlRecepcion(this.idUrl).pipe(
        tap((response: any) => {
          this.xmlRecepcion = response.xml.replaceAll("\n", "");
          recepcionXml(this.xmlRecepcion);
          this.activexmlRecepcion= true;
        })
      ).subscribe();
   
      }

  getData() {
    this.ordenContrasteService.getOrden(this.idUrl, 0).pipe(
      tap(response => {
        this.orden = response;
        this.initForm();
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.direccion
        this.distrito = response.distrito
        this.rutaLectura = response.rutaLectura
        this.idOrdenInspeccion = response.idOrdenInspeccion
        this.idOrden = response.idOrden
        this.idOrdenContraste = response.idOrdenContraste
        this.form.value.nroServicio = response.nroServicio
        this.marca = response.codMarca
        this.modelo = response.codModelo
        this.nroMedidor = response.nroComponente
        this.estado = response.estadoMedidor
        this.nroOrden = response.nroOrden
        this.estadoWkf = response.estadoWkf
        this.buscarCliente();
        this.check(response.idOrdenInspeccion);
        if(response.idOrdenSc4j == 0) {
          this.inicioCreacion = "SCOM";
        }
        //Mostrar en tabla datos de Pruebas de Contraste
        this.medContrastePruebasAlta = response?.medContrasteDTO?.pruebaAlta;
        this.medContrastePruebasBaja = response?.medContrasteDTO?.pruebaBaja;
        this.medContrastePruebasAislamiento = response?.medContrasteDTO?.pruebaAislamiento;
        this.medContrastePruebasNominal = response?.medContrasteDTO?.pruebaNominal;
      })
    ).subscribe();
  }

  initForm() {
    this.form = this.fb.group({
      nroServicio: [{ value: !this.orden ? '' : this.orden.nroServicio, disabled: true}, Validators.required],
      idMotivo: [{ value: !this.orden ? '' : this.orden.idMotivo, disabled: true}, Validators.required],
      observaciones: [{ value: !this.orden ? '' : this.orden.observacion, disabled: true}, Validators.required],
      idAnormalidad: []
    })
  }

  buscarCliente() {
    this.irregularidades = new MatTableDataSource<Irregularidad>();
    this.tareas = new MatTableDataSource<Tarea>();
    const nroServicio = this.form.value.nroServicio
    this.ordenContrasteService.getNroServicio(nroServicio).pipe(
      tap(response => {
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.direccion
        this.distrito = response.distrito
        this.rutaLectura = response.rutaLectura
        this.marca = response.codMarca
        this.modelo = response.codModelo
        this.nroMedidor = response.nroComponente
        this.estado = response.estadoMedidor
        this.ordenInspeccionNotificadas = new MatTableDataSource<OrdenInspeccionNotificada>(response.inspecciones);
      })
    ).subscribe();
  }

  getAnormalidades() {
    this.ordenContrasteService.getAnormalidades().pipe(
      tap(response => {
        this.anormalidades = response
      })
    ).subscribe();
  }

  getSubTipos() {
    this.ordenContrasteService.getSubTipos().pipe(
      tap(response => {
        this.subtipos = response
      })
    ).subscribe();
  }

  check(id: number) {
    this.idOrdenInspeccion = id;
    this.irregularidades = new MatTableDataSource<Irregularidad>();
    this.tareas = new MatTableDataSource<Tarea>();
    this.dataTarea = [];
    this.dataIrregularidad = [];
    this.ordenContrasteService.getOrden(id, 1).pipe(
      tap(response => {
        response.anormalidades.forEach((element: any) => {
          const dataAnormalidad = 
                        { 
                          'id': element.id,
                          'codigo' : element.codigo,
                          'descripcion': element.descripcion,
                          'generaCNR': element.generaCNR,
                          'generaOrdenNormalizacion': element.generaOrdenNormalizacion,
                          'causal': element.causal
                        };
          this.dataIrregularidad.push(dataAnormalidad);
        });
        this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
        response.tareas.forEach((element: any) => {
          const dataTareas = 
                        { 
                          'id': element.id,
                          'idTarea' : element.idTarea,
                          'idAnormalidad': element.idAnormalidad,
                          'tarea': element.tarea,
                          'codAnormalidad': element.codAnormalidad,
                          'codTarea': element.codTarea,
                          'tareaEstado': element.tareaEstado
                        };
          this.dataTarea.push(dataTareas);
        });
        this.tareas = new MatTableDataSource<Tarea>(this.dataTarea);
      })
    ).subscribe();
  }

  noRealizado(id: number) {
    const tarea = {
      id: id,
      tareaEstado: 'N'
    }
    this.ordenContrasteService.postCambiarEstado(tarea).subscribe();
  }

  realizado(id: number) {
    const tarea = {
      id: id,
      tareaEstado: 'S'
    }
    this.ordenContrasteService.postCambiarEstado(tarea).subscribe();
  }

  getTareas() {
    const medida = this.form.value.idAnormalidad.split('-');
    
    const dataAnormalidad = 
                        { 
                          'id': medida[0],
                          'codigo' : medida[1],
                          'descripcion': medida[2],
                          'generaCNR': medida[3],
                          'generaOrdenNormalizacion': medida[4],
                          'causal': medida[5]
                        };
    this.dataIrregularidad.push(dataAnormalidad);
    this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
    this.ordenContrasteService.getTareas(medida[0]).pipe(
      tap(response => {
        response.forEach((element: any) => {
          const dataTareas = 
                        { 
                          'id': element.id,
                          'idTarea' : element.idTarea,
                          'idAnormalidad': element.idAnormalidad,
                          'tarea': element.tarea,
                          'codAnormalidad': element.codAnormalidad,
                          'codTarea': element.codTarea,
                          'tareaEstado': 'N'
                        };
          this.dataTarea.push(dataTareas);
        });
        this.tareas = new MatTableDataSource<Tarea>(this.dataTarea);
      })
    ).subscribe();
  }

  getMotivo() {
    this.ordenContrasteService.getMotivo().pipe(
      tap(response => {
        this.motivos = response
      })
    ).subscribe();
  }

  remove(i: number) {
    this.dataIrregularidad.splice(i, 1);
    this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
  }

  store() {
    const form = 
                {
                  "idUsuario": this.usuarioService.usuarioStorage?.id,
                  "idOrden": this.idOrden,
                  "idOrdenContraste": this.idOrdenContraste,
                  "idOrdenInspeccion": this.idOrdenInspeccion,
                  "irregularidades": this.dataTarea,
                  ...this.form.value,
                }
    this.ordenContrasteService.postOrdenContraste(form).subscribe();
    this.router.navigate(['/gestion-ordenes/orden-trabajo']);
  }

  getXMLEnvio() {
    this.ordenInspeccionService.getXML(this.idUrl, 'ENVIO').pipe(
      tap(response => {
        if(response != null) {
          this.xmlEnvio = beautify(response.xml)
        }else {
          this.xmlEnvio = ""
        }
      })
    ).subscribe();
  }

  getXMLRecepcion() {
    this.ordenInspeccionService.getXML(this.idUrl, 'RECEPCION').pipe(
      tap(response => {
        if(response != null) {
          this.xmlRecepcion = beautify(response.xml)
        }else {
          this.xmlRecepcion = ""
        }
      })
    ).subscribe();
  }

  getOrdenTransfer() {
    this.ordenInspeccionService.getOrdenTransfer(this.idUrl).pipe(
      tap(response => {
        if(response != '') {
          this.ordenTransfer = response[0]
        }else {
          this.ordenTransfer = []
        }
      })
    ).subscribe();
  }

}
