import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenContrasteVerComponent } from './orden-contraste-ver.component';

describe('OrdenContrasteVerComponent', () => {
  let component: OrdenContrasteVerComponent;
  let fixture: ComponentFixture<OrdenContrasteVerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenContrasteVerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenContrasteVerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
