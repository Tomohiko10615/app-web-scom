import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenContrasteEditarComponent } from './orden-contraste-editar.component';

describe('OrdenContrasteEditarComponent', () => {
  let component: OrdenContrasteEditarComponent;
  let fixture: ComponentFixture<OrdenContrasteEditarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenContrasteEditarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenContrasteEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
