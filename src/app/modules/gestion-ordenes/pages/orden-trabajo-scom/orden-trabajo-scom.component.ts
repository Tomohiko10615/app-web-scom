import { SelectionModel } from '@angular/cdk/collections';
import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { errorMessages } from 'src/app/core/error/error.constant';
import { Parametro } from 'src/app/core/models/parametro.model';
import { ParametroService } from 'src/app/core/service/parametro.service';
import { UsuarioService } from 'src/app/core/service/usuario.service';
import { PROCESO_PERFIL, SOLO_NUMEROS, TIPOS_PROCESOS_ORDENES_SCOM, TIPO_ORDEN_TRANSFER, urlOrdenTrabajoSCOMVer, urlOrdenTrabajoVer } from '../../gestion-ordenes.contants';
import { TipoOrden } from '../../models/tipo-orden.model';
import { OrdenTrabajoService } from '../../services/orden-trabajo.service';
import { tap } from 'rxjs';
import { TipoOrdenService } from '../../services/tipo-orden.service';
import { OrdenTrabajoScomFilter } from '../../models/orden-trabajo-scom-filter.model';
import { ExcelExportService } from 'src/app/core/service/excel-export.service';
import * as moment from 'moment';

@Component({
  selector: 'app-orden-trabajo-scom',
  templateUrl: './orden-trabajo-scom.component.html',
  styleUrls: ['./orden-trabajo-scom.component.scss']
})
export class OrdenTrabajoScomComponent implements OnInit {
  rangoDiasFechaBusqueda: number;
  breadscrums = [
    {
      title: 'Órdenes de Trabajo',
      items: ['Home'],
      active: 'Órdenes de Trabajo'
    }
  ];

  filterToggle = false;

  displayedColumns = [
    'item',
    'nroOrden',
    'codTipoOrden',
    'estado',
    'nroServicio',
    'nroCuenta',
    'fechaEstado',
    'fechaCreacion',
    'usuarioCreador',
    // R.I. REQSCOM08 07/07/2023 INICIO
    'fechaEjecucion',
    // R.I. REQSCOM08 07/07/2023 FIN
    'actions'
  ];

  form: FormGroup;
  procesos: Parametro[] = [];
  tiposOrden: TipoOrden[] = [];
  usuarioLogueado: string;
  estados: Parametro[];

  selection = new SelectionModel<OrdenTrabajoScomFilter>(true, []);
  dataSource: MatTableDataSource<OrdenTrabajoScomFilter>= new MatTableDataSource();
  dataFilter: any = { idTipoOrden: '', fechaInicio: '', fechaFin: '', nroOrden: '', idServicio: 0, nroCuenta: '', codTipoProceso:'', idEstado:''}
  errorMessage = errorMessages;
  totalElements: number;
  pageSize: number;
  prueba: any[] = [];
  filterStorage: any;
  exportDataExcel: any[];

  // Configuration export data
  exportReport = { fileName:`ordenes-perdida-pmant-${this.datePipe.transform(new Date, 'dd.MM.yyyy')}`, sheet: 'Hoja1' }
  autoActivaciones: any[];
  countItem: number;
  elementsTiposTranfer: string = '';
  codsTipoOrdenEorder: any[] = [];
  elementsCodsTipoOrdenEorder = '';
  maxDate= new Date();
  botonAnular = false;
  disabledAutoActivacion: boolean;

  page = 0;
  size = 10;
  order = "id";
  asc = false;
  
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;
  @ViewChild(MatMenuTrigger)
  contextMenu: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };
  constructor(private formBuilder: FormBuilder,
    private parametroService: ParametroService,
    private ordenTrabajoService: OrdenTrabajoService,
    private datePipe: DatePipe,
    private snackBar: MatSnackBar,
    private usuarioService: UsuarioService,
    private router: Router,
    private tipoOrdenService: TipoOrdenService,
    private excelExportService: ExcelExportService) { }

  ngOnInit() {
    this.initForm();  
    this.validacionAcesosPorRol();
    this.getProcesos();
    this.getEstados();
    this.enviarValoresPorDefecto();

    if(sessionStorage.getItem("filtroOrdenScom") != null) {
      this.dataFilter = JSON.parse(sessionStorage.getItem("filtroOrdenScom"));
      this.filterStorage = JSON.parse(sessionStorage.getItem("filtroOrdenScom"));
      this.form.get('codTipoProceso')?.setValue(this.filterStorage["codTipoProceso"]);
      this.selectTipoOrden({value:this.filterStorage["codTipoProceso"]});
      this.form.get('idTipoOrden')?.setValue(this.filterStorage["idTipoOrden"]);
      this.form.get('nroOrden')?.setValue(this.filterStorage["nroOrden"]);
      this.form.get('idEstado')?.setValue(this.filterStorage["idEstado"]);
      //this.form.get('nroCuenta')?.setValue(this.filterStorage["nroCuenta"]);
      this.form.get('fechaInicio')?.setValue(this.transformDate(this.filterStorage["fechaInicio"]));
      this.form.get('fechaFin')?.setValue(this.transformDate(this.filterStorage["fechaFin"]));
      this.obtenerOrdenesTrabajoPorFiltro(this.page, this.size, this.order, this.asc,this.dataFilter);
    }
   
    this.dataFilter.codTipoProceso = this.form.get('codTipoProceso').value;
    if(this.form.get('codTipoProceso'). value != 'TODO') {
      this.dataFilter.codTipoProceso = this.elementsTiposTranfer
    }
    
  }

  
  validacionAcesosPorRol() {
    const codTodoProceso = 'TODO'
    let usuarioToken;
    if(sessionStorage.getItem("usuarioPerfil") != null) {
      usuarioToken = JSON.parse(sessionStorage.getItem("usuarioPerfil"));
    }
    //const usuarioToken = this.usuarioService.usuarioPerfilStorage;
    this.usuarioLogueado = usuarioToken.username;
    const codProceso = PROCESO_PERFIL.get(usuarioToken.paramNombre) || '';
    if(codProceso === codTodoProceso) {
      this.form.get('codTipoProceso')?.setValue(codTodoProceso);
    } else {
      this.form.get('codTipoProceso')?.setValue(codProceso);
       this.form.controls['codTipoProceso'].disable();
    }
    this.selectTipoOrden({value: codProceso});
  }


  initForm(): void {
    this.form = this.formBuilder.group({
      idTipoOrden: [''],
      fechaInicio: [''],
      fechaFin: [''],
      nroOrden: ['', Validators.pattern(SOLO_NUMEROS)],
      idServicio: ['', Validators.pattern(SOLO_NUMEROS)],
      nroCuenta: ['', Validators.pattern(SOLO_NUMEROS)],
      idEstado: [''],
      codTipoProceso: ['']
    });
  }

  enviarValoresPorDefecto(): void {
    this.form.get('idTipoOrden')?.setValue('TODO');
    this.form.get('idEstado')?.setValue('TODO');
  }
  
  getProcesos(): void {
    this.procesos = TIPOS_PROCESOS_ORDENES_SCOM;
  }

   getEstados(): void {
    this.parametroService.getEstados().pipe(
      tap((response: Parametro[]) => {
        this.estados = response;
      })
    ).subscribe();
  } 


  selectTipoOrden($event:any) {
    
    this.elementsTiposTranfer = '';
    if($event) {
      const codProceso = $event.value;
      this.form.get('idTipoOrden')?.setValue('TODO');
      this.tipoOrdenService.getTipoOrdenByCodProceso(codProceso).pipe((
        tap((response: TipoOrden[]) => this.tiposOrden = response)
      )).subscribe();

      let array : any[] =[];
      TIPO_ORDEN_TRANSFER.filter((x: any) => {
        if(x.codProceso === codProceso) {
          array.push("'"+x.codigoLegacy+"'");
         }
      }) 

      this.elementsTiposTranfer= array.join(',');
    }
  }

  
  submitOrdenTrabajo(): void {
    // if(((this.form.value.fechaInicio == '' || this.form.value.fechaInicio == null) && (this.form.value.fechaFin != '' && this.form.value.fechaFin != null))) {
    //   this.snackBar.open('La fecha inicio no debe estar vacio.','Close');
    //   return
    // }

    // if(((this.form.value.fechaFin == '' || this.form.value.fechaFin == null) && (this.form.value.fechaInicio != '' && this.form.value.fechaInicio != null))) {
    //   this.snackBar.open('La fecha fin no debe estar vacio.','Close');
    //   return
    // }

    // this.snackBar.open('111111111.','Close');

    // if(((this.form.value.fechaFin == '' || this.form.value.fechaFin == null) || (this.form.value.fechaInicio == '' || this.form.value.fechaInicio == null))){
    //   this.snackBar.open('Favor de completar los campos fecha de creación desde y hasta a.','Close');
    //   return
    // }

    
    // if(Math.round((this.form.value.fechaFin - this.form.value.fechaInicio)/(1000*60*60*24)) >= 6){
    //   this.snackBar.open('El reporte no debe ser mayor a 6 dias','Close');
    //   return
    // }
    // console.log("nro orden: ".concat(this.form.value.nroOrden));
    // console.log("nro servicio: ".concat(this.form.value.idServicio));
    if (
      (this.form.value.nroOrden.replace(/\s/g, "") == '' || this.form.value.nroOrden.replace(/\s/g, "") == null) && 
      (this.form.value.nroCuenta.replace(/\s/g, "") == '' || this.form.value.nroCuenta.replace(/\s/g, "") == null) && 
      (this.form.value.idServicio.replace(/\s/g, "") == '' || this.form.value.idServicio.replace(/\s/g, "") == null)
    ) {
      if(((this.form.value.fechaFin == '' || this.form.value.fechaFin == null) || (this.form.value.fechaInicio == '' || this.form.value.fechaInicio == null))){
        this.snackBar.open('Favor de completar los campos fecha de creación desde y hasta.','Close');
        return
      }
      else {
        this.rangoDiasFechaBusqueda = Number.parseInt(JSON.parse(sessionStorage.getItem('rangoFechaBusqueda')).valorNum);
        if(Math.round((this.form.value.fechaFin - this.form.value.fechaInicio)/(1000*60*60*24)) >= this.rangoDiasFechaBusqueda){
          this.snackBar.open('El reporte no debe ser mayor a ' + this.rangoDiasFechaBusqueda + ' dias','Close');
          return
        }
      }
    }
    

    this.selection.clear();
    this.page = 0;
    this.size = 10;
    this.dataFilter = this.form.value;
    this.dataFilter.codTipoProceso = this.form.get('codTipoProceso').value;
    sessionStorage.removeItem("filtroOrdenScom");
    sessionStorage.setItem('filtroOrdenScom',JSON.stringify(this.dataFilter));
    this.obtenerOrdenesTrabajoPorFiltro(this.page, this.size, this.order, this.asc,this.dataFilter);
  }

  transformDate(date: string): string{
    const dateTransform = this.datePipe.transform(date, 'yyyy-MM-dd');
    return dateTransform;
  }

  obtenerOrdenesTrabajoPorFiltro(page: number, size: number, order: string, asc: boolean, filtro: any): void {
    filtro.fechaInicio = (filtro.fechaInicio == null || filtro.fechaInicio == '') ? '1900-01-01' : this.transformDate(filtro?.fechaInicio);
    filtro.fechaFin = (filtro.fechaFin == null || filtro.fechaFin == '') ? '2999-01-01' : this.transformDate(filtro.fechaFin);
    filtro.nroOrden= this.dataFilter.nroOrden;
   // filtro.idEstado = filtro.idEstado == 0 ? '': filtro.idEstado;
    //filtro.idTipoOrden = filtro.idTipoOrden == 'TODO' ? this.elementsTiposTranfer : filtro.idTipoOrden;
    if (filtro.idTipoOrden === 'TODO') {
      filtro.codTipoProceso = this.elementsTiposTranfer;
    }

    console.log(filtro.idServicio);

    this.ordenTrabajoService.filtroOrdenSCOMTrabajo(page, size, order, asc, filtro).pipe(
      tap((response: any) => {
       this.dataSource = new MatTableDataSource<OrdenTrabajoScomFilter>(response.content);
       this.exportDataExcel = response.content;
       this.pageSize = response.size;
       this.totalElements = response.totalElements;
       this.paginator.pageIndex = this.page;
       this.countItem= response.pageable.offset + 1;
      })
    ).subscribe();
  }

  pageChanged(event: PageEvent) {
    this.selection.clear();
    this.size = event.pageSize;
    this.page = event.pageIndex;
    this.obtenerOrdenesTrabajoPorFiltro(this.page, this.size, this.order, this.asc, this.dataFilter);

  }

  sendUrlVer(tipoOrden: any, idOrdenTrabajo: number) {
      let url = urlOrdenTrabajoSCOMVer.get(tipoOrden)|| '';
      this.router.navigate([`/gestion-ordenes/orden-trabajo-scom/${url}/${idOrdenTrabajo}`])
    
  }

  exportFile() {
    if(this.totalElements == 0){
      this.snackBar.open('No hay datos de Resultado de busqueda para exportar','Close');
      return
    }

    this.ordenTrabajoService.filtroOrdenSCOMTrabajo(0, this.totalElements, this.order, this.asc, this.dataFilter).pipe(
      tap((response: any)=> {
        this.exportDataExcel = response.content.map(function(obj) {
          return {
            nroOrden: obj.nroOrden,
            codTipoOrden: obj.codTipoOrden,
            estado: obj.estado,
            nroServicio: obj.nroServicio,
            fechaEstado: obj.fechaEstado == null ? '' : moment(obj.fechaEstado).format('DD/MM/yyyy'),
            fechaCreacion: obj.fechaCreacion == null ? '' : moment(obj.fechaCreacion).format('DD/MM/yyyy'),
            usuarioCreador: obj.usuarioCreador,
            // R.I. REQSCOM06 06/10/2023 INICIO
   //         nroCuenta: obj.nroCuenta,
            distrito: obj.distrito,
            fechaEjecucion: obj.fechaEjecucion,
            observacionTecnico: obj.observacionTecnico
            // R.I. REQSCOM06 06/10/2023 FIN
          }
        });
      }),
      tap((response) => !!response),
      tap(() => this.excelExportService.exportAsExcelFile(this.exportDataExcel, this.exportReport.fileName))
    ).subscribe();
  //  this.excelExportService.exportAsExcelFile(exportDataExcelModi, this.exportReport.fileName);
  }

}
