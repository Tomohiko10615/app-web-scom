import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenTrabajoScomComponent } from './orden-trabajo-scom.component';

describe('OrdenTrabajoScomComponent', () => {
  let component: OrdenTrabajoScomComponent;
  let fixture: ComponentFixture<OrdenTrabajoScomComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenTrabajoScomComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenTrabajoScomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
