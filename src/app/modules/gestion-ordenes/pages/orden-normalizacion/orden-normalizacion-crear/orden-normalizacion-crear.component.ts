import { Component, NgModule, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, catchError, of, tap, throwError } from 'rxjs';
import { UsuarioService } from 'src/app/core/service/usuario.service';
import { Irregularidad } from '../../../models/irregularidad.model';
import { OrdenInspeccionNotificada } from '../../../models/orden-inspeccion-notificada.model';
import { Tarea } from '../../../models/tarea.model';
import { OrdenNormalizacionService } from '../../../services/orden-normalizacion.service';
import { MatSort } from '@angular/material/sort';
import { HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'app-orden-normalizacion-crear',
  templateUrl: './orden-normalizacion-crear.component.html',
  styleUrls: ['./orden-normalizacion-crear.component.sass']
})
export class OrdenNormalizacionCrearComponent implements OnInit {

  breadscrums = [
    {
      title: 'Creación de Orden de Normalización',
      items: ['Home'],
      active: 'Creación de Orden de Normalización'
    }
  ];

  filterToggle = false;

  displayedColumns = [
    'select',
    'nro',
    'nroOrden',
    'fechaCreacion',
    'contratista',
    'tipoInspeccion'
  ];

  displayedColumns2 = [
    'id',
    'codigo',
    'descripcion',
    'generaCNR',
    'generaOrdenNormalizacion',
    'causal',
    'actions'
  ];

  displayedColumns3 = [
    'select',
    'codAnormalidad',
    'codTarea',
    'tarea'
  ];

  ordenInspeccionNotificadas: MatTableDataSource<OrdenInspeccionNotificada> = new MatTableDataSource();
  irregularidades: MatTableDataSource<Irregularidad> = new MatTableDataSource();
  tareas: MatTableDataSource<Tarea> = new MatTableDataSource();

  // R.I. Corrección ordenar inspecciones notificadas por fecha 18/10/2023 INICIO
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  // R.I. Corrección ordenar inspecciones notificadas por fecha 18/10/2023 FIN
  
  form: FormGroup;
  form2: FormGroup;
  form3: FormGroup;

  nroServicio: string;
  nroCuenta: string;
  cliente: string;
  direccion: string;
  distrito: string;
  rutaLectura: string;

  anormalidades: any[];
  subtipos: any[];

  dataIrregularidad = new Array();
  dataTarea = new Array();

  idOrden: number = 0;
  idOrdenNormalizacion: number = 0;
  idOrdenInspeccion: number = 0;

  idUrl: number;
  orden: any;
  botonBuscar: boolean = true;
  botonSubmit: string = "Guardar";
  botonActivar: boolean = true;

  botonGuardar: boolean = false;

  nroOrden: string = "";

  constructor(private fb: FormBuilder, 
              private ordenNormalizacionService: OrdenNormalizacionService,
              private usuarioService : UsuarioService,
              private route : ActivatedRoute,
              private router: Router,
              private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.idUrl = this.route.snapshot.params['id'];

    this.initForm();
    this.getAnormalidades();
    this.getSubTipos();

    if(this.idUrl) {
      this.getData();
      this.breadscrums = [
        {
          title: 'Edición de Orden de Normalización',
          items: ['Home'],
          active: 'Edición de Orden de Normalización'
        }
      ];
      this.botonBuscar = false;
      this.botonSubmit = "Actualizar";
    }
  }

  getData() {
    this.ordenNormalizacionService.getOrden(this.idUrl, 0).pipe(
      tap(response => {
        this.orden = response;
        this.initForm();
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.direccion
        this.distrito = response.distrito
        this.rutaLectura = response.rutaLectura
        this.idOrdenInspeccion = response.idOrdenInspeccion
        this.idOrden = response.idOrden
        this.idOrdenNormalizacion = response.idOrdenNormalizacion
        this.form.value.nroServicio = response.nroServicio
        this.nroOrden = response.nroOrden
        this.buscarCliente();
        this.check(response.idOrdenInspeccion);
        response.anormalidades.forEach((element: any) => {
          this.dataIrregularidad.push(this.getDataAnormalidad(element));
        });
        this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
        response.tareas.forEach((element: any) => {
          this.dataTarea.push(this.getDataTareas(element, element.tareaEstado));
        });
        this.tareas = new MatTableDataSource<Tarea>(this.dataTarea);
      })
    ).subscribe();
  }

  initForm() {
    this.form = this.fb.group({
      nroServicio: [{ value: !this.orden ? '' : this.orden.nroServicio, disabled: !this.orden ? false : true}, [Validators.required, Validators.pattern('[0-9]*')]]
    })
    this.form2 = this.fb.group({
      subtipo: [{ value: !this.orden ? '' : this.orden.subtipo, disabled: false}, Validators.required],
      observaciones: [{ value: !this.orden ? '' : this.orden.observacion, disabled: false}, [Validators.required, Validators.pattern("[a-zA-Z0-9 ]*")]],
    })
    this.form3 = this.fb.group({
      idAnormalidad: []
    })
  }

  buscarCliente() {
    this.irregularidades = new MatTableDataSource<Irregularidad>();
    this.tareas = new MatTableDataSource<Tarea>();
    this.ordenNormalizacionService.getNroServicio(this.form.value.nroServicio).pipe(
      tap(response => {
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.direccion
        this.distrito = response.distrito
        this.rutaLectura = response.rutaLectura
         // R.I. Corrección ordenar inspecciones notificadas por fecha 18/10/2023 INICIO
        response.inspecciones.sort((a, b) => {
          return new Date(b.fechaCreacion).getTime() - new Date(a.fechaCreacion).getTime();
        });
        this.ordenInspeccionNotificadas = new MatTableDataSource<OrdenInspeccionNotificada>(response.inspecciones);
        this.ordenInspeccionNotificadas.sortingDataAccessor = (item, property) => {
          if (property === 'fechaCreacion') {
            return new Date(item.fechaCreacion);
          }
          return item[property];
        };
        this.sort.sort({ id: 'fechaCreacion', start: 'desc', disableClear: false });
         // R.I. Corrección ordenar inspecciones notificadas por fecha 18/10/2023 FIN
        if(response.inspecciones.length == 0) {
          this.botonActivar = false
        }else {
          this.botonActivar = true
        }
      })
    ).subscribe();
  }

  getAnormalidades() {
    this.ordenNormalizacionService.getAnormalidades().pipe(
      tap(response => {
        this.anormalidades = response
      })
    ).subscribe();
  }

  getSubTipos() {
    this.ordenNormalizacionService.getSubTipos().pipe(
      tap(response => {
        this.subtipos = response
      })
    ).subscribe();
  }

  check(id: number) {
    this.idOrdenInspeccion = id;
    this.irregularidades = new MatTableDataSource<Irregularidad>();
    this.tareas = new MatTableDataSource<Tarea>();
    this.dataTarea = [];
    this.dataIrregularidad = [];
    this.ordenNormalizacionService.getOrden(id, 1).pipe(
      tap(response => {
        response.anormalidades.forEach((element: any) => {
          this.dataIrregularidad.push(this.getDataAnormalidad(element));
        });
        this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
        response.tareas.forEach((element: any) => {
          this.dataTarea.push(this.getDataTareas(element, element.tareaEstado));
        });
        this.tareas = new MatTableDataSource<Tarea>(this.dataTarea);
      })
    ).subscribe();
  }

  noRealizado(element: any, i: number) {
    const updatedTarea = this.getDataTareas(element, 'N');
    this.dataTarea[i] = updatedTarea;
}

realizado(element: any, i: number) {
    const updatedTarea = this.getDataTareas(element, 'S');
    this.dataTarea[i] = updatedTarea;
}

  getDataTareas(element: any, estado: string) {
    const tarea = 
                { 
                  'id': element.id,
                  'idTarea' : element.idTarea,
                  'idAnormalidad': element.idAnormalidad,
                  'tarea': (element.desTarea) ? element.desTarea : element.tarea,
                  'codAnormalidad': element.codAnormalidad,
                  'codTarea': element.codTarea,
                  'tareaEstado': estado
                };
    return  tarea;
  }

  getDataAnormalidad(element: any) {
    const dataAnormalidad = 
          { 
            'id': element.id,
            'codigo' : element.codigo,
            'descripcion': element.descripcion,
            'generaCNR': element.generaCNR,
            'generaOrdenNormalizacion': element.generaOrdenNormalizacion,
            'causal': element.causal
          };
    return dataAnormalidad;
  }

  getTareas() {
    const medida = this.form3.value.idAnormalidad.split('-');
    const resultado = this.dataIrregularidad.filter(e => e.descripcion.includes(medida[2]));
    if(resultado.length > 0) {
      alert('La irregularidad seleccionada se encuentra agregado.')
    }else {
      const dataAnormalidad = 
                          { 
                            'id': medida[0],
                            'codigo' : medida[1],
                            'descripcion': medida[2],
                            'generaCNR': medida[3],
                            'generaOrdenNormalizacion': medida[4],
                            'causal': medida[5]
                          };
      this.dataIrregularidad.push(dataAnormalidad);
      this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
      this.ordenNormalizacionService.getTareas(medida[0]).pipe(
        tap(response => {
          response.forEach((element: any) => {
            this.dataTarea.push(this.getDataTareas(element, 'N'));
          });
          this.tareas = new MatTableDataSource<Tarea>(this.dataTarea);
        })
      ).subscribe();
    }
  }

  remove(i: number, irregularidad: any) {
    this.dataIrregularidad.splice(i, 1);
    this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
    this.dataTarea = this.dataTarea.filter(response => response?.codAnormalidad !== irregularidad.codigo)
    this.tareas = new MatTableDataSource<Tarea>(this.dataTarea);
  }

  store() {
    /* if(this.dataIrregularidad.length == 0) {
      alert('Debe seleccionar una orden de inspección notificada.')
    }else { */
      const form = 
                {
                  "idUsuario": this.usuarioService.usuarioStorage?.id,
                  "idOrden": this.idOrden,
                  "idOrdenNormalizacion": this.idOrdenNormalizacion,
                  "idOrdenInspeccion": this.idOrdenInspeccion,
                  "irregularidades": this.dataTarea,
                  ...this.form.value,
                  ...this.form2.value,
                  ...this.form3.value
                }
            //const nroCuentaForm = this.form.get('nroCuenta').value;
            this.ordenNormalizacionService.postOrdenNormalizacion(form).pipe(
            tap((response) => !!response),
            tap(() => {this.botonGuardar = true}),
            tap((response) => this.nroOrden = response.nroOrden),
            //tap((response) => this.snackBar.open(`Registro exitoso. Nro Orden: ${response.nroOrden} Nro Cuenta: ${this.nroCuenta}`, 'CERRAR')),
            // tap(() => this.router.navigate(['/gestion-ordenes/orden-trabajo']))
            tap((response) => {
              if (response.id === 0) {
                  this.snackBar.open('No puede crear la orden por que tiene una normalización abierta.', 'CERRAR');
              } else if (response.id === -1) {
                  this.snackBar.open('Error interno del servidor.', 'CERRAR');
              } else {
                  this.snackBar.open(`Registro exitoso. Nro Orden: ${response.nroOrden} Nro Cuenta: ${this.nroCuenta}`, 'CERRAR');
              }
          })
            ).subscribe();
    /* } */
  }

}

