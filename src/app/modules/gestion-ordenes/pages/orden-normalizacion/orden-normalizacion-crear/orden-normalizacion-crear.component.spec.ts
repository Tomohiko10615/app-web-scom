import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenNormalizacionCrearComponent } from './orden-normalizacion-crear.component';

describe('OrdenNormalizacionCrearComponent', () => {
  let component: OrdenNormalizacionCrearComponent;
  let fixture: ComponentFixture<OrdenNormalizacionCrearComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenNormalizacionCrearComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenNormalizacionCrearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
