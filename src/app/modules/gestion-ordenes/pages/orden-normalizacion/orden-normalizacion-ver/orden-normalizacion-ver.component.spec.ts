import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenNormalizacionVerComponent } from './orden-normalizacion-ver.component';

describe('OrdenNormalizacionVerComponent', () => {
  let component: OrdenNormalizacionVerComponent;
  let fixture: ComponentFixture<OrdenNormalizacionVerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenNormalizacionVerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenNormalizacionVerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
