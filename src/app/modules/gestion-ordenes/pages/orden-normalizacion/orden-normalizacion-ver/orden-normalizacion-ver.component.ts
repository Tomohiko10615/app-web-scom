import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { tap } from 'rxjs';
import { UsuarioService } from 'src/app/core/service/usuario.service';
import { Irregularidad } from '../../../models/irregularidad.model';
import { OrdenInspeccionNotificada } from '../../../models/orden-inspeccion-notificada.model';
import { Tarea } from '../../../models/tarea.model';
import { OrdenNormalizacionService } from '../../../services/orden-normalizacion.service';
import beautify from "xml-beautifier";
import { OrdenInspeccionNotificadaService } from '../../../services/orden-inspeccion-notificada.service';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { TransferXmlService } from '../../../services/transfer-xml.service';


//Se declara funciones creadas en src/assets/custom-js/transfer-xml.js
declare function envioXml(valorEnvio): any;
declare function recepcionXml(message): any;

@Component({
  selector: 'app-orden-normalizacion-ver',
  templateUrl: './orden-normalizacion-ver.component.html',
  styleUrls: ['./orden-normalizacion-ver.component.scss']
})
export class OrdenNormalizacionVerComponent implements OnInit {

  breadscrums = [
    {
      title: 'Creación de Orden de Normalización',
      items: ['Home'],
      active: 'Creación de Orden de Normalización'
    }
  ];

  filterToggle = false;

  displayedColumns = [
    'select',
    'nro',
    'nroOrden',
    'fechaCreacion',
    'contratista',
    'tipoInspeccion'
  ];

  displayedColumns2 = [
    'id',
    'codigo',
    'descripcion',
    'generaCNR',
    'generaOrdenNormalizacion'
  ];

  displayedColumns3 = [
    'select',
    'codAnormalidad',
    'codTarea',
    'tarea'
  ];

  ordenInspeccionNotificadas: MatTableDataSource<OrdenInspeccionNotificada> = new MatTableDataSource();
  irregularidades: MatTableDataSource<Irregularidad> = new MatTableDataSource();
  tareas: MatTableDataSource<Tarea> = new MatTableDataSource();
  
  form: FormGroup;

  nroServicio: string;
  nroCuenta: string;
  cliente: string;
  direccion: string;
  distrito: string;
  rutaLectura: string;
  estadoWkf: string;
  nroNotificacion: string;
  fechaNotificacion: Date;
  fechaEjecucion: Date; /* CHR 04-07-23 */

  anormalidades: any[];
  subtipos: any[];

  dataIrregularidad = new Array();
  dataTarea = new Array();

  idOrden: number = 0;
  idOrdenNormalizacion: number = 0;
  idOrdenInspeccion: number = 0;

  idUrl: number;
  orden: any;
  botonBuscar: boolean = true;
  botonSubmit: string = "Guardar";

/*   xmlEnvio: any;
  xmlRecepcion: any; */
  xml: any 
  ordenTransfer: any = [];

  nroOrden: string = "";
  inicioCreacion: string = "SC4J";

  @ViewChild('tabGroup') tabGroup;
  xmlEnvio: string ='';
  xmlRecepcion: string='';
  activeXmlEnvio: boolean = false;
  activexmlRecepcion: boolean = false;

  constructor(private fb: FormBuilder, 
              private ordenNormalizacionService: OrdenNormalizacionService,
              private ordenInspeccionService: OrdenInspeccionNotificadaService,
              private usuarioService : UsuarioService,
              private route : ActivatedRoute,
              private router: Router,
              private transferXml: TransferXmlService) { }

  ngOnInit() {
    this.idUrl = this.route.snapshot.params['id'];

    this.initForm();
    this.getAnormalidades();
    this.getSubTipos();
  /*   this.getXMLEnvio();
    this.getXMLRecepcion(); */
    this.getOrdenTransfer();

    if(this.idUrl) {
      this.getData();
      this.breadscrums = [
        {
          title: 'Ver de Orden de Normalización',
          items: ['Home'],
          active: 'Ver de Orden de Normalización'
        }
      ];
      this.botonBuscar = false;
      this.botonSubmit = "Actualizar";
    }
  }

  getData() {
    this.ordenNormalizacionService.getOrden(this.idUrl, 0).pipe(
      tap(response => {
        this.orden = response;
        this.initForm();
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.direccion
        this.distrito = response.distrito
        this.rutaLectura = response.rutaLectura
        this.idOrdenInspeccion = response.idOrdenInspeccion
        this.idOrden = response.idOrden
        this.idOrdenNormalizacion = response.idOrdenNormalizacion
        this.form.value.nroServicio = response.nroServicio
        this.nroOrden = response.nroOrden
        this.nroNotificacion = response.nroNotificacion
        this.fechaNotificacion = response.fechaNotificacion
        this.fechaEjecucion = response.fechaFinalizacion /* CHR 04-07-23 */
        this.estadoWkf = response.estadoWkf
        this.ordenInspeccionNotificadas = new MatTableDataSource<OrdenInspeccionNotificada>(response.ordInspeccionNotificada); // CHR 19-06-23
        this.buscarCliente();
        this.check(response.idOrdenInspeccion);
        if(response.idOrdenSc4j == 0) {
          this.inicioCreacion = "SCOM";
        }
        response.anormalidades.forEach((element: any) => {
          const dataAnormalidad = 
                        { 
                          'id': element.id,
                          'codigo' : element.codigo,
                          'descripcion': element.descripcion,
                          'generaCNR': element.generaCNR,
                          'generaOrdenNormalizacion': element.generaOrdenNormalizacion,
                          'causal': element.causal
                        };
          this.dataIrregularidad.push(dataAnormalidad);
        });
        this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
        response.tareas.forEach((element: any) => {
          const dataTareas = 
                        { 
                          'id': element.id,
                          'idTarea' : element.idTarea,
                          'idAnormalidad': element.idAnormalidad,
                          'tarea': element.tarea,
                          'codAnormalidad': element.codAnormalidad,
                          'codTarea': element.codTarea,
                          'tareaEstado': element.tareaEstado
                        };
          this.dataTarea.push(dataTareas);
        });
        this.tareas = new MatTableDataSource<Tarea>(this.dataTarea);
      })
    ).subscribe();
  }

  initForm() {
    this.form = this.fb.group({
      nroServicio: [{ value: !this.orden ? '' : this.orden.nroServicio, disabled: true}, Validators.required],
      subtipo: [{ value: !this.orden ? '' : this.orden.subtipo, disabled: true}, Validators.required],
      observaciones: [{ value: !this.orden ? '' : this.orden.observacion, disabled: true}, Validators.required]
    })
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {

    if(tabChangeEvent.tab.textLabel=== 'DATOS ENVIO FORCEBEAT'){
      if(this.activeXmlEnvio == false) {
        this.obtenerDatosXmlEnvio();
        
      }
    }

    if(tabChangeEvent.tab.textLabel=== 'DATOS RECEPCIÓN FORCEBEAT'){
      if(this.activexmlRecepcion == false) {
        this.obtenerDatosXmlRecepcion();
       
      }
    }
  }


  obtenerDatosXmlEnvio(): void {
    
    this.transferXml.obtenerXmlEnvio(this.idUrl).pipe(
      tap((response: any) => {
        this.xmlEnvio = response.xml.replaceAll("\n", "");
        envioXml(this.xmlEnvio);
        this.activeXmlEnvio= true;
      })
    ).subscribe();
 
    }
  
    obtenerDatosXmlRecepcion(): void {
    
      this.transferXml.obtenerXmlRecepcion(this.idUrl).pipe(
        tap((response: any) => {
          this.xmlRecepcion = response.xml.replaceAll("\n", "");
          recepcionXml(this.xmlRecepcion);
          this.activexmlRecepcion= true;
        })
      ).subscribe();
   
      }

  buscarCliente() {
    this.irregularidades = new MatTableDataSource<Irregularidad>();
    this.tareas = new MatTableDataSource<Tarea>();
    const nroServicio = this.form.value.nroServicio
    this.ordenNormalizacionService.getNroServicio(nroServicio).pipe(
      tap(response => {
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.direccion
        this.distrito = response.distrito
        this.rutaLectura = response.rutaLectura
        //this.ordenInspeccionNotificadas = new MatTableDataSource<OrdenInspeccionNotificada>(response.inspecciones); // CHR 19-06-23
      })
    ).subscribe();
  }

  getAnormalidades() {
    this.ordenNormalizacionService.getAnormalidades().pipe(
      tap(response => {
        this.anormalidades = response
      })
    ).subscribe();
  }

  getSubTipos() {
    this.ordenNormalizacionService.getSubTipos().pipe(
      tap(response => {
        this.subtipos = response
      })
    ).subscribe();
  }

  check(id: number) {
    this.idOrdenInspeccion = id;
    /*
    this.irregularidades = new MatTableDataSource<Irregularidad>();
    this.tareas = new MatTableDataSource<Tarea>();
    this.dataTarea = [];
    this.dataIrregularidad = [];
    this.ordenNormalizacionService.getOrden(id, 1).pipe(
      tap(response => {
        response.anormalidades.forEach((element: any) => {
          const dataAnormalidad = 
                        { 
                          'id': element.id,
                          'codigo' : element.codigo,
                          'descripcion': element.descripcion,
                          'generaCNR': element.generaCNR,
                          'generaOrdenNormalizacion': element.generaOrdenNormalizacion,
                          'causal': element.causal
                        };
          this.dataIrregularidad.push(dataAnormalidad);
        });
        this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
        response.tareas.forEach((element: any) => {
          const dataTareas = 
                        { 
                          'id': element.id,
                          'idTarea' : element.idTarea,
                          'idAnormalidad': element.idAnormalidad,
                          'tarea': element.tarea,
                          'codAnormalidad': element.codAnormalidad,
                          'codTarea': element.codTarea,
                          'tareaEstado': element.tareaEstado
                        };
          this.dataTarea.push(dataTareas);
        });
        this.tareas = new MatTableDataSource<Tarea>(this.dataTarea);
      })
    ).subscribe(); */
  }

  noRealizado(id: number) {
    const tarea = {
      id: id,
      tareaEstado: 'N'
    }
    this.ordenNormalizacionService.postCambiarEstado(tarea).subscribe();
  }

  realizado(id: number) {
    const tarea = {
      id: id,
      tareaEstado: 'S'
    }
    this.ordenNormalizacionService.postCambiarEstado(tarea).subscribe();
  }

  getTareas() {
    const medida = this.form.value.idAnormalidad.split('-');
    
    const dataAnormalidad = 
                        { 
                          'id': medida[0],
                          'codigo' : medida[1],
                          'descripcion': medida[2],
                          'generaCNR': medida[3],
                          'generaOrdenNormalizacion': medida[4],
                          'causal': medida[5]
                        };
    this.dataIrregularidad.push(dataAnormalidad);
    this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
    this.ordenNormalizacionService.getTareas(medida[0]).pipe(
      tap(response => {
        response.forEach((element: any) => {
          const dataTareas = 
                        { 
                          'id': element.id,
                          'idTarea' : element.idTarea,
                          'idAnormalidad': element.idAnormalidad,
                          'tarea': element.tarea,
                          'codAnormalidad': element.codAnormalidad,
                          'codTarea': element.codTarea,
                          'tareaEstado': 'N'
                        };
          this.dataTarea.push(dataTareas);
        });
        this.tareas = new MatTableDataSource<Tarea>(this.dataTarea);
      })
    ).subscribe();
  }

  remove(i: number) {
    this.dataIrregularidad.splice(i, 1);
    this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
  }

  store() {
    const form = 
                {
                  "idUsuario": this.usuarioService.usuarioStorage?.id,
                  "idOrden": this.idOrden,
                  "idOrdenNormalizacion": this.idOrdenNormalizacion,
                  "idOrdenInspeccion": this.idOrdenInspeccion,
                  "irregularidades": this.dataTarea,
                  ...this.form.value,
                }
    this.ordenNormalizacionService.postOrdenNormalizacion(form).subscribe();
    this.router.navigate(['/gestion-ordenes/orden-trabajo']);
  }

  getXMLEnvio() {
    this.ordenInspeccionService.getXML(this.idUrl, 'ENVIO').pipe(
      tap(response => {
        if(response != null) {
          this.xmlEnvio = beautify(response.xml)
        }else {
          this.xmlEnvio = ""
        }
      })
    ).subscribe();
  }

  getXMLRecepcion() {
    this.ordenInspeccionService.getXML(this.idUrl, 'RECEPCION').pipe(
      tap(response => {
        if(response != null) {
          this.xmlRecepcion = beautify(response.xml)
        }else {
          this.xmlRecepcion = ""
        }
      })
    ).subscribe();
  }

  getOrdenTransfer() {
    this.ordenInspeccionService.getOrdenTransfer(this.idUrl).pipe(
      tap(response => {
        if(response != '') {
          this.ordenTransfer = response[0]
        }else {
          this.ordenTransfer = []
        }
      })
    ).subscribe();
  }

}
