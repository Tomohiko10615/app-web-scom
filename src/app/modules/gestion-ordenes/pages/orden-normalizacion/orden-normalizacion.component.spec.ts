import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenNormalizacionComponent } from './orden-normalizacion.component';

describe('OrdenNormalizacionComponent', () => {
  let component: OrdenNormalizacionComponent;
  let fixture: ComponentFixture<OrdenNormalizacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenNormalizacionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenNormalizacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
