import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenNormalizacionEditarComponent } from './orden-normalizacion-editar.component';

describe('OrdenNormalizacionEditarComponent', () => {
  let component: OrdenNormalizacionEditarComponent;
  let fixture: ComponentFixture<OrdenNormalizacionEditarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenNormalizacionEditarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenNormalizacionEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
