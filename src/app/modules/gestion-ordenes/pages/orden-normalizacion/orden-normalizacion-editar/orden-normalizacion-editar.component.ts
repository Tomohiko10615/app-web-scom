import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DataSource, SelectionModel } from '@angular/cdk/collections';
import { OrdenInspeccionNotificada } from '../../../models/orden-inspeccion-notificada.model';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { BehaviorSubject, merge, Observable } from 'rxjs';
import { OrdenInspeccionNotificadaService } from '../../../services/orden-inspeccion-notificada.service';
import { HttpClient } from '@angular/common/http';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { MatMenuTrigger } from '@angular/material/menu';
import { IrregularidadService } from '../../../services/irregularidad.service';
import { Irregularidad } from '../../../models/irregularidad.model';
import { TareaService } from '../../../services/tarea.service';
import { Tarea } from '../../../models/tarea.model';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-orden-normalizacion-editar',
  templateUrl: './orden-normalizacion-editar.component.html',
  styleUrls: ['./orden-normalizacion-editar.component.sass']
})

export class OrdenNormalizacionEditarComponent extends UnsubscribeOnDestroyAdapter implements OnInit {

  breadscrums = [
    {
      title: 'Editar Orden de Normalización',
      items: ['Home'],
      active: 'Editar Orden de Normalización'
    }
  ];

  filterToggle = false;

  selection = new SelectionModel<OrdenInspeccionNotificada>(true, []);

  displayedColumns = [
    'select',
    'id',
    'nro',
    'nroOrden',
    'fechaCreacion',
    'contratista',
    'tipoInspeccion'
  ];

  displayedColumns2 = [
    'id',
    'nro',
    'codigo',
    'descripcion',
    'generaCNR',
    'generaOrdenNormalizacion',
    'causal',
    'actions'
  ];

  displayedColumns3 = [
    'id',
    'codigoAnormalidad',
    'codigoTarea',
    'descripcionTarea'
  ];

  ordenInspeccionNotificadaService: OrdenInspeccionNotificadaService | null;
  dsOrdenInspeccionNotificada: DSOrdenInspeccionNotificada | null;
  ordenInspeccionNotificada: OrdenInspeccionNotificada | null;

  irregularidadService: IrregularidadService | null;
  dsIrregularidad: DSIrregularidad | null;
  irregularidad: Irregularidad | null;

  tareaService: TareaService | null;
  tareas: MatTableDataSource<Tarea> = new MatTableDataSource();
  tarea: Tarea | null;

  constructor(
    public httpClient: HttpClient
  ) {
    super();
  }

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;
  @ViewChild(MatMenuTrigger)
  contextMenu: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };

  ngOnInit() {
    this.loadData();
  }

  public loadData() {
    this.ordenInspeccionNotificadaService = new OrdenInspeccionNotificadaService(this.httpClient);
    this.dsOrdenInspeccionNotificada = new DSOrdenInspeccionNotificada(this.ordenInspeccionNotificadaService, this.sort);
    this.irregularidadService = new IrregularidadService(this.httpClient);
    this.dsIrregularidad = new DSIrregularidad(this.irregularidadService, this.sort);
    this.tareaService = new TareaService(this.httpClient);
  }

}

export class DSOrdenInspeccionNotificada extends DataSource<OrdenInspeccionNotificada> {
  filterChange = new BehaviorSubject('');
  get filter(): string {
    return this.filterChange.value;
  }
  set filter(filter: string) {
    this.filterChange.next(filter);
  }
  filteredData: OrdenInspeccionNotificada[] = [];
  renderedData: OrdenInspeccionNotificada[] = [];

  constructor(
    public ordenInspeccionNotificadaService: OrdenInspeccionNotificadaService,
    public _sort: MatSort
  ) {
    super();
    this.filterChange.subscribe();
  }

  connect(): Observable<OrdenInspeccionNotificada[]> {
    const displayDataChanges = [
      this.ordenInspeccionNotificadaService.dataChange,
      this._sort.sortChange,
      this.filterChange
    ];
    this.ordenInspeccionNotificadaService.getAllOrdenInspeccionNotificada();
    return merge(...displayDataChanges).pipe(
      map(() => {
        this.filteredData = this.ordenInspeccionNotificadaService.data
          .slice()
          .filter((ordenInspeccionNotificada: OrdenInspeccionNotificada) => {
            const searchStr = (
              ordenInspeccionNotificada.id +
              ordenInspeccionNotificada.nro +
              ordenInspeccionNotificada.nroOrden +
              ordenInspeccionNotificada.fechaCreacion +
              ordenInspeccionNotificada.contratista +
              ordenInspeccionNotificada.tipoInspeccion
            ).toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        const sortedData = this.sortData(this.filteredData.slice());
        this.renderedData = sortedData;
        return this.renderedData;
      })
    );
  }

  disconnect() { }
  sortData(data: OrdenInspeccionNotificada[]): OrdenInspeccionNotificada[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'nro':
          [propertyA, propertyB] = [a.nro, b.nro];
          break;
        case 'nroOrden':
          [propertyA, propertyB] = [a.nroOrden, b.nroOrden];
          break;
        case 'fechaCreacion':
          [propertyA, propertyB] = [a.fechaCreacion, b.fechaCreacion];
          break;
        case 'contratista':
          [propertyA, propertyB] = [a.contratista, b.contratista];
          break;
        case 'tipoInspeccion':
          [propertyA, propertyB] = [a.tipoInspeccion, b.tipoInspeccion];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}

export class DSIrregularidad extends DataSource<Irregularidad> {
  filterChange = new BehaviorSubject('');
  get filter(): string {
    return this.filterChange.value;
  }
  set filter(filter: string) {
    this.filterChange.next(filter);
  }
  filteredData: Irregularidad[] = [];
  renderedData: Irregularidad[] = [];

  constructor(
    public irregularidadService: IrregularidadService,
    public _sort: MatSort
  ) {
    super();
    this.filterChange.subscribe();
  }

  connect(): Observable<Irregularidad[]> {
    const displayDataChanges = [
      this.irregularidadService.dataChange,
      this._sort.sortChange,
      this.filterChange
    ];
    this.irregularidadService.getAllIrregularidad();
    return merge(...displayDataChanges).pipe(
      map(() => {
        this.filteredData = this.irregularidadService.data
          .slice()
          .filter((irregularidad: Irregularidad) => {
            const searchStr = (
              irregularidad.id +
              irregularidad.nro +
              irregularidad.codigo +
              irregularidad.descripcion +
              irregularidad.generaCNR +
              irregularidad.generaOrdenNormalizacion +
              irregularidad.causal
            ).toLowerCase();
            return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
          });
        const sortedData = this.sortData(this.filteredData.slice());
        this.renderedData = sortedData;
        return this.renderedData;
      })
    );
  }

  disconnect() { }
  sortData(data: Irregularidad[]): Irregularidad[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      switch (this._sort.active) {
        case 'id':
          [propertyA, propertyB] = [a.id, b.id];
          break;
        case 'nro':
          [propertyA, propertyB] = [a.nro, b.nro];
          break;
        case 'codigo':
          [propertyA, propertyB] = [a.codigo, b.codigo];
          break;
        case 'descripcion':
          [propertyA, propertyB] = [a.descripcion, b.descripcion];
          break;
        case 'generaCNR':
          [propertyA, propertyB] = [a.generaCNR, b.generaCNR];
          break;
        case 'generaOrdenNormalizacion':
          [propertyA, propertyB] = [a.generaOrdenNormalizacion, b.generaOrdenNormalizacion];
          break;
        case 'causal':
          [propertyA, propertyB] = [a.causal, b.causal];
          break;
      }
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      return (
        (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1)
      );
    });
  }
}
