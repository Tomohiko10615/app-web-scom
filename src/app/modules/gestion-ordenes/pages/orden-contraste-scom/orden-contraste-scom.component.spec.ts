import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenContrasteScomComponent } from './orden-contraste-scom.component';

describe('OrdenContrasteScomComponent', () => {
  let component: OrdenContrasteScomComponent;
  let fixture: ComponentFixture<OrdenContrasteScomComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenContrasteScomComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenContrasteScomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
