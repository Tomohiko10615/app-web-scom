import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { tap } from 'rxjs';
import { UsuarioService } from 'src/app/core/service/usuario.service';
import { Irregularidad } from '../../../models/irregularidad.model';
import { OrdenInspeccionNotificada } from '../../../models/orden-inspeccion-notificada.model';
import { Tarea } from '../../../models/tarea.model';
import { OrdenContrasteService } from '../../../services/orden-contraste.service';
import { OrdenInspeccionNotificadaService } from '../../../services/orden-inspeccion-notificada.service';

@Component({
  selector: 'app-orden-contraste-scom-ver',
  templateUrl: './orden-contraste-scom-ver.component.html',
  styleUrls: ['./orden-contraste-scom-ver.component.sass']
})
export class OrdenContrasteScomVerComponent implements OnInit {

  breadscrums = [
    {
      title: 'Creación de Orden de Contraste',
      items: ['Home'],
      active: 'Creación de Orden de Contraste'
    }
  ];

  filterToggle = false;

  displayedColumns = [
    'select',
    'nro',
    'nroOrden',
    'fechaCreacion',
    'contratista',
    'tipoInspeccion'
  ];

  displayedColumns2 = [
    'id',
    'codigo',
    'descripcion',
    'generaCNR',
    'generaOrdenNormalizacion',
    'causal'
  ];

  displayedColumns3 = [
    'select',
    'codAnormalidad',
    'codTarea',
    'tarea'
  ];


  displayedColumnsPruebaAlta = [
    'pruebaAlta01',
    'pruebaAlta02',
    'pruebaAlta03'
  ]

  displayedColumnsPruebaBaja = [
    'pruebaBaja01',
    'pruebaBaja02',
    'pruebaBaja03'
  ]

  displayedColumnsPruebaAislamiento = [
    'pruebaAislamientoR',
    'pruebaAislamientoS',
    'pruebaAislamientoT'
  ]

  displayedColumnsPruebaNominal = [
    'pruebaNominal01',
    'pruebaNominal02',
    'pruebaNominal03'
  ]

  ordenInspeccionNotificadas: MatTableDataSource<OrdenInspeccionNotificada> = new MatTableDataSource();
  irregularidades: MatTableDataSource<Irregularidad> = new MatTableDataSource();
  tareas: MatTableDataSource<Tarea> = new MatTableDataSource();
  
  form: FormGroup;

  nroServicio: string;
  nroCuenta: string;
  cliente: string;
  direccion: string;
  distrito: string;
  rutaLectura: string;

  marca: string;
  modelo: string;
  nroMedidor: string;
  estado: string;

  estadoWkf: string;

  // R.I. REQSCOM08 11/07/2023 INICIO
  fechaCreacionOrden: string
  fechaEjecucion: string
  observacionTecnico: string
  // R.I. REQSCOM08 11/07/2023 FIN

  anormalidades: any[];
  subtipos: any[];
  motivos: any[];

  dataIrregularidad = new Array();
  dataTarea = new Array();

  idOrden: number = 0;
  idOrdenContraste: number = 0;
  idOrdenInspeccion: number = 0;

  idUrl: number;
  orden: any;
  botonBuscar: boolean = true;
  botonSubmit: string = "Guardar";

  xmlEnvio: any;
  xmlRecepcion: any;
  xml: any 
  ordenTransfer: any = [];

  nroOrden: string = "";

  medContrastePruebasAlta: any;
  medContrastePruebasBaja: any;
  medContrastePruebasNominal: any;
  medContrastePruebasAislamiento: any;

  inicioCreacion: string = "SC4J";

  constructor(private fb: FormBuilder, 
              private ordenContrasteService: OrdenContrasteService,
              private ordenInspeccionService: OrdenInspeccionNotificadaService,
              private usuarioService : UsuarioService,
              private route : ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.idUrl = this.route.snapshot.params['id'];
    
    this.initForm();
    this.getAnormalidades();
    this.getMotivo();

    if(this.idUrl) {
      // R.I. Correctivo 11/07/2023 INICIO
      //this.getData();
      this.getData(this.idUrl);
      // R.I. Correctivo 11/07/2023 FIN
      this.breadscrums = [
        {
          title: 'Ver de Orden de Contraste',
          items: ['Home'],
          active: 'Ver de Orden de Contraste'
        }
      ];
      this.botonBuscar = false;
      this.botonSubmit = "Actualizar";
    }
  }

  // R.I. Correctivo 11/07/2023 INICIO
  getData(id: number) {
    //this.ordenContrasteService.getOrden(this.idUrl, 0).pipe(
    this.ordenContrasteService.obtenerDetalleOrdenContraste(id).pipe(
  // R.I. Correctivo 11/07/2023 FIN
      tap(response => {
        this.orden = response;
        this.initForm();
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.direccion
        this.distrito = response.distrito
        this.rutaLectura = response.rutaLectura
        this.idOrdenInspeccion = response.idOrdenInspeccion
        this.idOrden = response.idOrden
        this.idOrdenContraste = response.idOrdenContraste
        this.form.value.nroServicio = response.nroServicio
        this.marca = response.codMarca
        this.modelo = response.codModelo
        this.nroMedidor = response.nroComponente
        this.estado = response.estadoMedidor
        this.nroOrden = response.nroOrden
        this.estadoWkf = response.estadoWkf
        // R.I. REQSCOM08 11/07/2023 INICIO
        this.fechaCreacionOrden = response.fechaCreacionOrden
        this.fechaEjecucion = response.fechaEjecucion
        this.observacionTecnico = response.observacionTecnico
        // R.I. REQSCOM08 11/07/2023 FIN
        this.buscarCliente();
        this.check(response.idOrdenInspeccion);
        if(response.idOrdenSc4j == 0) {
          this.inicioCreacion = "SCOM";
        }
        //Mostrar en tabla datos de Pruebas de Contraste
        this.medContrastePruebasAlta = response?.medContrasteDTO?.pruebaAlta;
        this.medContrastePruebasBaja = response?.medContrasteDTO?.pruebaBaja;
        this.medContrastePruebasAislamiento = response?.medContrasteDTO?.pruebaAislamiento;
        this.medContrastePruebasNominal = response?.medContrasteDTO?.pruebaNominal;
      })
    ).subscribe();
  }

  initForm() {
    this.form = this.fb.group({
      nroServicio: [{ value: !this.orden ? '' : this.orden.nroServicio, disabled: true}, Validators.required],
      idMotivo: [{ value: !this.orden ? '' : this.orden.idMotivo, disabled: true}, Validators.required],
      observaciones: [{ value: !this.orden ? '' : this.orden.observacion, disabled: true}, Validators.required],
      idAnormalidad: []
    })
  }

  buscarCliente() {
    this.irregularidades = new MatTableDataSource<Irregularidad>();
    this.tareas = new MatTableDataSource<Tarea>();
    const nroServicio = this.form.value.nroServicio
    this.ordenContrasteService.getNroServicio(nroServicio).pipe(
      tap(response => {
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.direccion
        this.distrito = response.distrito
        this.rutaLectura = response.rutaLectura
        this.marca = response.codMarca
        this.modelo = response.codModelo
        this.nroMedidor = response.nroComponente
        this.estado = response.estadoMedidor
        this.ordenInspeccionNotificadas = new MatTableDataSource<OrdenInspeccionNotificada>(response.inspecciones);
      })
    ).subscribe();
  }

  getAnormalidades() {
    this.ordenContrasteService.getAnormalidades().pipe(
      tap(response => {
        this.anormalidades = response
      })
    ).subscribe();
  }

  getSubTipos() {
    this.ordenContrasteService.getSubTipos().pipe(
      tap(response => {
        this.subtipos = response
      })
    ).subscribe();
  }

  check(id: number) {
    this.idOrdenInspeccion = id;
    this.irregularidades = new MatTableDataSource<Irregularidad>();
    this.tareas = new MatTableDataSource<Tarea>();
    this.dataTarea = [];
    this.dataIrregularidad = [];
    this.ordenContrasteService.getOrden(id, 1).pipe(
      tap(response => {
        response.anormalidades.forEach((element: any) => {
          const dataAnormalidad = 
                        { 
                          'id': element.id,
                          'codigo' : element.codigo,
                          'descripcion': element.descripcion,
                          'generaCNR': element.generaCNR,
                          'generaOrdenNormalizacion': element.generaOrdenNormalizacion,
                          'causal': element.causal
                        };
          this.dataIrregularidad.push(dataAnormalidad);
        });
        this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
        response.tareas.forEach((element: any) => {
          const dataTareas = 
                        { 
                          'id': element.id,
                          'idTarea' : element.idTarea,
                          'idAnormalidad': element.idAnormalidad,
                          'tarea': element.tarea,
                          'codAnormalidad': element.codAnormalidad,
                          'codTarea': element.codTarea,
                          'tareaEstado': element.tareaEstado
                        };
          this.dataTarea.push(dataTareas);
        });
        this.tareas = new MatTableDataSource<Tarea>(this.dataTarea);
      })
    ).subscribe();
  }

  noRealizado(id: number) {
    const tarea = {
      id: id,
      tareaEstado: 'N'
    }
    this.ordenContrasteService.postCambiarEstado(tarea).subscribe();
  }

  realizado(id: number) {
    const tarea = {
      id: id,
      tareaEstado: 'S'
    }
    this.ordenContrasteService.postCambiarEstado(tarea).subscribe();
  }

  getTareas() {
    const medida = this.form.value.idAnormalidad.split('-');
    
    const dataAnormalidad = 
                        { 
                          'id': medida[0],
                          'codigo' : medida[1],
                          'descripcion': medida[2],
                          'generaCNR': medida[3],
                          'generaOrdenNormalizacion': medida[4],
                          'causal': medida[5]
                        };
    this.dataIrregularidad.push(dataAnormalidad);
    this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
    this.ordenContrasteService.getTareas(medida[0]).pipe(
      tap(response => {
        response.forEach((element: any) => {
          const dataTareas = 
                        { 
                          'id': element.id,
                          'idTarea' : element.idTarea,
                          'idAnormalidad': element.idAnormalidad,
                          'tarea': element.tarea,
                          'codAnormalidad': element.codAnormalidad,
                          'codTarea': element.codTarea,
                          'tareaEstado': 'N'
                        };
          this.dataTarea.push(dataTareas);
        });
        this.tareas = new MatTableDataSource<Tarea>(this.dataTarea);
      })
    ).subscribe();
  }

  getMotivo() {
    this.ordenContrasteService.getMotivo().pipe(
      tap(response => {
        this.motivos = response
      })
    ).subscribe();
  }

  remove(i: number) {
    this.dataIrregularidad.splice(i, 1);
    this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
  }

}
