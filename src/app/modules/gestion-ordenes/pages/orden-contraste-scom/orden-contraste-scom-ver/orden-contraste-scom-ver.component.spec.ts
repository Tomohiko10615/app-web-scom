import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenContrasteScomVerComponent } from './orden-contraste-scom-ver.component';

describe('OrdenContrasteScomVerComponent', () => {
  let component: OrdenContrasteScomVerComponent;
  let fixture: ComponentFixture<OrdenContrasteScomVerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenContrasteScomVerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenContrasteScomVerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
