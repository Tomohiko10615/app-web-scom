import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteAsignacionPcrComponent } from './reporte-asignacion-pcr.component';

describe('ReporteAsignacionPcrComponent', () => {
  let component: ReporteAsignacionPcrComponent;
  let fixture: ComponentFixture<ReporteAsignacionPcrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReporteAsignacionPcrComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReporteAsignacionPcrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
