import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { errorMessages } from 'src/app/core/error/error.constant';
import { ChartBarReport } from 'src/app/core/interfaces/core.interface';
import { ExcelExportService } from 'src/app/core/service/excel-export.service';
import { REPORTE_ASIGNACION_PCR_SITUACION_PCR, SOLO_NUMEROS } from '../../gestion-ordenes.contants';
import { ReporteAsignacionPcr } from '../../models/reporte-asignacion-pcr.model';
import { tap } from 'rxjs';
import { ReporteAsignacionPcrService } from '../../services/reporte-signacion-pcr.service';

@Component({
  selector: 'app-reporte-asignacion-pcr',
  templateUrl: './reporte-asignacion-pcr.component.html',
  styleUrls: ['./reporte-asignacion-pcr.component.scss']
})
export class ReporteAsignacionPcrComponent implements OnInit {

  filterToggle = false;
  displayedColumns = [
    'item',
    'nroCliente',
    'nombreCliente',
    'fechaActivacion',
    'situacionPCR'
  ];

  breadscrums = [
    {
      title: 'Reporte de Asignación de PCR a Clientes',
      items: ['Home'],
      active: 'Reporte de Asignación de PCR a Clientes'
    }
  ];

  form: FormGroup;
  situaciones: any[];
  tipoAsignaciones: any[];
  dataSource: MatTableDataSource<ReporteAsignacionPcr> = new MatTableDataSource();

  totalElements: number;
  pageSize: number;

  fechaInicioActual = new Date();
  fechaFinActual = new Date();
  errorMessage = errorMessages;

  // Configuration export data

  exportReport = { fileName:`reporte-medidores-${this.datePipe.transform(new Date, 'dd.MM.yyyy')}`, sheet: 'Hoja1' }

  //Chart Bar Vertical
  colorScheme = {
    domain: ['#9370DB', '#87CEFA', '#FA8072', '#FF7F50', '#90EE90', '#9370DB']
  };

  public chartBarValues: ChartBarReport[] = [];

  gradient = false;
  showXAxis = true;
  showYAxis = true;
  showLegend = false;
  showXAxisLabel = true;
  showYAxisLabel = true;
  legendPosition = 'right';
  vbarxAxisLabel = 'Situación';
  vbaryAxisLabel = 'Cantidad';
  view: any;
  exportDataExcel: any[];

  //paginator values
  countItem: number;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  page = 0;
  size = 5;
  order = "id";
  asc = false;

  dataFilter: any = { fechaInicio: '', fechaFin:'', situacionPcr:'', nroCliente:''}

  constructor(public formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private excelExportService: ExcelExportService,
    private snackBar: MatSnackBar,
    private reporteAsignacionPcrService: ReporteAsignacionPcrService ) { }

  ngOnInit(): void {

    this.initForm();
    this.situaciones = REPORTE_ASIGNACION_PCR_SITUACION_PCR;
  }

  initForm(): void {
    this.form = this.formBuilder.group({
      fechaInicio: [''],
      fechaFin: [''],
      situacion: [''],
      nroCliente: ['', Validators.pattern(SOLO_NUMEROS)],
    });
  }

  onResize(event) {
    this.view = [event.target.innerWidth / 2, 400];
  }


  submitFilterReporteAsignacionPCR(): void {
     
    this.page = 0;
    this.size = 10;

    
    if(((this.form.value.fechaInicio == '' || this.form.value.fechaInicio == null) && (this.form.value.fechaFin != '' && this.form.value.fechaFin != null))) {
      this.snackBar.open('La fecha inicio no debe estar vacio.','Close');
      return
    }

    if(((this.form.value.fechaFin == '' || this.form.value.fechaFin == null) && (this.form.value.fechaInicio != '' && this.form.value.fechaInicio != null))) {
      this.snackBar.open('La fecha fin no debe estar vacio.','Close');
      return
    }
    
    this.getFiltroReporteAsignacionPcr();
    this.getFiltroReporteAsignacionPcrGrafico();
  }

  getFiltroReporteAsignacionPcr(): void {
    const formValue = this.form.value;
    this.dataFilter.fechaInicio = (this.dataFilter.fechaInicio == null || this.dataFilter.fechaInicio == '') ? '' : this.transformDate(this.dataFilter?.fechaInicio);
    this.dataFilter.fechaFin = (this.dataFilter.fechaFin == null || this.dataFilter.fechaFin == '') ? '' : this.transformDate(this.dataFilter.fechaFin);
    this.dataFilter.situacion = formValue.situacion;
    this.dataFilter.nroCliente = formValue.nroCliente;
    this.reporteAsignacionPcrService.filtroReporteAsignacionPcr(this.page, this.size, this.order, this.asc, this.dataFilter).pipe(
      tap((response: any) => { 
        this.dataSource = new MatTableDataSource<ReporteAsignacionPcr>(response.content);
        this.pageSize = response.size;
        this.totalElements = response.totalElements;
        this.paginator.pageIndex = this.page;
        this.countItem= response.pageable.offset + 1;
      })
    ).subscribe();
  }

  getFiltroReporteAsignacionPcrGrafico(): void {
    
    const formValue = this.form.value;
    this.dataFilter.fechaInicio = (this.dataFilter.fechaInicio == null || this.dataFilter.fechaInicio == '') ? '' : this.transformDate(this.dataFilter?.fechaInicio);
    this.dataFilter.fechaFin = (this.dataFilter.fechaFin == null || this.dataFilter.fechaFin == '') ? '' : this.transformDate(this.dataFilter.fechaFin);
    this.dataFilter.situacion = formValue.situacion;
    this.dataFilter.nroCliente = formValue.nroCliente;
    this.reporteAsignacionPcrService.filtroReporteAsignacionPcrGrafico(this.dataFilter).pipe(
      tap((reponse: any)=> this.chartBarValues = reponse)
    ).subscribe();
  }

  transformDate(date: string): string{
    const dateTransform = this.datePipe.transform(date, 'yyyy-MM-dd');
    return dateTransform;
  }

  pageChanged(event: PageEvent): void {
    this.size = event.pageSize;
    this.page = event.pageIndex;
    this.getFiltroReporteAsignacionPcr();
  }

  exportFile() {
    this.reporteAsignacionPcrService.filtroReporteAsignacionPcr(0, this.totalElements, this.order, this.asc, this.dataFilter).pipe(
      tap((response: any)=> {
        this.exportDataExcel = response.content;
      }),
      tap((response) => !!response),
      tap(() => this.excelExportService.exportAsExcelFile(this.exportDataExcel, this.exportReport.fileName))
    ).subscribe();
  }

}
