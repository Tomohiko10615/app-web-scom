import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteNovedadesComponent } from './reporte-novedades.component';

describe('ReporteNovedadesComponent', () => {
  let component: ReporteNovedadesComponent;
  let fixture: ComponentFixture<ReporteNovedadesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReporteNovedadesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReporteNovedadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
