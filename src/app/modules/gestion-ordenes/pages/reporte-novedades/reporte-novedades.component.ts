import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { errorMessages } from 'src/app/core/error/error.constant';
import { ChartBarReport } from 'src/app/core/interfaces/core.interface';
import { REPORTE_NOVEDADES_ESTADO_PROCESO, REPORTE_NOVEDADES_TIPO_ORDEN, SOLO_NUMEROS } from '../../gestion-ordenes.contants';
import { ReporteNovedades } from '../../models/reporte-novedades.model';
import { ReporteNovedadesService } from '../../services/reporte-novedades.service';
import { tap } from 'rxjs';
import { ExcelExportService } from 'src/app/core/service/excel-export.service';

@Component({
  selector: 'app-reporte-novedades',
  templateUrl: './reporte-novedades.component.html',
  styleUrls: ['./reporte-novedades.component.scss']
})
export class ReporteNovedadesComponent implements OnInit {

  filterToggle = false;
  displayedColumns = [
    'item',
    'nroCuenta',
    'nombreCliente',
    'fechaActivacion',
    'estadoProceso'
  ];

  breadscrums = [
    {
      title: 'Reporte de Novedades Clientes',
      items: ['Home'],
      active: 'Reporte de Novedades Clientes'
    }
  ];

  form: FormGroup;
  estados: any[];
  tipoOrdenes: any[];
  dataSource: MatTableDataSource<ReporteNovedades> = new MatTableDataSource();

  totalElements: number;
  pageSize: number;

  fechaInicioActual = new Date();
  fechaFinActual = new Date();
  errorMessage = errorMessages;
  countItem: number;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  // Configuration export data

  exportReport = { fileName:`reporte-novedades-${this.datePipe.transform(new Date, 'dd.MM.yyyy')}`, sheet: 'Hoja1' }

  //Chart Bar Vertical
  colorScheme = {
    domain: ['#9370DB', '#87CEFA', '#FA8072', '#FF7F50', '#90EE90', '#9370DB']
  };

  public chartBarValues: ChartBarReport[] = [];

  gradient = false;
  showXAxis = true;
  showYAxis = true;
  showLegend = false;
  showXAxisLabel = true;
  showYAxisLabel = true;
  legendPosition = 'right';
  vbarxAxisLabel = 'Situación';
  vbaryAxisLabel = 'Cantidad';
  view: any;


  //paginator values
  page = 0;
  size = 5;
  order = "id";
  asc = false;

  dataFilter: any = { fechaInicio: '', fechaFin:'', estadoProceso:'', nroCliente:'', tipoOrden:''}

  exportDataExcel: any[];

  constructor(public formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private snackBar: MatSnackBar,
    private reporteNovedaesService: ReporteNovedadesService,
    private excelExportService: ExcelExportService) { }

  ngOnInit(): void {
    this.initForm();
    this.estados = REPORTE_NOVEDADES_ESTADO_PROCESO;
    this.tipoOrdenes = REPORTE_NOVEDADES_TIPO_ORDEN;
  }

  initForm(): void {
    this.form = this.formBuilder.group({
      fechaInicio: [''],
      fechaFin: [''],
      estadoProceso: [''],
      nroCliente: ['', Validators.pattern(SOLO_NUMEROS)],
      tipoOrden: [''],
    });
  }

  onResize(event) {
    this.view = [event.target.innerWidth / 2, 400];
  }


  submitFilterReporteNovedades(): void {
    
    this.page = 0;
    this.size = 10;

    
    if(((this.form.value.fechaInicio == '' || this.form.value.fechaInicio == null) && (this.form.value.fechaFin != '' && this.form.value.fechaFin != null))) {
      this.snackBar.open('La fecha inicio no debe estar vacio.','Close');
      return
    }

    if(((this.form.value.fechaFin == '' || this.form.value.fechaFin == null) && (this.form.value.fechaInicio != '' && this.form.value.fechaInicio != null))) {
      this.snackBar.open('La fecha fin no debe estar vacio.','Close');
      return
    }
    
    this.getFiltroReporteNovedades();
    this.getFiltroReporteNovedadesGrafico();
  }

  getFiltroReporteNovedades(): void {
    const formValue = this.form.value;
    this.dataFilter.fechaInicio = (this.dataFilter.fechaInicio == null || this.dataFilter.fechaInicio == '') ? '' : this.transformDate(this.dataFilter?.fechaInicio);
    this.dataFilter.fechaFin = (this.dataFilter.fechaFin == null || this.dataFilter.fechaFin == '') ? '' : this.transformDate(this.dataFilter.fechaFin);
    this.dataFilter.estadoProceso = formValue.estadoProceso;
    this.dataFilter.nroCliente = formValue.nroCliente;
    this.reporteNovedaesService.filtroReporteNovedades(this.page, this.size, this.order, this.asc, this.dataFilter).pipe(
      tap((response: any) => { 
        this.dataSource = new MatTableDataSource<ReporteNovedades>(response.content);
        this.pageSize = response.size;
        this.totalElements = response.totalElements;
        this.paginator.pageIndex = this.page;
        this.countItem= response.pageable.offset + 1;
      })
    ).subscribe();
  }

  getFiltroReporteNovedadesGrafico(): void {
    
    const formValue = this.form.value;
    this.dataFilter.fechaInicio = (this.dataFilter.fechaInicio == null || this.dataFilter.fechaInicio == '') ? '' : this.transformDate(this.dataFilter?.fechaInicio);
    this.dataFilter.fechaFin = (this.dataFilter.fechaFin == null || this.dataFilter.fechaFin == '') ? '' : this.transformDate(this.dataFilter.fechaFin);
    this.dataFilter.situacion = formValue.situacion;
    this.dataFilter.nroCuenta = formValue.nroCuenta;
    this.reporteNovedaesService.filtroReporteNovedadesGrafico(this.dataFilter).pipe(
      tap((reponse: any)=> this.chartBarValues = reponse)
    ).subscribe();
  }

  transformDate(date: string): string{
    const dateTransform = this.datePipe.transform(date, 'yyyy-MM-dd');
    return dateTransform;
  }

  pageChanged(event: PageEvent): void {
    this.size = event.pageSize;
    this.page = event.pageIndex;
    this.getFiltroReporteNovedades();
  }

  exportFile() {
    this.reporteNovedaesService.filtroReporteNovedades(0, this.totalElements, this.order, this.asc, this.dataFilter).pipe(
      tap((response: any)=> {
        this.exportDataExcel = response.content;
      }),
      tap((response) => !!response),
      tap(() => this.excelExportService.exportAsExcelFile(this.exportDataExcel, this.exportReport.fileName))
    ).subscribe();
  }
 
}
