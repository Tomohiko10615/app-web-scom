import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AutoCompleteModel } from '../../models/auto-complete.model';
import { SetCadenaElectricaService } from '../../services/set-cadena-electrica.service';
import { BehaviorSubject, Observable, Subject, tap } from 'rxjs';
import { CharacterSelectionRequiredValidator } from 'src/app/core/pipe/validaciones.pipe';
import { errorMessages } from 'src/app/core/error/error.constant';
import { AlimentadorCadenaElectricaService } from '../../services/alimentador-cadena-electrica.service';
import { SedCadenaElectricaService } from '../../services/sed-cadena-electrica.service';
import { LlaveCadenaElectricaService } from '../../services/llave-cadena-electrica.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ActivarSuministroService } from '../../services/activar-suministro.service';
import { ActivarSuministroDetalle } from '../../models/activar-suministro-detalle.model';
import { RutaUbiNodeService } from '../../services/ruta-ubi-node.service';
import { ActivarSuministro } from '../../models/activar-suministro.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SOLO_NUMEROS } from '../../gestion-ordenes.contants';


@Component({
  selector: 'app-activar-suministro',
  templateUrl: './activar-suministro.component.html',
  styleUrls: ['./activar-suministro.component.scss']
})
export class ActivarSuministroComponent implements OnInit {

  
  breadscrums = [
    {
      title: 'Modificación de Cadena Eléctrica y Rutas.',
      items: ['Home'],
      active: 'Modificación de Cadena Eléctrica y Rutas.'
    }
  ];

  formCadenaElectrica: FormGroup;
  formRutas: FormGroup;
  idAutoAct: number | undefined;

  setsCadenaElectrica: AutoCompleteModel[]=[];
  setCadenaElectrica = new AutoCompleteModel;
  buscarSetCadenaElectricaSubject = new Subject<string>();

  alimentadoresCadenaElectrica: AutoCompleteModel[]=[];
  alimentadorCadenaElectrica= new AutoCompleteModel;
  buscarAlimentadorCadenaElectricaSubject = new Subject<string>();
  sendIdSetCadenaElectricaSubject = new Subject<number>();

  sedsCadenaElectrica: AutoCompleteModel[]=[];
  sedCadenaElectrica = new AutoCompleteModel;
  buscarSedCadenaElectricaSubject = new Subject<string>();

  llavesCadenaElectrica: AutoCompleteModel[]=[];
  llaveCadenaElectrica = new AutoCompleteModel;
  buscarLlaveCadenaElectricaSubject = new Subject<string>();


  private filteredOptionsSubject = new BehaviorSubject<AutoCompleteModel[]>([]);
  minLengthTerm = 3;

  errorMessage = errorMessages;

  activarSuministroDetalle = new ActivarSuministroDetalle;


  //---------Rutas Lectura-------------//
  buscarLecturaSectorSubject = new Subject<string>();
  sectoresLecturaRuta: AutoCompleteModel[]=[];
  sectorLecturaRuta = new AutoCompleteModel;


  buscarZonaLecturaSubject = new Subject<string>();
  zonasLecturaRuta: AutoCompleteModel[]=[];
  zonaLecturaRuta = new AutoCompleteModel;


  //---------Rutas Corte-------------//
  buscarCorteSectorSubject = new Subject<string>();
  sectoresCorteRuta: AutoCompleteModel[]=[];
  sectorCorteRuta = new AutoCompleteModel;


  buscarZonaCorteSubject = new Subject<string>();
  zonasCorteRuta: AutoCompleteModel[]=[];
  zonaCorteRuta = new AutoCompleteModel;

   //---------Rutas Facturacion-------------//
   buscarFacturacionSectorSubject = new Subject<string>();
   sectoresFacturacionRuta: AutoCompleteModel[]=[];
   sectorFacturacionRuta = new AutoCompleteModel;
 
 
   buscarZonaFacturacionSubject = new Subject<string>();
   zonasFacturacionRuta: AutoCompleteModel[]=[];
   zonaFacturacionRuta = new AutoCompleteModel;


    //---------Rutas Reparto-------------//
  buscarRepartoSectorSubject = new Subject<string>();
  sectoresRepartoRuta: AutoCompleteModel[]=[];
  sectorRepartoRuta = new AutoCompleteModel;


  buscarZonaRepartoSubject = new Subject<string>();
  zonasRepartoRuta: AutoCompleteModel[]=[];
  zonaRepartoRuta = new AutoCompleteModel;

  disabledButton = false;

  constructor(private formBuilder: FormBuilder,
    private setCadenaElectricaService: SetCadenaElectricaService,
    private alimentadorCadenaElectricaService: AlimentadorCadenaElectricaService,
    private sedCadenaElectricaService: SedCadenaElectricaService,
    private llaveCadenaElectricaService: LlaveCadenaElectricaService,
    private route : ActivatedRoute,
    private activarSuministroService: ActivarSuministroService,
    private rutaUbiNodoService: RutaUbiNodeService,
    private snackBar: MatSnackBar,
    private router: Router) { }

  ngOnInit(): void {

    this.idAutoAct = this.route.snapshot?.params['idAutoAct'];

    if(this.idAutoAct != null) {
      this.activarSuministroService.obtenerDetalleActivarSuministro(this.idAutoAct).pipe(
        tap((response: ActivarSuministroDetalle) => {
          this.activarSuministroDetalle = response;
          this.initForm();
          this.obtenerAlimentadorCadenaElectrica(this.activarSuministroDetalle?.idSet,this.buscarAlimentadorCadenaElectricaSubject);
          this.obtenerSedCadenaElectrica(this.activarSuministroDetalle?.idAlimentador,this.buscarSedCadenaElectricaSubject);
          this.obtenerLlaveCadenaElectrica(this.activarSuministroDetalle?.idSed,this.buscarLlaveCadenaElectricaSubject);
        })
      ).subscribe();
    }

    this.initForm();
    
    this.obtenerSetCadenaElectrica(this.buscarSetCadenaElectricaSubject);
    
    /* 
    
    this.obtenerLecturaSector(this.buscarLecturaSectorSubject);
    this.obtenerCorteSector(this.buscarCorteSectorSubject);
    this.obtenerFacturacionSector(this.buscarFacturacionSectorSubject);
    this.obtenerRepartoSector(this.buscarRepartoSectorSubject); */
  }

  initForm(): void {
    this.formCadenaElectrica = this.formBuilder.group({
      set: [ { id:this.activarSuministroDetalle.idSet, codigo: this.activarSuministroDetalle.codSet}, CharacterSelectionRequiredValidator],
      alimentador: [{ id:this.activarSuministroDetalle.idAlimentador, codigo: this.activarSuministroDetalle.codAlimentador }, CharacterSelectionRequiredValidator],
      sed: [ { id:this.activarSuministroDetalle.idSed, codigo: this.activarSuministroDetalle.codSed }, CharacterSelectionRequiredValidator],
      llave: [{ id:this.activarSuministroDetalle.idLlave, codigo: this.activarSuministroDetalle.codLlave, descripcion: this.activarSuministroDetalle.desLlave }, CharacterSelectionRequiredValidator],
    });

    this.formRutas = this.formBuilder.group({
      sector: [this.activarSuministroDetalle.sector ,[Validators.pattern(SOLO_NUMEROS), Validators.maxLength(2), Validators.minLength(2)]],
      zona: [this.activarSuministroDetalle.zona , [Validators.pattern(SOLO_NUMEROS), Validators.maxLength(3),  Validators.minLength(3)]],
      correlativo: [this.activarSuministroDetalle.correlativo, [Validators.pattern(SOLO_NUMEROS), Validators.maxLength(4),Validators.minLength(4)]],
    });
    
   /*  this.formRutas = this.formBuilder.group({
      codLecturaSector: [{ id:this.activarSuministroDetalle.idLecturaSector, codigo: this.activarSuministroDetalle.codLecturaSector} , [CharacterSelectionRequiredValidator, Validators.required]],
      codLecturaZona: [{ id:this.activarSuministroDetalle.idLecturaZona, codigo: this.activarSuministroDetalle.codLecturaZona} , [CharacterSelectionRequiredValidator, Validators.required]],
      lecturaCorrelativo: [this.activarSuministroDetalle.lecturaCorrelativo, [Validators.pattern(SOLO_NUMEROS), Validators.maxLength(4), Validators.required]],
      
      codRutaCorteSector: [{ id:this.activarSuministroDetalle.idRutaCorteSector, codigo: this.activarSuministroDetalle.codRutaCorteSector} , [ CharacterSelectionRequiredValidator, Validators.required ]],
      codRutaCorteZona: [{ id:this.activarSuministroDetalle.idRutaCorteZona, codigo: this.activarSuministroDetalle.codRutaCorteZona} , [ CharacterSelectionRequiredValidator, Validators.required ]],
      rutaCorteCorrelativo: [this.activarSuministroDetalle.rutaCorteCorrelativo, [Validators.pattern(SOLO_NUMEROS), Validators.maxLength(4), Validators.required]],
      
      codRutaFacturacionSector: [{ id:this.activarSuministroDetalle.idRutaFacturacionSector, codigo: this.activarSuministroDetalle.codRutaFacturacionSector} , [ CharacterSelectionRequiredValidator, Validators.required ]],
      codRutaFacturacionZona: [{ id:this.activarSuministroDetalle.idRutaFacturacionZona, codigo: this.activarSuministroDetalle.codRutaFacturacionZona} , [ CharacterSelectionRequiredValidator, Validators.required ]],
      rutaFacturacionCorrelativo: [this.activarSuministroDetalle.rutaFacturacionCorrelativo, [Validators.pattern(SOLO_NUMEROS), Validators.maxLength(4), Validators.required]],
      
      codRutaRepartoSector: [{ id:this.activarSuministroDetalle.idRutaRepartoSector, codigo: this.activarSuministroDetalle.codRutaRepartoSector} , [ CharacterSelectionRequiredValidator, Validators.required ]],
      codRutaRepartoZona: [{ id:this.activarSuministroDetalle.idRutaRepartoZona, codigo: this.activarSuministroDetalle.codRutaRepartoZona} ,[ CharacterSelectionRequiredValidator,  Validators.required ]],
      rutaRepartoCorrelativo: [this.activarSuministroDetalle.rutaRepartoCorrelativo, [Validators.pattern(SOLO_NUMEROS), Validators.maxLength(4), Validators.required]]
    }); */
  }

  /**
   * -----------------------INICIO AUTOCOMPLETE SET CADENA ELECTRICA --------------------------------
   */
  displaySet(set: AutoCompleteModel){
    return set ? set.codigo : set;
  }

  onSelectedSet(event: any) {
    //El $event obtiene el objeto con los valores de sus atributos (id, codigo y descripcion) seleccionado por el usuario
    let set : any = event?.option?.value;
    this.setCadenaElectrica = set;
    //Se obtiene el listado de alimentadores para el autocomplete anidado filtrado por el idSet cadena electrica
    this.obtenerAlimentadorCadenaElectrica(this.setCadenaElectrica.id, this.buscarAlimentadorCadenaElectricaSubject);
  }

  buscarSetCadenaElectrica(event: any){
    //En Subject se guarda el texto ingresado por el usuario para el filtro
    if(event) {
      this.buscarSetCadenaElectricaSubject.next(event.target.value);
    }
  }
  obtenerSetCadenaElectrica(codSet ?: Observable<string>){
    this.setCadenaElectricaService.buscarSetCadenaElectrica(codSet)
    .pipe(
      tap((response: AutoCompleteModel[]) => this.setsCadenaElectrica = response),
      tap((response: AutoCompleteModel[]) => this.filteredOptionsSubject.next(response))
    ).subscribe();
  }

   /**
   * -----------------------FIN AUTOCOMPLETE SET CADENA ELECTRICA --------------------------------
   */


    /**
   * -----------------------INICIO AUTOCOMPLETE ALIMENTADOR CADENA ELECTRICA --------------------------------
   */
     displayAlimentador(alimentador: AutoCompleteModel){
      return alimentador ? alimentador.codigo : alimentador;
    }
  
    onSelectedAlimentador(event: any) {
      //el $event obtiene el objeto con los valores de sus atributos (id, codigo y descripcion) seleccionado por el usuario
      let alimentador : any = event?.option?.value;
      this.alimentadorCadenaElectrica = alimentador;
       //Se obtiene el listado de SED para el autocomplete anidado filtrado por el idAlimentador cadena electrica
       this.obtenerSedCadenaElectrica(this.alimentadorCadenaElectrica.id, this.buscarSedCadenaElectricaSubject);
    }
  
    buscarAlimentadorCadenaElectrica(event: any){
      //En Subject se guarda el texto ingresado por el usuario para el filtro
      if(event) {
        this.buscarAlimentadorCadenaElectricaSubject.next(event.target.value);
      }
    }
    obtenerAlimentadorCadenaElectrica(idSet: number,codAlimentador ?: Observable<string>){
      //Envia como parametro la descripcion del alimentador y idSet
      this.alimentadorCadenaElectricaService.buscarAlimentadorCadenaElectrica(codAlimentador,idSet)
      .pipe(
        tap((response: AutoCompleteModel[]) => this.alimentadoresCadenaElectrica = response),
        tap((response: AutoCompleteModel[]) => this.filteredOptionsSubject.next(response))
      ).subscribe();
    }

     /**
   * -----------------------FIN AUTOCOMPLETE ALIMENTADOR CADENA ELECTRICA --------------------------------
   */


      /**
   * -----------------------INICIO AUTOCOMPLETE SED CADENA ELECTRICA --------------------------------
   */

       displaySed(sed: AutoCompleteModel){
        return sed ? sed.codigo : sed;
      }
    
      onSelectedSed(event: any) {
        //el $event obtiene el objeto con los valores de sus atributos (id, codigo y descripcion) seleccionado por el usuario
        let sed : any = event?.option?.value;
        this.sedCadenaElectrica = sed;
         //Se obtiene el listado de LLAVES para el autocomplete anidado filtrado por el idSed cadena electrica
         this.obtenerLlaveCadenaElectrica(this.sedCadenaElectrica.id, this.buscarLlaveCadenaElectricaSubject);
      }
    
      buscarSedCadenaElectrica(event: any){
        //En Subject se guarda el texto ingresado por el usuario para el filtro
        if(event) {
          this.buscarSedCadenaElectricaSubject.next(event.target.value);
        }
      }
      obtenerSedCadenaElectrica(idAlimentador: number, codSed ?: Observable<string>){
        //Envia como parametro la descripcion del sed y idAlimentador
        this.sedCadenaElectricaService.buscarSedCadenaElectrica(codSed, idAlimentador)
        .pipe(
          tap((response: AutoCompleteModel[]) => this.sedsCadenaElectrica = response),
          tap((response: AutoCompleteModel[]) => this.filteredOptionsSubject.next(response))
        ).subscribe();
      }
  
    /**
   * -----------------------FIN AUTOCOMPLETE SED CADENA ELECTRICA --------------------------------
   */



    /**
   * -----------------------INICIO AUTOCOMPLETE LLAVE CADENA ELECTRICA --------------------------------
   */

        displayLlave(llave: AutoCompleteModel){
          return llave ? llave.descripcion : llave;
        }
      
        onSelectedLlave(event: any) {
          //el $event obtiene el objeto con los valores de sus atributos (id, codigo y descripcion) seleccionado por el usuario
          let llave : any = event?.option?.value;
          this.llaveCadenaElectrica = llave;
        }
      
        buscarLlaveCadenaElectrica(event: any){
          //En Subject se guarda el texto ingresado por el usuario para el filtro
          if(event) {
            this.buscarLlaveCadenaElectricaSubject.next(event.target.value);
          }
        }
        obtenerLlaveCadenaElectrica(idSed?: number, codigo ?: Observable<string>){
          //Envia como parametro la descripcion del descripcion y idSed
          this.llaveCadenaElectricaService.buscarLlaveCadenaElectrica(codigo, idSed)
          .pipe(
            tap((response: AutoCompleteModel[]) => this.llavesCadenaElectrica = response),
            tap((response: AutoCompleteModel[]) => this.filteredOptionsSubject.next(response))
          ).subscribe();
        }

    /**
   * -----------------------FIN AUTOCOMPLETE LLAVE CADENA ELECTRICA --------------------------------
   */


    //-------------------------------------------------------------------------RUTAS -----------------------------------------------------------------------//

      /**
   * -----------------------INICIO AUTOCOMPLETE LECTURA SECTOR RUTA--------------------------------
   */
  displayLecturaSector(sector: AutoCompleteModel){
    return sector ? sector.codigo : sector;
  }

   onSelectedLecturaSector(event: any) {

    let sector : any = event?.option?.value;
    this.sectorLecturaRuta = sector;
    //1: RUTA DE LECTURA
    this.obtenerLecturaZona(this.buscarZonaLecturaSubject, 1, this.sectorLecturaRuta.id);
  } 

  buscarLecturaSector(event: any){
    if(event) {
      this.buscarLecturaSectorSubject.next(event.target.value);
    }
  }
  obtenerLecturaSector(codNodo ?: Observable<string>){
    // 1: RUTA DE LECTURA
    this.rutaUbiNodoService.buscarSectorRuta(codNodo, 1)
    .pipe(
      tap((response: AutoCompleteModel[]) => this.sectoresLecturaRuta = response),
      tap((response: AutoCompleteModel[]) => this.filteredOptionsSubject.next(response))
    ).subscribe();
  }

   /**
   * -----------------------FIN AUTOCOMPLETE LECTURA SECTOR RUTA --------------------------------
   */



         /**
   * -----------------------INICIO AUTOCOMPLETE LECTURA ZONA RUTA--------------------------------
   */
  displayLecturaZona(zona: AutoCompleteModel){
    return zona ? zona.codigo : zona;
  }

   onSelectedLecturaZona(event: any) {

    let zona : any = event?.option?.value;
    this.zonaLecturaRuta= zona;
  } 

  buscarLecturaZona(event: any){
    if(event) {
      this.buscarZonaLecturaSubject.next(event.target.value);
    }
  }
  obtenerLecturaZona(codNodo ?: Observable<string>, idTipoRuta?: number, idNodoPadre?: number){
    // 1: RUTA DE LECTURA
    this.rutaUbiNodoService.buscarZonaRuta(codNodo, idTipoRuta,idNodoPadre )
    .pipe(
      tap((response: AutoCompleteModel[]) => this.zonasLecturaRuta = response),
      tap((response: AutoCompleteModel[]) => this.filteredOptionsSubject.next(response))
    ).subscribe();
  }

   /**
   * -----------------------FIN AUTOCOMPLETE LECTURA ZONA RUTA --------------------------------
   */



         /**
   * -----------------------INICIO AUTOCOMPLETE CORTE SECTOR RUTA--------------------------------
   */
  displayCorteSector(sector: AutoCompleteModel){
    return sector ? sector.codigo : sector;
  }

   onSelectedCorteSector(event: any) {

    let sector : any = event?.option?.value;
    this.sectorCorteRuta = sector;
    // 3: RUTA DE CORTE
    // 1: RUTA DE LECTURA 
    this.obtenerCorteZona(this.buscarZonaCorteSubject, 1, this.sectorCorteRuta.id);
  } 

  buscarCorteSector(event: any){
    if(event) {
      this.buscarCorteSectorSubject.next(event.target.value);
    }
  }
  obtenerCorteSector(codNodo ?: Observable<string>){
    // 3: RUTA DE CORTE
     // 1: RUTA DE LECTURA 
    this.rutaUbiNodoService.buscarSectorRuta(codNodo, 1)
    .pipe(
      tap((response: AutoCompleteModel[]) => this.sectoresCorteRuta = response),
      tap((response: AutoCompleteModel[]) => this.filteredOptionsSubject.next(response))
    ).subscribe();
  }

   /**
   * -----------------------FIN AUTOCOMPLETE CORTE SECTOR RUTA --------------------------------
   */



         /**
   * -----------------------INICIO AUTOCOMPLETE CORTE ZONA RUTA--------------------------------
   */
  displayCorteZona(zona: AutoCompleteModel){
    return zona ? zona.codigo : zona;
  }

   onSelectedCorteZona(event: any) {

    let zona : any = event?.option?.value;
    this.zonaCorteRuta= zona;
  } 

  buscarCorteZona(event: any){
    if(event) {
      this.buscarZonaCorteSubject.next(event.target.value);
    }
  }
  obtenerCorteZona(codNodo ?: Observable<string>, idTipoRuta?: number, idNodoPadre?: number){
    this.rutaUbiNodoService.buscarZonaRuta(codNodo, idTipoRuta,idNodoPadre )
    .pipe(
      tap((response: AutoCompleteModel[]) => this.zonasCorteRuta = response),
      tap((response: AutoCompleteModel[]) => this.filteredOptionsSubject.next(response))
    ).subscribe();
  }

   /**
   * -----------------------FIN AUTOCOMPLETE CORTE ZONA RUTA --------------------------------
   */


         /**
   * -----------------------INICIO AUTOCOMPLETE FACTURACION SECTOR RUTA--------------------------------
   */
  displayFacturacionSector(sector: AutoCompleteModel){
    return sector ? sector.codigo : sector;
  }

   onSelectedFacturacionSector(event: any) {

    let sector : any = event?.option?.value;
    this.sectorFacturacionRuta = sector;
    //2: RUTA DE FACTURACION
    // 1: RUTA DE LECTURA 
    this.obtenerFacturacionZona(this.buscarZonaFacturacionSubject, 1, this.sectorFacturacionRuta.id);
  } 

  buscarFacturacionSector(event: any){
    if(event) {
      this.buscarFacturacionSectorSubject.next(event.target.value);
    }
  }
  obtenerFacturacionSector(codNodo ?: Observable<string>){
    //2: RUTA DE FACTURACION
    // 1: RUTA DE LECTURA 
    this.rutaUbiNodoService.buscarSectorRuta(codNodo, 1)
    .pipe(
      tap((response: AutoCompleteModel[]) => this.sectoresFacturacionRuta = response),
      tap((response: AutoCompleteModel[]) => this.filteredOptionsSubject.next(response))
    ).subscribe();
  }

   /**
   * -----------------------FIN AUTOCOMPLETE FACTURACION SECTOR RUTA --------------------------------
   */



         /**
   * -----------------------INICIO AUTOCOMPLETE FACTURACION ZONA RUTA--------------------------------
   */
  displayFacturacionZona(zona: AutoCompleteModel){
    return zona ? zona.codigo : zona;
  }

   onSelectedFacturacionZona(event: any) {

    let zona : any = event?.option?.value;
    this.zonaFacturacionRuta= zona;
  } 

  buscarFacturacionZona(event: any){
    if(event) {
      this.buscarZonaFacturacionSubject.next(event.target.value);
    }
  }
  obtenerFacturacionZona(codNodo ?: Observable<string>, idTipoRuta?: number, idNodoPadre?: number){
    this.rutaUbiNodoService.buscarZonaRuta(codNodo, idTipoRuta, idNodoPadre)
    .pipe(
      tap((response: AutoCompleteModel[]) => this.zonasFacturacionRuta = response),
      tap((response: AutoCompleteModel[]) => this.filteredOptionsSubject.next(response))
    ).subscribe();
  }

   /**
   * -----------------------FIN AUTOCOMPLETE FACTURACION ZONA RUTA --------------------------------
   */


         /**
   * -----------------------INICIO AUTOCOMPLETE REPARTO SECTOR RUTA--------------------------------
   */
  displayRepartoSector(sector: AutoCompleteModel){
    return sector ? sector.codigo : sector;
  }

   onSelectedRepartoSector(event: any) {

    let sector : any = event?.option?.value;
    this.sectorRepartoRuta = sector;
    //4: RUTA DE REPARTO
    //1: RUTA DE LECTURA
    this.obtenerRepartoZona(this.buscarZonaRepartoSubject, 1, this.sectorRepartoRuta.id);
  } 

  buscarRepartoSector(event: any){
    if(event) {
      this.buscarRepartoSectorSubject.next(event.target.value);
    }
  }
  obtenerRepartoSector(codNodo ?: Observable<string>){
    //4: RUTA DE REPARTO
    //2: RUTA DE FACTURACION
    this.rutaUbiNodoService.buscarSectorRuta(codNodo, 1)
    .pipe(
      tap((response: AutoCompleteModel[]) => this.sectoresRepartoRuta = response),
      tap((response: AutoCompleteModel[]) => this.filteredOptionsSubject.next(response))
    ).subscribe();
  }

   /**
   * -----------------------FIN AUTOCOMPLETE REPARTO SECTOR RUTA --------------------------------
   */



         /**
   * -----------------------INICIO AUTOCOMPLETE REPARTO ZONA RUTA--------------------------------
   */
  displayRepartoZona(zona: AutoCompleteModel){
    return zona ? zona.codigo : zona;
  }

   onSelectedRepartoZona(event: any) {

    let zona : any = event?.option?.value;
    this.zonaRepartoRuta= zona;
  } 

  buscarRepartoZona(event: any){
    if(event) {
      this.buscarZonaRepartoSubject.next(event.target.value);
    }
  }
  obtenerRepartoZona(codNodo ?: Observable<string>, idTipoRuta?: number, idNodoPadre?: number){
    this.rutaUbiNodoService.buscarZonaRuta(codNodo, idTipoRuta, idNodoPadre)
    .pipe(
      tap((response: AutoCompleteModel[]) => this.zonasRepartoRuta = response),
      tap((response: AutoCompleteModel[]) => this.filteredOptionsSubject.next(response))
    ).subscribe();
  }

   /**
   * -----------------------FIN AUTOCOMPLETE REPARTO ZONA RUTA --------------------------------
   */

  onAutocompleteKeyUp(searchText: string, options: AutoCompleteModel[]): void {
		const lowerSearchText = searchText?.toUpperCase();
		this.filteredOptionsSubject.next(
      !lowerSearchText 
      ? options 
      : options.filter(r => r.codigo.toUpperCase().includes(lowerSearchText)));
	}

  onSectorKeyUp(searchText: string): void {
    if(searchText) {
      this.formRutas.get("sector").addValidators(Validators.required);
    }
  }

  onZonaKeyUp(searchText: string): void {
    if(searchText) {
      this.formRutas.get("zona").addValidators(Validators.required);
    }
  }

  onCorrelativoKeyUp(searchText: string): void {
    if(searchText) {
      this.formRutas.get("correlativo").addValidators(Validators.required);
    }
  }


  submitActivarSuministro(): void {
    const valueFormCadenaElectrica = this.formCadenaElectrica.value;
    const valueFormRutas = this.formRutas.value;
    let activar = new ActivarSuministro;
    activar.idAutoActiva = this.idAutoAct;
    activar.idSet = valueFormCadenaElectrica.set.id;
    activar.idAlimentador= valueFormCadenaElectrica.alimentador.id;
    activar.idSed = valueFormCadenaElectrica.sed.id;
    activar.idLlave = valueFormCadenaElectrica.llave.id;
    activar.sector = valueFormRutas.sector;
    activar.zona = valueFormRutas.zona;
    activar.correlativo = valueFormRutas.correlativo;


  /*   activar.idLecturaSector = valueFormRutas.codLecturaSector.id;
    activar.codLecturaSector = valueFormRutas.codLecturaSector.codigo;
    activar.idLecturaZona = valueFormRutas.codLecturaZona.id;
    activar.codLecturaZona = valueFormRutas.codLecturaZona.codigo;
    activar.lecturaCorrelativo = valueFormRutas.lecturaCorrelativo;

    activar.idRutaCorteSector = valueFormRutas.codRutaCorteSector.id;
    activar.codRutaCorteSector = valueFormRutas.codRutaCorteSector.codigo;
    activar.idRutaCorteZona = valueFormRutas.codRutaCorteZona.id;
    activar.codRutaCorteZona = valueFormRutas.codRutaCorteZona.codigo;
    activar.rutaCorteCorrelativo = valueFormRutas.rutaCorteCorrelativo;

    activar.idRutaFacturacionSector = valueFormRutas.codRutaFacturacionSector.id;
    activar.codRutaFacturacionSector = valueFormRutas.codRutaFacturacionSector.codigo;
    activar.idRutaFacturacionZona = valueFormRutas.codRutaFacturacionZona.id;
    activar.codRutaFacturacionZona = valueFormRutas.codRutaFacturacionZona.codigo;
    activar.rutaFacturacionCorrelativo = valueFormRutas.rutaFacturacionCorrelativo;

    activar.idRutaRepartoSector = valueFormRutas.codRutaRepartoSector.id;
    activar.codRutaRepartoSector = valueFormRutas.codRutaRepartoSector.codigo;
    activar.idRutaRepartoZona = valueFormRutas.codRutaRepartoZona.id;
    activar.codRutaRepartoZona = valueFormRutas.codRutaRepartoZona.codigo;
    activar.rutaRepartoCorrelativo = valueFormRutas.rutaRepartoCorrelativo; */
    
   
   this.activarSuministroService.actualizarCadenaElectricaYrutas(activar).pipe(
      tap((response) => !!response),
      tap(() => this.disabledInputs()),
      tap(() => this.snackBar.open('Registro exitoso. ', 'CERRAR'))
    ).subscribe(); 
  }


  disabledInputs() : void {
    this.disabledButton= true;
    this.formCadenaElectrica.get('set').disable();
    this.formCadenaElectrica.get('alimentador').disable();
    this.formCadenaElectrica.get('sed').disable();
    this.formCadenaElectrica.get('llave').disable();

    this.formRutas.get('sector').disable();
    this.formRutas.get('zona').disable();
    this.formRutas.get('correlativo').disable();
  }


  cancelar(): void {
    this.router.navigate(['gestion-ordenes/orden-trabajo']);
  }
}
