import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivarSuministroComponent } from './activar-suministro.component';

describe('ActivarSuministroComponent', () => {
  let component: ActivarSuministroComponent;
  let fixture: ComponentFixture<ActivarSuministroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActivarSuministroComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ActivarSuministroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
