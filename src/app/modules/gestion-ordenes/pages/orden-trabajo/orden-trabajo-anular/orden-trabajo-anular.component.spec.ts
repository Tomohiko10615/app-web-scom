import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AnularOrdenTrabajoComponent } from './orden-trabajo-anular.component';

describe('AnularOrdenTrabajoComponent', () => {
  let component: AnularOrdenTrabajoComponent;
  let fixture: ComponentFixture<AnularOrdenTrabajoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AnularOrdenTrabajoComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnularOrdenTrabajoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
