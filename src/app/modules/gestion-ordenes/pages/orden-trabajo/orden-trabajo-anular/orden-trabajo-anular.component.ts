import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import { OrdenTrabajoService } from '../../../services/orden-trabajo.service';

@Component({
  selector: 'app-orden-trabajo-anular',
  templateUrl: './orden-trabajo-anular.component.html',
  styleUrls: ['./orden-trabajo-anular.component.sass']
})
export class AnularOrdenTrabajoComponent {
  constructor(
    public dialogRef: MatDialogRef<AnularOrdenTrabajoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public ordenTrabajoService: OrdenTrabajoService
  ) { }
  onNoClick(): void {
    this.dialogRef.close(false);
  }
  confirmAnularOrdenTrabajo(): void {
    this.dialogRef.close(true);
    //this.ordenTrabajoService.anularOrdenTrabajo();
  }
}
