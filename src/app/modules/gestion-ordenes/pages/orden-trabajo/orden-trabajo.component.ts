import { SelectionModel } from '@angular/cdk/collections';
import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { OrdenTrabajoService } from '../../services/orden-trabajo.service';
import { ParametroService } from '../../../../core/service/parametro.service';
import { AnularOrdenTrabajoComponent } from './orden-trabajo-anular/orden-trabajo-anular.component';
import { CrearOrdenTrabajoComponent } from './orden-trabajo-crear/orden-trabajo-crear.component';
import { tap } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Parametro } from 'src/app/core/models/Parametro.model';
import { TipoOrden } from '../../models/tipo-orden.model';
import { DatePipe } from '@angular/common';
import { AUTO_ACTIVACIONES, COD_TIPO_ORDEN_EORDER, PROCESO_PERFIL, SOLO_NUMEROS, TIPO_ORDEN_TRANSFER, urlOrdenTrabajoCrear, urlOrdenTrabajoVer } from '../../gestion-ordenes.contants';
import { MatSnackBar } from '@angular/material/snack-bar';
import { errorMessages } from 'src/app/core/error/error.constant';
import { MatTableDataSource } from '@angular/material/table';
import { UsuarioService } from 'src/app/core/service/usuario.service';
import { OrdenTransferenciaFilter } from '../../models/orden-transferencia-filter.model';
import { Router } from '@angular/router';
import { TransferAnular } from '../../models/transfer-anular.model';
import { ExcelExportService } from 'src/app/core/service/excel-export.service';
import * as moment from 'moment';

@Component({
  selector: 'app-orden-trabajo',
  templateUrl: './orden-trabajo.component.html',
  styleUrls: ['./orden-trabajo.component.scss']
})

export class OrdenTrabajoComponent implements OnInit, AfterViewInit {
  rangoDiasFechaBusqueda: number;
  breadscrums = [
    {
      title: 'Gestión de Órdenes de Trabajo',
      items: ['Home'],
      active: 'Gestión de Órdenes de Trabajo'
    }
  ];

  filterToggle = false;

  displayedColumns = [
    'select',
    'item',
    'tipoOrdenLegacy',
    'nroOrdenLegacy',
    'tipoOrdenTdc',
    'nroOrdenTdc',
    'estado',
    'paralizada',
    'fechaEstadoActual',
    'nroCuenta',
    'origenCreacion',
    'fechaExtraccion',
    'generacion',
    'actions'
  ];

  form: FormGroup;
  procesos: Parametro[] = [];
  tiposOrden: TipoOrden[] = [];
 // estados: Parametro[];
  estadosTransferencia : Parametro[];
  autoActivaciones: any[];
  countItem: number;
  elementsTiposTranfer: string = '';
  codsTipoOrdenEorder: any[] = [];
  elementsCodsTipoOrdenEorder = '';
  maxDate= new Date();
  botonAnular = false;
  usuarioLogueado: string;
  disabledAutoActivacion: boolean;

  selection = new SelectionModel<OrdenTransferenciaFilter>(true, []);
  dataSource: MatTableDataSource<OrdenTransferenciaFilter>= new MatTableDataSource();
  dataFilter: any = { idTipoOrden: '', fechaInicio: '', fechaFin: '', nroOrden: '', nroOrdenTdc: -1, nroCuenta: '', codTipoProceso:'', codEstadTransfer:0, codTipoOrdenEorder: '', autoActivacion: ''}
  fechaInicioActual = new Date();
  fechaFinActual = new Date();
  errorMessage = errorMessages;
  totalElements: number;
  pageSize: number;
  prueba: any[] = [];
  filterStorage: any;
  

  page = 0;
  size = 10;
  order = "id";
  asc = false;

  exportDataExcel: any[];

  // Configuration export data
  exportReport = { fileName:`ordenes-trabajo-${this.datePipe.transform(new Date, 'dd.MM.yyyy')}`, sheet: 'Hoja1' }

  constructor(
    public httpClient: HttpClient,
    private formBuilder: FormBuilder,
    private parametroService: ParametroService,
    private ordenTrabajoService: OrdenTrabajoService,
    public dialog: MatDialog,
    public datePipe: DatePipe,
    private snackBar: MatSnackBar,
    private usuarioService: UsuarioService,
    private router: Router,
    private excelExportService: ExcelExportService
  ) {
    
  }
  
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;
  @ViewChild(MatMenuTrigger)
  contextMenu: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };


  ngOnInit() {
    this.initForm();  
    this.validacionAcesosPorRol();
    this.getProcesosSelect();
    // this.getEstados();
    this.getEstadosTrasferencia();
    this.enviarValoresPorDefecto();
    

    if(sessionStorage.getItem("filtro") != null) {
      this.dataFilter = JSON.parse(sessionStorage.getItem("filtro"));
      this.filterStorage = JSON.parse(sessionStorage.getItem("filtro"));
      this.form.get('codTipoProceso')?.setValue(this.filterStorage["codTipoProceso"]);
      this.selectTipoOrden({value:this.filterStorage["codTipoProceso"]});
      this.form.get('idTipoOrden')?.setValue(this.filterStorage["idTipoOrden"]);
      this.selectCodTipoOrdenEorder({value: this.filterStorage["idTipoOrden"]});
      this.form.get('codTipoOrdenEorder')?.setValue(this.filterStorage["codTipoOrdenEorder"]);
      this.form.get('nroOrden')?.setValue(this.filterStorage["nroOrden"]);
      this.form.get('nroOrdenTdc')?.setValue((this.filterStorage["nroOrdenTdc"] == -1) ? '' : this.filterStorage["nroOrdenTdc"]);
      this.form.get('nroCuenta')?.setValue(this.filterStorage["nroCuenta"]);
      this.form.get('codEstadTransfer')?.setValue(this.filterStorage["codEstadTransfer"]);
      this.form.get('fechaInicio')?.setValue(this.transformDate(this.filterStorage["fechaInicio"]));
      this.form.get('fechaFin')?.setValue(this.transformDate(this.filterStorage["fechaFin"]));
      this.form.get('autoActivacion')?.setValue(this.filterStorage["autoActivacion"]);
      this.obtenerOrdenesTrabajoPorFiltro(this.page, this.size, this.order, this.asc,this.dataFilter);
    }
   
    this.dataFilter.fechaInicio = this.transformDate(this.fechaInicioActual.toString());
    this.dataFilter.fechaFin = this.transformDate(this.fechaFinActual.toString());   
    this.dataFilter.codTipoProceso = this.form.get('codTipoProceso').value;
    if(this.form.get('codTipoProceso'). value != 'TODO') {
      this.dataFilter.codTipoProceso = this.elementsTiposTranfer
    }
    // this.obtenerOrdenesTrabajoPorFiltro(this.page, this.size, this.order, this.asc,this.dataFilter);
    this.autoActivaciones = AUTO_ACTIVACIONES;
    
  }

  disabledOrEnabledAutoActivacion(codProceso: string){
    this.form.get('autoActivacion').disable();
    if(codProceso ==='CONEX') {
      this.form.get('autoActivacion').enable();
    /*   this.displayedColumns= this.columnsShow;
      this.displayedColumns.splice(13,1);
      this.displayedColumns.push('autoActivacion');
      this.displayedColumns.push('actions'); */
    } else {
      this.form.get('autoActivacion').disable();
      this.form.get('autoActivacion')?.setValue(-1);
    // this.displayedColumns= this.columnsShow;
    }
  }

  ngAfterViewInit(): void {
  }
  
  validacionAcesosPorRol() {
    const codTodoProceso = 'TODO'
    let usuarioToken;
    if(sessionStorage.getItem("usuarioPerfil") != null) {
      usuarioToken = JSON.parse(sessionStorage.getItem("usuarioPerfil"));
    }
   // const usuarioToken = this.usuarioService.usuarioPerfilStorage;
    this.usuarioLogueado = usuarioToken.username;
    const codProceso = PROCESO_PERFIL.get(usuarioToken.paramNombre) || '';
    if(codProceso === codTodoProceso) {
      this.form.get('codTipoProceso')?.setValue(codTodoProceso);
    } else {
      this.form.get('codTipoProceso')?.setValue(codProceso);
       this.form.controls['codTipoProceso'].disable();
    }
    this.selectTipoOrden({value: codProceso});
  }


  initForm(): void {
    this.form = this.formBuilder.group({
      idTipoOrden: [''],
      fechaInicio: [''],
      fechaFin: [''],
      nroOrden: ['', Validators.pattern(SOLO_NUMEROS)],
      nroOrdenTdc: ['', Validators.pattern(SOLO_NUMEROS)],
      nroCuenta: ['', Validators.pattern(SOLO_NUMEROS)],
    //  idEstado: [''],
      codTipoProceso: [''],
      codEstadTransfer: [''],
      codTipoOrdenEorder: [''],
      autoActivacion: ['']
    });
  }

  enviarValoresPorDefecto(): void {
    this.form.get('autoActivacion')?.setValue(-1);
    this.form.get('idTipoOrden')?.setValue('TODO');
    this.form.get('codTipoOrdenEorder')?.setValue('TODO');
   // this.form.get('idEstado')?.setValue(0);
    this.form.get('codEstadTransfer')?.setValue(0);
  }
  
  getProcesosSelect(): void {
    this.parametroService.getProcesos().pipe(
      tap((response: Parametro[]) => {
        this.procesos = response;
      })
      ).subscribe();
  }

 /*  getEstados(): void {
    this.parametroService.getEstados().pipe(
      tap((response: Parametro[]) => {
        this.estados = response;
      })
    ).subscribe();
  } */

  getEstadosTrasferencia(): void {
    this.parametroService.getEstadosTransferencia().pipe(
      tap((response: Parametro[]) => {
        this.estadosTransferencia = response;
      })
    ).subscribe();
  }


  selectTipoOrden($event:any) {
    this.elementsTiposTranfer = '';
    if($event) {
      const codProceso = $event.value;
      this.disabledOrEnabledAutoActivacion(codProceso);
      this.form.get('idTipoOrden')?.setValue('TODO');
      this.tiposOrden = TIPO_ORDEN_TRANSFER.filter((x: any) => x.codProceso === codProceso) || '';
     /*  const codsLegacy = this.tiposOrden.map((x: any) => {
        if(x.codProceso === codProceso) {
          this.elementsTiposTranfer += x.codigoLegacy.join(',');
         }
      }) */
      let array : any[] =[];
      TIPO_ORDEN_TRANSFER.filter((x: any) => {
        if(x.codProceso === codProceso) {
          array.push("'"+x.codigoLegacy+"'");
         }
      }) 

      this.elementsTiposTranfer= array.join(',');
     
      /* this.tipoOrdenService.getTipoOrdenByCodProceso(codProceso).pipe(
        tap((reponse: TipoOrden[]) => {
          this.tiposOrden = reponse;
        })
      ).subscribe(); */
    }
  }

  selectCodTipoOrdenEorder($event:any): void {
    this.elementsCodsTipoOrdenEorder='';
    if($event) {
      const cod = $event.value;
      this.form.get('codTipoOrdenEorder')?.setValue('TODO');
      this.codsTipoOrdenEorder = COD_TIPO_ORDEN_EORDER.filter((x: any) => x.codigo === cod) || '';

      let array : any[] =[];
      COD_TIPO_ORDEN_EORDER.filter((x: any) => {
        if(x.codigo === cod) {
          array.push("'"+x.value+"'");
         }
      }) 

      this.elementsCodsTipoOrdenEorder= array.join(',');
    }

  }



  /* selectAutoActivacion($event:any) : void{
    if($event) {
      const selectOption = $event.value;
      if(selectOption == -1){
        this.displayedColumns.splice(13,1);
        this.displayedColumns.push('autoActivacion');
        this.displayedColumns.push('actions');
      }
    }
  } */


  
  submitOrdenTrabajo(): void {
    // if(((this.form.value.fechaInicio == '' || this.form.value.fechaInicio == null) && (this.form.value.fechaFin != '' && this.form.value.fechaFin != null))) {
    //   this.snackBar.open('La fecha inicio no debe estar vacio.','Close');
    //   return
    // }

    // if(((this.form.value.fechaFin == '' || this.form.value.fechaFin == null) && (this.form.value.fechaInicio != '' && this.form.value.fechaInicio != null))) {
    //   this.snackBar.open('La fecha fin no debe estar vacio.','Close');
    //   return
    // }

    // this.snackBar.open('22222222222222.','Close');

    // if(((this.form.value.fechaFin == '' || this.form.value.fechaFin == null) || (this.form.value.fechaInicio == '' || this.form.value.fechaInicio == null))){
    //   this.snackBar.open('Favor de completar los campos fecha de creación desde y hasta b.','Close');
    //   return
    // }

    
    // if(Math.round((this.form.value.fechaFin - this.form.value.fechaInicio)/(1000*60*60*24)) >= 6){
    //   this.snackBar.open('El reporte no debe ser mayor a 6 dias','Close');
    //   return
    // }
    // console.log("fecha inicio: ".concat(this.form.value.fechaInicio));
    // console.log("fecha fin: ".concat(this.form.value.fechaFin));
    if (
      (this.form.value.nroOrden.replace(/\s/g, "") == '' || this.form.value.nroOrden.replace(/\s/g, "") == null) && 
      (this.form.value.nroCuenta.replace(/\s/g, "") == '' || this.form.value.nroCuenta.replace(/\s/g, "") == null) && 
      (this.form.value.nroOrdenTdc.replace(/\s/g, "") == '' || this.form.value.nroOrdenTdc.replace(/\s/g, "") == null)
    ) {
      if(((this.form.value.fechaFin == '' || this.form.value.fechaFin == null) || (this.form.value.fechaInicio == '' || this.form.value.fechaInicio == null))){
        this.snackBar.open('Favor de completar los campos fecha de creación desde y hasta.','Close');
        return
      }
      else {
        
        this.rangoDiasFechaBusqueda = Number.parseInt(JSON.parse(sessionStorage.getItem('rangoFechaBusqueda')).valorNum);
        if(Math.round((this.form.value.fechaFin - this.form.value.fechaInicio)/(1000*60*60*24)) >= this.rangoDiasFechaBusqueda){
          this.snackBar.open('El reporte no debe ser mayor a ' + this.rangoDiasFechaBusqueda + ' dias','Close');
          return
        }
      }
    }

    this.selection.clear();
    this.page = 0;
    this.size = 10;
    this.dataFilter = this.form.value;
    this.dataFilter.codTipoProceso = this.form.get('codTipoProceso').value;
    this.dataFilter.autoActivacion = this.form.get('autoActivacion').value;
    sessionStorage.removeItem("filtro");
    sessionStorage.setItem('filtro',JSON.stringify(this.dataFilter));
    this.obtenerOrdenesTrabajoPorFiltro(this.page, this.size, this.order, this.asc,this.dataFilter);
  }

  transformDate(date: string): string{
    const dateTransform = this.datePipe.transform(date, 'yyyy-MM-dd');
    return dateTransform;
  }

  obtenerOrdenesTrabajoPorFiltro(page: number, size: number, order: string, asc: boolean, filtro: any): void {
    filtro.fechaInicio = (filtro.fechaInicio == null || filtro.fechaInicio == '') ? '1900-01-01' : this.transformDate(filtro?.fechaInicio);
    filtro.fechaFin = (filtro.fechaFin == null || filtro.fechaFin == '') ? '2999-01-01' : this.transformDate(filtro.fechaFin);
    filtro.nroOrden= this.dataFilter.nroOrden;
    filtro.autoActivacion = this.dataFilter.autoActivacion == -1 ? '' :this.dataFilter.autoActivacion;
    filtro.nroOrdenTdc= (this.dataFilter.nroOrdenTdc == '')? -1 : this.dataFilter.nroOrdenTdc;
   // filtro.idEstado = filtro.idEstado == 0 ? '': filtro.idEstado;
    //filtro.idTipoOrden = filtro.idTipoOrden == 'TODO' ? this.elementsTiposTranfer : filtro.idTipoOrden;
    if (filtro.idTipoOrden === 'TODO') {
      filtro.codTipoProceso = this.elementsTiposTranfer;
    }

    this.ordenTrabajoService.filtroOrdenTrabajo(page, size, order, asc, filtro).pipe(
      tap((response: any) => {
       response.content.forEach(element => {
         if (element.pararilizada == 'S') {
          element.pararilizada = 'Si';
         } else if (element.pararilizada == 'N') {
          element.pararilizada = 'No';
         } else {
          element.pararilizada = '';
         }
       });
       this.dataSource = new MatTableDataSource<OrdenTransferenciaFilter>(response.content);
       this.exportDataExcel = response.content;
       this.pageSize = response.size;
       this.totalElements = response.totalElements;
       this.paginator.pageIndex = this.page;
       this.countItem= response.pageable.offset + 1;
      })
    ).subscribe();
  }

  anularOrdenTrabajoSeleccionadas(itemsAnular: TransferAnular[]): void {

    this.ordenTrabajoService.anularOrdenTrabajo(itemsAnular).pipe(
        tap((response) => !!response),
        tap(() => this.snackBar.open(`Anulación exitosa.`, 'CERRAR')),
        tap(() => this.obtenerOrdenesTrabajoPorFiltro(this.page, this.size, this.order, this.asc,this.dataFilter))
    ).subscribe();
    this.selection.clear();
  }

  dialogCrearOrdenTrabajo() {
    this.dialog.open(CrearOrdenTrabajoComponent);
  }

  dialogAnularOrdenTrabajo() {
    
    this.validarEstadoOrdeSelecionada();

    /* const estado = this.selection.selected.filter(s => s.estado != ESTADO_ORDEN_SCOM.EMITIDA &&
    s.estado != ESTADO_ORDEN_SCOM.CREADA).length; */

   /*  const estado = this.selection.selected.filter(s => s.estado != ESTADO_ORDEN_SCOM.EMITIDA &&
      s.estado != ESTADO_ORDEN_SCOM.CREADA).length;

    if(estado > 0){
      this.snackBar.open('Solo se anular una orden con estado CREADA o EMITIDA.','Close');
      return;
    } */

    let itemsAnular: TransferAnular[] = [];
    this.selection.selected.forEach(element => {
      let itemAnular = new TransferAnular;
      itemAnular.id = element.idtransferencia;
      itemAnular.nroOrdenLegacy =element.nroOrdenLegacy;
      itemAnular.codTipoOrdenLegacy = element.tipoOrdenLegacy;
      itemAnular.codEstadoOrden = element.estado;
      itemAnular.usuarioAnula = this.usuarioLogueado;
      itemsAnular.push(itemAnular);
    });
    
    this.dialog.open(AnularOrdenTrabajoComponent).afterClosed().subscribe((result)=>{
      if(result){
        this.anularOrdenTrabajoSeleccionadas(itemsAnular);
      }
    });
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.filteredData.length;
    return numSelected === numRows;
  }

  validarEstadoOrdeSelecionada() {
   /*  const ordenNoScom = this.selection.selected.filter(s => ((s.codTipoOrden != TIPO_ORDEN_SCOM.CONTRASTE) &&
    (s.codTipoOrden != TIPO_ORDEN_SCOM.INSPECCION) &&
    (s.codTipoOrden != TIPO_ORDEN_SCOM.MANTENIMIENTO) &&
    (s.codTipoOrden != TIPO_ORDEN_SCOM.NORMALIZACION))).length;
    if(ordenNoScom > 0){
      this.snackBar.open('Ha seleccionado una orden que no puede ser anulada.','Close');
      return;
    }  */
      
  }

  rowSelected(row: any, $event) {
    if($event){
      $event.stopPropagation();
    }
   /*  const isSelected = !this.selection.isSelected(row);
    if(isSelected){
     if((row.codTipoOrden != TIPO_ORDEN_SCOM.CONTRASTE) &&
        (row.codTipoOrden != TIPO_ORDEN_SCOM.INSPECCION) &&
        (row.codTipoOrden != TIPO_ORDEN_SCOM.MANTENIMIENTO) &&
        (row.codTipoOrden != TIPO_ORDEN_SCOM.NORMALIZACION)){
        this.snackBar.open('La orden de tipo ' + row.tipoOrden + ' no puede ser Anulada.','Close');
        return;
      }  
 
    } */
  }

  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) =>
        this.selection.select(row)
      );

    // this.validarEstadoOrdeSelecionada();
  }

  pageChanged(event: PageEvent) {
    console.log(event);
    this.selection.clear();
    this.size = event.pageSize;
    this.page = event.pageIndex;
    if(sessionStorage.getItem("filtro") != null) {
      this.dataFilter = JSON.parse(sessionStorage.getItem("filtro"));
      this.filterStorage = JSON.parse(sessionStorage.getItem("filtro"));
      this.form.get('codTipoProceso')?.setValue(this.filterStorage["codTipoProceso"]);
      this.selectTipoOrden({value:this.filterStorage["codTipoProceso"]});
      this.form.get('idTipoOrden')?.setValue(this.filterStorage["idTipoOrden"]);
      this.selectCodTipoOrdenEorder({value: this.filterStorage["idTipoOrden"]});
      this.form.get('codTipoOrdenEorder')?.setValue(this.filterStorage["codTipoOrdenEorder"]);
      this.form.get('nroOrden')?.setValue(this.filterStorage["nroOrden"]);
      this.form.get('nroOrdenTdc')?.setValue((this.filterStorage["nroOrdenTdc"] == -1) ? '' : this.filterStorage["nroOrdenTdc"]);
      this.form.get('nroCuenta')?.setValue(this.filterStorage["nroCuenta"]);
      this.form.get('codEstadTransfer')?.setValue(this.filterStorage["codEstadTransfer"]);
      this.form.get('fechaInicio')?.setValue(this.transformDate(this.filterStorage["fechaInicio"]));
      this.form.get('fechaFin')?.setValue(this.transformDate(this.filterStorage["fechaFin"]));
      this.form.get('autoActivacion')?.setValue(this.filterStorage["autoActivacion"]);
      this.obtenerOrdenesTrabajoPorFiltro(this.page, this.size, this.order, this.asc,this.dataFilter);
    } else {
      this.obtenerOrdenesTrabajoPorFiltro(this.page, this.size, this.order, this.asc, this.dataFilter);
    }
  }

  sendUrlVer(tipoOrdenLegacy: any, idTransferencia: number, generacion: string) {
    let url = urlOrdenTrabajoVer.get(tipoOrdenLegacy)|| '';
    let urlFinal ='';
    if(url =='' || (tipoOrdenLegacy == 'CONT' && generacion=='M')) {
      urlFinal = 'orden-xml'
    } else {
      urlFinal = url;
    }
    this.router.navigate([`/gestion-ordenes/orden-trabajo/${urlFinal}/${idTransferencia}`])
  }

  sendUrlCrear(tipoOrdenLegacy: any, idTransferencia: number) {
    let url = urlOrdenTrabajoCrear.get(tipoOrdenLegacy)|| '';
    const urlFinal = url =='' ? 'orden-xml': url;
    this.router.navigate([`/gestion-ordenes/orden-trabajo/${urlFinal}/${idTransferencia}`])
  }

  exportFile() {
    if(this.totalElements == 0){
      this.snackBar.open('No hay datos de Resultado de busqueda para exportar','Close');
      return
    }

    this.ordenTrabajoService.filtroOrdenTrabajo(0, this.totalElements, this.order, this.asc, this.dataFilter).pipe(
      tap((response: any)=> {
        this.exportDataExcel = response.content.map(function(obj) {
          return {
            tipoOrdenLegacy: obj.tipoOrdenLegacy,
            nroOrdenLegacy: obj.nroOrdenLegacy,
            tipoOrdenTdc: obj.tipoOrdenTdc,
            nroTdc: obj.nroTdc,
            estado: obj.estado,
            paralizada: obj.pararilizada,
            fecEstadoActual: obj.fecEstadoActual == null ? '' : moment(obj.fecEstadoActual).format('DD/MM/yyyy'),
            nroCuenta: obj.nroCuenta,
            origenCrea: obj.origenCrea,
            fecExtracion: obj.fechaCrea == null ? '' : moment(obj.fechaCrea).format('DD/MM/yyyy'),
            generacion: obj.generacion,
          }
        });
      }),
      tap((response) => !!response),
      tap(() => this.excelExportService.exportAsExcelFile(this.exportDataExcel, this.exportReport.fileName))
    ).subscribe();
  }

  
}