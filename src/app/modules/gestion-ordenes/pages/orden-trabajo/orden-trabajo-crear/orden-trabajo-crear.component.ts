import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OrdenTrabajoService } from '../../../services/orden-trabajo.service';

@Component({
  selector: 'app-orden-trabajo-crear',
  templateUrl: './orden-trabajo-crear.component.html',
  styleUrls: ['./orden-trabajo-crear.component.sass']
})
export class CrearOrdenTrabajoComponent {

  form: FormGroup;
  ot: string = 'Inspección';
  constructor(
    public dialogRef: MatDialogRef<CrearOrdenTrabajoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public ordenTrabajoService: OrdenTrabajoService
  ) { }
  onNoClick(): void {
    this.dialogRef.close();
  }
  confirmCrearOrdenTrabajo(): void {
    this.ordenTrabajoService.crearOrdenTrabajo();
  }
}
