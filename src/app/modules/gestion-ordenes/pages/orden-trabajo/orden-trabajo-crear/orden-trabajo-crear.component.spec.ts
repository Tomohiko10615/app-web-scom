import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CrearOrdenTrabajoComponent } from './orden-trabajo-crear.component';

describe('CrearOrdenTrabajoComponent', () => {
  let component: CrearOrdenTrabajoComponent;
  let fixture: ComponentFixture<CrearOrdenTrabajoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [CrearOrdenTrabajoComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearOrdenTrabajoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
