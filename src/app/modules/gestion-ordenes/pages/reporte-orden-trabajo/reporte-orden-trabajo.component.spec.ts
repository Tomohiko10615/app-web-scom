import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteOrdenTrabajoComponent } from './reporte-orden-trabajo.component';

describe('ReporteOrdenTrabajoComponent', () => {
  let component: ReporteOrdenTrabajoComponent;
  let fixture: ComponentFixture<ReporteOrdenTrabajoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReporteOrdenTrabajoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReporteOrdenTrabajoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
