import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { errorMessages } from 'src/app/core/error/error.constant';
import { ChartBarReport } from 'src/app/core/interfaces/core.interface';
import { REPORTE_SITUACION, REPORTE_TIPO_PROCESO_ORDEN, SOLO_NUMEROS, TIPO_ORDEN_TRANSFER } from '../../gestion-ordenes.contants';
import { ReporteOrdenTrabajo } from '../../models/reporte-orden-trabajo.model';
import { ReporteOrdenTrabajoService } from '../../services/reporte-orden-trabajo.service';
import { tap } from 'rxjs';
import { ParametroService } from 'src/app/core/service/parametro.service';
import { Parametro } from 'src/app/core/models/Parametro.model';
import { ExcelExportService } from 'src/app/core/service/excel-export.service';

@Component({
  selector: 'app-reporte-orden-trabajo',
  templateUrl: './reporte-orden-trabajo.component.html',
  styleUrls: ['./reporte-orden-trabajo.component.scss']
})
export class ReporteOrdenTrabajoComponent implements OnInit, AfterViewInit {
  rangoDiasFechaBusqueda: number;

  filterToggle = false;
  displayedColumns = [
    'item',
    'nroCuenta',
    'nroServicio',
    'fechaCreacion',
    'tipoProceso',
    'tipoOrden',
    'estado',
    'situacion'
  ];

  breadscrums = [
    {
      title: 'Reporte de Ordenes de Trabajo',
      items: ['Home'],
      active: 'Reporte de Ordenes de Trabajo'
    }
  ];

  form: FormGroup;
  situaciones: any[];
  tipoProcesos: any[];
  tipoOrdenes: any[];
  dataSource: MatTableDataSource<ReporteOrdenTrabajo> = new MatTableDataSource();

  totalElements: number;
  pageSize: number;

  fechaInicioActual = new Date();
  fechaFinActual = new Date();
  errorMessage = errorMessages;

  // Configuration export data

  exportReport = { fileName:`reporte-ordenes-${this.datePipe.transform(new Date, 'dd.MM.yyyy')}`, sheet: 'Hoja1' }

  //Chart Bar Vertical
  colorScheme = {
    domain: ['#9370DB', '#87CEFA', '#FA8072', '#FF7F50', '#90EE90', '#9370DB']
  };

  public chartBarValues: ChartBarReport[] = [];

  gradient = false;
  showXAxis = true;
  showYAxis = true;
  showLegend = false;
  showXAxisLabel = true;
  showYAxisLabel = true;
  legendPosition = 'right';
  vbarxAxisLabel = 'Situación';
  vbaryAxisLabel = 'Cantidad';
  view: any;

  dataFilter: any = { fechaInicio: '', fechaFin:'', situacion:'', idTipoProceso:'', nroCuenta:'', idTipoOrden: ''}


  //paginator values
  page = 0;
  size = 10;
  order = "id";
  asc = false;
  totalPages: Array<number>;
  countItem: number;
  exportDataExcel: any[];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  
  constructor(public formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private snackBar: MatSnackBar,
    private reporteOrdenTrabajoService: ReporteOrdenTrabajoService,
    private excelExportService: ExcelExportService ) { }

  ngOnInit(): void {
    this.initForm();
    this.situaciones = REPORTE_SITUACION;
    this.tipoProcesos = REPORTE_TIPO_PROCESO_ORDEN;
    
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  initForm(): void {
    this.form = this.formBuilder.group({
      fechaInicio: [''],
      fechaFin: [''],
      situacion: [''],
      idTipoProceso: [''],
      nroCuenta: ['', Validators.pattern(SOLO_NUMEROS)],
      idTipoOrden: [''],
    });
  }

  onResize(event) {
    this.view = [event.target.innerWidth / 2, 400];
  }

  transformDate(date: string): string{
    const dateTransform = this.datePipe.transform(date, 'yyyy-MM-dd');
    return dateTransform;
  }

  selectTipoOrden(codigo:any) {
    this.form.get('idTipoOrden')?.setValue('');
    if(codigo) {
      const codProceso = codigo;
      this.tipoOrdenes = TIPO_ORDEN_TRANSFER.filter((x: any) => x.codProceso === codProceso) || '';
    }
  }



  submitFilterReporteOrdenTrabajo(): void {

    this.page = 0;
    this.size = 10;

    
    /*if(((this.form.value.fechaInicio == '' || this.form.value.fechaInicio == null) && (this.form.value.fechaFin != '' && this.form.value.fechaFin != null))) {
      this.snackBar.open('La fecha inicio no debe estar vacio.','Close');
      return
    }

    if(((this.form.value.fechaFin == '' || this.form.value.fechaFin == null) && (this.form.value.fechaInicio != '' && this.form.value.fechaInicio != null))) {
      this.snackBar.open('La fecha fin no debe estar vacio.','Close');
      return
    }*/
    
    
    // //CHR 19-06-23, no funciona -.- 




    if (this.form.value.nroCuenta.replace(/\s/g, "") == '' || this.form.value.nroCuenta.replace(/\s/g, "") == null) {
      if(((this.form.value.fechaFin == '' || this.form.value.fechaFin == null) || (this.form.value.fechaInicio == '' || this.form.value.fechaInicio == null))){
        this.snackBar.open('Favor de completar los campos fecha de creación desde y hasta.','Close');
        return
      }
      else {
        this.rangoDiasFechaBusqueda = Number.parseInt(JSON.parse(sessionStorage.getItem('rangoFechaBusqueda')).valorNum);
        if(Math.round((this.form.value.fechaFin - this.form.value.fechaInicio)/(1000*60*60*24)) >= this.rangoDiasFechaBusqueda){
          this.snackBar.open('El reporte no debe ser mayor a ' + this.rangoDiasFechaBusqueda + ' dias','Close');
          return
        }
      }
    }
    
    

    // //CHR 19-06-23
    // if(Math.round((this.form.value.fechaFin - this.form.value.fechaInicio)/(1000*60*60*24)) >= 7){
    //   this.snackBar.open('El reporte no debe ser mayor a 7 dias','Close');
    //   return
    // }

    

    this.getFiltroReporteOrdenesTrabajo();
    this.getFiltroReporteOrdenesTrabajoGrafico();
  

  }

  getFiltroReporteOrdenesTrabajo(): void {
    
    const formValue = this.form.value;

    console.log(formValue.fechaInicio);
    console.log(this.transformDate(formValue.fechaInicio));


    console.log(formValue.fechaFin);
    console.log(this.transformDate(formValue.fechaFin));

    //this.dataFilter.fechaInicio = (this.dataFilter.fechaInicio == null || this.dataFilter.fechaInicio == '') ? '' : this.transformDate(this.dataFilter?.fechaInicio);
    //this.dataFilter.fechaFin = (this.dataFilter.fechaFin == null || this.dataFilter.fechaFin == '') ? '' : this.transformDate(this.dataFilter.fechaFin);
    this.dataFilter.fechaInicio = (formValue.fechaInicio == null || formValue.fechaInicio == '') ? '1900-01-01' : this.transformDate(formValue.fechaInicio); //CHR 19-06-23
    this.dataFilter.fechaFin = (formValue.fechaFin == null || formValue.fechaFin == '') ? '2999-01-01' : this.transformDate(formValue.fechaFin); //CHR 19-06-23
    this.dataFilter.situacion = formValue.situacion;
    this.dataFilter.idTipoProceso = REPORTE_TIPO_PROCESO_ORDEN.find((x: any) => x.codigo === formValue.idTipoProceso)?.value || '';
    this.dataFilter.idTipoOrden = formValue.idTipoOrden;
    this.dataFilter.nroCuenta = formValue.nroCuenta;



    this.reporteOrdenTrabajoService.filtroReporteOrdenes(this.page, this.size, this.order, this.asc, this.dataFilter).pipe(
      tap((response: any) => { 
        this.dataSource = new MatTableDataSource<ReporteOrdenTrabajo>(response.content);
        this.pageSize = response.size;
        this.totalElements = response.totalElements;
        this.paginator.pageIndex = this.page;
        this.countItem= response.pageable.offset + 1;
      })
    ).subscribe();
  }

  pageChanged(event: PageEvent): void {
    this.size = event.pageSize;
    this.page = event.pageIndex;
    this.getFiltroReporteOrdenesTrabajo();
  }

  exportFile() {
    if(this.totalElements == 0){
      this.snackBar.open('No hay datos de Resultado de busqueda para exportar','Close');
      return
    }

    this.reporteOrdenTrabajoService.filtroReporteOrdenes(0, this.totalElements, this.order, this.asc, this.dataFilter).pipe(
      tap((response: any)=> {
        this.exportDataExcel = response.content;
      }),
      tap((response) => !!response),
      tap(() => this.excelExportService.exportAsExcelFile(this.exportDataExcel, this.exportReport.fileName))
    ).subscribe();
  }
 
   getFiltroReporteOrdenesTrabajoGrafico(): void {
    
    const formValue = this.form.value;
    //this.dataFilter.fechaInicio = (this.dataFilter.fechaInicio == null || this.dataFilter.fechaInicio == '') ? '' : this.transformDate(this.dataFilter?.fechaInicio);
    //this.dataFilter.fechaFin = (this.dataFilter.fechaFin == null || this.dataFilter.fechaFin == '') ? '' : this.transformDate(this.dataFilter.fechaFin);
    this.dataFilter.fechaInicio = (formValue.fechaInicio == null || formValue.fechaInicio == '') ? '' : this.transformDate(formValue.fechaInicio); //CHR 19-06-23
    this.dataFilter.fechaFin = (formValue.fechaFin == null || formValue.fechaFin == '') ? '' : this.transformDate(formValue.fechaFin); //CHR 19-06-23
    this.dataFilter.situacion = formValue.situacion;
    this.dataFilter.idTipoProceso = REPORTE_TIPO_PROCESO_ORDEN.find((x: any) => x.codigo === formValue.idTipoProceso)?.value || '';
    this.dataFilter.idTipoOrden = formValue.idTipoOrden;
    this.dataFilter.nroCuenta = formValue.nroCuenta;
    this.reporteOrdenTrabajoService.filtroReporteOrdenesGrafico(this.dataFilter).pipe(
      tap((reponse: any)=> this.chartBarValues = reponse)
    ).subscribe();
  }
}
