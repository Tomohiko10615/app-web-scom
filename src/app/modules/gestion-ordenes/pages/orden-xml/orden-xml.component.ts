import { AfterViewInit, Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { tap } from 'rxjs';
import { OrdenInspeccionNotificadaService } from '../../services/orden-inspeccion-notificada.service';
import { OrdenContrasteService } from '../../services/orden-contraste.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TransferXmlService } from '../../services/transfer-xml.service';
import { MatTabChangeEvent } from '@angular/material/tabs';

//Se declara funciones creadas en src/assets/custom-js/transfer-xml.js
declare function envioXml(valorEnvio): any;
declare function recepcionXml(message): any;

@Component({
  selector: 'app-orden-xml',
  templateUrl: './orden-xml.component.html',
  styleUrls: ['./orden-xml.component.scss'],
})
export class OrdenXmlComponent implements OnInit {

  breadscrums = [];

  displayedColumnsPruebaAlta = [
    'pruebaAlta01',
    'pruebaAlta02',
    'pruebaAlta03'
  ]

  displayedColumnsPruebaBaja = [
    'pruebaBaja01',
    'pruebaBaja02',
    'pruebaBaja03'
  ]

  displayedColumnsPruebaAislamiento = [
    'pruebaAislamientoR',
    'pruebaAislamientoS',
    'pruebaAislamientoT'
  ]

  displayedColumnsPruebaNominal = [
    'pruebaNominal01',
    'pruebaNominal02',
    'pruebaNominal03'
  ]

  idUrl: any;
  xml: any 
  ordenTransfer: any = {};
  
  nroServicio: string;
  nroCuenta: string;
  cliente: string;
  direccion: string;
  distrito: string;
  rutaLectura: string;

  marca: string;
  modelo: string;
  nroMedidor: string;
  estado: string;

  orden: any;
  nroOrden: string = "";
  estadoWkf: string = "";
  
  idOrden: number = 0;
  idOrdenContraste: number = 0;
  idOrdenInspeccion: number = 0;

  form: FormGroup;
  medContrastePruebasAlta: any;
  medContrastePruebasBaja: any;
  medContrastePruebasNominal: any;
  medContrastePruebasAislamiento: any;

  inicioCreacion: string = "SC4J";

  @ViewChild('tabGroup') tabGroup;
  xmlEnvio: string ='';
  xmlRecepcion: string='';
  activeXmlEnvio: boolean = false;
  activexmlRecepcion: boolean = false;
  
  constructor(private ordenInspeccionNotificadaService: OrdenInspeccionNotificadaService,
              private route : ActivatedRoute,
              private ordenContrasteService: OrdenContrasteService,
              private router: Router,
              private fb: FormBuilder,
              private transferXml: TransferXmlService) { }


  ngOnInit(): void {
    this.idUrl = this.route.snapshot.params['id'];
    this.initForm();
 /*    this.getXMLEnvio();
    this.getXMLRecepcion(); */
    this.getOrdenTransfer();
    this.breadscrums = [
      {
        title: 'Identificador Ord Transferencia SCOM: ' + this.idUrl,
        items: ['Home'],
        active: 'Detalle Orden Transferencia '
      }
    ]
    
  }


  tabChanged(tabChangeEvent: MatTabChangeEvent): void {

    if(tabChangeEvent.tab.textLabel=== 'DATOS ENVIO FORCEBEAT'){
      if(this.activeXmlEnvio == false) {
        this.obtenerDatosXmlEnvio();
        
      }
     
     
    }

    if(tabChangeEvent.tab.textLabel=== 'DATOS RECEPCION FORCEBEAT'){
      if(this.activexmlRecepcion == false) {
        this.obtenerDatosXmlRecepcion();
       
      }
     
    }
  }


  obtenerDatosXmlEnvio(): void {
    
    this.transferXml.obtenerXmlEnvio(this.idUrl).pipe(
      tap((response: any) => {
        this.xmlEnvio = response.xml.replaceAll("\n", "");
        envioXml(this.xmlEnvio);
        this.activeXmlEnvio= true;
      })
    ).subscribe();
 
    }
  
    obtenerDatosXmlRecepcion(): void {
    
      this.transferXml.obtenerXmlRecepcion(this.idUrl).pipe(
        tap((response: any) => {
          this.xmlRecepcion = response.xml.replaceAll("\n", "");
          recepcionXml(this.xmlRecepcion);
          this.activexmlRecepcion= true;
        })
      ).subscribe();
   
      }

  initForm() {
    this.form = this.fb.group({
      nroServicio: [{ value: !this.orden ? '' : this.orden.nroServicio, disabled: true}, Validators.required],
      idMotivo: [{ value: !this.orden ? '' : this.orden.idMotivo, disabled: true}, Validators.required],
      observaciones: [{ value: !this.orden ? '' : this.orden.observacion, disabled: true}, Validators.required],
      idAnormalidad: []
    })
  }

/*   getXMLEnvio() {
    this.ordenInspeccionNotificadaService.getXML(this.idUrl, 'ENVIO').pipe(
      tap(response => {
        if(response != null) {
          this.xmlEnvio = beautify(response.xml)
        }else {
          this.xmlEnvio = ""
        }
      })
    ).subscribe();
  } */

 /*  getXMLRecepcion() {
    this.ordenInspeccionNotificadaService.getXML(this.idUrl, 'RECEPCION').pipe(
      tap(response => {
        if(response != null) {
          this.xmlRecepcion = beautify(response.xml)
        }else {
          this.xmlRecepcion = ""
        }
      })
    ).subscribe();
  }
 */
  getOrdenTransfer() {
    console.log(this.ordenTransfer)
    this.ordenInspeccionNotificadaService.getOrdenTransfer(this.idUrl).pipe(
      tap(response => {
        console.log(response[0])
        this.ordenTransfer = response[0]
        console.log(this.ordenTransfer)
        if(this.ordenTransfer.generacion == 'M' && this.ordenTransfer.codTipoOrdenLegacy == 'CONT') {
          this.getData();
        }
      })
    ).subscribe();
    this.ordenTransfer.codTipoOrdenLegacy = '324'
  }


  sendActivarSuministro(idAutoAct: number){
    this.router.navigate(['gestion-ordenes/orden-trabajo/activar-suministro', idAutoAct]);
  }

  getData() {
    this.ordenContrasteService.getOrden(this.idUrl, 0).pipe(
      tap(response => {
        this.orden = response;
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.direccion
        this.distrito = response.distrito
        this.rutaLectura = response.rutaLectura
        this.idOrdenInspeccion = response.idOrdenInspeccion
        this.idOrden = response.idOrden
        this.idOrdenContraste = response.idOrdenContraste
        this.form.value.nroServicio = response.nroServicio
        this.marca = response.codMarca
        this.modelo = response.codModelo
        this.nroMedidor = response.nroComponente
        this.estado = response.estadoMedidor
        this.nroOrden = response.nroOrden
        this.estadoWkf = response.estadoWkf
        this.buscarCliente();
        if(response.idOrdenSc4j == 0) {
          this.inicioCreacion = "SCOM";
        }
        //Mostrar en tabla datos de Pruebas de Contraste
        this.medContrastePruebasAlta = response?.medContrasteDTO?.pruebaAlta;
        this.medContrastePruebasBaja = response?.medContrasteDTO?.pruebaBaja;
        this.medContrastePruebasAislamiento = response?.medContrasteDTO?.pruebaAislamiento;
        this.medContrastePruebasNominal = response?.medContrasteDTO?.pruebaNominal;
      })
    ).subscribe();
  }

  buscarCliente() {
    const nroServicio = this.form.value.nroServicio
    this.ordenContrasteService.getNroServicio(nroServicio).pipe(
      tap(response => {
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.direccion
        this.distrito = response.distrito
        this.rutaLectura = response.rutaLectura
        this.marca = response.codMarca
        this.modelo = response.codModelo
        this.nroMedidor = response.nroComponente
        this.estado = response.estadoMedidor
      })
    ).subscribe();
  }
}
