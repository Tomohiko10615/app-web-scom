import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenInspeccionActualizarDatosCnrComponent } from './orden-inspeccion-actualizar-datos-cnr.component';

describe('OrdenInspeccionActualizarDatosCnrComponent', () => {
  let component: OrdenInspeccionActualizarDatosCnrComponent;
  let fixture: ComponentFixture<OrdenInspeccionActualizarDatosCnrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenInspeccionActualizarDatosCnrComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenInspeccionActualizarDatosCnrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
