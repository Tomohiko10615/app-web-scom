import { Component, Inject,Injectable, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
/*import { MatSnackBar } from '@angular/material/snack-bar';*/
import { errorMessages } from 'src/app/core/error/error.constant';
import {OrdenInspeccionActualizarDatosCnrService} from '../../../../services/orden-inspeccion-actualizar-datos-cnr.service'
import { ActivatedRoute, Router } from '@angular/router';
import { tap,Observable } from 'rxjs';
import { UsuarioService } from 'src/app/core/service/usuario.service';

import { map, catchError } from 'rxjs/operators';
import { of, first ,throwError,Subject} from 'rxjs';
import Swal from 'sweetalert2';
import * as moment from 'moment';

@Component({
  selector: 'app-popup-actualizar-datos',
  templateUrl: './popup-actualizar-datos.component.html',
  styleUrls: ['./popup-actualizar-datos.component.sass'],
})
export class PopupActualizarDatosComponent implements OnInit {

  form: FormGroup;
  inputData:any;
  errorMessage = errorMessages;


  constructor(
    @Inject(MAT_DIALOG_DATA) public data:any,
    private fb: FormBuilder,
    /*private snackBar: MatSnackBar,*/
    private ordenInspeccionActualizarDatosCnrService: OrdenInspeccionActualizarDatosCnrService ,
    private usuarioService: UsuarioService,
    public dialogRef: MatDialogRef<PopupActualizarDatosComponent>
    ) { 
      //this.data = "starting";
    }
    
    numDecimalRegex = /^-?\d*[.]?\d{0,2}$/;
    numRegex = /^-?(0|[1-9]\d*)?$/;

  ngOnInit(): void {
    console.log(moment().format('MMMM Do YYYY, h:mm:ss a'));

    /*No utilizamos el Validators.required debido a que son datos que puede encontrase con valor NULO
    El dato de lectura no es requerido ya que es un dato que depende de un proceso de CNR, por lo que al momento
    de edicion el valor puede visualizarse como nulo*/

    /*inpt_carga_r: ['', [Validators.required, Validators.pattern(this.numDecimalRegex), Validators.min(0.01)]],*/
    this.form = this.fb.group({
      inpt_carga_r: ['', [Validators.pattern(this.numDecimalRegex), Validators.min(0.01)]],
      inpt_carga_s: ['', [Validators.pattern(this.numDecimalRegex), Validators.min(0.01)]],
      inpt_carga_t: ['', [Validators.pattern(this.numDecimalRegex), Validators.min(0.01)]],
      inpt_lectura: ['', [Validators.pattern(this.numRegex), Validators.min(1)]],
      inpt_fecha_notificacion: [''],
      inpt_comentario: ['',Validators.maxLength(500)]
    })

    this.inputData = this.data;
    console.log("data edit",this.inputData);

    if(this.inputData != null) {
   
      this.form.get('inpt_carga_r')?.setValue(this.inputData["cargaR"]);
      this.form.get('inpt_carga_s')?.setValue(this.inputData["cargaS"]);
      this.form.get('inpt_carga_t')?.setValue(this.inputData["cargaT"]);
      this.form.get('inpt_lectura')?.setValue(this.inputData["lecturaNotificacion"]);
      
      this.form.get('inpt_fecha_notificacion')?.setValue(this.inputData["fechaNotificacion"]);

    }
  }

  submitEditar(){
    
    console.log(this.inputData);
    console.log(this.form.value);
    //console.log(this.usuarioService.usuarioStorage);
    const objForm = 
    {
      "userName": this.usuarioService.usuarioStorage?.username
      ,"idOrden": this.inputData.idOrden
      ,"idOrdenInsp": this.inputData.idOrdenInsp
      ,"idOrdenMediInsp": this.inputData.idOrdenMediInsp

      ,"nroSuministro": this.inputData.nroSuministro
      ,"nroNotificacion": this.inputData.nroNotificacion
      ,"codExpedienteCNR": this.inputData.codExpedienteCNR
      
	    ,"idMagnitud": this.inputData.idMagnitud
	    ,"codInspeccionCNR": this.inputData.codInspeccionCNR

      ,"cargaR": this.form.value.inpt_carga_r
      ,"cargaS": this.form.value.inpt_carga_s
      ,"cargaT": this.form.value.inpt_carga_t
      ,"lecturaNotificacion": this.form.value.inpt_lectura
      ,"fechaNotificacion": moment(this.form.value.inpt_fecha_notificacion).format('YYYY-MM-DD')

      ,"comentario": this.form.value.inpt_comentario
    }
    //console.log(moment(this.form.value.inpt_fecha_notificacion).format('YYYY-MM-DD'));
    console.log("object request:",objForm);
   
    
    //ini--valida si se realizaron cambios en el VALOR de los campos
    var cambios = false;
    if(cambios==false && (this.inputData.cargaR != objForm.cargaR)){
      cambios=true;
    }
    if(cambios==false && (this.inputData.cargaS != objForm.cargaS)){
      cambios=true;
    }
    if(cambios==false && (this.inputData.cargaT != objForm.cargaT)){
      cambios=true;
    }
    if(cambios==false){
      if(this.inputData.lecturaNotificacion!=null){
        if(this.inputData.lecturaNotificacion != objForm.lecturaNotificacion){
          cambios=true;
        }   
      }
    }
    if(cambios==false && (this.inputData.fechaNotificacion != objForm.fechaNotificacion)){
      cambios=true;
    }

    if(cambios==false){
      Swal.fire('No hay datos por actualizar', 'No se detectaron modificaciones en los campos de carga, lectura y fecha del formulario.', 'info');
      return;
    }

    //fin--valida si se realizaron cambios en el VALOR de los campos

    Swal.fire({
      title: 'Confirmación',
      icon: 'question',
      html:
        '¿Esta seguro de editar los datos de la inspección?',
      focusConfirm: false,
      showDenyButton: true,
  confirmButtonText: 'Si',
  denyButtonText: `No`
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        //Swal.fire('Saved!', '', 'success')
        this.ordenInspeccionActualizarDatosCnrService.postOrdenInspeccion(objForm).pipe(
          catchError((err:any) => {
            console.log(err);
            Swal.fire('Error de actualización', 
            'Ocurrió un error interno al procesar la actualización!', 
            'error');
            return throwError(err);
          })
          ).subscribe(data => {
            console.log(data);
            Swal.fire('Actualización completada', 'Los datos de la inspección fueron actualizados satisfactoriamente.', 'success');
            this.dialogRef.close({data:"sucess_update"});
          });
      } else if (result.isDenied) {
        //Swal.fire('Changes are not saved', '', 'info')
      }
    })
    
    /*this.ordenInspeccionActualizarDatosCnrService.postOrdenInspeccion(form).pipe(
      catchError((err:any) => {
        console.log(err);
        Swal.fire('Error de actualización', 
        'Ocurrió un error interno al procesar la actualización!', 
        'error');
        return throwError(err);
      })
      ).subscribe(data => {
        console.log(data);
        Swal.fire('Actualizado', 'Se actualizó!', 'success');
        this.dialogRef.close({data:"sucess_update"});
      });*/

     

  }

  
}


