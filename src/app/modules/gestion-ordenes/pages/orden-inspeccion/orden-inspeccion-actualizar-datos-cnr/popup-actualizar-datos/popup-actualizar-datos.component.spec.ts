import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupActualizarDatosComponent } from './popup-actualizar-datos.component';

describe('PopupActualizarDatosComponent', () => {
  let component: PopupActualizarDatosComponent;
  let fixture: ComponentFixture<PopupActualizarDatosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PopupActualizarDatosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PopupActualizarDatosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
