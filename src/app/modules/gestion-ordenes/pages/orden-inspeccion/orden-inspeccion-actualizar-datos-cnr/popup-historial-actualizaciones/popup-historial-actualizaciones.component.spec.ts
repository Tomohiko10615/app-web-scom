import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupHistorialActualizacionesComponent } from './popup-historial-actualizaciones.component';

describe('PopupHistorialActualizacionesComponent', () => {
  let component: PopupHistorialActualizacionesComponent;
  let fixture: ComponentFixture<PopupHistorialActualizacionesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PopupHistorialActualizacionesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PopupHistorialActualizacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
