import { Component, Inject, OnInit, AfterViewInit  , ElementRef, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClient } from '@angular/common/http';
import { HistorialActualizaciones } from '../../../../models/historialActualizaciones.model';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatMenuTrigger } from '@angular/material/menu';
import { OrdenInspeccionActualizarDatosCnrService } from '../../../../services/orden-inspeccion-actualizar-datos-cnr.service';
import { tap } from 'rxjs';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'app-popup-historial-actualizaciones',
  templateUrl: './popup-historial-actualizaciones.component.html',
  styleUrls: ['./popup-historial-actualizaciones.component.sass']
})
export class PopupHistorialActualizacionesComponent implements OnInit , AfterViewInit {

  
  filterToggle = false;
  inputData:any;
  displayedColumns = [
    'fechaEdicion',
    'userName',
    'nombreCampoActualizado',
    'valorAntes',
    'valorDespues',
    'comentario'
  ];

selection = new SelectionModel<HistorialActualizaciones>(true, []);
dataSource: MatTableDataSource<HistorialActualizaciones>= new MatTableDataSource();
dataFilter: any = { nroSuministro: '', nroNotificacion:''}

totalElements: number;
pageSize: number;

filterStorage: any;
countItem: number;

  page = 0;
  size = 10;
  order = "id";
  asc = false;

 // exportDataExcel: any[];


  constructor( 
    public httpClient: HttpClient,
    @Inject(MAT_DIALOG_DATA) public data:any,
    private ordenInspeccionActualizarDatosCnrService: OrdenInspeccionActualizarDatosCnrService,) { }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;
  @ViewChild(MatMenuTrigger)
  contextMenu: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };
  

 ngOnInit(): void {
  console.log("---ngOnInit() Demo---");
    this.inputData = this.data;
    
    console.log(this.inputData);
   
  

  }
  ngAfterViewInit() {
    console.log("---ngAfterViewInit() Demo---");

    setTimeout(() => {
      if(this.dataFilter!=null){
      this.dataFilter.nroSuministro = this.inputData.nroSuministro;
      this.dataFilter.nroNotificacion =  this.inputData.nroNotificacion;
      this.obtenerHistorialActualizaciones(this.page, this.size, this.order, this.asc,this.dataFilter);
    }
    }, 0);

    
  }
 
  obtenerHistorialActualizaciones(page: number, size: number, order: string, asc: boolean, filtro: any): void {
  
    console.log(filtro);
    
      this.ordenInspeccionActualizarDatosCnrService.filtroHistorialActualizaciones(page, size, order, asc, filtro).pipe(
         tap((response: any) => {
          console.log(this.dataSource);

          this.dataSource = new MatTableDataSource<HistorialActualizaciones>(response.content);
         // this.exportDataExcel = response.content;
          this.pageSize = response.size;
          this.totalElements = response.totalElements;
          this.paginator.pageIndex = this.page;
          this.countItem= response.pageable.offset + 1;
         })
       ).subscribe();
  }

     
  //necesario?
  rowSelected(row: any, $event) {
    if($event){
      $event.stopPropagation();
    }
  }
  //necesario?
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) =>
        this.selection.select(row)
      );

    // this.validarEstadoOrdeSelecionada();
  }
//necesario?
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.filteredData.length;
    return numSelected === numRows;
  }

  pageChanged(event: PageEvent) {
    console.log(event);
    this.selection.clear();
    this.size = event.pageSize;
    this.page = event.pageIndex;
   //codigo faltante...
   this.obtenerHistorialActualizaciones(this.page, this.size, this.order, this.asc,this.dataFilter);
  }
}
