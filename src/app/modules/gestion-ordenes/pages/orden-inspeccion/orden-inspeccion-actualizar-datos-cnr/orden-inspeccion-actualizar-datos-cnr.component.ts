import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { errorMessages } from 'src/app/core/error/error.constant';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { OrdenInspecActualizaDatosCNRFilter } from '../../../models/orden-inspec-actualiza-datos-cnr-filter.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatMenuTrigger } from '@angular/material/menu';
import { AUTO_ACTIVACIONES, COD_TIPO_ORDEN_EORDER, PROCESO_PERFIL, SOLO_NUMEROS, TIPO_ORDEN_TRANSFER, urlOrdenTrabajoCrear, urlOrdenTrabajoVer } from '../../../gestion-ordenes.contants';
import { SelectionModel } from '@angular/cdk/collections';
import { OrdenInspeccionActualizarDatosCnrService } from '../../../services/orden-inspeccion-actualizar-datos-cnr.service';
import { tap } from 'rxjs';
import { PopupActualizarDatosComponent } from './popup-actualizar-datos/popup-actualizar-datos.component';
import { PopupHistorialActualizacionesComponent } from './popup-historial-actualizaciones/popup-historial-actualizaciones.component';

import { AppComponent } from 'src/app/app.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-orden-inspeccion-actualizar-datos-cnr',
  templateUrl: './orden-inspeccion-actualizar-datos-cnr.component.html',
  styleUrls: ['./orden-inspeccion-actualizar-datos-cnr.component.sass']
})
export class OrdenInspeccionActualizarDatosCnrComponent implements OnInit {
  
  breadscrums = [
    {
      title: 'Actualización de datos de Ordenes de Inspección CNR',
      items: ['Home'],
      active: 'Actualización de datos de Ordenes de Inspección CNR'
    }
  ];  
  
  filterToggle = false;

  form: FormGroup;
  displayedColumns = [
    'nroSuministro',
    'nroNotificacion',
    'codExpedienteCNR',
    'nombreExpedienteCNR',
    'cargaR',
    'cargaS',
    'cargaT',
    'lectura',
    'fechaNotificacion',
    'actions'
  ];

  selection = new SelectionModel<OrdenInspecActualizaDatosCNRFilter>(true, []);
  dataSource: MatTableDataSource<OrdenInspecActualizaDatosCNRFilter>= new MatTableDataSource();
  dataFilter: any = { nroSuministro: '', nroNotificacion:''}

  errorMessage = errorMessages;
  totalElements: number;
  pageSize: number;
  prueba: any[] = [];
  filterStorage: any;
  countItem: number;
  
  page = 0;
  size = 10;
  order = "id";
  asc = false;

  exportDataExcel: any[];

 

  constructor( public httpClient: HttpClient,
    private formBuilder: FormBuilder,    
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private ordenInspeccionActualizarDatosCnrService: OrdenInspeccionActualizarDatosCnrService,
    private appComponent:AppComponent,
    ) { 
    
    }

      
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;
  @ViewChild(MatMenuTrigger)
  contextMenu: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };
  
  ngOnInit(): void {

    //Observable, si al culminar la sesion se tenia abierto algun modal, cerrarlo
    this.appComponent.getSessionInfo().subscribe(data=>{
      console.log("data obtenida de suscripcion de app component :"+data);
      if(data=="SESSION_ENDED"){
        this.dialog.closeAll();
        Swal.close();
      }
    });
   

    this.initForm();  
    //this.validacionAcesosPorRol();

    if(sessionStorage.getItem("filtro") != null) {
      this.dataFilter = JSON.parse(sessionStorage.getItem("filtro"));
      this.filterStorage = JSON.parse(sessionStorage.getItem("filtro"));
      this.form.get('nroSuministro')?.setValue(this.filterStorage["nroSuministro"]);
      this.form.get('nroNotificacion')?.setValue(this.filterStorage["nroNotificacion"]);
      this.obtenerOrdenesTrabajoPorFiltro(this.page, this.size, this.order, this.asc,this.dataFilter);
    }
  }
  initForm(): void {
    this.form = this.formBuilder.group({
      nroSuministro: ['', Validators.pattern(SOLO_NUMEROS)],
      nroNotificacion: ['', Validators.pattern(SOLO_NUMEROS)],
    });
  }

  obtenerOrdenesTrabajoPorFiltro(page: number, size: number, order: string, asc: boolean, filtro: any): void {
  
   this.ordenInspeccionActualizarDatosCnrService.filtroOrdenInspec(page, size, order, asc, filtro).pipe(
      tap((response: any) => {
       console.log(this.dataSource);
       this.dataSource = new MatTableDataSource<OrdenInspecActualizaDatosCNRFilter>(response.content);
       this.exportDataExcel = response.content;
       this.pageSize = response.size;
       this.totalElements = response.totalElements;
       this.paginator.pageIndex = this.page;
       this.countItem= response.pageable.offset + 1;
      })
    ).subscribe();
  }

  submitOrdenTrabajo(): void {
   
    console.log(this.form.value);

    if(this.form.value.nroSuministro=='' && this.form.value.nroNotificacion==''){
      this.snackBar.open("Especifique un filtro de busqueda",'', {duration:200});
      return;
    }

    this.selection.clear();
    this.page = 0;
    this.size = 10;
    this.dataFilter = this.form.value;
    this.dataFilter.nroSuministro = this.form.get('nroSuministro').value;
    this.dataFilter.nroNotificacion = this.form.get('nroNotificacion').value;
    sessionStorage.removeItem("filtro");
    sessionStorage.setItem('filtro',JSON.stringify(this.dataFilter));
    this.obtenerOrdenesTrabajoPorFiltro(this.page, this.size, this.order, this.asc,this.dataFilter);
  }

  
  //necesario?
  rowSelected(row: any, $event) {
    if($event){
      $event.stopPropagation();
    }
  }
  //necesario?
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) =>
        this.selection.select(row)
      );

    // this.validarEstadoOrdeSelecionada();
  }
//necesario?
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.filteredData.length;
    return numSelected === numRows;
  }

  pageChanged(event: PageEvent) {
    console.log(event);
    this.selection.clear();
    this.size = event.pageSize;
    this.page = event.pageIndex;
   //codigo faltante...
    this.obtenerOrdenesTrabajoPorFiltro(this.page, this.size, this.order, this.asc,this.dataFilter);
  }


  showModalEdit(rowSel: any) {

    if(rowSel.tipoCliente!="M"){
      Swal.fire('Validación', 'Solo se puede editar registros de clientes RESIDENCIALES.', 'info');
      return;
    }

    /*if(rowSel.escenarioDescripcion!="CALCULO_LIBRE"){
      Swal.fire({
        title:'Validación', 
        html:'El escenario del expediente seleccionado es '+rowSel.escenarioDescripcion+',<br/>Solo se puede editar registros de expedientes con escenario igual a CALCULO_LIBRE.'
      , icon:'info'});
      return;
    }*/

    console.log(rowSel);
    this.dialog.open(PopupActualizarDatosComponent,{
      width:'800px',
      data:rowSel
    }).afterClosed().subscribe(result=>{
      console.log('The dialog was closed');
      console.log(result);
      if(result!=undefined){
        if(result.data=="sucess_update"){
          this.submitOrdenTrabajo();
        }
      }
      
    });
  }
  showEditHistorial(rowSel: any) {
    console.log(rowSel);
  
    this.dialog.open(PopupHistorialActualizacionesComponent,{
    
      data:rowSel
    }).afterClosed().subscribe(result=>{
      console.log('The dialog historial was closed');
      console.log(result);
      
    });
  }
}
