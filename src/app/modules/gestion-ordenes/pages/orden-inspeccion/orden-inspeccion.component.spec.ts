import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenInspeccionComponent } from './orden-inspeccion.component';

describe('OrdenInspeccionComponent', () => {
  let component: OrdenInspeccionComponent;
  let fixture: ComponentFixture<OrdenInspeccionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenInspeccionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenInspeccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
