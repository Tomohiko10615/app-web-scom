import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenInspeccionEditarComponent } from './orden-inspeccion-editar.component';

describe('OrdenInspeccionEditarComponent', () => {
  let component: OrdenInspeccionEditarComponent;
  let fixture: ComponentFixture<OrdenInspeccionEditarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenInspeccionEditarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenInspeccionEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
