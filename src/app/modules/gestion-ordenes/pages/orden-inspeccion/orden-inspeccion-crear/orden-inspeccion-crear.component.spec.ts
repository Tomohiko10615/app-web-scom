import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenInspeccionCrearComponent } from './orden-inspeccion-crear.component';

describe('OrdenInspeccionCrearComponent', () => {
  let component: OrdenInspeccionCrearComponent;
  let fixture: ComponentFixture<OrdenInspeccionCrearComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenInspeccionCrearComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenInspeccionCrearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
