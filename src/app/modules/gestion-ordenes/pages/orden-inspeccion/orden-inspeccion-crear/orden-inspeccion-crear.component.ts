import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { tap } from 'rxjs';
import { UsuarioService } from 'src/app/core/service/usuario.service';
import { OrdenInspeccionNotificadaService } from '../../../services/orden-inspeccion-notificada.service';

@Component({
  selector: 'app-orden-inspeccion-crear',
  templateUrl: './orden-inspeccion-crear.component.html',
  styleUrls: ['./orden-inspeccion-crear.component.sass']
})
export class OrdenInspeccionCrearComponent implements OnInit {

  breadscrums = [
    {
      title: 'Creación de Orden de Inspección',
      items: ['Home'],
      active: 'Creación de Orden de Inspección'
    }
  ];

  form: FormGroup;
  form2: FormGroup;

  nroServicio: string;
  nroCuenta: string;
  cliente: string;
  direccion: string;
  distrito: string;
  rutaLectura: string;

  tipoInspecciones: any[];
  motivos: any[];
  productos: any[];
  jefeProductos: any[];
  contratistas: any[];

  idUrl: number;
  orden: any;
  idOrden: number = 0;
  idOrdenInspeccion: number = 0;  

  botonBuscar: boolean = true;
  botonSubmit: string = "Guardar";

  botonGuardar: boolean = false;

  nroOrden: string = "";

  constructor(private fb: FormBuilder, 
              private ordenInspeccionService: OrdenInspeccionNotificadaService,
              private usuarioService : UsuarioService,
              private route : ActivatedRoute,
              private router: Router,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.idUrl = this.route.snapshot.params['id'];
    
    this.initForm();    

    if(this.idUrl) {
      this.getData(this.idUrl) 
      this.breadscrums = [
        {
          title: 'Edición de Orden de Inspección',
          items: ['Home'],
          active: 'Edición de Orden de Inspección'
        }
      ];
      this.botonBuscar = false;
      this.botonSubmit = "Actualizar";
    }

    this.getTipoInspeccion();
    this.getMotivos();
    this.getJefeProductos();
    this.getContratistas();
  }

  initForm() {
    this.form = this.fb.group({
      nroServicio: [{ value: !this.orden ? '' : this.orden.nroServicio, disabled: !this.orden ? false : true}, [Validators.required, Validators.pattern('[0-9]*')]]
    })
    this.form2 = this.fb.group({
      tipInspeccion: [(!this.orden) ? '' : this.orden.tipoInspeccion, Validators.required],
      idMotivo: [(!this.orden) ? '' : this.orden.idMotivo, Validators.required],
      idProducto: [(!this.orden) ? '' : this.orden.idProducto, Validators.required],
      idJefeProducto: [(!this.orden) ? '' : this.orden.idJefeProducto, Validators.required],
      idContratista: [(!this.orden) ? 0 : this.orden.idContratista, ''],
      entreCalles: [(!this.orden) ? '' : this.orden.entreCalles, [Validators.pattern("[a-zA-Z0-9 ]*")]],
      informacionComplementaria: [(!this.orden) ? '' : this.orden.informacionComplementaria, ''],
      autogenerado: [(!this.orden) ? '' : this.orden.autogenerado, ''],
      observaciones: [(!this.orden) ? '' : this.orden.observacion, [Validators.required, Validators.pattern("[a-zA-Z0-9 ]*")]]
    })
  }

  getData(id: number) {
    this.ordenInspeccionService.getOrdenById(id, 0).pipe(
      tap(response => {
        this.orden = response;
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.direccion
        this.distrito = response.distrito
        this.rutaLectura = response.rutaLectura
        this.idOrden = response.idOrden
        this.idOrdenInspeccion = response.idOrdenInspeccion
        this.nroOrden = "Nro de Orden: " + response.nroOrden
        this.getProductos(response.idMotivo);
        this.initForm();
      })
    ).subscribe();
  }

  buscarCliente() {
    const nroServicio = this.form.value.nroServicio
    this.ordenInspeccionService.getNroServicio(nroServicio).pipe(
      tap(response => {
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.direccion
        this.distrito = response.distrito
        this.rutaLectura = response.rutaLectura
      })
    ).subscribe();
  }

  getTipoInspeccion() {
    this.ordenInspeccionService.getTipoInspeccion().pipe(
      tap(response => {
        this.tipoInspecciones = response
      })
    ).subscribe();
  }

  getMotivos() {
    this.ordenInspeccionService.getMotivos().pipe(
      tap(response => {
        this.motivos = response
      })
    ).subscribe();
  }

  getProductos(idMotivo: number) {
    this.ordenInspeccionService.getProductos(idMotivo).pipe(
      tap(response => {
        this.productos = response
      })
    ).subscribe();
    this.form2.get("idProducto").setValue("");
    this.form2.get("idProducto").setValidators(Validators.required);
  }

  getJefeProductos() {
    this.ordenInspeccionService.getJefeProductos().pipe(
      tap(response => {
        this.jefeProductos = response
      })
    ).subscribe();
  }

  getContratistas() {
    this.ordenInspeccionService.getContratistas().pipe(
      tap(response => {
        this.contratistas = response
      })
    ).subscribe();
  }

  store() {
    const form = 
                {
                  "idUsuario": this.usuarioService.usuarioStorage?.id,
                  "idOrden": this.idOrden,
                  "idOrdenInspeccion": this.idOrdenInspeccion,
                  ...this.form.value,
                  ...this.form2.value,
                }
    this.ordenInspeccionService.postOrdenInspeccion(form).pipe(
      tap((response) => !!response),
      tap(() => {this.botonGuardar = true}),
      tap((response) => this.nroOrden = response.nroOrden),
      tap((response) => this.snackBar.open(`Registro exitoso. Nro Orden: ${response.nroOrden} Nro Cuenta: ${this.nroCuenta}`, 'CERRAR')),
      // tap(() => this.router.navigate(['/gestion-ordenes/orden-trabajo']))
    ).subscribe();
  }

}
