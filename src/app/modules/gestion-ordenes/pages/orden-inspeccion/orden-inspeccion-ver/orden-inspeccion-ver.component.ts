import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { tap } from 'rxjs';
import { UsuarioService } from 'src/app/core/service/usuario.service';
import { Irregularidad } from '../../../models/irregularidad.model';
import { Tarea } from '../../../models/tarea.model';
import { OrdenInspeccionNotificadaService } from '../../../services/orden-inspeccion-notificada.service';
import beautify from "xml-beautifier";
import { MatTabChangeEvent } from '@angular/material/tabs';
import { TransferXmlService } from '../../../services/transfer-xml.service';

//Se declara funciones creadas en src/assets/custom-js/transfer-xml.js
declare function envioXml(valorEnvio): any;
declare function recepcionXml(message): any;

@Component({
  selector: 'app-orden-inspeccion-ver',
  templateUrl: './orden-inspeccion-ver.component.html',
  styleUrls: ['./orden-inspeccion-ver.component.scss']
})
export class OrdenInspeccionVerComponent implements OnInit {

  breadscrums = [
    {
      title: 'Ver Orden de Inspección',
      items: ['Home'],
      active: 'Ver Orden de Inspección'
    }
  ];

  form: FormGroup;

  nroServicio: string;
  nroCuenta: string;
  cliente: string;
  direccion: string;
  distrito: string;
  rutaLectura: string;
  nroNotificacion: string;
  fechaNotificacion: Date;
  contratista: string;
  ejecutor: string;
  estadoWkf: string;
  estadoTransicion: String; //REQ # 4 - JEGALARZA

  tipoInspecciones: any[];
  motivos: any[];
  productos: any[];
  jefeProductos: any[];
  contratistas: any[];

  idUrl: number;
  orden: any;
  idOrden: number = 0;
  idOrdenInspeccion: number = 0;
/* 
  xmlEnvio: any;
  xmlRecepcion: any; */
  xml: any 
  ordenTransfer: any = [];

  nroOrden: string = "";
  inicioCreacion: string = "SC4J";

  irregularidades: MatTableDataSource<Irregularidad> = new MatTableDataSource();
  tareas: MatTableDataSource<Tarea> = new MatTableDataSource();

  displayedColumns2 = [
    'nro',
    'codigo',
    'descripcion',
    'generaCNR',
    'generaOrdenNormalizacion',
    'causal'
  ];

  displayedColumns3 = [
    'select',
    'codAnormalidad',
    'codTarea',
    'tarea'
  ];

  
  @ViewChild('tabGroup') tabGroup;
  xmlEnvio: string ='';
  xmlRecepcion: string='';
  activeXmlEnvio: boolean = false;
  activexmlRecepcion: boolean = false;

  constructor(private fb: FormBuilder, 
              private ordenInspeccionService: OrdenInspeccionNotificadaService,
              private usuarioService : UsuarioService,
              private route : ActivatedRoute,
              private router: Router,
              private transferXml: TransferXmlService) { }

  ngOnInit(): void {
    this.idUrl = this.route.snapshot.params['id'];
    
    this.initForm();    

    if(this.idUrl) {
      this.getData(this.idUrl) 
      this.breadscrums = [
        {
          title: 'Ver de Orden de Inspección',
          items: ['Home'],
          active: 'Ver de Orden de Inspección'
        }
      ];
    }

    this.getTipoInspeccion();
    this.getMotivos();
    this.getJefeProductos();
    this.getContratistas();
/*     this.getXMLEnvio();
    this.getXMLRecepcion(); */
    this.getOrdenTransfer();
  }

  initForm() {
    this.form = this.fb.group({
      nroServicio: [{ value: !this.orden ? '' : this.orden.nroServicio, disabled: true }, ''],
      idTipoInspeccion: [{ value: !this.orden ? '' : this.orden.tipoInspeccion, disabled: true }, ''],
      denunciaRealizada: [{ value: !this.orden ? '' : this.orden.denunciaRealizada, disabled: true }, ''],
      idMotivo: [{ value: !this.orden ? '' : this.orden.idMotivo, disabled: true }, ''],
      idProducto: [{ value: !this.orden ? '' : this.orden.idProducto, disabled: true }, ''],
      idJefeProducto: [{ value: !this.orden ? '' : this.orden.idJefeProducto, disabled: true }, ''],
      idContratista: [{ value: !this.orden ? '' : this.orden.idContratista, disabled: true }, ''],
      entreCalles: [{ value: !this.orden ? '' : this.orden.entreCalles, disabled: true }, ''],
      informacionComplementaria: [{ value: !this.orden ? '' : this.orden.informacionComplementaria, disabled: true }, ''],
      autogenerado: [{ value: !this.orden ? '' : this.orden.autogenerado, disabled: true }, ''],
      observaciones: [{ value: !this.orden ? '' : this.orden.observacion, disabled: true }, '']
    })
  }


  
  tabChanged(tabChangeEvent: MatTabChangeEvent): void {

    if(tabChangeEvent.tab.textLabel=== 'DATOS ENVIO FORCEBEAT'){
      if(this.activeXmlEnvio == false) {
        this.obtenerDatosXmlEnvio();
        
      }
    }

    if(tabChangeEvent.tab.textLabel=== 'DATOS RECEPCIÓN FORCEBEAT'){
      if(this.activexmlRecepcion == false) {
        this.obtenerDatosXmlRecepcion();
       
      }
    }
  }


  obtenerDatosXmlEnvio(): void {
    
    this.transferXml.obtenerXmlEnvio(this.idUrl).pipe(
      tap((response: any) => {
        this.xmlEnvio = response.xml.replaceAll("\n", "");
        envioXml(this.xmlEnvio);
        this.activeXmlEnvio= true;
      })
    ).subscribe();
 
    }
  
    obtenerDatosXmlRecepcion(): void {
    
      this.transferXml.obtenerXmlRecepcion(this.idUrl).pipe(
        tap((response: any) => {
          this.xmlRecepcion = response.xml.replaceAll("\n", "");
          recepcionXml(this.xmlRecepcion);
          this.activexmlRecepcion= true;
        })
      ).subscribe();
   
      }

  getData(id: number) {
    this.ordenInspeccionService.getOrdenById(id, 0).pipe(
      tap(response => {
        this.orden = response;
        this.initForm();
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.direccion
        this.distrito = response.distrito
        this.rutaLectura = response.rutaLectura
        this.orden = response.idOrden
        this.idOrdenInspeccion = response.idOrdenInspeccion
        this.getProductos(response.idMotivo);
        this.nroOrden = response.nroOrden
        this.nroNotificacion = response.nroNotificacion
        this.fechaNotificacion = response.fechaNotificacion
        this.contratista = response.contratista
        this.ejecutor = response.ejecutor
        this.estadoWkf = response.estadoWkf
        this.estadoTransicion = response.estadoTrans //REQ # 4 - JEGALARZA
        if(response.idOrdenSc4j == 0) {
          this.inicioCreacion = "SCOM";
        }
        this.irregularidades = new MatTableDataSource<Irregularidad>(response.anormalidades);
        this.tareas = new MatTableDataSource<Tarea>(response.tareas);
      })
    ).subscribe();
  }

  buscarCliente() {
    const nroServicio = this.form.value.nroServicio
    this.ordenInspeccionService.getNroServicio(nroServicio).pipe(
      tap(response => {
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.direccion
        this.distrito = response.distrito
        this.rutaLectura = response.rutaLectura
      })
    ).subscribe();
  }

  getTipoInspeccion() {
    this.ordenInspeccionService.getTipoInspeccion().pipe(
      tap(response => {
        this.tipoInspecciones = response
      })
    ).subscribe();
  }

  getMotivos() {
    this.ordenInspeccionService.getMotivos().pipe(
      tap(response => {
        this.motivos = response
      })
    ).subscribe();
  }

  getProductos(idMotivo: number) {
    this.ordenInspeccionService.getProductos(idMotivo).pipe(
      tap(response => {
        this.productos = response
      })
    ).subscribe();
  }

  getJefeProductos() {
    this.ordenInspeccionService.getJefeProductos().pipe(
      tap(response => {
        this.jefeProductos = response
      })
    ).subscribe();
  }

  getContratistas() {
    this.ordenInspeccionService.getContratistas().pipe(
      tap(response => {
        this.contratistas = response
      })
    ).subscribe();
  }

  store() {
    const form = 
                {
                  "idUsuario": this.usuarioService.usuarioStorage?.id,
                  "idOrden": this.idOrden,
                  "idOrdenInspeccion": this.idOrdenInspeccion,
                  ...this.form.value,
                }
    this.ordenInspeccionService.postOrdenInspeccion(form).subscribe();
    this.router.navigate(['/gestion-ordenes/orden-trabajo']);
  }

  getXMLEnvio() {
    this.ordenInspeccionService.getXML(this.idUrl, 'ENVIO').pipe(
      tap(response => {
        if(response != null) {
          this.xmlEnvio = beautify(response.xml)
        }else {
          this.xmlEnvio = ""
        }
      })
    ).subscribe();
  }

  getXMLRecepcion() {
    this.ordenInspeccionService.getXML(this.idUrl, 'RECEPCION').pipe(
      tap(response => {
        if(response != null) {
          this.xmlRecepcion = beautify(response.xml)
        }else {
          this.xmlRecepcion = ""
        }
      })
    ).subscribe();
  }

  getOrdenTransfer() {
    this.ordenInspeccionService.getOrdenTransfer(this.idUrl).pipe(
      tap(response => {
        if(response != '') {
          this.ordenTransfer = response[0]
        }else {
          this.ordenTransfer = []
        }
      })
    ).subscribe();
  }

}
