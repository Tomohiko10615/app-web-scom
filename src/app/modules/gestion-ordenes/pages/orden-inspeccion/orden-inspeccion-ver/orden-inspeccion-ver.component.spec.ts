import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenInspeccionVerComponent } from './orden-inspeccion-ver.component';

describe('OrdenInspeccionVerComponent', () => {
  let component: OrdenInspeccionVerComponent;
  let fixture: ComponentFixture<OrdenInspeccionVerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenInspeccionVerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenInspeccionVerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
