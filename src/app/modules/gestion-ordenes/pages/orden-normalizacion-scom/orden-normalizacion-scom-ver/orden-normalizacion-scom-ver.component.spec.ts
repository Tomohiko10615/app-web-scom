import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenNormalizacionScomVerComponent } from './orden-normalizacion-scom-ver.component';

describe('OrdenNormalizacionScomVerComponent', () => {
  let component: OrdenNormalizacionScomVerComponent;
  let fixture: ComponentFixture<OrdenNormalizacionScomVerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenNormalizacionScomVerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenNormalizacionScomVerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
