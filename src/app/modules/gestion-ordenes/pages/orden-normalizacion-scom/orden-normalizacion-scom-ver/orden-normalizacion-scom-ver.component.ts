import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { tap } from 'rxjs';
import { Irregularidad } from '../../../models/irregularidad.model';
import { OrdenInspeccionNotificada } from '../../../models/orden-inspeccion-notificada.model';
import { OrdenTrabajoScomDetalle } from '../../../models/orden-trabajo-scom-detalle.model';
import { Tarea } from '../../../models/tarea.model';
import { OrdenInspeccionNotificadaService } from '../../../services/orden-inspeccion-notificada.service';
import { OrdenNormalizacionService } from '../../../services/orden-normalizacion.service';
import { Lectura, Medidor } from '../../../models/medidor.model';
import { TareaXml } from '../../../models/tarea-xml.model';

@Component({
  selector: 'app-orden-normalizacion-scom-ver',
  templateUrl: './orden-normalizacion-scom-ver.component.html',
  styleUrls: ['./orden-normalizacion-scom-ver.component.css']
})
export class OrdenNormalizacionScomVerComponent implements OnInit {

  breadscrums = [
    {
      title: 'Creación de Orden de Normalización',
      items: ['Home'],
      active: 'Creación de Orden de Normalización'
    }
  ];

  filterToggle = false;

  displayedColumns = [
    'select',
    'nro',
    'nroOrden',
    'fechaCreacion',
    'contratista',
    'tipoInspeccion'
  ];

  displayedColumns2 = [
    'id',
    'codigo',
    'descripcion',
    'generaCNR',
    'generaOrdenNormalizacion'
  ];

  displayedColumns3 = [
    'select',
    'codAnormalidad',
    'codTarea',
    'tarea',
    //'predeterminada',
    'ejecutada'
  ];

  // R.I. REQSCOM03 14/07/2023 INICIO
  displayedColumns4 = [
    'accionMedidor',
    'marcaMedidor',
    'modeloMedidor',
    'numeroMedidor',
    'detalles'
  ];

  displayedColumns5 = [
    'desTarea',
    //'predeterminada',
    'ejecutada',
  ];
  displayedColumnsDetalles = ['tipoLectura', 'horarioLectura', 'estadoLeido', 'fechaLectura'];
  detallesMedidor: Lectura[] = [];
  medidorSeleccionado: Medidor | null = null; 
  // R.I. REQSCOM03 14/07/2023 FIN

  ordenInspeccionNotificadas: MatTableDataSource<OrdenInspeccionNotificada> = new MatTableDataSource();
  irregularidades: MatTableDataSource<Irregularidad> = new MatTableDataSource();
  tareas: MatTableDataSource<Tarea> = new MatTableDataSource();
  
  form: FormGroup;

  nroServicio: string;
  nroCuenta: string;
  cliente: string;
  direccion: string;
  distrito: string;
  rutaLectura: string;
  estadoWkf: string;
  nroNotificacion: string;
  fechaNotificacion: Date;

  // R.I. REQSCOM08 12/07/2023 INICIO
  fechaCreacionOrden: string
  fechaEjecucion: string
  observacionTecnico: string
  // R.I. REQSCOM08 12/07/2023 FIN

  // R.I. REQSCOM02 13/07/2023 INICIO
  estadoLeidoEncontrado: string
  estadoLeidoInstalado: string
  // R.I. REQSCOM02 13/07/2023 INICIO

  // R.I. REQSCOM03 14/07/2023 INICIO
  codigoResultado: string
  seCambioElMedidor: string
  codigoContratista: string
  nroNotificacionInspeccion: string
  codidoTdcInspeccionAsociada: string
  tipoAnomalia: string
  medidores: MatTableDataSource<Medidor> = new MatTableDataSource();
  tareasXml: MatTableDataSource<TareaXml> = new MatTableDataSource();
  // R.I. REQSCOM03 14/07/2023 FIN

  anormalidades: any[];
  subtipos: any[];

  dataIrregularidad = new Array();
  dataTarea = new Array();

  idOrden: number = 0;
  idOrdenNormalizacion: number = 0;
  idOrdenInspeccion: number = 0;

  idUrl: number;
  orden: any;
  botonBuscar: boolean = true;
  botonSubmit: string = "Guardar";

/*   xmlEnvio: any;
  xmlRecepcion: any; */
  xml: any 
  ordenTransfer: any = [];

  nroOrden: string = "";
  inicioCreacion: string = "SC4J";

  constructor(private fb: FormBuilder, 
              private ordenNormalizacionService: OrdenNormalizacionService,
              private ordenInspeccionService: OrdenInspeccionNotificadaService,
              private route : ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.idUrl = this.route.snapshot.params['id'];

    this.initForm();
    this.getAnormalidades();
    this.getSubTipos();

    if(this.idUrl) {
      this.getData();
      this.breadscrums = [
        {
          title: 'Ver de Orden de Normalización',
          items: ['Home'],
          active: 'Ver de Orden de Normalización'
        }
      ];
      this.botonBuscar = false;
      this.botonSubmit = "Actualizar";
    }
  }

  getData() {
    this.ordenNormalizacionService.obtenerDetalleOrdenNormalizacion(this.idUrl).pipe(
      tap(response => {
        console.log(response);
        this.orden = response;
        this.initForm();
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.direccion
        this.distrito = response.distrito
        this.rutaLectura = response.rutaLectura
        this.idOrdenInspeccion = response.idOrdenInspeccion
        this.idOrden = response.idOrden
        this.idOrdenNormalizacion = response.idOrdenNormalizacion
        this.form.value.nroServicio = response.nroServicio
        this.nroOrden = response.nroOrden
        this.nroNotificacion = response.nroNotificacion
        this.fechaNotificacion = response.fechaNotificacion
        this.estadoWkf = response.estadoWkf
        // R.I. REQSCOM08 12/07/2023 INICIO
        this.fechaCreacionOrden = response.fechaCreacionOrden
        this.fechaEjecucion = response.fechaEjecucion
        this.observacionTecnico = response.observacionTecnico
        // R.I. REQSCOM08 12/07/2023 FIN
        // R.I. REQSCOM02 13/07/2023 INICIO
        this.estadoLeidoEncontrado = response.estadoLeidoEncontrado
        this.estadoLeidoInstalado = response.estadoLeidoInstalado
        // R.I. REQSCOM02 13/07/2023 FIN
        // R.I. REQSCOM03 14/07/2023 INICIO
        this.codigoResultado = response.codigoResultado
        this.seCambioElMedidor = response.seCambioElMedidor
        this.codigoContratista = response.codigoContratista
        this.nroNotificacionInspeccion = response.nroNotificacionInspeccion
        this.codidoTdcInspeccionAsociada = response.codidoTdcInspeccionAsociada
        this.tipoAnomalia = response.tipoAnomalia
        this.medidores = new MatTableDataSource<Medidor>(response.medidores);
        //this.tareasXml = new MatTableDataSource<TareaXml>(response.tareasXml);
        // R.I. REQSCOM03 14/07/2023 FIN
        this.buscarCliente();
        this.check(response.idOrdenInspeccion);
        if(response.idOrdenSc4j == 0) {
          this.inicioCreacion = "SCOM";
        }
        response.anormalidades.forEach((element: any) => {
          const dataAnormalidad = 
                        { 
                          'id': element.id,
                          'codigo' : element.codigo,
                          'descripcion': element.descripcion,
                          'generaCNR': element.generaCNR,
                          'generaOrdenNormalizacion': element.generaOrdenNormalizacion,
                          'causal': element.causal
                        };
          this.dataIrregularidad.push(dataAnormalidad);
        });
        this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
        response.tareas.forEach((element: any) => {
          const dataTareas = 
                        { 
                          'id': element.id,
                          'idTarea' : element.idTarea,
                          'idAnormalidad': element.idAnormalidad,
                          'tarea': element.tarea,
                          'codAnormalidad': element.codAnormalidad,
                          'codTarea': element.codTarea,
                          'tareaEstado': element.tareaEstado,
                          //'predeterminada': element.predeterminada,
                          'ejecutada': element.ejecutada
                        };
          this.dataTarea.push(dataTareas);
        });
        this.tareas = new MatTableDataSource<Tarea>(this.dataTarea);
      })
    ).subscribe();
  }

  initForm() {
    this.form = this.fb.group({
      nroServicio: [{ value: !this.orden ? '' : this.orden.nroServicio, disabled: true}, Validators.required],
      subtipo: [{ value: !this.orden ? '' : this.orden.subtipo, disabled: true}, Validators.required],
      observaciones: [{ value: !this.orden ? '' : this.orden.observacion, disabled: true}, Validators.required]
    })
  }

  buscarCliente() {
    this.irregularidades = new MatTableDataSource<Irregularidad>();
    this.tareas = new MatTableDataSource<Tarea>();
    const nroServicio = this.form.value.nroServicio
    this.ordenNormalizacionService.getNroServicio(nroServicio).pipe(
      tap(response => {
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.direccion
        this.distrito = response.distrito
        this.rutaLectura = response.rutaLectura
        this.ordenInspeccionNotificadas = new MatTableDataSource<OrdenInspeccionNotificada>(response.inspecciones);
      })
    ).subscribe();
  }

  getAnormalidades() {
    this.ordenNormalizacionService.getAnormalidades().pipe(
      tap(response => {
        this.anormalidades = response
      })
    ).subscribe();
  }

  getSubTipos() {
    this.ordenNormalizacionService.getSubTipos().pipe(
      tap(response => {
        this.subtipos = response
      })
    ).subscribe();
  }

  check(id: number) {
    this.idOrdenInspeccion = id;
    /*
    this.irregularidades = new MatTableDataSource<Irregularidad>();
    this.tareas = new MatTableDataSource<Tarea>();
    this.dataTarea = [];
    this.dataIrregularidad = [];
    this.ordenNormalizacionService.getOrden(id, 1).pipe(
      tap(response => {
        response.anormalidades.forEach((element: any) => {
          const dataAnormalidad = 
                        { 
                          'id': element.id,
                          'codigo' : element.codigo,
                          'descripcion': element.descripcion,
                          'generaCNR': element.generaCNR,
                          'generaOrdenNormalizacion': element.generaOrdenNormalizacion,
                          'causal': element.causal
                        };
          this.dataIrregularidad.push(dataAnormalidad);
        });
        this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
        response.tareas.forEach((element: any) => {
          const dataTareas = 
                        { 
                          'id': element.id,
                          'idTarea' : element.idTarea,
                          'idAnormalidad': element.idAnormalidad,
                          'tarea': element.tarea,
                          'codAnormalidad': element.codAnormalidad,
                          'codTarea': element.codTarea,
                          'tareaEstado': element.tareaEstado
                        };
          this.dataTarea.push(dataTareas);
        });
        this.tareas = new MatTableDataSource<Tarea>(this.dataTarea);
      })
    ).subscribe(); */
  }

  noRealizado(id: number) {
    const tarea = {
      id: id,
      tareaEstado: 'N'
    }
    this.ordenNormalizacionService.postCambiarEstado(tarea).subscribe();
  }

  realizado(id: number) {
    const tarea = {
      id: id,
      tareaEstado: 'S'
    }
    this.ordenNormalizacionService.postCambiarEstado(tarea).subscribe();
  }

  getTareas() {
    const medida = this.form.value.idAnormalidad.split('-');
    
    const dataAnormalidad = 
                        { 
                          'id': medida[0],
                          'codigo' : medida[1],
                          'descripcion': medida[2],
                          'generaCNR': medida[3],
                          'generaOrdenNormalizacion': medida[4],
                          'causal': medida[5]
                        };
    this.dataIrregularidad.push(dataAnormalidad);
    this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
    this.ordenNormalizacionService.getTareas(medida[0]).pipe(
      tap(response => {
        response.forEach((element: any) => {
          const dataTareas = 
                        { 
                          'id': element.id,
                          'idTarea' : element.idTarea,
                          'idAnormalidad': element.idAnormalidad,
                          'tarea': element.tarea,
                          'codAnormalidad': element.codAnormalidad,
                          'codTarea': element.codTarea,
                          'tareaEstado': 'N'
                        };
          this.dataTarea.push(dataTareas);
        });
        this.tareas = new MatTableDataSource<Tarea>(this.dataTarea);
      })
    ).subscribe();
  }

  remove(i: number) {
    this.dataIrregularidad.splice(i, 1);
    this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
  }

// R.I. CORRECCION 13/09/2023 INICIO
  mostrarDetalles(row: Medidor) {
    // Cambiar el estado de detallesVisible solo para el medidor seleccionado
    if (this.medidorSeleccionado === row) {
      this.medidorSeleccionado = null; // Cerrar detalles si se hace clic nuevamente
    } else {
      this.medidorSeleccionado = row; // Establecer el medidor seleccionado
      this.detallesMedidor = row.lecturas;
    }
}
// R.I. CORRECCION 13/09/2023 FIN

}
