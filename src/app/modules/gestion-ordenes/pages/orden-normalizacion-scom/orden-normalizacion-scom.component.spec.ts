import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenNormalizacionScomComponent } from './orden-normalizacion-scom.component';

describe('OrdenNormalizacionScomComponent', () => {
  let component: OrdenNormalizacionScomComponent;
  let fixture: ComponentFixture<OrdenNormalizacionScomComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenNormalizacionScomComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenNormalizacionScomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
