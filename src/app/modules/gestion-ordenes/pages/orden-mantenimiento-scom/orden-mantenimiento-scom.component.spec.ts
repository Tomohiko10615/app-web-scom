import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenMantenimientoScomComponent } from './orden-mantenimiento-scom.component';

describe('OrdenMantenimientoScomComponent', () => {
  let component: OrdenMantenimientoScomComponent;
  let fixture: ComponentFixture<OrdenMantenimientoScomComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenMantenimientoScomComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenMantenimientoScomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
