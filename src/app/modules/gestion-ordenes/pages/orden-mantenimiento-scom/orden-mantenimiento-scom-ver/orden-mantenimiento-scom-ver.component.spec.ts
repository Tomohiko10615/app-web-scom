import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenMantenimientoScomVerComponent } from './orden-mantenimiento-scom-ver.component';

describe('OrdenMantenimientoScomVerComponent', () => {
  let component: OrdenMantenimientoScomVerComponent;
  let fixture: ComponentFixture<OrdenMantenimientoScomVerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenMantenimientoScomVerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenMantenimientoScomVerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
