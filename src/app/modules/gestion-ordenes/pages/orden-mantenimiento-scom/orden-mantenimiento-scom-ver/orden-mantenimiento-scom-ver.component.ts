import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { tap } from 'rxjs';
import { UsuarioService } from 'src/app/core/service/usuario.service';
import { Irregularidad } from '../../../models/irregularidad.model';
import { OrdenInspeccionNotificadaService } from '../../../services/orden-inspeccion-notificada.service';
import { OrdenMantenimientoService } from '../../../services/orden-mantenimiento.service';
import { Lectura, Medidor } from '../../../models/medidor.model';

@Component({
  selector: 'app-orden-mantenimiento-scom-ver',
  templateUrl: './orden-mantenimiento-scom-ver.component.html',
  styleUrls: ['./orden-mantenimiento-scom-ver.component.sass']
})
export class OrdenMantenimientoScomVerComponent implements OnInit {
  breadscrums = [
    {
      title: 'Ver de Orden de Mantenimiento',
      items: ['Home'],
      active: 'Ver de Orden de Mantenimiento'
    }
  ];

  tipoResponsable = [
    {
      id: 'Usuario',
      descripcion: 'Usuario'
    },
    {
      id: 'Area',
      descripcion: 'Area'
    },
    {
      id: 'Rol',
      descripcion: 'Rol'
    }
  ]

  displayedColumns2 = [
    'id',
    'codigo',
    'tarea',
    'tipoTarea'
  ];

  form: FormGroup;

  nroServicio: string;
  nroCuenta: string;
  cliente: string;
  direccion: string;
  distrito: string;
  rutaLectura: string;
  tipoAcometida: string;
  documento: string;
  estadoWkf: string;

  // R.I. REQSCOM08 12/07/2023 INICIO
  fechaCreacionOrden: string
  fechaEjecucion: string
  observacionTecnico: string
  nroOrden: string
  // R.I. REQSCOM08 12/07/2023 FIN

  // R.I. REQSCOM06 16/10/2023 INICIO
  medidores: MatTableDataSource<Medidor> = new MatTableDataSource();
  displayedColumns3 = [
    'accionMedidor',
    'marcaMedidor',
    'modeloMedidor',
    'numeroMedidor',
    'detalles'
  ];

  displayedColumnsDetalles = ['tipoLectura', 'horarioLectura', 'estadoLeido', 'fechaLectura'];
  detallesMedidor: Lectura[] = [];
  medidorSeleccionado: Medidor | null = null; 
  // R.I. REQSCOM06 16/10/2023 FIN

  trabajos: any[];
  prioridades: any[];
  tareas: any[];
  tipos: any[];
  responsables: any[] = [];
  irregularidades: MatTableDataSource<Irregularidad> = new MatTableDataSource();
  
  dataIrregularidad = new Array();

  idUrl: number;
  orden: any;
  idOrden: number = 0;

  botonBuscar: boolean = true;
  botonSubmit: string = "Guardar";

 /*  xmlEnvio: any;
  xmlRecepcion: any; */
  xml: any 
  ordenTransfer: any = [];

  constructor(private fb: FormBuilder, 
              private ordenMantenimiento: OrdenMantenimientoService,
              private ordenInspeccionService: OrdenInspeccionNotificadaService,
              private usuarioService : UsuarioService,
              private route : ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    this.idUrl = this.route.snapshot.params['id'];
    
    this.initForm();    

    if(this.idUrl) {
      this.getData(this.idUrl) 
      this.breadscrums = [
        {
          title: 'Ver de Orden de Mantenimiento',
          items: ['Home'],
          active: 'Ver de Orden de Mantenimiento'
        }
      ];
      this.botonBuscar = false;
      this.botonSubmit = "Actualizar";
    }

    this.getTrabajos();
    this.getPrioridades();
    this.getTipos();
  }

  initForm() {
    this.form = this.fb.group({
      nroCuenta: [{ value: !this.orden ? '' : this.orden.nroServicio, disabled: true}, Validators.required],
      idTrabajo: [{ value: !this.orden ? '' : this.orden.idTrabajo, disabled: true}, Validators.required],
      idPrioridad: [{ value: !this.orden ? '' : this.orden.idPrioridad, disabled: true}, Validators.required],
      idTipoResponsable: [{ value: !this.orden ? '' : this.orden.tipoResponsable, disabled: true}, Validators.required],
      idResponsable: [{ value: !this.orden ? '' : this.orden.idResponsable, disabled: true}, Validators.required],
      observaciones: [{ value: !this.orden ? '' : this.orden.observacion, disabled: true}, Validators.required]
    })
  }
  getData(id: number) {
    this.ordenMantenimiento.obtenerDetalleOrdenMantenimiento(id).pipe(
      tap(response => {
        this.orden = response;
        this.getResponsable(response.tipoResponsable)
        this.initForm();
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.textoDireccion
        this.distrito = response.distrito
        this.rutaLectura = response.codRuta
        this.orden = response.idOrden
        this.documento = response.documento
        this.tipoAcometida = response.tipoAcometida
        this.direccion = response.direccion
        this.rutaLectura = response.rutaLectura
        this.estadoWkf = response.estadoWkf
        // R.I. REQSCOM08 12/07/2023 INICIO
        this.fechaCreacionOrden = response.fechaCreacionOrden
        this.fechaEjecucion = response.fechaEjecucion
        this.observacionTecnico = response.observacionTecnico
        this.nroOrden = response.nroOrden
        // R.I. REQSCOM08 12/07/2023 FIN
        // R.I. REQSCOM06 16/10/2023 INICIO
        this.medidores = new MatTableDataSource<Medidor>(response.medidores);
          // R.I. REQSCOM06 16/10/2023 FIN
        response.ormTareas.forEach(element => {
          const dataAnormalidad = 
                        { 
                          'idTarea': element.idTarea,
                          'codigo' : element.codigo,
                          'tarea': element.tarea,
                          'tipoTarea': element.tipoTarea
                        };
          this.dataIrregularidad.push(dataAnormalidad);
          this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);  
        });
      })
    ).subscribe();
  }

  buscarCliente() {
    const nroCuenta = this.form.value.nroCuenta
    this.ordenMantenimiento.getNroCuenta(nroCuenta).pipe(
      tap(response => {
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.direccion
        this.distrito = response.distrito
        this.rutaLectura = response.codRuta
        this.documento = response.documento
        this.tipoAcometida = response.tipoAcometida        
      })
    ).subscribe();
  }

  getTrabajos() {
    this.ordenMantenimiento.getTrabajos().pipe(
      tap(response => {
        this.trabajos = response
      })
    ).subscribe();
  }

  getPrioridades() {
    this.ordenMantenimiento.getPrioridades().pipe(
      tap(response => {
        this.prioridades = response
      })
    ).subscribe();
  }

  getTipos() {
    this.ordenMantenimiento.getTipos().pipe(
      tap(response => {
        this.tipos = response
      })
    ).subscribe();
  }

  getOrmTareas(idTipo: number) {
    this.ordenMantenimiento.getOrmTareas(idTipo).pipe(
      tap(response => {
        this.tareas = response
      })
    ).subscribe();
  }

  getResponsable(event: any) {
    this.ordenMantenimiento.getResponsable(event).pipe(
      tap(response => {
        console.log(response)
        this.responsables = response
      })
    ).subscribe();
  }

  getTareas() {
    const tarea = this.form.value.idTarea.split('-');
    
    const dataAnormalidad = 
                        { 
                          'idTarea': tarea[0],
                          'codigo' : tarea[1],
                          'tarea': tarea[2],
                          'tipoTarea': tarea[3]
                        };
    this.dataIrregularidad.push(dataAnormalidad);
    this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
  }

  remove(i: number, irregularidad: any) {
    this.dataIrregularidad.splice(i, 1);
    this.irregularidades = new MatTableDataSource<Irregularidad>(this.dataIrregularidad);
  }

  // R.I. CORRECCION 13/09/2023 INICIO
  mostrarDetalles(row: Medidor) {
    // Cambiar el estado de detallesVisible solo para el medidor seleccionado
    if (this.medidorSeleccionado === row) {
      this.medidorSeleccionado = null; // Cerrar detalles si se hace clic nuevamente
    } else {
      this.medidorSeleccionado = row; // Establecer el medidor seleccionado
      this.detallesMedidor = row.lecturas;
    }
  }
  // R.I. CORRECCION 13/09/2023 FIN

}
