import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenInspeccionScomVerComponent } from './orden-inspeccion-scom-ver.component';

describe('OrdenInspeccionScomVerComponent', () => {
  let component: OrdenInspeccionScomVerComponent;
  let fixture: ComponentFixture<OrdenInspeccionScomVerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenInspeccionScomVerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenInspeccionScomVerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
