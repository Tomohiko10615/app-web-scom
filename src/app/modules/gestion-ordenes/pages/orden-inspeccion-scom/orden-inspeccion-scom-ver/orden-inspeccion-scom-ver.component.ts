import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { tap } from 'rxjs';
import { UsuarioService } from 'src/app/core/service/usuario.service';
import { Irregularidad } from '../../../models/irregularidad.model';
import { Tarea } from '../../../models/tarea.model';
import { OrdenInspeccionNotificadaService } from '../../../services/orden-inspeccion-notificada.service';

@Component({
  selector: 'app-orden-inspeccion-scom-ver',
  templateUrl: './orden-inspeccion-scom-ver.component.html',
  styleUrls: ['./orden-inspeccion-scom-ver.component.css']
})
export class OrdenInspeccionScomVerComponent implements OnInit {

  breadscrums = [
    {
      title: 'Ver Orden de Inspección',
      items: ['Home'],
      active: 'Ver Orden de Inspección'
    }
  ];

  form: FormGroup;

  nroServicio: string;
  nroCuenta: string;
  cliente: string;
  direccion: string;
  distrito: string;
  rutaLectura: string;
  nroNotificacion: string;
  fechaNotificacion: Date;
  contratista: string;
  ejecutor: string;
  estadoWkf: string;

  // R.I. REQSCOM08 11/07/2023 INICIO
  fechaCreacionOrden: string
  fechaEjecucion: string
  observacionTecnico: string
  // R.I. REQSCOM08 11/07/2023 FIN
  // R.I. CORRECCION 11/09/2023 INICIO
  tipoInspeccion: string;
  estadoTrans: string
  codigoResultado: string;
  nroInformeInspeccion: string;
  motivoInspeccion: string;
  identificadorSuministro: string;
  trabajoCoordinado: string;
  apruebaInspeccion: string;
  fechaNotificacionAvisoIntervencion: string;
  registroCumplimientoArt171RLCE: string;
  registroCumplimientoNumeral71NormaReintegrosRecuperos: string;
  evalGeneralConexionElectrica: string;
  evalSistemaMedicion: string;
  fechaPropuestaInicioAvisoIntervencion: string;
  horaPropuestaAvisoPrevioIntervencion: string;
  fechaRecepcionAvisoPrevioIntervencion: string;
  horaRecepcionAvisoPrevioIntervencion: string;
  pruebaVacioAD: string;
  stickerContraste: string;
  sinMicaMicaRota: string;
  cajaSinTapaTapaMalEstado: string;
  sinCerraduraCerraduraMalEstado: string;
  estadoConexion: string;
  clientePermiteInventarioCarga: string;
  cortadoArt90: string;
  estadoCajaPortaMedidor: string;
  tomaConReja: string;
  cajaConReja: string;  
  cargas: any[];
  // R.I. CORRECCION 11/09/2023 FIN
  tipoInspecciones: any[];
  motivos: any[];
  productos: any[];
  jefeProductos: any[];
  contratistas: any[];

  idUrl: number;
  orden: any;
  idOrden: number = 0;
  idOrdenInspeccion: number = 0;
/* 
  xmlEnvio: any;
  xmlRecepcion: any; */
  xml: any 
  ordenTransfer: any = [];

  nroOrden: string = "";
  inicioCreacion: string = "SC4J";

  irregularidades: MatTableDataSource<Irregularidad> = new MatTableDataSource();
  tareas: MatTableDataSource<Tarea> = new MatTableDataSource();

  displayedColumns2 = [
    'nro',
    'codigo',
    'descripcion',
    'generaCNR',
    'generaOrdenNormalizacion',
    'causal'
  ];

  // R.I. CORRECCION 11/09/2023 INICIO
  displayedColumnsCargas = [
    'nro',
    'pruebas_Carga_en_el_primario',
    'fase'
  ];
  // R.I. CORRECCION 11/09/2023 FIN

  displayedColumns3 = [
    'select',
    'codAnormalidad',
    'codTarea',
    'tarea'
  ];
  activeXmlEnvio: boolean;
  activexmlRecepcion: boolean;

  constructor(private fb: FormBuilder, 
              private ordenInspeccionService: OrdenInspeccionNotificadaService,
              private usuarioService : UsuarioService,
              private route : ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    this.idUrl = this.route.snapshot.params['id'];
    
    this.initForm();    

    if(this.idUrl) {
      this.getData(this.idUrl) 
      this.breadscrums = [
        {
          title: 'Ver de Orden de Inspección',
          items: ['Home'],
          active: 'Ver de Orden de Inspección'
        }
      ];
    }

    this.getTipoInspeccion();
    this.getMotivos();
    this.getJefeProductos();
    this.getContratistas();
  }

  initForm() {
    this.form = this.fb.group({
      nroServicio: [{ value: !this.orden ? '' : this.orden.nroServicio, disabled: true }, ''],
      idTipoInspeccion: [{ value: !this.orden ? '' : this.orden.tipoInspeccion, disabled: true }, ''],
      denunciaRealizada: [{ value: !this.orden ? '' : this.orden.denunciaRealizada, disabled: true }, ''],
      idMotivo: [{ value: !this.orden ? '' : this.orden.idMotivo, disabled: true }, ''],
      idProducto: [{ value: !this.orden ? '' : this.orden.idProducto, disabled: true }, ''],
      idJefeProducto: [{ value: !this.orden ? '' : this.orden.idJefeProducto, disabled: true }, ''],
      idContratista: [{ value: !this.orden ? '' : this.orden.idContratista, disabled: true }, ''],
      entreCalles: [{ value: !this.orden ? '' : this.orden.entreCalles, disabled: true }, ''],
      informacionComplementaria: [{ value: !this.orden ? '' : this.orden.informacionComplementaria, disabled: true }, ''],
      autogenerado: [{ value: !this.orden ? '' : this.orden.autogenerado, disabled: true }, ''],
      observaciones: [{ value: !this.orden ? '' : this.orden.observacion, disabled: true }, ''],
      // R.I. REQSCOM08 11/07/2023 INICIO
      fechaCreacionOrden: [{ value: !this.orden ? '' : this.orden.fechaCreacionOrden, disabled: true }, ''],
      fechaEjecucion: [{ value: !this.orden ? '' : this.orden.fechaEjecucion, disabled: true }, ''],
      observacionTecnico: [{ value: !this.orden ? '' : this.orden.bservacionTecnico, disabled: true }, ''],
      // R.I. REQSCOM08 11/07/2023 FIN
    })
  }

  getData(id: number) {
    this.ordenInspeccionService.obtenerDetalleOrdenInspeccion(id).pipe(
      tap(response => {
        this.orden = response;
        this.initForm();
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.direccion
        this.distrito = response.distrito
        this.rutaLectura = response.rutaLectura
        this.orden = response.idOrden
        this.idOrdenInspeccion = response.idOrdenInspeccion
        this.getProductos(response.idMotivo);
        this.nroOrden = response.nroOrden
        this.nroNotificacion = response.nroNotificacion
        this.fechaNotificacion = response.fechaNotificacion
        this.contratista = response.contratista
        this.ejecutor = response.ejecutor
        this.estadoWkf = response.estadoWkf
        if(response.idOrdenSc4j == 0) {
          this.inicioCreacion = "SCOM";
        }
        this.irregularidades = new MatTableDataSource<Irregularidad>(response.anormalidades);
        this.tareas = new MatTableDataSource<Tarea>(response.tareas);
        // R.I. REQSCOM08 11/07/2023 INICIO
        this.fechaCreacionOrden = response.fechaCreacionOrden
        this.fechaEjecucion = response.fechaEjecucion
        this.observacionTecnico = response.observacionTecnico
        // R.I. REQSCOM08 11/07/2023 FIN
        // R.I. CORRECCION 11/09/2023 INICIO
        this.tipoInspeccion = response.tipoInspeccion
        this.estadoTrans = response.estadoTrans
        this.codigoResultado = response.codigoResultado;
        this.nroInformeInspeccion = response.nroInformeInspeccion;
        this.motivoInspeccion = response.motivoInspeccion;
        this.identificadorSuministro = response.identificadorSuministro;
        this.trabajoCoordinado = response.trabajoCoordinado;
        this.apruebaInspeccion = response.apruebaInspeccion;
        this.fechaNotificacionAvisoIntervencion = response.fechaNotificacionAvisoIntervencion;
        this.registroCumplimientoArt171RLCE = response.registroCumplimientoArt171RLCE;
        this.registroCumplimientoNumeral71NormaReintegrosRecuperos = response.registroCumplimientoNumeral71NormaReintegrosRecuperos;
        this.evalGeneralConexionElectrica = response.evalGeneralConexionElectrica;
        this.evalSistemaMedicion = response.evalSistemaMedicion;
        this.fechaPropuestaInicioAvisoIntervencion = response.fechaPropuestaInicioAvisoIntervencion;
        this.horaPropuestaAvisoPrevioIntervencion = response.horaPropuestaAvisoPrevioIntervencion;
        this.fechaRecepcionAvisoPrevioIntervencion = response.fechaRecepcionAvisoPrevioIntervencion;
        this.horaRecepcionAvisoPrevioIntervencion = response.horaRecepcionAvisoPrevioIntervencion;
        this.pruebaVacioAD = response.pruebaVacioAD;
        this.stickerContraste = response.stickerContraste;
        this.sinMicaMicaRota = response.sinMicaMicaRota;
        this.cajaSinTapaTapaMalEstado = response.cajaSinTapaTapaMalEstado;
        this.sinCerraduraCerraduraMalEstado = response.sinCerraduraCerraduraMalEstado;
        this.estadoConexion = response.estadoConexion;
        this.clientePermiteInventarioCarga = response.clientePermiteInventarioCarga;
        this.cortadoArt90 = response.cortadoArt90;
        this.estadoCajaPortaMedidor = response.estadoCajaPortaMedidor;
        this.tomaConReja = response.tomaConReja;
        this.cajaConReja = response.cajaConReja;
        this.cargas = response.cargas;
        // R.I. CORRECCION 11/09/2023 FIN
      })
    ).subscribe();
  }

  buscarCliente() {
    const nroServicio = this.form.value.nroServicio
    this.ordenInspeccionService.getNroServicio(nroServicio).pipe(
      tap(response => {
        this.nroServicio = response.nroServicio
        this.nroCuenta = response.nroCuenta
        if(response.apellidoPat == null || response.apellidoMat == null) {
          this.cliente = response.nombre
        }else {
          this.cliente = response.nombre + ' ' + response.apellidoPat + ' ' + response.apellidoMat
        }
        this.direccion = response.direccion
        this.distrito = response.distrito
        this.rutaLectura = response.rutaLectura
      })
    ).subscribe();
  }

  getTipoInspeccion() {
    this.ordenInspeccionService.getTipoInspeccion().pipe(
      tap(response => {
        this.tipoInspecciones = response
      })
    ).subscribe();
  }

  getMotivos() {
    this.ordenInspeccionService.getMotivos().pipe(
      tap(response => {
        this.motivos = response
      })
    ).subscribe();
  }

  getProductos(idMotivo: number) {
    this.ordenInspeccionService.getProductos(idMotivo).pipe(
      tap(response => {
        this.productos = response
      })
    ).subscribe();
  }

  getJefeProductos() {
    this.ordenInspeccionService.getJefeProductos().pipe(
      tap(response => {
        this.jefeProductos = response
      })
    ).subscribe();
  }

  getContratistas() {
    this.ordenInspeccionService.getContratistas().pipe(
      tap(response => {
        this.contratistas = response
      })
    ).subscribe();
  }

}
