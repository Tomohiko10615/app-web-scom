import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdenInspeccionScomComponent } from './orden-inspeccion-scom.component';

describe('OrdenInspeccionScomComponent', () => {
  let component: OrdenInspeccionScomComponent;
  let fixture: ComponentFixture<OrdenInspeccionScomComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdenInspeccionScomComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdenInspeccionScomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
