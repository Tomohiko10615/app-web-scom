import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { OrdenTrabajoScomDetalle } from '../models/orden-trabajo-scom-detalle.model';

@Injectable({
  providedIn: 'root'
})
export class OrdenContrasteService {

  private readonly API_CLIENTE = `${environment.apiUrlGestion}/api/cliente`;
  private readonly API_ORDEN = `${environment.apiUrlGestion}/api/orden`;
  private readonly API_ORDEN_CONTRASTE = `${environment.apiUrlGestion}/api/ordenContraste`;
  private readonly API_ANORMALIDAD = `${environment.apiUrlGestion}/api/anormalidad`;
  private readonly API_PARAMETRO = `${environment.apiUrlGestion}/api/parametro`;
  private readonly API_DISORDTAREA = `${environment.apiUrlGestion}/api/disOrdTarea`;
  private readonly API_DISTAREAANORMALIDAD = `${environment.apiUrlGestion}/api/disTareaAnormalidad`;
  private readonly API_MOTIVO = `${environment.apiUrlGestion}/api/motivo`;

  constructor(private http: HttpClient) { }

  getNroServicio(nroServicio: string) {
    return this.http.get<any>(this.API_CLIENTE + '/nroServicio/' + nroServicio);
  }

  getAnormalidades() {
    return this.http.get<any>(this.API_ANORMALIDAD);
  }

  getSubTipos() {
    return this.http.get<any>(this.API_PARAMETRO + '/subtipos');
  }

  getOrden(id: number, tipo: number) {
    return this.http.get<any>(this.API_ORDEN + '/' + id + '/' + tipo);
  }

  postCambiarEstado(tarea: any) {
    return this.http.post<any>(this.API_DISORDTAREA + '/cambiarEstado', tarea);
  }

  getTareas(id: number) {
    return this.http.get<any>(this.API_DISTAREAANORMALIDAD + '/anormalidad/' + id);
  }

  postOrdenContraste(form: any) {
    return this.http.post<any>(this.API_ORDEN_CONTRASTE + '/store', form);
  }

  getMotivo() {
    return this.http.get<any>(this.API_MOTIVO + "/contrastes");
  }

  obtenerDetalleOrdenContraste(id: number) {
    return this.http.get<any>(this.API_ORDEN + '/orden-scom/' + id);
  }
}
