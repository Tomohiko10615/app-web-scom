import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UnsubscribeOnDestroyAdapter } from '../../../shared/UnsubscribeOnDestroyAdapter';
import { GestionOrdenes } from '../models/gestion-ordenes.model';

@Injectable({
  providedIn: 'root'
})

export class TransferXmlService {

  private urlEndPoint = `${environment.apiUrlGestion}/api/orden`;

  constructor(private http: HttpClient) {
  }

  obtenerXmlEnvio(idOrden: number): Observable<any[]>{
    return this.http.get<any[]>(this.urlEndPoint +'/obtenerXMLenvio/' +idOrden);
  }


  obtenerXmlRecepcion(idOrden: number): Observable<any[]>{
    return this.http.get<any[]>(this.urlEndPoint +'/obtenerXMLRecepcion/' +idOrden);
  }
}