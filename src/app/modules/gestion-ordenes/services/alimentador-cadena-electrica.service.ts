import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import {debounceTime, Observable, distinctUntilChanged, switchMap} from "rxjs";

@Injectable({
    providedIn: 'root'
  })
  
export class AlimentadorCadenaElectricaService {
  
    private urlEndPoint = `${environment.apiUrlGestion}/api/srv-alimentador`;
  
    constructor(private http: HttpClient) {
    }

    buscarAlimentadorCadenaElectrica(desAlimentador: Observable<string>, idSet: number) {
        return desAlimentador.pipe(distinctUntilChanged(),debounceTime(400),
        switchMap(val => this.buscarAlimentadorCadenaElectricaHttp(val,idSet)));
      }

    buscarAlimentadorCadenaElectricaHttp(desAlimentador: string, idSet: number) {
        let queryParams = new HttpParams();
        queryParams = queryParams.append('desAlimentador', desAlimentador);
        queryParams = queryParams.append('idSet', idSet);
        return this.http.get<any>(this.urlEndPoint + '/search', { params: queryParams });
    }
}