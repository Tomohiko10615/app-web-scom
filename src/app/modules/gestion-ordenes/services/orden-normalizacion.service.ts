import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { OrdenTrabajoScomDetalle } from '../models/orden-trabajo-scom-detalle.model';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class OrdenNormalizacionService {

  private readonly API_CLIENTE = `${environment.apiUrlGestion}/api/cliente`;
  private readonly API_ORDEN = `${environment.apiUrlGestion}/api/orden`;
  private readonly API_ORDEN_NORMALIZACION = `${environment.apiUrlGestion}/api/ordenNormalizacion`;
  private readonly API_ANORMALIDAD = `${environment.apiUrlGestion}/api/anormalidad`;
  private readonly API_PARAMETRO = `${environment.apiUrlGestion}/api/parametro`;
  private readonly API_DISORDTAREA = `${environment.apiUrlGestion}/api/disOrdTarea`;
  private readonly API_DISTAREAANORMALIDAD = `${environment.apiUrlGestion}/api/disTareaAnormalidad`;

  constructor(private http: HttpClient) { }

  getNroServicio(nroServicio: number) {
    return this.http.get<any>(this.API_CLIENTE + '/nroServicio/' + nroServicio);
  }

  getAnormalidades() {
    return this.http.get<any>(this.API_ANORMALIDAD);
  }

  getSubTipos() {
    return this.http.get<any>(this.API_PARAMETRO + '/subtipos');
  }

  getOrden(id: number, tipo: number) {
    return this.http.get<any>(this.API_ORDEN + '/' + id + '/' + tipo);
  }

  postCambiarEstado(tarea: any) {
    return this.http.post<any>(this.API_DISORDTAREA + '/cambiarEstado', tarea);
  }

  getTareas(id: number) {
    return this.http.get<any>(this.API_DISTAREAANORMALIDAD + '/anormalidad/' + id);
  }

  postOrdenNormalizacion(form: any) {
    return this.http.post<any>(this.API_ORDEN_NORMALIZACION + '/store', form);
  }
  

  obtenerDetalleOrdenNormalizacion(id: number) {
    return this.http.get<OrdenTrabajoScomDetalle>(this.API_ORDEN + '/orden-scom/' + id);
  }
}
