import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import {debounceTime, Observable, distinctUntilChanged, switchMap} from "rxjs";
import { ActivarSuministro } from "../models/activar-suministro.model";

@Injectable({
    providedIn: 'root'
  })
  
export class ActivarSuministroService {
  
    private urlEndPoint = `${environment.apiUrlGestion}/api/activar-suministro`;
  
    constructor(private http: HttpClient) {
    }

    obtenerDetalleActivarSuministro(idAutoAct: number) {
        return this.http.get<any>(this.urlEndPoint + '/' + idAutoAct);
    }

    actualizarCadenaElectricaYrutas(data: ActivarSuministro) {
        return this.http.post<any>(this.urlEndPoint + '/update', data);
      }
}