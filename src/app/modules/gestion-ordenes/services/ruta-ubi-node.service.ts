import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { debounceTime, distinctUntilChanged, Observable, switchMap } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
  })
  
export class RutaUbiNodeService {
  
    private urlEndPoint = `${environment.apiUrlGestion}/api/ubi-nodo`;
  
    constructor(private http: HttpClient) {
    }

    buscarSectorRuta(codNodo: Observable<string>, idTipoRuta: number) {
        return codNodo.pipe(distinctUntilChanged(),debounceTime(800),
        switchMap(val => this.buscarSectorRutaHttp(val, idTipoRuta)));
      }

    buscarSectorRutaHttp(codNodo: string, idTipoRuta: number) {
        let queryParams = new HttpParams();
        queryParams = queryParams.append('codNodo', codNodo);
        queryParams = queryParams.append('idTipoRuta', idTipoRuta);
        return this.http.get<any>(this.urlEndPoint + '/search/sector', { params: queryParams });
    }


    buscarZonaRuta(codNodo: Observable<string>, idTipoRuta: number, idNodoPadre: number) {
        return codNodo.pipe(distinctUntilChanged(),debounceTime(800),
        switchMap(val => this.buscarZonaRutaHttp(val, idTipoRuta, idNodoPadre)));
      }

    buscarZonaRutaHttp(codNodo: string, idTipoRuta: number, idNodoPadre: number) {
        let queryParams = new HttpParams();
        queryParams = queryParams.append('codNodo', codNodo);
        queryParams = queryParams.append('idTipoRuta', idTipoRuta);
        queryParams = queryParams.append('idNodoPadre', idNodoPadre);
        return this.http.get<any>(this.urlEndPoint + '/search/zona', { params: queryParams });
    }
}