import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { OrdenTrabajoScomDetalle } from '../models/orden-trabajo-scom-detalle.model';

@Injectable({
  providedIn: 'root'
})
export class OrdenMantenimientoService {

  private readonly API_CLIENTE = `${environment.apiUrlGestion}/api/cliente`;
  private readonly API_TRABAJO = `${environment.apiUrlGestion}/api/ormTrabajo`;
  private readonly API_PRIORIDAD = `${environment.apiUrlGestion}/api/prioridad`;
  private readonly API_ORM_TAREA = `${environment.apiUrlGestion}/api/ormTarea`;
  private readonly API_ORM_TIPO_TAREA = `${environment.apiUrlGestion}/api/ormTipoTarea`;
  private readonly API_RESPONSABLE = `${environment.apiUrlGestion}/api/responsable`;
  private readonly API_ORDEN_MANTENIMIENTO = `${environment.apiUrlGestion}/api/ordenMantenimiento`;
  private readonly API_ORDEN = `${environment.apiUrlGestion}/api/orden`;
  

  constructor(private http: HttpClient) {}

  getNroServicio(nroServicio: string) {
    return this.http.get<any>(this.API_CLIENTE + '/nroServicio/' + nroServicio);
  }

  getNroCuenta(nroCuenta: string) {
    return this.http.get<any>(this.API_CLIENTE + '/nroCuenta/' + nroCuenta);
  }

  getTrabajos() {
    return this.http.get<any>(this.API_TRABAJO + '/trabajos');
  }

  getPrioridades() {
    return this.http.get<any>(this.API_PRIORIDAD);
  }

  getResponsable(responsable: number) {
    return this.http.get<any>(this.API_RESPONSABLE + '/' + responsable);
  }

  getOrmTareas(idTipo: number) {
    return this.http.get<any>(this.API_ORM_TAREA + '/' + idTipo);
  }
  
  postOrdenMantenimiento(form: any) {
    return this.http.post<any>(this.API_ORDEN_MANTENIMIENTO + '/store', form);
  }

  getOrdenById(id: number, tipo: number) {
    return this.http.get<any>(this.API_ORDEN + '/' + id + '/' + tipo);
  }

  getTipos() {
    return this.http.get<any>(this.API_ORM_TIPO_TAREA);
  }

  obtenerDetalleOrdenMantenimiento(id: number) {
    return this.http.get<any>(this.API_ORDEN + '/orden-scom/' + id);
  }

}
