import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UnsubscribeOnDestroyAdapter } from '../../../shared/UnsubscribeOnDestroyAdapter';
import { GestionOrdenes } from '../models/gestion-ordenes.model';

@Injectable({
  providedIn: 'root'
})

export class OrdenTrabajoService {

  private urlEndPoint = `${environment.apiUrlGestion}/api/orden-trabajo`;
  private readonly API_URL = 'assets/data/ordenesTrabajo.json';
  isTblLoading = true;
  dataChange: BehaviorSubject<GestionOrdenes[]> = new BehaviorSubject<GestionOrdenes[]>([]);
  dialogData: any;

  constructor(private http: HttpClient) {
  }

  get data(): GestionOrdenes[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllOrdenTrabajo(): void {
   /*  this.subs.sink = this.http.get<GestionOrdenes[]>(this.API_URL).subscribe(
      (data) => {
        this.isTblLoading = false;
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        this.isTblLoading = false;
        console.log(error.name + ' ' + error.message);
      }
    ); */
  }

  crearOrdenTrabajo(): void {
    console.log("crearOrdenTrabajo");
  }


  filtroOrdenTrabajo(page: number, size: number, order: string, asc: boolean,filtro: any): Observable<any[]>{
    let queryParams = new HttpParams();
    queryParams = queryParams.append('page', page);
    queryParams = queryParams.append('size', size);
    queryParams = queryParams.append('order', order);
    queryParams = queryParams.append('order', asc);
    queryParams = queryParams.append('fechaFin', filtro?.fechaFin);
    queryParams = queryParams.append('fechaInicio', filtro?.fechaInicio);
    queryParams = queryParams.append('idTipoOrden', filtro?.idTipoOrden);
    queryParams = queryParams.append('estado', filtro?.idEstado);
    queryParams = queryParams.append('numOrden', filtro?.nroOrden);
    queryParams = queryParams.append('numOrdenTdc', filtro?.nroOrdenTdc);
    queryParams = queryParams.append('codTipoProceso', filtro?.codTipoProceso);
    queryParams = queryParams.append('nroCuenta', filtro?.nroCuenta);
    queryParams = queryParams.append('codEstadTransfer', filtro?.codEstadTransfer);
    queryParams = queryParams.append('codTipoOrdenEorder', filtro?.codTipoOrdenEorder);
    queryParams = queryParams.append('autoActivacion', filtro?.autoActivacion);
    return this.http.get<any[]>(this.urlEndPoint, { params: queryParams });
  }

  anularOrdenTrabajo(ordenes: any[]): Observable<any[]> {
    return this.http.post<any[]>(this.urlEndPoint +'/anulacion', ordenes);
  }

  filtroOrdenSCOMTrabajo(page: number, size: number, order: string, asc: boolean,filtro: any): Observable<any[]>{
    let queryParams = new HttpParams();
    queryParams = queryParams.append('page', page);
    queryParams = queryParams.append('size', size);
    queryParams = queryParams.append('order', order);
    queryParams = queryParams.append('order', asc);
    queryParams = queryParams.append('fechaFin', filtro?.fechaFin);
    queryParams = queryParams.append('fechaInicio', filtro?.fechaInicio);
    queryParams = queryParams.append('idTipoOrden', filtro?.idTipoOrden);
    queryParams = queryParams.append('estado', filtro?.idEstado);
    queryParams = queryParams.append('numOrden', filtro?.nroOrden);
    queryParams = queryParams.append('codTipoProceso', filtro?.codTipoProceso);
    queryParams = queryParams.append('idServicio', filtro?.idServicio);
    queryParams = queryParams.append('nroCuenta', filtro?.nroCuenta);
    return this.http.get<any[]>(this.urlEndPoint + '/orden-trabajo-scom', { params: queryParams });
  }
}