import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import {debounceTime, Observable, distinctUntilChanged, switchMap} from "rxjs";

@Injectable({
    providedIn: 'root'
  })
  
export class SedCadenaElectricaService {
  
    private urlEndPoint = `${environment.apiUrlGestion}/api/srv-sed`;
  
    constructor(private http: HttpClient) {
    }

    buscarSedCadenaElectrica(desSed: Observable<string>, idAlimentador: number) {
        return desSed.pipe(distinctUntilChanged(),debounceTime(400),
        switchMap(val => this.buscarSedCadenaElectricaHttp(val,idAlimentador)));
      }

    buscarSedCadenaElectricaHttp(desSed: string, idAlimentador: number) {
        let queryParams = new HttpParams();
        queryParams = queryParams.append('desSed', desSed);
        queryParams = queryParams.append('idAlimentador', idAlimentador);
        return this.http.get<any>(this.urlEndPoint + '/search', { params: queryParams });
    }
}