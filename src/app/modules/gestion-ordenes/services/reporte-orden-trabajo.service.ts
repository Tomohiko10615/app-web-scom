import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ChartBarReport } from 'src/app/core/interfaces/core.interface';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReporteOrdenTrabajoService {

  private urlEndPoint = `${environment.apiUrlGestion}/api/reportes/orden-trabajo/`;


  constructor(private http: HttpClient) { }

  filtroReporteOrdenes(page: number, size: number, order: string, asc: boolean,filtro: any): Observable<any[]>{
    let queryParams = new HttpParams();
    queryParams = queryParams.append('page', page);
    queryParams = queryParams.append('size', size);
    queryParams = queryParams.append('order', order);
    queryParams = queryParams.append('order', asc);
    queryParams = queryParams.append('fechaFin', filtro?.fechaFin);
    queryParams = queryParams.append('fechaInicio', filtro?.fechaInicio);
    queryParams = queryParams.append('situacion', filtro?.situacion);
    queryParams = queryParams.append('idTipoProceso', filtro?.idTipoProceso);
    queryParams = queryParams.append('nroCuenta', filtro?.nroCuenta);
    queryParams = queryParams.append('idTipoOrden', filtro?.idTipoOrden);
    return this.http.get<any[]>(this.urlEndPoint + 'general', { params: queryParams });
  }


  filtroReporteOrdenesGrafico(filtro: any): Observable<any[]>{
    let queryParams = new HttpParams();
    queryParams = queryParams.append('fechaFin', filtro?.fechaFin);
    queryParams = queryParams.append('fechaInicio', filtro?.fechaInicio);
    queryParams = queryParams.append('situacion', filtro?.situacion);
    queryParams = queryParams.append('idTipoProceso', filtro?.idTipoProceso);
    queryParams = queryParams.append('nroCuenta', filtro?.nroCuenta);
    queryParams = queryParams.append('idTipoOrden', filtro?.idTipoOrden);
    return this.http.get<ChartBarReport[]>(this.urlEndPoint + 'cantidadXsituacion', { params: queryParams });
  }
}