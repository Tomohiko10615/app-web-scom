import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import {debounceTime, Observable, distinctUntilChanged, switchMap} from "rxjs";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
  })
  
export class LlaveCadenaElectricaService {
  
    private urlEndPoint = `${environment.apiUrlGestion}/api/srv-llave`;
  
    constructor(private http: HttpClient) {
    }

    buscarLlaveCadenaElectrica(descripcion: Observable<string>, idSed: number) {
        return descripcion.pipe(distinctUntilChanged(),debounceTime(400),
        switchMap(val => this.buscarLlaveCadenaElectricaHttp(val,idSed)));
      }

    buscarLlaveCadenaElectricaHttp(descripcion: string, idSed: number) {
        let queryParams = new HttpParams();
        queryParams = queryParams.append('descripcion', descripcion);
        queryParams = queryParams.append('idSed', idSed);
        return this.http.get<any>(this.urlEndPoint + '/search', { params: queryParams });
    }
}