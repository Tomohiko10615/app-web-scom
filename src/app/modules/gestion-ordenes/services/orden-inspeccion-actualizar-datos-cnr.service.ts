import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrdenInspeccionActualizarDatosCnrService {
  private readonly urlEndPoint = `${environment.apiUrlGestion}/api/ordenInspeccion`;
  constructor(private http: HttpClient) { }

  filtroOrdenInspec(page: number, size: number, order: string, asc: boolean,filtro: any): Observable<any[]>{
    let queryParams = new HttpParams();
    queryParams = queryParams.append('numSuministro', filtro?.nroSuministro);
    queryParams = queryParams.append('numNotificacion', filtro?.nroNotificacion);
    queryParams = queryParams.append('page', page);
    queryParams = queryParams.append('size', size);
    queryParams = queryParams.append('order', order);
    queryParams = queryParams.append('asc', asc);
    return this.http.get<any[]>(this.urlEndPoint + '/filtroOrdenInspActualizarXPaginacion', { params: queryParams });
  }

  postOrdenInspeccion(form: any) {
    return this.http.post<any>(this.urlEndPoint + '/actualizarDatosInspCNR', form);
  }

  filtroHistorialActualizaciones(page: number, size: number, order: string, asc: boolean,filtro: any): Observable<any[]>{
    let queryParams = new HttpParams();
    queryParams = queryParams.append('numSuministro', filtro?.nroSuministro);
    queryParams = queryParams.append('numNotificacion', filtro?.nroNotificacion);
    queryParams = queryParams.append('page', page);
    queryParams = queryParams.append('size', size);
    queryParams = queryParams.append('order', order);
    queryParams = queryParams.append('asc', asc);
    return this.http.get<any[]>(this.urlEndPoint + '/filtrarHistorialActualizacionesXPaginacion', { params: queryParams });
  }

}
