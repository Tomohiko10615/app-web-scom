import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UnsubscribeOnDestroyAdapter } from '../../../shared/UnsubscribeOnDestroyAdapter';
import { Irregularidad } from '../models/irregularidad.model';

@Injectable({
    providedIn: 'root'
})

export class IrregularidadService extends UnsubscribeOnDestroyAdapter {
    private readonly API_URL = 'assets/data/irregularidades.json';
    isTblLoading = true;
    dataChange: BehaviorSubject<Irregularidad[]> = new BehaviorSubject<Irregularidad[]>([]);
    dialogData: any;

    constructor(private http: HttpClient) {
        super();
    }

    get data(): Irregularidad[] {
        return this.dataChange.value;
    }

    getDialogData() {
        return this.dialogData;
    }

    getAllIrregularidad(): void {
        this.subs.sink = this.http.get<Irregularidad[]>(this.API_URL).subscribe(
            (data) => {
                this.isTblLoading = false;
                this.dataChange.next(data);
            },
            (error: HttpErrorResponse) => {
                this.isTblLoading = false;
                console.log(error.name + ' ' + error.message);
            }
        );
    }
}