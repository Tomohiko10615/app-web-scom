import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UnsubscribeOnDestroyAdapter } from '../../../shared/UnsubscribeOnDestroyAdapter';
import { Tarea } from '../models/tarea.model';

@Injectable({
    providedIn: 'root'
})

export class TareaService extends UnsubscribeOnDestroyAdapter {
    private readonly API_URL = 'assets/data/tareas.json';
    isTblLoading = true;
    dataChange: BehaviorSubject<Tarea[]> = new BehaviorSubject<Tarea[]>([]);
    dialogData: any;

    constructor(private http: HttpClient) {
        super();
    }

    get data(): Tarea[] {
        return this.dataChange.value;
    }

    getDialogData() {
        return this.dialogData;
    }

    getAllTarea(): void {
        this.subs.sink = this.http.get<Tarea[]>(this.API_URL).subscribe(
            (data) => {
                this.isTblLoading = false;
                this.dataChange.next(data);
            },
            (error: HttpErrorResponse) => {
                this.isTblLoading = false;
                console.log(error.name + ' ' + error.message);
            }
        );
    }
}