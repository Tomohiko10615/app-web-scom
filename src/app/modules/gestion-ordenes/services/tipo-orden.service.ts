import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TipoOrdenService {

  private urlEndPoint = `${environment.apiUrlGestion}/api/tipo-orden`;

  constructor(private http: HttpClient) { }

  getTipoOrdenByCodProceso(codProceso: string) {
    return this.http.get<any>(this.urlEndPoint + '/proceso/' + codProceso);
  }
}
