import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import {debounceTime, Observable, distinctUntilChanged, switchMap} from "rxjs";

@Injectable({
    providedIn: 'root'
  })
  
export class SetCadenaElectricaService {
  
    private urlEndPoint = `${environment.apiUrlGestion}/api/srv-set`;
  
    constructor(private http: HttpClient) {
    }

    buscarSetCadenaElectrica(value: Observable<string>) {
        return value.pipe(distinctUntilChanged(),debounceTime(400),
        switchMap(val => this.buscarSetCadenaElectricaHttp(val)));
      }

    buscarSetCadenaElectricaHttp(desSet: string) {
        let queryParams = new HttpParams();
        queryParams = queryParams.append('desSet', desSet);
        return this.http.get<any>(this.urlEndPoint + '/search', { params: queryParams });
    }
}