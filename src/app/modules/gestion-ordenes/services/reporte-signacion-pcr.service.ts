import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ChartBarReport } from 'src/app/core/interfaces/core.interface';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReporteAsignacionPcrService {

  private urlEndPoint = `${environment.apiUrlGestion}/api/reportes/asignacion-pcr-cliente/`;


  constructor(private http: HttpClient) { }

  filtroReporteAsignacionPcr(page: number, size: number, order: string, asc: boolean,filtro: any): Observable<any[]>{
    let queryParams = new HttpParams();
    queryParams = queryParams.append('page', page);
    queryParams = queryParams.append('size', size);
    queryParams = queryParams.append('order', order);
    queryParams = queryParams.append('order', asc);
    queryParams = queryParams.append('fechaFin', filtro?.fechaFin);
    queryParams = queryParams.append('fechaInicio', filtro?.fechaInicio);
    queryParams = queryParams.append('situacionPcr', filtro?.situacionPcr);
    queryParams = queryParams.append('nroCliente', filtro?.nroCliente);
    return this.http.get<any[]>(this.urlEndPoint + 'general', { params: queryParams });
  }


  filtroReporteAsignacionPcrGrafico(filtro: any): Observable<any[]>{
    let queryParams = new HttpParams();
    queryParams = queryParams.append('fechaFin', filtro?.fechaFin);
    queryParams = queryParams.append('fechaInicio', filtro?.fechaInicio);
    queryParams = queryParams.append('situacionPcr', filtro?.situacionPcr);
    queryParams = queryParams.append('nroCliente', filtro?.nroCliente);
    return this.http.get<ChartBarReport[]>(this.urlEndPoint + 'cantidadXsituacion', { params: queryParams });
  }
}