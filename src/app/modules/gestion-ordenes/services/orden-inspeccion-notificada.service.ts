import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UnsubscribeOnDestroyAdapter } from '../../../shared/UnsubscribeOnDestroyAdapter';
import { OrdenInspeccionNotificada } from '../models/orden-inspeccion-notificada.model';
import { OrdenTrabajoScomDetalle } from '../models/orden-trabajo-scom-detalle.model';

@Injectable({
    providedIn: 'root'
})

export class OrdenInspeccionNotificadaService extends UnsubscribeOnDestroyAdapter {
    
    private readonly API_URL = 'assets/data/ordenesInspeccionNotificadas.json';
    private readonly API_CLIENTE = `${environment.apiUrlGestion}/api/cliente`;
    private readonly API_PARAMETRO = `${environment.apiUrlGestion}/api/parametro`;
    private readonly API_MOTIVO = `${environment.apiUrlGestion}/api/motivo`;
    private readonly API_PRODUCTO = `${environment.apiUrlGestion}/api/producto`;
    private readonly API_JEFE_PRODUCTO = `${environment.apiUrlGestion}/api/jefeProducto`;
    private readonly API_CONTRATISTA = `${environment.apiUrlGestion}/api/contratista`;
    private readonly API_ORDEN_INSPECCION = `${environment.apiUrlGestion}/api/ordenInspeccion`;
    private readonly API_ORDEN = `${environment.apiUrlGestion}/api/orden`;
    private readonly API_ORDEN_TRANSFER = `${environment.apiUrlGestion}/api/ordenTransfer`;
    
    isTblLoading = true;
    dataChange: BehaviorSubject<OrdenInspeccionNotificada[]> = new BehaviorSubject<OrdenInspeccionNotificada[]>([]);
    dialogData: any;

    constructor(private http: HttpClient) {
        super();
    }

    get data(): OrdenInspeccionNotificada[] {
        return this.dataChange.value;
    }

    getDialogData() {
        return this.dialogData;
    }

    getAllOrdenInspeccionNotificada(): void {
        this.subs.sink = this.http.get<OrdenInspeccionNotificada[]>(this.API_URL).subscribe(
            (data) => {
                this.isTblLoading = false;
                this.dataChange.next(data);
            },
            (error: HttpErrorResponse) => {
                this.isTblLoading = false;
                console.log(error.name + ' ' + error.message);
            }
        );
    }

    getNroServicio(nroServicio: string) {
        return this.http.get<any>(this.API_CLIENTE + '/nroServicio/' + nroServicio);
    }

    getTipoInspeccion() {
        return this.http.get<any>(this.API_PARAMETRO + '/tipoInspeccion');
    }

    getMotivos() {
        return this.http.get<any>(this.API_MOTIVO);
    }

    getProductos(idMotivo: number) {
        return this.http.get<any>(this.API_PRODUCTO + '/motivo/' + idMotivo);
    }

    getJefeProductos() {
        return this.http.get<any>(this.API_JEFE_PRODUCTO);
    }

    getContratistas() {
        return this.http.get<any>(this.API_CONTRATISTA);
    }

    postOrdenInspeccion(form: any) {
        return this.http.post<any>(this.API_ORDEN_INSPECCION + '/store', form);
    }

    getOrdenById(id: number, tipo: number) {
        return this.http.get<any>(this.API_ORDEN + '/' + id + '/' + tipo);
    }

    getXML(id: number, accion: string) {
        return this.http.get<any>(this.API_ORDEN + '/obtenerXML/' + id + '/' + accion);
    }

    getOrdenTransfer(id: number) {
        console.log(this.API_ORDEN_TRANSFER + '/idOrden/' + id)
        return this.http.get<any>(this.API_ORDEN_TRANSFER + '/idOrden/' + id);
    }

    obtenerDetalleOrdenInspeccion(id: number) {
      return this.http.get<any>(this.API_ORDEN + '/orden-scom/' + id);
    }
}