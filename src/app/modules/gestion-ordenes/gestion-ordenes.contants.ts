export const TIPO_ORDEN_SCOM= {
    INSPECCION:'ORINSP',
    NORMALIZACION: 'NORM',
    CONTRASTE: 'CONT',
    MANTENIMIENTO: 'MANT'
}

export const SOLO_NUMEROS = '^[0-9]+';

export const ESTADO_ORDEN_SCOM= {
    CREADA:'Creada',
    EMITIDA: 'Emitida',
    ANULADA: 'Anulada'
}

export const urlOrdenTrabajoVer: Map<string, string> = new Map([
    ['ORINSP', 'ver-inspeccion'],
    ['NORM', 'ver-normalizacion'],
    ['CONT', 'ver-contraste'],
    ['PMANT', 'ver-mantenimiento']
 
  ]);


  export const urlOrdenTrabajoSCOMVer: Map<string, string> = new Map([
    ['ORINSP', 'ver-inspeccion-scom'],
    ['NORM', 'ver-normalizacion-scom'],
    ['CONT', 'ver-contraste-scom'],
    ['PMANT', 'ver-mantenimiento-scom']
 
  ]);


  export const urlOrdenTrabajoCrear: Map<string, string> = new Map([
    ['ORINSP', 'editar-inspeccion'],
    ['NORM', 'editar-normalizacion'],
    ['CONT', 'editar-contraste'],
    ['PMANT', 'editar-mantenimiento']
 
  ]);


  export const TIPO_ORDEN_TRANSFER: any = [
    { codProceso:'PERD', descripcion: 'Inspección', codigoLegacy:'ORINSP'},
    { codProceso:'PERD', descripcion: 'Normalización', codigoLegacy:'NORM'},
    { codProceso:'PERD', descripcion: 'Contraste', codigoLegacy:'CONT'},
    { codProceso:'IMPAG', descripcion: 'Cortes', codigoLegacy:'CORT'},
    { codProceso:'IMPAG', descripcion: 'Verificaciones', codigoLegacy:'VERI'},
    { codProceso:'IMPAG', descripcion: 'Reconexiones', codigoLegacy:'REPO'},
    { codProceso:'IMPAG', descripcion: 'Desmantelamientos', codigoLegacy:'DESM'},
    { codProceso:'IMPAG', descripcion: 'Cobranzas', codigoLegacy:'COB'},
    { codProceso:'MANT', descripcion: 'Mantenimiento (Plan de Mantenimiento)', codigoLegacy:'PMANT'},
    { codProceso:'MANT', descripcion: 'Mantenimiento (Reclamos)', codigoLegacy:'RMANT'},
    { codProceso:'MANT', descripcion: 'Factibilidad y Verificación de Lectura', codigoLegacy:'MANT'},
    { codProceso:'CONEX', descripcion: 'Nuevas Conexiones', codigoLegacy:'NOCNX'},
    { codProceso:'CONEX', descripcion: 'Ampliación de Carga o Modificación de Conexión | Retiro de Conexión', codigoLegacy:'OCNX'},
  ]

  export const COD_TIPO_ORDEN_EORDER: any = [
    { codigo:'ORINSP', value:'INS.01'},
    { codigo:'ORINSP', value:'INS.02'},
    { codigo:'NORM', value:'NOR.01'},
    { codigo:'NORM', value:'NOR.02'},
    { codigo:'NORM', value:'NOR.03'},
    { codigo:'CONT', value:'INS.04'},
    { codigo:'CORT', value:'SCR.01'},
    { codigo:'VERI', value:'SCR.01'},
    { codigo:'REPO', value:'SCR.02'},
    { codigo:'DESM', value:'SCR.01'},
    { codigo:'COB', value:'COB.01'},
    { codigo:'OCNX', value:'NCX.02'},
    { codigo:'NOCNX', value:'NCX.01'},
    { codigo:'PMANT', value:'NCX.03'},
    { codigo:'RMANT', value:'NCX.03'},
    { codigo:'MANT', value:'NCX.05'}
  ]

  export const AUTO_ACTIVACIONES: any = [
    { codigo:'OK', value:'OK'},
    { codigo:'KO', value:'KO'},
  ]


  export const TIPOS_PROCESOS_ORDENES_SCOM: any = [
    {id: 1596, codigo: "MANT", descripcion: "Mantenimiento", valorAlf: "MANT", valorNum: null},
    {id: 1594, codigo: "PERD", descripcion: "Pérdida", valorAlf: "PERD", valorNum: null}
  ]


  export const TIPO_ORDENES_SCOM: any = [
    { codProceso:'PERD', descripcion: 'Inspección', codigoLegacy:'ORINSP'},
    { codProceso:'PERD', descripcion: 'Normalización', codigoLegacy:'NORM'},
    { codProceso:'PERD', descripcion: 'Contraste', codigoLegacy:'CONT'},
    { codProceso:'MANT', descripcion: 'Mantenimiento (Plan de Mantenimiento)', codigoLegacy:'PMANT'},
  ]



  export const REPORTE_SITUACION: any = [
    { codigo:'Pendiente', value:'PENDIENTE'},
    { codigo:'Anulada', value:'ANULADA'},
    { codigo:'Paralizada', value:'PARALIZADA'},
    { codigo:'Ejecutada', value:'EJECUTADA'},
    { codigo: 'Con error', value: 'CON_ERROR'}
  ]


  export const REPORTE_TIPO_PROCESO_ORDEN: any = [
    { descripcion:'Impago' ,codigo:'IMPAG', value:"'CORT','VERI','REPO','DESM','COB'"},
    { descripcion:'Mantenimiento', codigo:'MANT', value:"'MANT'"},
    { descripcion:'Conexiones', codigo:'CONEX', value:"'NOCNX','OCNX'"},
    { descripcion:'Pérdida', codigo: 'PERD', value:"'ORINSP','NORM','CONT'"}
  ]

export const PROCESO_PERFIL = new Map([
    ['ADMIN_MANTENIMIENTO', 'MANT'], 
    ['ADMIN_PERDIDAS', 'PERD'],
    ['ADMIN_IMPAGOS', 'IMPAG'],
    ['ADMIN_CONEXIONES', 'CONEX'],
    ['ADMIN_ORDENES', 'TODO'],
    ['ADMIN_GENERAL', 'TODO']
]);


export const REPORTE_NOVEDADES_ESTADO_PROCESO: any = [
  { codigo:'Pendiente PCR', value:'PendientePcr'},
  { codigo:'Pendiente PCR Manual', value:'PendientePcrManual'},
  { codigo:'Pendiente Envío', value:'PendienteEnvio'},
  { codigo:'Pendiente Reenvío', value:'PendienteReenvio'},
]


export const REPORTE_NOVEDADES_TIPO_ORDEN: any = [
  { codProceso:'CONEX', descripcion: 'Nuevas Conexiones', codigoLegacy:'NOCNX', codTipoEorder: 'NCX01'},
  { codProceso:'CONEX', descripcion: 'Ampliación de Carga o Modificación de Conexión | Retiro de Conexión', codigoLegacy:'OCNX', codTipoEorder: 'NCX02'},
]

export const REPORTE_ASIGNACION_PCR_SITUACION_PCR: any = [
  { codProceso:'Pendiente', descripcion: 'PENDIENTE', },
  { codProceso:'Asignado', descripcion: 'ASIGNADO'},
]