import { CommonModule, DatePipe } from "@angular/common";
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarConfig, MatSnackBarModule, MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from "@angular/material/tabs";
import { MatToolbarModule } from '@angular/material/toolbar';
import { PermissionGuard } from "src/app/core/guard/permission.guard";
import { ComponentsModule } from '../../shared/components/components.module';
import { GestionOrdenesRoutingModule } from "./gestion-ordenes-routing.module";
import { CrearOrdenTrabajoComponent } from './pages/orden-trabajo/orden-trabajo-crear/orden-trabajo-crear.component';
import { AnularOrdenTrabajoComponent } from './pages/orden-trabajo/orden-trabajo-anular/orden-trabajo-anular.component';
import { OrdenTrabajoComponent } from "./pages/orden-trabajo/orden-trabajo.component";
import { OrdenTrabajoService } from './services/orden-trabajo.service';
import { OrdenInspeccionVerComponent } from './pages/orden-inspeccion/orden-inspeccion-ver/orden-inspeccion-ver.component';
import { OrdenInspeccionEditarComponent } from './pages/orden-inspeccion/orden-inspeccion-editar/orden-inspeccion-editar.component';
import { OrdenInspeccionCrearComponent } from './pages/orden-inspeccion/orden-inspeccion-crear/orden-inspeccion-crear.component';
import { OrdenMantenimientoCrearComponent } from './pages/orden-mantenimiento/orden-mantenimiento-crear/orden-mantenimiento-crear.component';
import { OrdenMantenimientoVerComponent } from './pages/orden-mantenimiento/orden-mantenimiento-ver/orden-mantenimiento-ver.component';
import { OrdenMantenimientoEditarComponent } from './pages/orden-mantenimiento/orden-mantenimiento-editar/orden-mantenimiento-editar.component';
import { OrdenContrasteVerComponent } from "./pages/orden-contraste/orden-contraste-ver/orden-contraste-ver.component";
import { OrdenContrasteEditarComponent } from "./pages/orden-contraste/orden-contraste-editar/orden-contraste-editar.component";
import { OrdenContrasteCrearComponent } from "./pages/orden-contraste/orden-contraste-crear/orden-contraste-crear.component";
import { OrdenNormalizacionVerComponent } from "./pages/orden-normalizacion/orden-normalizacion-ver/orden-normalizacion-ver.component";
import { OrdenNormalizacionEditarComponent } from "./pages/orden-normalizacion/orden-normalizacion-editar/orden-normalizacion-editar.component";
import { OrdenNormalizacionCrearComponent } from "./pages/orden-normalizacion/orden-normalizacion-crear/orden-normalizacion-crear.component";
import { ReporteAsignacionPcrComponent } from "./pages/reporte-asignacion-pcr/reporte-asignacion-pcr.component";
import { ReporteNovedadesComponent } from "./pages/reporte-novedades/reporte-novedades.component";
import { ReporteOrdenTrabajoComponent } from "./pages/reporte-orden-trabajo/reporte-orden-trabajo.component";
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { MatTableExporterModule } from 'mat-table-exporter';
import { MatTooltipModule } from "@angular/material/tooltip";
import { OrdenXmlComponent } from "./pages/orden-xml/orden-xml.component";
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from "@angular/material/core";
import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import { ActivarSuministroComponent } from './pages/activar-suministro/activar-suministro.component';
import { MatDividerModule } from "@angular/material/divider";
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { OrdenTrabajoScomComponent } from './pages/orden-trabajo-scom/orden-trabajo-scom.component';
import { OrdenNormalizacionScomComponent } from './pages/orden-normalizacion-scom/orden-normalizacion-scom.component';
import { OrdenInspeccionScomComponent } from './pages/orden-inspeccion-scom/orden-inspeccion-scom.component';
import { OrdenMantenimientoScomComponent } from './pages/orden-mantenimiento-scom/orden-mantenimiento-scom.component';
import { OrdenContrasteScomComponent } from './pages/orden-contraste-scom/orden-contraste-scom.component';
import { OrdenContrasteScomVerComponent } from './pages/orden-contraste-scom/orden-contraste-scom-ver/orden-contraste-scom-ver.component';
import { OrdenMantenimientoScomVerComponent } from './pages/orden-mantenimiento-scom/orden-mantenimiento-scom-ver/orden-mantenimiento-scom-ver.component';
import { OrdenInspeccionScomVerComponent } from './pages/orden-inspeccion-scom/orden-inspeccion-scom-ver/orden-inspeccion-scom-ver.component';
import { OrdenNormalizacionScomVerComponent } from './pages/orden-normalizacion-scom/orden-normalizacion-scom-ver/orden-normalizacion-scom-ver.component';
import { OrdenInspeccionActualizarDatosCnrComponent } from './pages/orden-inspeccion/orden-inspeccion-actualizar-datos-cnr/orden-inspeccion-actualizar-datos-cnr.component';
import { PopupActualizarDatosComponent } from './pages/orden-inspeccion/orden-inspeccion-actualizar-datos-cnr/popup-actualizar-datos/popup-actualizar-datos.component';
import { PopupHistorialActualizacionesComponent } from './pages/orden-inspeccion/orden-inspeccion-actualizar-datos-cnr/popup-historial-actualizaciones/popup-historial-actualizaciones.component';

@NgModule({
  declarations: [
    OrdenTrabajoComponent,
    CrearOrdenTrabajoComponent,
    AnularOrdenTrabajoComponent,
    OrdenInspeccionEditarComponent,
    OrdenInspeccionVerComponent,
    OrdenInspeccionCrearComponent,
    OrdenContrasteVerComponent,
    OrdenContrasteEditarComponent,
    OrdenContrasteCrearComponent,
    OrdenNormalizacionVerComponent,
    OrdenNormalizacionEditarComponent,
    OrdenNormalizacionCrearComponent,
    OrdenMantenimientoCrearComponent,
    OrdenMantenimientoVerComponent,
    OrdenMantenimientoEditarComponent,
    ReporteAsignacionPcrComponent,
    ReporteNovedadesComponent,
    ReporteOrdenTrabajoComponent,
    OrdenXmlComponent,
    ActivarSuministroComponent,
    OrdenTrabajoScomComponent,
    OrdenNormalizacionScomComponent,
    OrdenInspeccionScomComponent,
    OrdenMantenimientoScomComponent,
    OrdenContrasteScomComponent,
    OrdenContrasteScomVerComponent,
    OrdenMantenimientoScomVerComponent,
    OrdenInspeccionScomVerComponent,
    OrdenNormalizacionScomVerComponent,
    OrdenInspeccionActualizarDatosCnrComponent,
    PopupActualizarDatosComponent,
    PopupHistorialActualizacionesComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    GestionOrdenesRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatButtonModule,
    MatIconModule,
    MatRadioModule,
    MatSelectModule,
    MatCheckboxModule,
    MatCardModule,
    MatDatepickerModule,
    MatDialogModule,
    MatSortModule,
    MatToolbarModule,
    MatMenuModule,
    MatTabsModule,
    MatProgressSpinnerModule,
    ComponentsModule,
    NgxChartsModule,
    MatTooltipModule,
    MatTableExporterModule,
    MatDividerModule,
    MatAutocompleteModule
  ],
  providers: [PermissionGuard, OrdenTrabajoService, DatePipe,
    {provide: MAT_DATE_LOCALE, useValue: 'es-PE'},
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {
      duration: 20000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: ['color-snackbar', 'color-snackbar']} as MatSnackBarConfig},
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GestionOrdenesModule { }
