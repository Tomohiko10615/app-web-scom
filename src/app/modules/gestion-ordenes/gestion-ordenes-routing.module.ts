import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/core/guard/auth.guard';
import { PermissionGuard } from 'src/app/core/guard/permission.guard';
import { ActivarSuministroComponent } from './pages/activar-suministro/activar-suministro.component';
import { OrdenContrasteScomVerComponent } from './pages/orden-contraste-scom/orden-contraste-scom-ver/orden-contraste-scom-ver.component';
import { OrdenContrasteCrearComponent } from './pages/orden-contraste/orden-contraste-crear/orden-contraste-crear.component';
import { OrdenContrasteVerComponent } from './pages/orden-contraste/orden-contraste-ver/orden-contraste-ver.component';
import { OrdenInspeccionScomVerComponent } from './pages/orden-inspeccion-scom/orden-inspeccion-scom-ver/orden-inspeccion-scom-ver.component';
import { OrdenInspeccionCrearComponent } from './pages/orden-inspeccion/orden-inspeccion-crear/orden-inspeccion-crear.component';
import { OrdenInspeccionVerComponent } from './pages/orden-inspeccion/orden-inspeccion-ver/orden-inspeccion-ver.component';
import { OrdenMantenimientoScomVerComponent } from './pages/orden-mantenimiento-scom/orden-mantenimiento-scom-ver/orden-mantenimiento-scom-ver.component';
import { OrdenMantenimientoCrearComponent } from './pages/orden-mantenimiento/orden-mantenimiento-crear/orden-mantenimiento-crear.component';
import { OrdenMantenimientoVerComponent } from './pages/orden-mantenimiento/orden-mantenimiento-ver/orden-mantenimiento-ver.component';
import { OrdenNormalizacionScomVerComponent } from './pages/orden-normalizacion-scom/orden-normalizacion-scom-ver/orden-normalizacion-scom-ver.component';
import { OrdenNormalizacionScomComponent } from './pages/orden-normalizacion-scom/orden-normalizacion-scom.component';
import { OrdenNormalizacionCrearComponent } from './pages/orden-normalizacion/orden-normalizacion-crear/orden-normalizacion-crear.component';
import { OrdenNormalizacionVerComponent } from './pages/orden-normalizacion/orden-normalizacion-ver/orden-normalizacion-ver.component';
import { OrdenTrabajoScomComponent } from './pages/orden-trabajo-scom/orden-trabajo-scom.component';
import { OrdenTrabajoComponent } from "./pages/orden-trabajo/orden-trabajo.component";
import { OrdenXmlComponent } from './pages/orden-xml/orden-xml.component';
import { ReporteAsignacionPcrComponent } from "./pages/reporte-asignacion-pcr/reporte-asignacion-pcr.component";
import { ReporteNovedadesComponent } from "./pages/reporte-novedades/reporte-novedades.component";
import { ReporteOrdenTrabajoComponent } from "./pages/reporte-orden-trabajo/reporte-orden-trabajo.component";
import { OrdenInspeccionActualizarDatosCnrComponent } from './pages/orden-inspeccion/orden-inspeccion-actualizar-datos-cnr/orden-inspeccion-actualizar-datos-cnr.component';


const routes: Routes = [
  {
    path: 'orden-trabajo',
    component: OrdenTrabajoComponent,
    canActivate: [PermissionGuard, AuthGuard],
    data: {
      permission: [
        'ROL_ORDEN_TRABAJO_PERDIDAS',
        'ROL_ORDEN_TRABAJO_IMPAGOS',
        'ROL_ORDEN_TRABAJO_MANTENIMIENTO',
        'ROL_ORDEN_TRABAJO_CONEXIONES']
    }
  },
  {
    path: 'orden-trabajo-scom',
    component: OrdenTrabajoScomComponent,
    canActivate: [PermissionGuard, AuthGuard],
    data: {
      permission: [
        'ROL_ORDEN_TRABAJO_PERDIDAS',
        'ROL_ORDEN_TRABAJO_IMPAGOS',
        'ROL_ORDEN_TRABAJO_MANTENIMIENTO',
        'ROL_ORDEN_TRABAJO_CONEXIONES']
    }
  },
  {
    path: 'orden-trabajo/crear-contraste',
    component: OrdenContrasteCrearComponent,
    canActivate: [PermissionGuard, AuthGuard],
    data: {
      permission: [
        'ROL_ORDEN_TRABAJO_PERDIDAS',]
    }
  },
  {
    path: 'orden-trabajo/ver-contraste/:id',
    component: OrdenContrasteVerComponent,
    canActivate: [PermissionGuard, AuthGuard],
    data: {
      permission: [
        'ROL_ORDEN_TRABAJO_PERDIDAS']
    }
  },
  {
    path: 'orden-trabajo/editar-contraste/:id',
    component: OrdenContrasteCrearComponent,
    canActivate: [PermissionGuard, AuthGuard],
    data: {
      permission: [
        'ROL_ORDEN_TRABAJO_PERDIDAS']
    }
  },
  {
    path: 'orden-trabajo/crear-inspeccion',
    component: OrdenInspeccionCrearComponent,
    canActivate: [PermissionGuard, AuthGuard],
    data: {
      permission: [
        'ROL_ORDEN_TRABAJO_PERDIDAS']
    }
  },
  {
    path: 'orden-trabajo/ver-inspeccion/:id',
    component: OrdenInspeccionVerComponent,
    canActivate: [PermissionGuard, AuthGuard],
    data: {
      permission: [
        'ROL_ORDEN_TRABAJO_PERDIDAS']
    }
  },
  {
    path: 'orden-trabajo/editar-inspeccion/:id',
    component: OrdenInspeccionCrearComponent,
    canActivate: [PermissionGuard, AuthGuard],
    data: {
      permission: [
        'ROL_ORDEN_TRABAJO_PERDIDAS']
    }
  },
  {
    path: 'orden-trabajo/crear-mantenimiento',
    component: OrdenMantenimientoCrearComponent,
    canActivate: [PermissionGuard, AuthGuard],
    data: {
      permission: [
        'ROL_ORDEN_TRABAJO_MANTENIMIENTO']
    }
  },
  {
    path: 'orden-trabajo/ver-mantenimiento/:id',
    component: OrdenMantenimientoVerComponent,
    canActivate: [PermissionGuard, AuthGuard],
    data: {
      permission: [
        'ROL_ORDEN_TRABAJO_MANTENIMIENTO',
      '']
    }
  },
  {
    path: 'orden-trabajo/editar-mantenimiento/:id',
    component: OrdenMantenimientoCrearComponent,
    canActivate: [PermissionGuard],
    data: {
      permission: [
        'ROL_ORDEN_TRABAJO_MANTENIMIENTO']
    }
  },
  {
    path: 'orden-trabajo/crear-normalizacion',
    component: OrdenNormalizacionCrearComponent,
    canActivate: [PermissionGuard, AuthGuard],
    data: {
      permission: [
        'ROL_ORDEN_TRABAJO_PERDIDAS']
    }
  },
  {
    path: 'orden-trabajo/ver-normalizacion/:id',
    component: OrdenNormalizacionVerComponent,
    canActivate: [PermissionGuard, AuthGuard],
    data: {
      permission: [
        'ROL_ORDEN_TRABAJO_PERDIDAS']
    }
  },
  {
    path: 'orden-trabajo/editar-normalizacion/:id',
    component: OrdenNormalizacionCrearComponent,
    canActivate: [PermissionGuard, AuthGuard],
    data: {
      permission: [
        'ROL_ORDEN_TRABAJO_PERDIDAS']
    }
  },
  {
    path: 'orden-trabajo/orden-xml/:id',
    component: OrdenXmlComponent,
    canActivate: [PermissionGuard, AuthGuard],
    data: { permission: ['ROL_ORDEN_TRABAJO_MANTENIMIENTO', 'ROL_ORDEN_TRABAJO_IMPAGOS', 'ROL_ORDEN_TRABAJO_CONEXIONES'] }
  },
  {
    path: 'reporte-orden-trabajo',
    component: ReporteOrdenTrabajoComponent,
    canActivate: [PermissionGuard, AuthGuard],
    data: { permission: ['ROL_REPORTE_ORDENES'] }
  },
  {
    path: 'reporte-novedades',
    component: ReporteNovedadesComponent,
    canActivate: [PermissionGuard, AuthGuard],
    data: { permission: ['ROL_REPORTE_NOVEDADES_CLIENTE'] }
  },
  {
    path: 'reporte-asignacion-pcr',
    component: ReporteAsignacionPcrComponent,
    canActivate: [PermissionGuard, AuthGuard],
    data: { permission: ['ROL_REPORTE_ASIGNACION_PCR'] }
  },
  {
    path: 'orden-inspeccion-actualizar-datos-cnr',
    component: OrdenInspeccionActualizarDatosCnrComponent,
    canActivate: [PermissionGuard, AuthGuard],
    data: {
      permission: [
        'ROL_ORDEN_TIPO_INSPEC_ACTUALIZA_DATOS',
      '']
    }
  },
  {
    path: 'orden-trabajo/activar-suministro/:idAutoAct',
    component: ActivarSuministroComponent,
    canActivate: [PermissionGuard, AuthGuard],
    data: {
      permission: [
        'ROL_ORDEN_TRABAJO_CONEXIONES']
    }
  },
  {
    path: 'orden-trabajo-scom/ver-normalizacion-scom/:id',
    component: OrdenNormalizacionScomVerComponent,
    canActivate: [PermissionGuard, AuthGuard],
    data: {
      permission: [
        'ROL_ORDEN_TRABAJO_PERDIDAS']
    }
  },
  {
    path: 'orden-trabajo-scom/ver-contraste-scom/:id',
    component: OrdenContrasteScomVerComponent,
    canActivate: [PermissionGuard, AuthGuard],
    data: {
      permission: [
        'ROL_ORDEN_TRABAJO_PERDIDAS']
    }
  },
  {
    path: 'orden-trabajo-scom/ver-inspeccion-scom/:id',
    component: OrdenInspeccionScomVerComponent,
    canActivate: [PermissionGuard, AuthGuard],
    data: {
      permission: [
        'ROL_ORDEN_TRABAJO_PERDIDAS']
    }
  },
  {
    path: 'orden-trabajo-scom/ver-mantenimiento-scom/:id',
    component: OrdenMantenimientoScomVerComponent,
    canActivate: [PermissionGuard, AuthGuard],
    data: {
      permission: [
        'ROL_ORDEN_TRABAJO_MANTENIMIENTO',
      '']
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class GestionOrdenesRoutingModule { }