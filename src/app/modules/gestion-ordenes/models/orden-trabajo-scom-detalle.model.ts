import { MedContrasteDetalle } from "./med-contraste-detalle.model";
import { Medidor } from "./medidor.model";
import { OrdenAnormalidadDetalle } from "./orden-anormalidad-detalle.model";
import { OrdenOrmTareaDetalle } from "./orden-orm-tarea-detalle.model";
import { OrdenTareaDetalle } from "./orden-tarea-detalle.model";
import { TareaXml } from "./tarea-xml.model";

export class OrdenTrabajoScomDetalle {
    idOrdenInspeccion: number;
	idOrden: number;
	idMotivo: number;
	idProducto: number;
	idJefeProducto: number;
	idContratista: number;
	idTipInspeccion: number;
	idOrdenSc4j: number;
	
	nroServicio: string;
	nroCuenta: string;
	nombre: string;
	apellidoPat: string;
	apellidoMat: string;
	distrito: string;
	rutaLectura: string;
	direccion: string;
	
	nroOrden: string;
	entreCalles: string;
	motivo: string;
	producto: string;
	tipoInspeccion: string;
	jefeProducto: string;
	contratista: string;
	nroNotificacion: string;
	usuarioCreacion: string;
	denunciaRealizada: string;
	ejecutor: string;
	informacionComplementaria: boolean;
	autogenerado: boolean;
	observacion: string;
	documento: string;
	tipoAcometida: string;
	estadoWkf: string;

	// R.I. REQSCOM08 12/07/2023 INICIO
	fechaCreacionOrden: string
	fechaEjecucion: string
	observacionTecnico: string
	// R.I. REQSCOM08 12/07/2023 FIN

	// R.I. REQSCOM02 13/07/2023 INICIO
	estadoLeidoEncontrado: string
	estadoLeidoInstalado: string
	// R.I. REQSCOM02 13/07/2023 FIN

	// R.I. REQSCOM03 14/07/2023 INICIO
	codigoResultado: string
	seCambioElMedidor: string
	codigoContratista: string
	nroNotificacionInspeccion: string
	codidoTdcInspeccionAsociada: string
	tipoAnomalia: string
	medidores: Medidor[] = [];
	tareasXml: TareaXml[] = [];
	// R.I. REQSCOM03 14/07/2023 FIN
	
	fechaCreacion: Date;
	fechaN: Date
	fechaFinalizacion: Date;
	
	idOrdenNormalizacion: number;
	idSubtipo: number;
	subtipo: string;
	
	idOrdenContraste: number;

	idTrabajo: number;
	idPrioridad: number;
	tipoResponsable: string;
	idResponsable: number;

	fechaNotificacion: Date;
	
	tareas: OrdenTareaDetalle[] = [];
	anormalidades: OrdenAnormalidadDetalle[] = [];
	ormTareas: OrdenOrmTareaDetalle[] = [];
	
	medContrasteDTO: MedContrasteDetalle;
}