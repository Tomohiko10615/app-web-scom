/*objeto para mapear resultado al invocar a servicio de busqueda de orndes de inspeccion */
export class OrdenInspecActualizaDatosCNRFilter {
    idOrden:number
    idOrdenInsp:number;  /*dato importante para actualizar Fecha notificacion*/
    idOrdenMediInsp:number; /*dato importante para actualizar cargas R S T*/
    nroSuministro:number;
    nroNotificacion:number;
    
    codExpedienteCNR:number;

	idMagnitud:number;
	codInspeccionCNR:number;

    nombreExpedienteCNR:String;
    cargaR:number;
    cargaS:number;
    cargaT:number;

    lecturaNotificacion:number;

    fechaNotificacion:String;
    escenarioDescripcion:String;
    tipoCliente:String;
}