// R.I. REQSCOM03 14/07/2023 INICIO
export class TareaXml {
    desTarea: string
    predeterminada: string
    ejecutada: string

    constructor(tareaXml) {
        {
            this.desTarea = tareaXml.accionMedidor
            this.predeterminada = tareaXml.predeterminada
            this.ejecutada = tareaXml.ejecutada
        }
    }
}
// R.I. REQSCOM03 14/07/2023 FIN