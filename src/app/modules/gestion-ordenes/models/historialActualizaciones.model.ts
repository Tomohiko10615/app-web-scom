export class HistorialActualizaciones{
    fechaEdicion:String;
    userName:String;
    nroSuministro:number;
    
    nroNotificacion:number;
    nombreCampoActualizado:String;
    valorAntes:String;
    valorDespues:String;
    comentario:String;
}