export class OrdenTrabajoAnular {
    id: number;
	nroOrden: number;
	idTipoOrden: number;
	tipoOrden: string;
	idEstado: string;
	estado: string;
}