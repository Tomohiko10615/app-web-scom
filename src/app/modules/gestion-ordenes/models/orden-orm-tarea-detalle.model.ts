export class OrdenOrmTareaDetalle {
    idTarea: number;
    codigo: string;
    tarea: string;
    tipoTarea: string;
    cambio: string;
    contraste: string;
    valor: string;
}