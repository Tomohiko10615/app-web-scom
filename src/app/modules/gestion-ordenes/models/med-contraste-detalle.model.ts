import { MedContrastePruebaDetalle } from "./med-contraste-prueba-detalle.model";

export class MedContrasteDetalle {
    idMedidor: number;
	fechaEjecucion: Date;
	conPromReal: number;
    pruebaBaja: MedContrastePruebaDetalle[] = [];
    pruebaNominal: MedContrastePruebaDetalle[] = [];
    pruebaAlta: MedContrastePruebaDetalle[] = [];
    pruebaAislamiento: MedContrastePruebaDetalle[] = [];
}