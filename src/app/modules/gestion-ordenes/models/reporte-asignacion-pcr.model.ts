export class ReporteAsignacionPcr {
    nroCliente: string;
    nombreCliente: string;
    fechaActivacion: Date;
    situacionPCR: string;
}