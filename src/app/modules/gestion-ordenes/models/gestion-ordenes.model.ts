import { formatDate } from '@angular/common';
export class GestionOrdenes {
  id: number;
  item: string;
  numOrden: number;
  tipoOrden: string;
  fechaCreacion: string;
  numServicio: number;
  estado: string;
  fechaEstado: string;
  numTDC: number;
  usuarioCreador: string;
  paralizado: string;

  constructor(gestionOrdenes) {
    {
      this.id = gestionOrdenes.id || this.getRandomID();
      this.item = gestionOrdenes.item || '';
      this.numOrden = gestionOrdenes.numOrden || this.getRandomID();
      this.tipoOrden = gestionOrdenes.tipoOrden || '';
      this.fechaCreacion = formatDate(new Date(), 'yyyy-MM-dd', 'en') || '';
      this.numServicio = gestionOrdenes.numServicio || this.getRandomID();
      this.estado = gestionOrdenes.estado || '';
      this.fechaEstado = formatDate(new Date(), 'yyyy-MM-dd', 'en') || '';
      this.numTDC = gestionOrdenes.numTDC || this.getRandomID();
      this.usuarioCreador = gestionOrdenes.usuarioCreador || '';
      this.paralizado = gestionOrdenes.paralizado || '';
    }
  }
  public getRandomID(): string {
    const S4 = () => {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return S4() + S4();
  }
}
