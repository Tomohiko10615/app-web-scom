export class TransferAnular {
    id: number;
    nroOrdenLegacy: string;
    codTipoOrdenLegacy: string;
    codEstadoOrden: string;
    usuarioAnula: string;
}