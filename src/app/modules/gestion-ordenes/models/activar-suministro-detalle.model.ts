export class ActivarSuministroDetalle {
    
    nroOrdenLgcRelac: string;
    numeroCuenta: number;
    
    idAlimentador: number;
    codAlimentador: string;

    idSet: number;
    codSet: string;

    idSed: number;
    codSed: string;

    idLlave: number;
    codLlave: string;
    desLlave: string;


    sector: number;
    zona: number;
    correlativo: number;

 /*    idLecturaSector: number;
    codLecturaSector: string;

    idLecturaZona: number;
    codLecturaZona: string;

    lecturaCorrelativo: string;

    idRutaCorteSector: number;
    codRutaCorteSector: string;

    idRutaCorteZona: number;
    codRutaCorteZona: string;

    rutaCorteCorrelativo: string;

    idRutaFacturacionSector: number;
    codRutaFacturacionSector: string;

    idRutaFacturacionZona: number;
    codRutaFacturacionZona: string;

    rutaFacturacionCorrelativo: string;

    idRutaRepartoSector: number;
    codRutaRepartoSector: string;

    idRutaRepartoZona: number;
    codRutaRepartoZona: string;
    
    rutaRepartoCorrelativo: string; */
}