export class OrdenAnormalidadDetalle {

    id: number;
	idAnormalidad: number;
	idTarea: number;
	codigo: string;
	descripcion: string;
	generaCNR: string;
	generaOrdenNormalizacion: string;
	causal: string;
}