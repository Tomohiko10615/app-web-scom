export class OrdenTrabajoFilter {
    id: number;
	item: number;
	nroOrden: number;
    idTipoOrden: number;
    tipoOrden: string;
	codTipoOrden: string;
    fechaCreacion: string;
    nroServicio: string;
	idEstado: string;
	estado: string;
	fechaEstado: string;
	nroTdc: string;
	usuarioCreador: string;
	paralizado: string;
}