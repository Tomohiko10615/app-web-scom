export class OrdenTareaDetalle {
    id: number;
	idTarea: number;
	idAnormalidad: number;
	anormalidad: string;
	tarea: string;
	codTarea: string;
	codAnormalidad: string;
	tareaEstado: string;
}