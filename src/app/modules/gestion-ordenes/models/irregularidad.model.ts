export class Irregularidad {
    id: number;
    nro: string;
    codigo: string;
    descripcion: string;
    generaCNR: string;
    generaOrdenNormalizacion: string;
    causal: string;

    constructor(irregularidad) {
        {
            this.id = irregularidad.id || this.getRandomID();
            this.codigo = irregularidad.codigo || ''
            this.descripcion = irregularidad.descripcion || '';
            this.generaCNR = irregularidad.generaCNR || '';
            this.generaOrdenNormalizacion = irregularidad.generaOrdenNormalizacion || '';
            this.causal = irregularidad.causal || '';
        }
    }
    public getRandomID(): string {
        const S4 = () => {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return S4() + S4();
    }
}
