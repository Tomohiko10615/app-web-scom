export class ReporteNovedades {
    nroCliente: string;
    nombreCliente: string;
    fechaActivacion: Date;
    tipoOrden: string;
    estadoProceso: string;
}