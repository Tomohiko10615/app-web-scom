export class OrdenTransferenciaFilter {
    idtransferencia: number;
    tipoOrdenLegacy: string;
    nroOrdenLegacy: string;
    tipoOrdenTdc: string;
    nroTdc: string;
    estado: string;
    pararilizada: string;
    fecEstadoActual: string;
    nroCuenta: string;
    origenCrea: string;
    idMotivo: string;
    fechaCrea: string;
    generacion: string;
    estadoActivacion: string;
    idAutoAct: string;
}