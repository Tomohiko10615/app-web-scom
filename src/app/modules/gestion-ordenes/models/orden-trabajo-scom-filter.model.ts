export class OrdenTrabajoScomFilter {
    
	id: number;
	nroOrden: string;
	idTipoOrden: number;
	tipoOrden: string;
	codTipoOrden: string;
	fechaCreacion: string;
	nroServicio: number;
	estado: string;
	fechaEstado: string;
	usuarioCreador: string;
	idMotivo: number;
	// R.I. REQSCOM08 07/07/2023 INICIO
	suministro: number;
	cicloDeTrabajo: string;
	fechaCreacionOrden: string;
	fechaEjecucion: string;
	distrito: string;
	observacion: string;
	// R.I. REQSCOM08 07/07/2023 FIN
}