export class ReporteOrdenTrabajo {
    nroCuenta: string;
    nroServicio: string;
    fechaCreacion: Date;
    tipoProceso: string;
    tipoOrden: string;
    estado: string;
    situacion: string;
}