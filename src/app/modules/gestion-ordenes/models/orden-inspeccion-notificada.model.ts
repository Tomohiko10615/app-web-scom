import { formatDate } from '@angular/common';
export class OrdenInspeccionNotificada {
    id: number;
    nro: string;
    nroOrden: string;
    fechaCreacion: string;
    contratista: string;
    tipoInspeccion: string;

    constructor(ordenInspeccionNotificada) {
        {
            this.id = ordenInspeccionNotificada.id || this.getRandomID();
            this.nro = ordenInspeccionNotificada.nro || '';
            this.nroOrden = ordenInspeccionNotificada.nroOrden || ''
            this.fechaCreacion = formatDate(new Date(), 'yyyy-MM-dd', 'en') || '';
            this.contratista = ordenInspeccionNotificada.contratista || '';
            this.tipoInspeccion = ordenInspeccionNotificada.tipoInspeccion || '';
        }
    }
    public getRandomID(): string {
        const S4 = () => {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return S4() + S4();
    }
}
