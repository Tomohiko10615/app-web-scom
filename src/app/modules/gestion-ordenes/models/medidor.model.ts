// R.I. REQSCOM03 14/07/2023 INICIO
export class Medidor {
    accionMedidor: string
    marcaMedidor: string
    modeloMedidor: string
    numeroMedidor: string
    lecturas: Lectura[]
    detallesVisible: boolean

    constructor(medidor) {
        {
            this.accionMedidor = medidor.accionMedidor
            this.marcaMedidor = medidor.marcaMedidor
            this.modeloMedidor = medidor.modeloMedidor
            this.numeroMedidor = medidor.numeroMedidor
            this.lecturas = []
            this.detallesVisible = false
        }
    }
}

export class Lectura {
    tipoLectura: string
    horarioLectura: string
    estadoLeido: string
    fechaLectura: string

    constructor(lectura) {
        {
            this.tipoLectura = lectura.tipoLectura
            this.horarioLectura = lectura.horarioLectura
            this.estadoLeido = lectura.estadoLeido
            this.fechaLectura = lectura.fechaLectura
        }
    }
}
// R.I. REQSCOM03 14/07/2023 FIN