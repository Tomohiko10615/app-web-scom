export class Tarea {
    id: number;
    idTarea: number;
    idAnormalidad: number;
    tarea: string;
    codAnormalidad: string;
    codTarea: string;
    tareaEstado: string;

    constructor(tarea) {
        {
            this.id = tarea.id || this.getRandomID();
            this.tarea = tarea.desTarea;
            this.codAnormalidad = tarea.codAnormalidad || '';
            this.codTarea = tarea.codTarea || '';
            this.tareaEstado = tarea.tareaEstado;
            this.idAnormalidad = tarea.idAnormalidad;
            this.idTarea = tarea.idTarea;
        }
    }
    public getRandomID(): string {
        const S4 = () => {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return S4() + S4();
    }
}
