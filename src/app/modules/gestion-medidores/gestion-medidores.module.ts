import { CommonModule, DatePipe } from "@angular/common";
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { PermissionGuard } from "src/app/core/guard/permission.guard";
import { ComponentsModule } from '../../shared/components/components.module';
import { GestionMedidoresRoutingModule } from "./gestion-medidores-routing.module";
import { MarcaCrearComponent } from './pages/marca/marca-crear/marca-crear.component';
import { MarcaEliminarComponent } from './pages/marca/marca-eliminar/marca-eliminar.component';
import { MarcaComponent } from "./pages/marca/marca.component";
import { MedidorCrearComponent } from './pages/medidor/medidor-crear/medidor-crear.component';
import { MedidorEliminarComponent } from "./pages/medidor/medidor-eliminar/medidor-eliminar.component";
import { MedidorVerComponent } from "./pages/medidor/medidor-ver/medidor-ver.component";
import { MedidorComponent } from "./pages/medidor/medidor.component";
import { MedidaCrearComponent } from './pages/modelo/medida-crear/medida-crear.component';
import { ModeloCrearComponent } from './pages/modelo/modelo-crear/modelo-crear.component';
import { ModeloEliminarComponent } from "./pages/modelo/modelo-eliminar/modelo-eliminar.component";
import { ModeloVerComponent } from './pages/modelo/modelo-ver/modelo-ver.component';
import { ModeloComponent } from "./pages/modelo/modelo.component";
import { ReporteMedidoresComponent } from "./pages/reporte-medidores/reporte-medidores.component";
import { MarcaService } from "./services/marca.service";
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { MatTableExporterModule } from 'mat-table-exporter';
import { MatTooltipModule } from "@angular/material/tooltip";
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from "@angular/material/core";
import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import { ModalConfirmacionComponent } from './pages/modal-confirmacion/modal-confirmacion.component';

@NgModule({
  declarations: [
    MarcaCrearComponent,
    MarcaEliminarComponent,
    MarcaComponent,
    MedidorCrearComponent,
    MedidorEliminarComponent,
    MedidorComponent,
    MedidaCrearComponent,
    ModeloCrearComponent,
    MarcaEliminarComponent,
    MarcaComponent,
    MedidorComponent,
    MedidorVerComponent,
    MedidorEliminarComponent,
    ModeloComponent,
    ModeloEliminarComponent,
    ModeloVerComponent,
    ReporteMedidoresComponent,
    ModalConfirmacionComponent
  ],
  imports: [
    ComponentsModule,
    FormsModule,
    GestionMedidoresRoutingModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSelectModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatToolbarModule,
    ReactiveFormsModule,
    NgxChartsModule,
    MatTooltipModule,
    MatTableExporterModule,
  ],
  providers: [
    PermissionGuard, MarcaService, DatePipe,
    {provide: MAT_DATE_LOCALE, useValue: 'es-PE'},
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GestionMedidoresModule { }