import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UnsubscribeOnDestroyAdapter } from '../../../shared/UnsubscribeOnDestroyAdapter';
import { Marca } from '../models/marca.model';

@Injectable({
  providedIn: 'root'
})
export class MarcaService extends UnsubscribeOnDestroyAdapter {
  private readonly API_URL = `${environment.apiUrlGestion}/api/marca`;
  isTblLoading = true;
  dataChange: BehaviorSubject<Marca[]> = new BehaviorSubject<Marca[]>([]);
  dialogData: any;
  constructor(private http: HttpClient) {
    super();
  }

  get data(): Marca[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  getAllMarca(): void {
    this.subs.sink = this.http.get<Marca[]>(this.API_URL).subscribe(
      (data) => {
        this.isTblLoading = false;
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        this.isTblLoading = false;
        console.log(error.name + ' ' + error.message);
      }
    );
  }

  getMarcaPaginacion(page: number, size: number, order: string, asc: boolean, cod_marca: string, des_marca: string, usuario: string, fecha_inicio: string, fecha_fin: string): Observable<any> {
    return this.http.get<any[]>(this.API_URL + `?page=${page}&size=${size}&order=${order}&asc=${asc}&codMarca=${cod_marca}&desMarca=${des_marca}&usuario=${usuario}&fechaInicio=${fecha_inicio}&fechaFin=${fecha_fin}`);
  }

  postMarca(marca: any): Observable<any> {
    return this.http.post<any>(this.API_URL + '/save', marca);
  }

  postMarcaFiltro(marca: any) {
    return this.http.post<any>(this.API_URL + '/save/', marca);
  }

  putMarca(id: number, marca: Marca) {
    return this.http.put<Marca>(this.API_URL + '/update/' + id, marca);
  }

  deleteMarca(id: number) {
    return this.http.delete<any>(this.API_URL + '/delete/' + id);
  }
}
