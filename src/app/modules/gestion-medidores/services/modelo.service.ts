import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Amperaje } from 'src/app/core/models/amperaje.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ModeloService {
  private urlEndPointMarca = `${environment.apiUrlGestion}/api/marca/all`;
  private urlEndPointMedida = `${environment.apiUrlGestion}/api/medida`;
  private urlEndPointTipoCalculo = `${environment.apiUrlGestion}/api/tipoCalculo`; /* CHR 04-07-23 */
  private urlEndPointModelo = `${environment.apiUrlGestion}/api/modelo`;
  private urlEndPointAmperaje = `${environment.apiUrlGestion}/api/amperaje`;
  private urlEndPointEntDec = `${environment.apiUrlGestion}/api/entdec`;
  private urlEndPointFactor = `${environment.apiUrlGestion}/api/factor`;
  private urlEndPointFase = `${environment.apiUrlGestion}/api/fase`;
  private urlEndPointTipoMedicion = `${environment.apiUrlGestion}/api/tipo-medicion`;
  private urlEndPointVoltaje = `${environment.apiUrlGestion}/api/voltaje`;
  private urlEndPointTension = `${environment.apiUrlGestion}/api/tension`;
  private urlEndPointTecnologia = `${environment.apiUrlGestion}/api/tecnologia`;
  private urlEndPointRegistrador = `${environment.apiUrlGestion}/api/registrador`;
  private urlEndPointUnidadMedidaConstante = `${environment.apiUrlGestion}/api/unidad-medida-constante`;

  constructor(private http: HttpClient) { }

  getModeloPaginacion(page: number, size: number, order: string, asc: boolean, cod_modelo : string, des_modelo : string, usuario : string, fecha_inicio : string, fecha_fin : string, id_marca: number): Observable<any> {
    return this.http.get<any[]>(this.urlEndPointModelo + `?page=${page}&size=${size}&order=${order}&asc=${asc}&cod_modelo=${cod_modelo}&des_modelo=${des_modelo}&usuario=${usuario}&fecha_inicio=${fecha_inicio}&fecha_fin=${fecha_fin}&id_marca=${id_marca}`);
  }

  getData(id: number): Observable<any> {
    return this.http.get<any>(this.urlEndPointModelo + `/${id}`);
  }

  getAmperaje(): Observable<Amperaje[]> {
    return this.http.get<Amperaje[]>(this.urlEndPointAmperaje);
  }

  getEntDec() {
    return this.http.get<Amperaje[]>(this.urlEndPointEntDec);
  }

  getTipoMedicion() {
    return this.http.get<any>(this.urlEndPointTipoMedicion);
  }

  getVoltaje() {
    return this.http.get<any>(this.urlEndPointVoltaje);
  }

  getTension() {
    return this.http.get<any>(this.urlEndPointTension);
  }

  getTecnologia() {
    return this.http.get<any>(this.urlEndPointTecnologia);
  }

  getFase() {
    return this.http.get<any>(this.urlEndPointFase);
  }

  getFactor() {
    return this.http.get<any>(this.urlEndPointFactor);
  }

  getRegistrador() {
    return this.http.get<any>(this.urlEndPointRegistrador);
  }

  getUnidadMedidaConstante() {
    return this.http.get<any>(this.urlEndPointUnidadMedidaConstante);
  }

  getMarca() {
    return this.http.get<any>(this.urlEndPointMarca);
  }

  getMedida() {
    return this.http.get<any>(this.urlEndPointMedida);
  }

  /* CHR 04-07-23 */
  getTipoCalculo(){
    return this.http.get<any>(this.urlEndPointTipoCalculo);
  }

  postModelo(modelo: any): Observable<any> {
    console.log(modelo)
    return this.http.post<any>(this.urlEndPointModelo + '/store', modelo);
  }

  deleteModelo(id: number) {
    return this.http.delete<any>(this.urlEndPointModelo + '/delete/' + id);
  }
}
