import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MedFactMedMod } from '../models/med-fac-med-mod-request.model';

@Injectable({
  providedIn: 'root'
})
export class MedFactMedModService {

  private urlEndPoint = `${environment.apiUrlGestion}/api/factor`;
 

  constructor(private http: HttpClient) { }

  crearFactor(modelo: MedFactMedMod): Observable<any> {
    return this.http.post<any>(this.urlEndPoint + '/crear', modelo);
  }

  deleteFactor(idMedidaModelo: number, idFactor: number) {
    return this.http.delete<any>(this.urlEndPoint + '/delete/' + idMedidaModelo +'/'+ idFactor);
  }

  obtenerFactoresPorIdMedMedidaModelo(idMedidaModelo: number) {
    return this.http.get<any>(this.urlEndPoint + '/factores-medida-modelo/' + idMedidaModelo);
  }
}
