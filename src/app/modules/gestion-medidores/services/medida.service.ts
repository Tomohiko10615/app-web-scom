import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UnsubscribeOnDestroyAdapter } from '../../../shared/UnsubscribeOnDestroyAdapter';
import { Medida } from '../models/medida.model';

@Injectable({
    providedIn: 'root'
})

export class MedidaService extends UnsubscribeOnDestroyAdapter {
    private readonly API_URL = 'assets/data/medidas.json';
    isTblLoading = true;
    dataChange: BehaviorSubject<Medida[]> = new BehaviorSubject<Medida[]>([]);
    dialogData: any;

    constructor(private http: HttpClient) {
        super();
    }

    get data(): Medida[] {
        return this.dataChange.value;
    }

    getDialogData() {
        return this.dialogData;
    }

    getAllMedida(): void {
        this.subs.sink = this.http.get<Medida[]>(this.API_URL).subscribe(
            (data) => {
                this.isTblLoading = false;
                this.dataChange.next(data);
            },
            (error: HttpErrorResponse) => {
                this.isTblLoading = false;
                console.log(error.name + ' ' + error.message);
            }
        );
    }

    eliminarMedida(): void {
        console.log("eliminarMedida");
    }
}
