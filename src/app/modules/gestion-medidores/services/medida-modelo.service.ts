import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { MedidaModeloRequest } from "../models/medida-modelo-request.model";

@Injectable({
    providedIn: 'root'
  })
  export class MedidaModeloService {
  
    private urlEndPoint = `${environment.apiUrlGestion}/api/medida-modelo`;
   
  
    constructor(private http: HttpClient) { }
  
    crearMedidaModelo(modelo: MedidaModeloRequest): Observable<any> {
      return this.http.post<any>(this.urlEndPoint + '/crear', modelo);
    }

    obtenerMedidasPorIdModelo(idModelo: number) {
        return this.http.get<any>(this.urlEndPoint + '/por-modelo/' + idModelo);
      }
  }
  