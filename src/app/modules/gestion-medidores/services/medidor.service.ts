import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Medidor } from 'src/app/core/models/medidor.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MedidorService {
  private urlEndPointMedidor = `${environment.apiUrlGestion}/api/medidor`;
  private urlEndPointUbicacion = `${environment.apiUrlGestion}/api/ubicacion`;
  private urlEndPointMarca = `${environment.apiUrlGestion}/api/marca/all`;
  private urlEndPointMarcaModelos = `${environment.apiUrlGestion}/api/modelo/marca`;
  private urlEndPointMedidas = `${environment.apiUrlGestion}/api/modelo`;
  private urlEndPointEstados = `${environment.apiUrlGestion}/api/estado-medidor`;

  constructor(private http: HttpClient) { }

  getMedidorPaginacion(page: number, size: number, order: string, asc: boolean, nro_medidor: string, id_marca: number, id_modelo: number, usuario: string, fecha_inicio: string, fecha_fin: string, fecha_inicio_estado: string, fecha_fin_estado: string, id_estado: number): Observable<any> {
    return this.http.get<any[]>(this.urlEndPointMedidor + `?page=${page}&size=${size}&order=${order}&asc=${asc}&nro_medidor=${nro_medidor}&id_marca=${id_marca}&id_modelo=${id_modelo}&usuario=${usuario}&fecha_inicio=${fecha_inicio}&fecha_fin=${fecha_fin}&fecha_inicio_estado=${fecha_inicio_estado}&fecha_fin_estado=${fecha_fin_estado}&id_estado=${id_estado}`);
  }

  getData(id: number) {
    return this.http.get<any>(this.urlEndPointMedidor + '/' + id);
  }

  getUbicaciones(): Observable<any> {
    return this.http.get<any[]>(this.urlEndPointUbicacion);
  }

  getMarcas(): Observable<any> {
    return this.http.get<any[]>(this.urlEndPointMarca);
  }

  getModelos(id: any): Observable<any> {
    return this.http.get<any[]>(this.urlEndPointMarcaModelos + `/${id}`);
  }

  getMedidas(id: any): Observable<any> {
    return this.http.get<any[]>(this.urlEndPointMedidas + `/${id}/medidas`);
  }

  getEstados(): Observable<any> {
    return this.http.get<any[]>(this.urlEndPointEstados);
  }

  postMedidor(medidor: Medidor): Observable<any> {
    return this.http.post<any>(this.urlEndPointMedidor + '/store', medidor);
  }

  deleteMedidor(id: number) {
    return this.http.delete<any>(this.urlEndPointMedidor + '/delete/' + id);
  }
}
