import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ChartBarReport } from 'src/app/core/interfaces/core.interface';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReporteMedidoresService {

  private urlEndPoint = `${environment.apiUrlGestion}/api/reportes/medidores/`;


  constructor(private http: HttpClient) { }

  filtroReporteMedidores(page: number, size: number, order: string, asc: boolean,filtro: any): Observable<any[]>{
    let queryParams = new HttpParams();
    queryParams = queryParams.append('page', page);
    queryParams = queryParams.append('size', size);
    queryParams = queryParams.append('order', order);
    // queryParams = queryParams.append('order', asc); //2023.06.26 correcion, se enviaba 2 valores en un solo parametro "order"
    queryParams = queryParams.append('asc', asc); //2023.06.26 agregado
    queryParams = queryParams.append('fechaFin', filtro?.fechaFin);
    queryParams = queryParams.append('fechaInicio', filtro?.fechaInicio);
    queryParams = queryParams.append('idEstadoMedidor', filtro?.idEstadoMedidor);
    queryParams = queryParams.append('tipoUbicacion', filtro?.tipoUbicacion);
    queryParams = queryParams.append('nroCuenta', filtro?.nroCuenta);
    queryParams = queryParams.append('nroComponente', filtro?.nroComponente);
    queryParams = queryParams.append('marcaMedidor', filtro?.marcaMedidor); //REQ # 8 - JEGALARZA
    queryParams = queryParams.append('modeloMedidor', filtro?.modeloMedidor); //R.I. Corrección 23/10/2023
    return this.http.get<any[]>(this.urlEndPoint + 'general', { params: queryParams });
  }


 /*  filtroReporteMedidoresGrafico(filtro: any): Observable<any[]>{
    let queryParams = new HttpParams();
    queryParams = queryParams.append('fechaFin', filtro?.fechaFin);
    queryParams = queryParams.append('fechaInicio', filtro?.fechaInicio);
    queryParams = queryParams.append('idEstadoMedidor', filtro?.idEstadoMedidor);
    queryParams = queryParams.append('tipoUbicacion', filtro?.tipoUbicacion);
    queryParams = queryParams.append('nroCuenta', filtro?.nroCuenta);
    return this.http.get<ChartBarReport[]>(this.urlEndPoint + 'cantidadXestado', { params: queryParams });
  } */
}