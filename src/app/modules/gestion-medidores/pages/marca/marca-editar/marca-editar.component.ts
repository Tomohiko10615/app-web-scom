import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import { MarcaService } from '../../../services/marca.service';
import {
  UntypedFormControl,
  Validators,
  UntypedFormGroup,
  UntypedFormBuilder
} from '@angular/forms';
import { Marca } from '../../../models/marca.model';

@Component({
  selector: 'app-marca-editar',
  templateUrl: './marca-editar.component.html',
  styleUrls: ['./marca-editar.component.sass']
})
export class EditarMarcaComponent {
  dialogTitle: string;
  marcaForm: UntypedFormGroup;
  marca: Marca;
  grabar: string;
  readonly: boolean;
  constructor(
    public dialogRef: MatDialogRef<EditarMarcaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public marcaService: MarcaService,
    private fb: UntypedFormBuilder
  ) {
    this.dialogTitle = 'Editar Marca de Medidor';
    this.data = data;
    this.marcaForm = this.createContactForm();
    this.grabar = 'Guardar';
    this.readonly = true;
  }
  createContactForm(): UntypedFormGroup {
    return this.fb.group({
      id: [this.data.id],
      codMarca: [this.data.codMarca],
      desMarca: [this.data.desMarca]
    });
  }
  formControl = new UntypedFormControl('', [
    Validators.required
  ]);
  submit() {
    console.log("submit")
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  public save(): void {
    console.log("save")
  }
}
