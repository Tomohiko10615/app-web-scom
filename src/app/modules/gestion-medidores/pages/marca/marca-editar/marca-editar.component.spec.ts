import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EditarMarcaComponent } from './marca-editar.component';

describe('EditarMarcaComponent', () => {
  let component: EditarMarcaComponent;
  let fixture: ComponentFixture<EditarMarcaComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [EditarMarcaComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarMarcaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
