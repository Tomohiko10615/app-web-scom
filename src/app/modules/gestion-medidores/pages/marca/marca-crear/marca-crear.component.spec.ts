import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MarcaCrearComponent } from './marca-crear.component';

describe('MarcaCrearComponent', () => {
  let component: MarcaCrearComponent;
  let fixture: ComponentFixture<MarcaCrearComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [MarcaCrearComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarcaCrearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
