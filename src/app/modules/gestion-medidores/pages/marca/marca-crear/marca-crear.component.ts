import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import { MarcaService } from '../../../services/marca.service';
import {
  UntypedFormControl,
  Validators,
  UntypedFormGroup,
  UntypedFormBuilder
} from '@angular/forms';
import { Marca } from '../../../models/marca.model';
import { UsuarioService } from 'src/app/core/service/usuario.service';
import { tap } from 'rxjs';

@Component({
  selector: 'app-marca-crear',
  templateUrl: './marca-crear.component.html',
  styleUrls: ['./marca-crear.component.sass']
})
export class MarcaCrearComponent {
  dialogTitle: string;
  marcaForm: UntypedFormGroup;
  marca: Marca;
  grabar: string;
  readonly: boolean;
  constructor(
    public dialogRef: MatDialogRef<MarcaCrearComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public marcaService: MarcaService,
    private fb: UntypedFormBuilder,
    private usuarioService : UsuarioService
  ) {
    
    this.data = data;
    this.grabar = 'Guardar';
  }

  createContactForm(): UntypedFormGroup {
    return this.fb.group({
      id: [this.data.id],
      codMarca: [this.data.codigo],
      desMarca: [this.data.descripcion]
    });
  }

  formControl = new UntypedFormControl('', [
    Validators.required, Validators.pattern("[a-zA-Z0-9 ]*")
  ]);

  ngOnInit() {
    this.marcaForm = this.createContactForm();
    if(this.data != '') {
      this.dialogTitle = 'Editar Marca de Medidor';
      this.marcaForm.controls['codMarca'].disable();
    }else {
      this.dialogTitle = 'Crear Marca de Medidor';
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public save(): void {
    const usuario = this.usuarioService.usuarioStorage
    if(this.data.id) {
      const form = {
        "idUsuarioRegistro" : usuario?.id,
        ...this.marcaForm.value
      }
      this.marcaService.putMarca(this.data.id, form).pipe(
        tap((response) => !!response),
        tap((response) => this.dialogRef.close())
      ).subscribe();
    }else {
      const form = {
        "idUsuarioRegistro" : usuario?.id,
        ...this.marcaForm.value
      }
      this.marcaService.postMarca(form).pipe(
        tap((response) => !!response),
        tap((response) => this.dialogRef.close())
      ).subscribe();
    }
  }
}
