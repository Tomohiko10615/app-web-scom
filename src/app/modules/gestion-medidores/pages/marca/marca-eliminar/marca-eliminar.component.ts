import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import { MarcaService } from '../../../services/marca.service';
import { tap } from 'rxjs';

@Component({
  selector: 'app-marca-eliminar',
  templateUrl: './marca-eliminar.component.html',
  styleUrls: ['./marca-eliminar.component.sass']
})
export class MarcaEliminarComponent {
  constructor(
    public dialogRef: MatDialogRef<MarcaEliminarComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public marcaService: MarcaService
  ) {}
  onNoClick(): void {
    this.dialogRef.close();
  }
  confirmDelete(): void {
    this.marcaService.deleteMarca(this.data.id).pipe(
      tap((response) => !!response),
      tap((response) => this.dialogRef.close())
    ).subscribe();
  }
}
