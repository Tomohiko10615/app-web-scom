import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MarcaEliminarComponent } from './marca-eliminar.component';

describe('MarcaEliminarComponent', () => {
  let component: MarcaEliminarComponent;
  let fixture: ComponentFixture<MarcaEliminarComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [MarcaEliminarComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarcaEliminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
