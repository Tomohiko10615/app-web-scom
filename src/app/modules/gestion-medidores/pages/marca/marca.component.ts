
import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { tap } from 'rxjs/operators';
import { errorMessages } from 'src/app/core/error/error.constant';
import { Marca } from '../../models/marca.model';
import { MarcaService } from '../../services/marca.service';
import { MarcaCrearComponent } from './marca-crear/marca-crear.component';
import { MarcaEliminarComponent } from './marca-eliminar/marca-eliminar.component';
import { DatePipe } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-marca',
  templateUrl: './marca.component.html',
  styleUrls: ['./marca.component.sass']
})

export class MarcaComponent implements OnInit {

  filterToggle = false;
  displayedColumns = [
    'id',
    'codMarca',
    'desMarca',
    'fechaCreacion',
    'usuario',
    'actions'
  ];

  breadscrums = [
    {
      title: 'Marcas de Medidores',
      items: ['Home'],
      active: 'Marcas de Medidores'
    }
  ];

  marcas: Marca[];
  marcaPaginacion: MatTableDataSource<Marca> = new MatTableDataSource();;
  form: FormGroup;

  totalPages: Array<number>;
  totalPage: number;
  totalElements: number;
  errorMessage = errorMessages;

  page = 0;
  size = 5;
  order = "id";
  asc = false;

  isFirst = false;
  isLast = false;

  codMarca: string = '';
  desMarca: string = '';
  usuario: string = '';
  fechaInicio: any = '';
  fechaFin: any = '';

  fechaInicioActual = new Date();
  fechaFinActual = new Date();

  maxDate = new Date();

  constructor(
    public httpClient: HttpClient,
    public dialog: MatDialog,
    public marcaService: MarcaService,
    private formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private snackBar: MatSnackBar
  ) {    
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;
  @ViewChild(MatMenuTrigger)
  contextMenu: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };

  ngOnInit() {
    this.initForm();
    this.enviarValoresPorDefecto();
    this.form.value.fechaInicio = this.transformDate(this.form.value.fechaInicio.toString());
    this.form.value.fechaFin = this.transformDate(this.form.value.fechaFin.toString());
    this.getMarca();
  }

  initForm() {
    this.form = this.formBuilder.group({
      codMarca: ['', Validators.pattern('[a-zA-Z0-9 ]*')],
      desMarca: ['', Validators.pattern('[a-zA-Z0-9 ]*')],
      usuario: ['', Validators.pattern('[a-zA-Z0-9 ]*')],
      fechaInicio: ['', ''],
      fechaFin: ['', '']
    });
  }

  enviarValoresPorDefecto(): void {
    this.form.get("fechaInicio").setValue(this.fechaInicioActual);
    this.form.get("fechaFin").setValue(this.fechaFinActual);
  }

  dias(fecha, dias){
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
  }

  transformDate(date: string): string{
    const dateTransform = this.datePipe.transform(date, 'yyyy-MM-dd');
    return dateTransform;
  }

  getMarca() {
    if(this.form.value.fechaInicio == '' && this.form.value.fechaFin != '') {
      this.snackBar.open('La fecha inicio no debe estar vacio.','Close');
      return
    }

    if(this.form.value.fechaFin == '' && this.form.value.fechaInicio != '') {
      this.snackBar.open('La fecha fin no debe estar vacio.','Close');
      return
    }
    
    this.codMarca = this.form.value.codMarca;
    this.desMarca = this.form.value.desMarca;
    this.usuario = this.form.value.usuario;
    this.fechaInicio = (!this.form.value.fechaInicio) ? '' : this.datePipe.transform(this.form.value.fechaInicio, 'yyyy-MM-dd');
    this.fechaFin = (!this.form.value.fechaFin) ? '' : this.datePipe.transform(this.form.value.fechaFin, 'yyyy-MM-dd');
    this.marcaService.getMarcaPaginacion(this.page, this.size, this.order, this.asc, this.codMarca, this.desMarca, this.usuario, this.fechaInicio, this.fechaFin).pipe(
      tap((response) => {
        this.marcaPaginacion = new MatTableDataSource<Marca>(response.content);
        this.isFirst = response.first;
        this.isLast = response.last;
        this.totalElements = response.totalElements;
        this.size = response.size;
        this.paginator.pageIndex = this.page;
      })
    ).subscribe();
  }

  dialogCrearMarca(row? : any) {
    const dialogRef = this.dialog.open(MarcaCrearComponent, { data: row ? row : '', disableClose: true });
    dialogRef.afterClosed().subscribe(result => {
      this.getMarca()
    });
  }

  dialogEliminarMarca(row) {
    const dialogRef = this.dialog.open(MarcaEliminarComponent, { data: row ? row : '', disableClose: true });
    dialogRef.afterClosed().subscribe(result => {
      this.getMarca()
    });
  }

  pageChanged(event: PageEvent) {
    this.size = event.pageSize;
    this.page = event.pageIndex;
    this.getMarca();
  }

  ngAfterViewInit() {
    this.marcaPaginacion.paginator = this.paginator;
  }
}
