import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteMedidoresComponent } from './reporte-medidores.component';

describe('ReporteMedidoresComponent', () => {
  let component: ReporteMedidoresComponent;
  let fixture: ComponentFixture<ReporteMedidoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReporteMedidoresComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReporteMedidoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
