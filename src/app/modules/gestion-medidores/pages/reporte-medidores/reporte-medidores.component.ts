import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { errorMessages } from 'src/app/core/error/error.constant';
import { ChartBarReport } from 'src/app/core/interfaces/core.interface';
import { SOLO_NUMEROS } from 'src/app/modules/gestion-ordenes/gestion-ordenes.contants';
import { ReporteMedidores } from '../../models/reporte-medidores.model';
import { MedidorService } from '../../services/medidor.service';
import { tap } from 'rxjs';
import { ParametroService } from 'src/app/core/service/parametro.service';
import { ReporteMedidoresService } from '../../services/reporte-medidores.service';
import { ExcelExportService } from 'src/app/core/service/excel-export.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Medidor } from 'src/app/core/models/medidor.model';

@Component({
  selector: 'app-reporte-medidores',
  templateUrl: './reporte-medidores.component.html',
  styleUrls: ['./reporte-medidores.component.scss','./../../../../app.component.scss']
})
export class ReporteMedidoresComponent implements OnInit {
  rangoDiasFechaBusqueda: number;
  filterToggle = false;
  displayedColumns = [
    'item',
    'tipoComponente',
    'marca',
    'modelo',
    'nroMedidor',
    'nroCuenta',
    'estado',
    'fechaDesde',
    'fechaHasta',
    'ubicacion',
    'username',
    'codPrimarioApe',       //INICIO - REQ # 7 - JEGALARZA 
    'codSecundarioApe',
    'codPrimarioCaja',
    'codSecundarioCaja',
    'codMedidor'            //FIN - REQ # 7 - JEGALARZA 
  ];

  breadscrums = [
    {
      title: 'Reporte Medidores',
      items: ['Home'],
      active: 'Reporte Medidores'
    }
  ];
//INICIO - REQ # 8 - JEGALARZA
  marcas: Medidor[];
  medidorPaginacion: MatTableDataSource<Medidor> = new MatTableDataSource();
//FIN - REQ # 8 - JEGALARZA-->
  form: FormGroup;
  estados: any[];
  ubicaciones: any[];
  valor: number = 0;
  modelos: any[];//REQ # 8 - JEGALARZA-->
  codigo_marca: any[];//REQ # 8 - JEGALARZA-->
  dataSource: MatTableDataSource<ReporteMedidores> = new MatTableDataSource();
  dataFilter: any = { fechaInicio: '', fechaFin:'', idEstadoMedidor:0, tipoUbicacion:'', nroCuenta:'', nroComponente: '',marcaMedidor: '', modeloMedidor: ''} //REQ # 8 - JEGALARZA //R.I. Corrección 23/10/2023
  totalElements: number;
  pageSize: number;
  totalPages: Array<number>;
  exportDataExcel: any[];
  countItem: number;

  fechaInicioActual = new Date();
  fechaFinActual = new Date();
  errorMessage = errorMessages;
  mostrarNroCuenta= false;
  maxDate= new Date();


  // Configuration export data

  exportReport = { fileName:`reporte-medidores-${this.datePipe.transform(new Date, 'dd.MM.yyyy')}`, sheet: 'Hoja1' }

  //Chart Bar Vertical
  colorScheme = {
    domain: ['#9370DB', '#87CEFA', '#FA8072', '#FF7F50', '#90EE90', '#9370DB']
  };

  chartBarValues: ChartBarReport[] = [];

  gradient = false;
  showXAxis = true;
  showYAxis = true;
  showLegend = false;
  showXAxisLabel = true;
  showYAxisLabel = true;
  legendPosition = 'right';
  vbarxAxisLabel = 'Estados';
  vbaryAxisLabel = 'Cantidad';
  view: any;
  marcaMedidor: number = 0;

  //paginator values
  page = 0;
  size = 10;
  order = "id";
  asc = false;
  // R.I. Corrección 20/10/2023 INICIO
  limiteDias: number = 0;
  modeloMedidor: number = 0;
  // R.I. Corrección 20/10/2023 FIN

  constructor(
    public formBuilder: FormBuilder,
    private datePipe: DatePipe,
    public medidorService: MedidorService,  // REQ # 8 - JEGALARZA-->
    private parametroService: ParametroService,
    private reporteMedidoresService: ReporteMedidoresService,
    private excelExportService: ExcelExportService,
    private snackBar: MatSnackBar
    ) { }

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  ngOnInit(): 
  void {
    this.initForm();
    
    this.getEstadosMedidor();
    this.getTiposUbicacionMedidor();
    this.enviarValoresPorDefecto();
    this.getMarca();
    this.dataFilter.fechaInicio = this.transformDate(this.fechaInicioActual.toString());
    this.dataFilter.fechaFin = this.transformDate(this.fechaFinActual.toString());   
    // R.I. Corrección 20/10/2023 INICIO
    this.form.value.fechaInicio = this.transformDate(this.form.value.fechaInicio.toString());
    this.form.value.fechaFin = this.transformDate(this.form.value.fechaFin.toString());
    if (this.limiteDias == 0) {
      this.getLimiteDias();
    }
    // R.I. Corrección 20/10/2023 FIN
    //this.getFiltroReporteMedidoresGrafico();
    this.getFiltroReporteMedidores();

  }

  // R.I. Corrección 20/10/2023 INICIO
  getLimiteDias() {
    this.parametroService.getLimiteDias().pipe(
      tap(response => {
        this.limiteDias = response.valorNum;
      })
    ).subscribe();
  }
  // R.I. Corrección 20/10/2023 FIN

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  enviarValoresPorDefecto(): void {
    this.form.get('fechaInicio')?.setValue(this.fechaInicioActual);
    this.form.get('fechaFin')?.setValue(this.fechaFinActual);
    this.form.get('tipoUbicacion').setValue(0);
    this.form.get('idEstadoMedidor').setValue(0);
  }

  getEstadosMedidor(): void {
    this.medidorService.getEstados().pipe(
      tap((response) => this.estados = response)
    ).subscribe()
  }

  /* getFiltroReporteMedidoresGrafico(): void {
    
    const formvalue = this.form.value;
    this.dataFilter.fechaInicio = formvalue.fechaInicio == null ? '' : this.transformDate(formvalue.fechaInicio);
    this.dataFilter.fechaFin = formvalue.fechaFin == null ? '' : this.transformDate(formvalue.fechaFin);
    this.dataFilter.idEstadoMedidor = formvalue.idEstadoMedidor == 0 ? '' : formvalue.idEstadoMedidor;
    this.dataFilter.tipoUbicacion = formvalue.tipoUbicacion == 0 ? '' : formvalue.tipoUbicacion;
    this.dataFilter.nroCuenta = formvalue.nroCuenta;
    this.reporteMedidoresService.filtroReporteMedidoresGrafico(this.dataFilter).pipe(
      tap((reponse: any)=> this.chartBarValues = reponse)
    ).subscribe();
  } */

  getFiltroReporteMedidores(): void {

    const formvalue = this.form.value;

    if (
      (this.form.value.nroComponente.replace(/\s/g, "") == '' || this.form.value.nroComponente.replace(/\s/g, "") == null) &&
      (this.form.value.marcaMedidor === '' || this.form.value.marcaMedidor === null) ||
      (this.form.value.fechaInicio !== '' || this.form.value.fechaFin !== '' || this.form.value.fechaInicio !== null || this.form.value.fechaFin !== null)
    ) {
      if (
        (this.form.value.fechaFin !== '' && this.form.value.fechaFin !== null) ||
        (this.form.value.fechaInicio !== '' && this.form.value.fechaInicio !== null)
      ) {
        if (
          (this.form.value.fechaInicio === '' || this.form.value.fechaFin === '' || this.form.value.fechaInicio === null || this.form.value.fechaFin === null)
        ) {
          this.snackBar.open('Favor de completar ambas fechas de creación desde y hasta.','Close');
          return;
        }
        if(Math.round((this.form.value.fechaFin - this.form.value.fechaInicio)/(1000*60*60*24)) >= this.limiteDias){
          this.snackBar.open('El reporte no debe ser mayor a ' + this.limiteDias + ' dias','Close');
          return
        }
      }
    }
    console.log(this.form.value.fechaInicio)
    console.log(this.form.value.fechaFin)
    console.log(this.form.value.marcaMedidor)
    console.log(this.modeloMedidor)
    console.log(formvalue.nroComponente)
    if ((this.form.value.fechaInicio == '' || this.form.value.fechaFin == '' 
    || this.form.value.fechaInicio == null || this.form.value.fechaFin == null) 
    && (this.form.value.marcaMedidor === '' || this.form.value.marcaMedidor === null || this.form.value.marcaMedidor === undefined)
    && (this.form.value.nroComponente.replace(/\s/g, "") === '' || this.form.value.nroComponente.replace(/\s/g, "") === null)) {
      this.snackBar.open('Favor de completar los campos fecha de creación desde y hasta.','Close');
        return
    }
    
    

   /*
    if (
      (this.form.value.nroComponente.replace(/\s/g, "") == '' || this.form.value.nroComponente.replace(/\s/g, "") == null) 
    )
    */
      /*
    if (
      (this.form.value.nroComponente.replace(/\s/g, "") == '' || this.form.value.nroComponente.replace(/\s/g, "") == null) &&
      (this.form.value.marcaMedidor === '' || this.form.value.marcaMedidor === null)
    )
    {
      if(((this.form.value.fechaFin == '' || this.form.value.fechaFin == null) || (this.form.value.fechaInicio == '' || this.form.value.fechaInicio == null))){
        this.snackBar.open('Favor de completar los campos fecha de creación desde y hasta.','Close');
        return
      }
      else {
        this.rangoDiasFechaBusqueda = Number.parseInt(JSON.parse(sessionStorage.getItem('rangoFechaBusqueda')).valorNum);
        if(Math.round((this.form.value.fechaFin - this.form.value.fechaInicio)/(1000*60*60*24)) >= this.rangoDiasFechaBusqueda){
          this.snackBar.open('El reporte no debe ser mayor a ' + this.rangoDiasFechaBusqueda + ' dias','Close');
          return
        }
      }
    }*/

    this.dataFilter.fechaInicio = formvalue.fechaInicio == null ? '1900-01-01' : this.transformDate(formvalue.fechaInicio);
    this.dataFilter.fechaFin = formvalue.fechaFin == null ? '2999-01-01' : this.transformDate(formvalue.fechaFin);
    this.dataFilter.idEstadoMedidor = formvalue.idEstadoMedidor == 0 ? '' : formvalue.idEstadoMedidor;
    this.dataFilter.tipoUbicacion = formvalue.tipoUbicacion == 0 ? '' : formvalue.tipoUbicacion;
    this.dataFilter.nroCuenta = formvalue.nroCuenta;
    this.dataFilter.nroComponente = formvalue.nroComponente;
    this.dataFilter.marcaMedidor = ((this.form.value.marcaMedidor == undefined) || (this.form.value.marcaMedidor == '')) ? '' : this.form.value.marcaMedidor;  //REQ # 8 - JEGALARZA
    this.dataFilter.modeloMedidor = ((this.form.value.modeloMedidor == undefined) || (this.form.value.modeloMedidor == '')) ? '' : this.form.value.modeloMedidor;  //R.I. Corrección 23/10/2023
    this.reporteMedidoresService.filtroReporteMedidores(this.page, this.size, this.order, this.asc, this.dataFilter).pipe(
      tap((response: any)=> {
        this.dataSource = new MatTableDataSource<ReporteMedidores>(response.content);
        this.pageSize = response.size;
        this.totalElements = response.totalElements;
        this.paginator.pageIndex = this.page;
        this.countItem= response.pageable.offset + 1;
      })
    ).subscribe();
  }

  getTiposUbicacionMedidor(): void {
    this.parametroService.getTiposUbicacionMedidor().pipe(
      tap((response: any) => this.ubicaciones = response)
    ).subscribe();
  }

  selectTipoUbicacion($event:any) {
    const tipoUbicacion = $event.value;
    if($event){
      this.form.get('nroCuenta').setValue('');
      if(tipoUbicacion =='com.synapsis.synergia.nucleo.domain.interfaces.servicioelectrico.ServicioElectrico'){
        this.mostrarNroCuenta = true;
      } else {
        this.mostrarNroCuenta = false;
      }
    }
  }

//INICIO - REQ # 8 - JEGALARZA-->

  getModelos(id: any) {
    if(id == undefined) {
      this.valor = 0;
    }else {
      this.valor = id;
    }
    this.form.get('modeloMedidor').setValue(''); // R.I. Corrección 23/10/2023
    this.dataFilter.modeloMedidor = ((this.form.value.modeloMedidor == undefined) || (this.form.value.modeloMedidor == '')) ? '' : this.form.value.modeloMedidor;  //R.I. Corrección 23/10/2023
    this.medidorService.getModelos(this.valor).pipe(
      tap(response => {
        this.modelos = response;
        if(id > 0) {
          this.codigo_marca = this.marcas.filter(function(marca : any) {
            return marca.id == id;
          });
        }
      })
    ).subscribe();
  }

  getMarca() {
    this.medidorService.getMarcas().pipe(
      tap(response => {
        this.marcas = response;
      })
    ).subscribe();
  }
//FIN - REQ # 8 - JEGALARZA-->
  initForm(): void {
    this.form = this.formBuilder.group({
      fechaInicio: [''],
      fechaFin: [''],
      idEstadoMedidor: [''],
      tipoUbicacion: [''],
      nroCuenta: ['', Validators.pattern(SOLO_NUMEROS)],
      nroComponente: ['', Validators.pattern(SOLO_NUMEROS)],
      marcaMedidor: ['','']
      ,modeloMedidor: ['',''] //R.I. Corrección 23/10/2023
    });
  }

  onResize(event) {
    this.view = [event.target.innerWidth / 2, 400];
  }


  submitFilterReporteMedidores(): void {
    this.page = 0;
    this.size = 10;
    //this.getFiltroReporteMedidoresGrafico();
    this.getFiltroReporteMedidores();
  }

  transformDate(date: string): string{
    const dateTransform = this.datePipe.transform(date, 'yyyy-MM-dd');
    return dateTransform;
  }

  pageChanged(event: PageEvent): void {
    this.size = event.pageSize;
    this.page = event.pageIndex;
    this.getFiltroReporteMedidores();
  }

 

  exportFile() {

    if(this.totalElements == 0){
      this.snackBar.open('No hay datos de Resultado de busqueda para exportar, seleccione filtrar para realizar nueva busqueda','Close');
      return
    }

    this.reporteMedidoresService.filtroReporteMedidores(this.page, this.totalElements, this.order, this.asc, this.dataFilter).pipe(
      tap((response: any)=> {
        this.exportDataExcel = response.content;
      }),
      tap((response) => !!response),
      tap(() => this.excelExportService.exportAsExcelFile(this.exportDataExcel, this.exportReport.fileName))
    ).subscribe();
    
  }



}
