import { SelectionModel } from '@angular/cdk/collections';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatSort } from '@angular/material/sort';
import { map, tap } from 'rxjs/operators';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { Medida } from '../../../models/medida.model';
import { MedidaCrearComponent } from '../medida-crear/medida-crear.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Amperaje } from 'src/app/core/models/amperaje.model';
import { TipoMedicion } from 'src/app/core/models/tipomedicion.model';
import { Voltaje } from 'src/app/core/models/voltaje.model';
import { Tension } from 'src/app/core/models/tension.model';
import { Tecnologia } from 'src/app/core/models/tecnologia.model';
import { Registrador } from 'src/app/core/models/registrador.model';
import { UnidadMedidaConstante } from 'src/app/core/models/unidadmedidaconstante.model';
import { Marca } from '../../../models/marca.model';
import { Fase } from 'src/app/core/models/fase.model';
import { errorMessages } from 'src/app/core/error/error.constant';
import { ModeloEditar } from 'src/app/core/models/modeloEditar.model';
import { ModeloService } from '../../../services/modelo.service';
import { UsuarioService } from 'src/app/core/service/usuario.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { Factor } from '../../../models/factor.model';
import { ModalConfirmacionComponent } from '../../modal-confirmacion/modal-confirmacion.component';
import { MedFactMedModService } from '../../../services/med-fact-med-mod.service';
import { element } from 'protractor';
import { Modelo } from 'src/app/core/models/modelo.model';
import { MedFactMedMod } from '../../../models/med-fac-med-mod-request.model';
import { MedidaModeloService } from '../../../services/medida-modelo.service';
import { MedidaModeloRequest } from '../../../models/medida-modelo-request.model';

@Component({
  selector: 'app-modelo-crear',
  templateUrl: './modelo-crear.component.html',
  styleUrls: ['./modelo-crear.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class ModeloCrearComponent implements OnInit, AfterViewInit{

  breadscrums = [
    {
      title: 'Creación de Modelo de Medidor',
      items: ['Home'],
      active: 'Creación de Modelo de Medidor'
    }
  ];

  displayedColumns = [
    'nro',
    'id',
    'cant_ent',
    'cant_dec',
    'des_tip_calculo',
    'select',
  ];

  columnsToDisplayWithExpand = [...this.displayedColumns,'expand', 'accion'];

  factorDisplayedColumns = [
    'nro',
    'desFactor',
    'valFactor',
    'actions'
  ];

  id: number;
  gestionOrdenes: Medida | null;

  amperajes: Amperaje[];
  tipoMediciones: TipoMedicion[];
  voltajes: Voltaje[];
  tensiones: Tension[];
  tecnologias: Tecnologia[];
  registradores: Registrador[];
  unidadMedidaConstantes: UnidadMedidaConstante[];
  marcas: Marca[];
  fases: Fase[];
  medidas:any[];
  entDec: any[];
  tipoCalculo: any[]; /* CHR 04-07-23 */
  errorMessage = errorMessages;

  obtenerCodigo: Marca[] = [];

  modelo: any;

  codigo_marca: any[];

  form: FormGroup;

  formMedida : FormGroup;

  modeloDataSimpleArray: any[] = [];
  modeloData: MatTableDataSource<Medida> = new MatTableDataSource();
  medidaModeloSeleccionada: any;

  modeloFactoresData: MatTableDataSource<Factor> = new MatTableDataSource();
  dataModeloFactores: any[] = [];
  idUrl : number;

  ver: boolean = false;

  titulo: string = "Creación de Modelo de Medidor";
  boton: string = "Crear Modelo";

  disabled: boolean = false
  desEditar: boolean = false


  constructor(private modeloService: ModeloService, private formBuilder: FormBuilder, public dialog: MatDialog, 
              private usuarioService : UsuarioService, private route : ActivatedRoute, private router: Router, 
              private snackBar: MatSnackBar,
              private medFactMedModService: MedFactMedModService,
              private medidaModeloService: MedidaModeloService) { }

  ngAfterViewInit(): void {
    this.disabled = true
  }

  ngOnInit() {
    this.idUrl = this.route.snapshot.params['id'];
    

    this.initForm()
    this.getMedida();
    this.getMarca();
    this.getAmperaje();
    this.getTipoMedicion();
    this.getVoltaje();
    this.getTension();
    this.getTecnologia();
    this.getRegistrador();
    this.getUnidadMedidaConstante();
    this.getFase();
    this.getEntDec();
    this.getTipoCalculo(); /* CHR 04-07-23 */

    
    if(this.idUrl) {
      this.getData(this.idUrl) 
      this.titulo = "Edición de Modelo de Medidor";
      this.boton = "Editar Modelo";
    }

    if(!this.idUrl) {
      this.form.get("nro_sellos_medidor").addValidators([Validators.required, Validators.pattern("^[0-9]*$")]);
      this.form.get("nro_sellos_bornera").addValidators([Validators.required, Validators.pattern("^[0-9]*$")]);
      this.form.get("clase_medidor").addValidators([Validators.required, Validators.pattern("^[0-9]+(.[0-9]+)?$")]); /* CHR - 03-07-23 */
    }
    
    if(document.location.href.search('ver') != -1) {
      this.disabled = true
      this.ver = true;
      this.titulo = "Ver Modelo de Medidor";
    }
  }

  initForm () {
    this.form = this.formBuilder.group({
      cod_marca: [{ value: this.modelo ? this.modelo.des_marca: '', disabled: true}, [Validators.required, Validators.pattern('[a-zA-Z0-9 ]*')]],
      cod_modelo: [{ value: this.modelo ? this.modelo.cod_modelo : '', disabled: false}, [Validators.required, Validators.pattern('[a-zA-Z0-9 ]*')]],
      des_modelo: [{ value: this.modelo ? this.modelo.des_modelo: '', disabled: false}, [Validators.required, Validators.pattern('[a-zA-Z0-9 ]*')]],
      cant_anos_vida: [{ value: this.modelo ? this.modelo.cant_anos_vida : '', disabled: false}, [Validators.required, Validators.pattern("^[0-9]*$")]],
      cant_anos_almacen: [{ value: this.modelo ? this.modelo.cant_anos_almacen : '', disabled: false}, [Validators.required, Validators.pattern("^[0-9]*$")]],
      nro_sellos_medidor: [{ value: this.modelo ? this.modelo.nro_sellos_medidor : '', disabled: false}],
      nro_sellos_bornera: [{ value: this.modelo ? this.modelo.nro_sellos_bornera : '', disabled: false}],
      es_reacondicionador: [{ value: this.modelo ? (this.modelo.es_reacondicionador == 'N') ? false : true : '', disabled: false}, ''],
      es_reseteado: [{ value: this.modelo ? (this.modelo.es_reseteado == 'N') ? false : true : '', disabled: false}, ''],
      es_patron: [{ value: this.modelo ? (this.modelo.es_patron == 'N') ? false : true : '', disabled: false}, ''],
      es_totalizador: [(!this.modelo) ? '' : (this.modelo.es_totalizador == 'N') ? false : true, ''],
      clase_medidor: [{ value: this.modelo ? this.modelo.clase_medidor : '', disabled: false}],
      constante: [{ value: this.modelo ? this.modelo.constante : '', disabled: false}, Validators.required],
      cant_hilos: [{ value: this.modelo ? this.modelo.cant_hilos : '', disabled: false}, [Validators.required, Validators.pattern("^[0-9]*$")]],
      id_marca: [{ value: this.modelo ? this.modelo.id_marca : '', disabled: false}, Validators.required],
      id_fase: [{ value: this.modelo ? this.modelo.id_fase : '', disabled: false}, Validators.required],
      id_amperaje: [{ value: this.modelo ? this.modelo.id_amperaje : '', disabled: false}, Validators.required],
      id_tipo_medicion: [{ value: this.modelo ? this.modelo.id_tipo_medicion : '', disabled: false}, Validators.required],
      id_voltaje: [{ value: this.modelo ? this.modelo.id_voltaje : '', disabled: false}, Validators.required],
      id_tension: [{ value: this.modelo ? this.modelo.id_tension : '', disabled: false}, Validators.required],
      id_tecnologia: [{ value: this.modelo ? this.modelo.id_tecnologia : '', disabled: false}, Validators.required],
      id_registrador: [{ value: this.modelo ? this.modelo.id_registrador : '', disabled: false}, Validators.required],
      id_unidad_medida_constante: [{ value: this.modelo ? this.modelo.id_unidad_medida_constante : '', disabled: false}, Validators.required],
    });
    
    this.formMedida = this.formBuilder.group({
      id_ent_dec: ['', Validators.required],
      id_medida: ['', Validators.required],
      id_tip_calculo: ['', Validators.required] /* CHR 04-07-23 */
    });
  }

  getData(id: number) {
    this.modeloService.getData(id).pipe(
      tap((response: any) => {
        const modelo = response
        this.modelo = modelo
        this.form.controls["cod_modelo"].disable();
        this.initForm();
        this.disabled = true;
      }) 
    ).subscribe();
    this.getMedidasPorIdModelo(this.idUrl);
  }

  getMedidasPorIdModelo(idModelo:number){
    this.medidaModeloService.obtenerMedidasPorIdModelo(idModelo).pipe(
      tap((response) => {
        this.modeloData = new MatTableDataSource<Medida>(response);
        this.modeloDataSimpleArray = response;
      })
      ).subscribe();
  }

  getMedida() {
    this.modeloService.getMedida().pipe(
      tap(response => {
        this.medidas = response
      })
    ).subscribe();
  }

  getEntDec() {
    this.modeloService.getEntDec().pipe(
      tap(response => {
        this.entDec = response
      })
    ).subscribe();
  }

  /* INICIO CHR 04-07-23 */
  getTipoCalculo() {
    this.modeloService.getTipoCalculo().pipe(
      tap(response => {
        this.tipoCalculo = response
      })
    ).subscribe();
  }
  /* FIN CHR 04-07-23 */

  getMarca() {
    this.modeloService.getMarca().pipe(
      tap(response => {
        this.marcas = response
        this.obtenerCodigo = response
      })
    ).subscribe();
  }

  getAmperaje() {
    this.modeloService.getAmperaje().pipe(
      tap((response) => {
        this.amperajes = response
      })
    ).subscribe();
  }

  getFase() {
    this.modeloService.getFase().pipe(
      tap((response) => {
        this.fases = response
      })
    ).subscribe();
  }

  getTipoMedicion() {
    this.modeloService.getTipoMedicion().pipe(
      tap((response) => {
        this.tipoMediciones = response
      })
    ).subscribe();
  }

  getVoltaje() {
    this.modeloService.getVoltaje().pipe(
      tap((response) => {
        this.voltajes = response
      })
    ).subscribe();
  }

  getTension() {
    this.modeloService.getTension().pipe(
      tap((response) => {
        this.tensiones = response
      })
    ).subscribe();
  }

  getTecnologia() {
    this.modeloService.getTecnologia().pipe(
      tap((response) => {
        this.tecnologias = response
      })
    ).subscribe();
  }

  getRegistrador() {
    this.modeloService.getRegistrador().pipe(
      tap((response) => {
        this.registradores = response
      })
    ).subscribe();
  }

  getUnidadMedidaConstante() {
    this.modeloService.getUnidadMedidaConstante().pipe(
      tap((response) => {
        this.unidadMedidaConstantes = response
      })
    ).subscribe();
  }

  onKeyUpNroSellosMedidor(value: any) {
    if(value && this.idUrl) {
      this.form.get("nro_sellos_medidor").addValidators([Validators.required, Validators.pattern("^[0-9]*$")]);
    }
  }

  onKeyUpNroSelloBornea(value: any) {
    if(value && this.idUrl) {
      this.form.get("nro_sellos_bornera").addValidators([Validators.required, Validators.pattern("^[0-9]*$")]);
    }
  }

  onKeyClaseMedidor(value: any) {
    if(value && this.idUrl) {
      this.form.get("clase_medidor").addValidators([Validators.required, Validators.pattern("^[0-9]+(.[0-9]+)?$")]); /* CHR - 03-07-23 */
    }
  }

  store() {
  /*   let dataModeloNuevos: any[] = [];
    dataModeloNuevos =  this.dataModelo.filter(item => item.id_medida_modelo == null);
    console.log(dataModeloNuevos);
  */
  
      const usuario = this.usuarioService.usuarioStorage;
      const id_modelo = (this.idUrl > 0) ? this.idUrl : 0;
      const form = {
        "id_usuario_registro" : usuario?.id,
        "id_modelo": id_modelo,
        ...this.form.value,
        ...{
          'medidas' : this.modeloDataSimpleArray
        }
      }
      this.modeloService.postModelo(form).pipe(
        tap((response) => !!response),
        tap(() => this.router.navigate(['/gestion-medidores/modelos']))
      ).subscribe();
   
  }

  getCodigoMarca(marcaId : any) {
    this.codigo_marca = this.marcas.filter(function(marca : Marca) {
      return marca.id == marcaId
    });
  }

  agregarMedida() {
    const valoresMedida = this.formMedida.value;
    console.log(this.modeloDataSimpleArray);
    const elementMedidaFind = this.modeloDataSimpleArray.find((medida: any) => medida.id_medida === valoresMedida.id_medida);
    
    if(elementMedidaFind){
      this.snackBar.open('La Medida para el modelo seleccionado ya se encuentra agregado.', 'CERRAR');
      return;
    }

    //const valoresMedida = this.formMedida.value;

    const entdec = valoresMedida.id_ent_dec.split('-');
    let medidaModeloRequest = new MedidaModeloRequest();
    medidaModeloRequest.idMedida = valoresMedida.id_medida;
    medidaModeloRequest.idModelo = this.idUrl;
    medidaModeloRequest.idEntDec = entdec[0];
    medidaModeloRequest.idTipoCalculo = valoresMedida.id_tip_calculo; /* CHR 04-07-23 */
    this.medidaModeloService.crearMedidaModelo(medidaModeloRequest).pipe(
      tap((response) => {
        this.getMedidasPorIdModelo(this.idUrl);
      })
    ).subscribe();
  }

  check(seledMedidaModelo: any){
    console.log(seledMedidaModelo);
    this.medidaModeloSeleccionada= seledMedidaModelo;
   
    this.obtenerFactoresPorIdMedidaModelo(seledMedidaModelo.id_medida_modelo);
   
  }

  obtenerFactoresPorIdMedidaModelo(idMedidaModelo: number) {
    this.medFactMedModService.obtenerFactoresPorIdMedMedidaModelo(idMedidaModelo).pipe(
      tap((response) => !!response),
      tap((response) => this.modeloFactoresData = new MatTableDataSource<Factor>(response)),
      tap((response) => this.dataModeloFactores.push(response)))
      .subscribe();
  }
  

  openModal(): void {

    if(this.medidaModeloSeleccionada === undefined){
      this.snackBar.open('Debe seleccionar una medida para agregar FACTOR.', 'CERRAR');
    return;
    }

    const dialogRef = this.dialog.open(MedidaCrearComponent, {
      data: '', disableClose: false
    });

   
    dialogRef.afterClosed().subscribe(result => {
      const elementFactorFind = this.dataModeloFactores[0].find((factor: any) => factor.desFactor === result.factor_descripcion);
    
      if(elementFactorFind){
        this.snackBar.open('El factor seleccionado ya se encuentra agregado.', 'CERRAR');
        return;
      }

      const elementMedidaFind = this.modeloDataSimpleArray.find((medida: any) => medida.id_medida_modelo === this.medidaModeloSeleccionada.id_medida_modelo);
      console.log(elementMedidaFind);
      let existeConincidencia: boolean = false;
      this.medFactMedModService.obtenerFactoresPorIdMedMedidaModelo(elementMedidaFind.id_medida_modelo).pipe(
        tap((response) => !!response),
        tap(() => { })
        )
        .subscribe();
    
        if(existeConincidencia) {
          this.snackBar.open('La medida y el factor ya seleccionada se encuentra agregado.', 'CERRAR');
          return;
        }

    

      /*   console.log(this.dataModeloFactores[0]);
        let factores: any[] = [];
        factores = this.dataModeloFactores[0];
        factores.filter((element: any) => {
          console.log(result.factor_descripcion +'|'+element);
          element?.desFactor == result.factor_descripcion});  */
       /*  if(resultado.length >= 1) {
          this.snackBar.open('La medida y el factor seleccionada se encuentra agregado.', 'CERRAR');
          return;
        } */

      /*  const elementFind = this.modeloDataSimpleArray.find((medida: any) => medida.id_medida_modelo);
        console.log(elementFind );

        this.dataModeloFactores.forEach((element: any) => {
        
          if((element?.desFactor == result.factor_descripcion)){
            this.snackBar.open('La medida y el factor seleccionada se encuentra agregado.', 'CERRAR');
          return;
          }
        }); */
        
         
        if(result.id_medida != '') {
          let medMedidaModelo = new MedFactMedMod();
            console.log(medMedidaModelo);
            medMedidaModelo.idMedidaModelo = this.medidaModeloSeleccionada.id_medida_modelo;
            medMedidaModelo.idFactor = Number.parseInt(result.id_factor);
              
            this.medFactMedModService.crearFactor(medMedidaModelo).pipe(
              tap((response) => !!response),
              tap(() => {
                this.obtenerFactoresPorIdMedidaModelo(medMedidaModelo.idMedidaModelo);
              })
            ).subscribe();
        }
            
    });
  }

  remove(index: number, row: any) {
    this.dialog.open(ModalConfirmacionComponent).afterClosed().subscribe((result)=>{
      if(result){
        
        //Envia como parametros el idMedidaModelo y idFactor seleccionado
        this.medFactMedModService.deleteFactor(this.medidaModeloSeleccionada.id_medida_modelo, row.id).pipe(
          tap((esponse) =>  this.obtenerFactoresPorIdMedidaModelo(this.medidaModeloSeleccionada.id_medida_modelo))
        ).subscribe();
        
        
        this.obtenerFactoresPorIdMedidaModelo(this.medidaModeloSeleccionada.id_medida_modelo);
      }
    });
  }
}
