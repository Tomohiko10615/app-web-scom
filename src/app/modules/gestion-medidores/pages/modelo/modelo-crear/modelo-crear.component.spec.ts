import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModeloCrearComponent } from './modelo-crear.component';

describe('ModeloCrearComponent', () => {
  let component: ModeloCrearComponent;
  let fixture: ComponentFixture<ModeloCrearComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModeloCrearComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModeloCrearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
