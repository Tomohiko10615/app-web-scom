import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MedidaCrearComponent } from './medida-crear.component';

describe('MedidaCrearComponent', () => {
  let component: MedidaCrearComponent;
  let fixture: ComponentFixture<MedidaCrearComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [MedidaCrearComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedidaCrearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
