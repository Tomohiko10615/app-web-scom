import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import { MedidaService } from '../../../services/medida.service';
import {
  UntypedFormControl,
  Validators,
  UntypedFormGroup,
  UntypedFormBuilder,
  FormGroup,
  FormBuilder
} from '@angular/forms';
import { Medida } from '../../../models/medida.model';
import { tap } from 'rxjs';
import { errorMessages } from 'src/app/core/error/error.constant';
import { ModeloService } from '../../../services/modelo.service';

@Component({
  selector: 'app-medida-crear',
  templateUrl: './medida-crear.component.html',
  styleUrls: ['./medida-crear.component.sass']
})
export class MedidaCrearComponent {
  medidas:any[];
  factores: any[];
 
  errorMessage = errorMessages;

  form : UntypedFormGroup;

  constructor(private dialogRef: MatDialogRef<MedidaCrearComponent>, 
              @Inject(MAT_DIALOG_DATA) public data: any,
              private fb: UntypedFormBuilder,
              private modeloService: ModeloService) { }

  ngOnInit() {
    this.getFactor();
   // this.getEntDec();

    this.form = this.createContactForm();
  }

  createContactForm(): UntypedFormGroup {
    return this.fb.group({
     // id_ent_dec: [this.data.id_ent_dec],
      id_factor: [this.data.id_factor]
    });
  }

  formControl = new UntypedFormControl('', [
    Validators.required
  ]);

  onNoClick() {
    const dataResponse = 
                          { 
                            'id_medida': '',
                            'medida_descripcion' : '',
                            'id_ent_dec': '',
                            'cantEnteros': '',
                            'cantDecimales': '',
                            'id_factor': '',
                            'factor_codigo': '',
                            'factor_descripcion': '',
                            'factor_valor': '',
                            'id_medida_modelo': ''
                          };
    this.dialogRef.close(dataResponse);
  }

  getFactor() {
    this.modeloService.getFactor().pipe(
      tap(response => {
        this.factores = response
      })
    ).subscribe();
  }

 /*  getEntDec() {
    this.modeloService.getEntDec().pipe(
      tap(response => {
        this.entDec = response
      })
    ).subscribe();
  } */


  save() {
    //const medida = this.form.value.id_medida.split('-');
    //const entdec = this.form.value.id_ent_dec.split('-');
    const factor = this.form.value.id_factor.split('-');
    const dataResponse = 
                        { 
                          'id_medida': null,
                          'medida_descripcion' : null,
                          'id_ent_dec': null,
                          'cantEnteros': null,
                          'cantDecimales': null,
                          'id_factor': factor[0],
                          'factor_codigo': factor[1],
                          'factor_descripcion': factor[2],
                          'factor_valor': factor[3],
                          'id_medida_modelo': null
                        };
    this.dialogRef.close(dataResponse);
  }
}
