import { SelectionModel } from '@angular/cdk/collections';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatSort } from '@angular/material/sort';
import { map, tap } from 'rxjs/operators';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { Medida } from '../../../models/medida.model';
import { MedidaCrearComponent } from '../medida-crear/medida-crear.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Amperaje } from 'src/app/core/models/amperaje.model';
import { TipoMedicion } from 'src/app/core/models/tipomedicion.model';
import { Voltaje } from 'src/app/core/models/voltaje.model';
import { Tension } from 'src/app/core/models/tension.model';
import { Tecnologia } from 'src/app/core/models/tecnologia.model';
import { Registrador } from 'src/app/core/models/registrador.model';
import { UnidadMedidaConstante } from 'src/app/core/models/unidadmedidaconstante.model';
import { Marca } from '../../../models/marca.model';
import { Fase } from 'src/app/core/models/fase.model';
import { errorMessages } from 'src/app/core/error/error.constant';
import { ModeloEditar } from 'src/app/core/models/modeloEditar.model';
import { ModeloService } from '../../../services/modelo.service';
import { UsuarioService } from 'src/app/core/service/usuario.service';
import { ActivatedRoute } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-modelo-ver',
  templateUrl: './modelo-ver.component.html',
  styleUrls: ['./modelo-ver.component.sass']
})

export class ModeloVerComponent extends UnsubscribeOnDestroyAdapter implements OnInit, AfterViewInit{

  breadscrums = [
    {
      title: 'Ver Modelo de Medidor',
      items: ['Home'],
      active: 'Ver Modelo de Medidor'
    }
  ];

  displayedColumns = [
    'nro',
    'id',
    'cant_ent',
    'cant_dec',
    'factor_codigo',
    'factor_descripcion',
    'factor_valor'
  ];


  filterToggle = false;
  selection = new SelectionModel<Medida>(true, []);
  id: number;
  gestionOrdenes: Medida | null;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;
  @ViewChild(MatMenuTrigger)
  contextMenu: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };

  amperajes: Amperaje[];
  tipoMediciones: TipoMedicion[];
  voltajes: Voltaje[];
  tensiones: Tension[];
  tecnologias: Tecnologia[];
  registradores: Registrador[];
  unidadMedidaConstantes: UnidadMedidaConstante[];
  marcas: Marca[];
  fases: Fase[];
  errorMessage = errorMessages;

  obtenerCodigo: Marca[] = [];

  modelo = new ModeloEditar();

  codigo_marca: any[];

  form: FormGroup;

  dataModelo = new Array();
  modeloData: MatTableDataSource<Medida> = new MatTableDataSource();

  idUrl : number;

  ver: boolean = false;

  titulo: string = "Creación de Modelo de Medidor";
  boton: string = "Crear Modelo";

  disabled: boolean = false
  desEditar: boolean = false

  constructor(private modeloService: ModeloService, private formBuilder: FormBuilder, public dialog: MatDialog, 
              private usuarioService : UsuarioService, private route : ActivatedRoute) { 
                super();
              }

  ngAfterViewInit(): void {
    this.disabled = true
  }

  ngOnInit() {
    this.idUrl = this.route.snapshot.params['id'];
    

    this.initForm()

    this.getMarca();
    this.getAmperaje();
    this.getTipoMedicion();
    this.getVoltaje();
    this.getTension();
    this.getTecnologia();
    this.getRegistrador();
    this.getUnidadMedidaConstante();
    this.getFase();
    
    if(this.idUrl) {
      this.getData(this.idUrl) 
      this.titulo = "Edición de Modelo de Medidor";
      this.boton = "Editar Modelo";
    }
    
    if(document.location.href.search('ver') != -1) {
      this.disabled = true
      this.ver = true;
      this.titulo = "Ver Modelo de Medidor";
    }
  }

  initForm () {
    this.form = this.formBuilder.group({
      cod_marca: [{ value: this.modelo ? this.modelo.des_marca: '', disabled: true}, Validators.required],
      cod_modelo: [{ value: this.modelo ? this.modelo.cod_modelo : '', disabled: true}, Validators.required],
      des_modelo: [{ value: this.modelo ? this.modelo.des_modelo: '', disabled: true}, Validators.required],
      cant_anos_vida: [{ value: this.modelo ? this.modelo.cant_anos_vida : '', disabled: true}, Validators.required],
      cant_anos_almacen: [{ value: this.modelo ? this.modelo.cant_anos_almacen : '', disabled: true}, Validators.required],
      nro_sellos_medidor: [{ value: this.modelo ? this.modelo.nro_sellos_medidor : '', disabled: true}, Validators.required],
      nro_sellos_bornera: [{ value: this.modelo ? this.modelo.nro_sellos_bornera : '', disabled: true}, Validators.required],
      nro_registrador: [{ value: this.modelo ? this.modelo.nro_registrador : '', disabled: true}, Validators.required],
      es_reacondicionador: [{ value: this.modelo ? (this.modelo.es_reacondicionador == 'N') ? false : true : '', disabled: true}, ''],
      es_reseteado: [{ value: this.modelo ? (this.modelo.es_reseteado == 'N') ? false : true : '', disabled: true}, ''],
      es_patron: [{ value: this.modelo ? (this.modelo.es_patron == 'N') ? false : true : '', disabled: true}, ''],
      es_totalizador: [{ value: this.modelo ? (this.modelo.es_totalizador == 'N') ? false : true : '', disabled: true}, ''],
      clase_medidor: [{ value: this.modelo ? this.modelo.clase_medidor : '', disabled: true}, Validators.required],
      constante: [{ value: this.modelo ? this.modelo.constante : '', disabled: true}, Validators.required],
      cant_hilos: [{ value: this.modelo ? this.modelo.cant_hilos : '', disabled: true}, Validators.required],
      id_marca: [{ value: this.modelo ? this.modelo.id_marca : '', disabled: true}, Validators.required],
      id_fase: [{ value: this.modelo ? this.modelo.id_fase : '', disabled: true}, Validators.required],
      id_amperaje: [{ value: this.modelo ? this.modelo.id_amperaje : '', disabled: true}, Validators.required],
      id_tipo_medicion: [{ value: this.modelo ? this.modelo.id_tipo_medicion : '', disabled: true}, Validators.required],
      id_voltaje: [{ value: this.modelo ? this.modelo.id_voltaje : '', disabled: true}, Validators.required],
      id_tension: [{ value: this.modelo ? this.modelo.id_tension : '', disabled: true}, Validators.required],
      id_tecnologia: [{ value: this.modelo ? this.modelo.id_tecnologia : '', disabled: true}, Validators.required],
      id_registrador: [{ value: this.modelo ? this.modelo.id_registrador : '', disabled: true}, Validators.required],
      id_unidad_medida_constante: [{ value: this.modelo ? this.modelo.id_unidad_medida_constante : '', disabled: true}, Validators.required],
    });
    
  }

  getData(id: number) {
    this.modeloService.getData(id).pipe(
      tap((response: any) => {
        const modelo = response
        this.modelo = modelo
        this.initForm()
        this.form.controls["des_modelo"].disable();
        response.medidas.forEach((element: any) => {
          const dataResponse = 
                        { 
                          'id_medida': element.id_medida,
                          'medida_descripcion' : element.medida_descripcion,
                          'id_ent_dec': element.id_ent_dec,
                          'cantEnteros': element.cant_enteros,
                          'cantDecimales': element.cant_decimales,
                          'id_factor': element.id_factor,
                          'factor_codigo': element.factor_codigo,
                          'factor_descripcion': element.factor_descripcion,
                          'factor_valor': element.factor_valor
                        };
          this.dataModelo.push(dataResponse)
          this.modeloData = new MatTableDataSource<Medida>(this.dataModelo);
        });
        this.disabled = true;
      }) 
    ).subscribe();
  }

  getMarca() {
    this.modeloService.getMarca().pipe(
      tap(response => {
        this.marcas = response
        this.obtenerCodigo = response
      })
    ).subscribe();
  }

  getAmperaje() {
    this.modeloService.getAmperaje().pipe(
      tap((response) => {
        this.amperajes = response
      })
    ).subscribe();
  }

  getFase() {
    this.modeloService.getFase().pipe(
      tap((response) => {
        this.fases = response
      })
    ).subscribe();
  }

  getTipoMedicion() {
    this.modeloService.getTipoMedicion().pipe(
      tap((response) => {
        this.tipoMediciones = response
      })
    ).subscribe();
  }

  getVoltaje() {
    this.modeloService.getVoltaje().pipe(
      tap((response) => {
        this.voltajes = response
      })
    ).subscribe();
  }

  getTension() {
    this.modeloService.getTension().pipe(
      tap((response) => {
        this.tensiones = response
      })
    ).subscribe();
  }

  getTecnologia() {
    this.modeloService.getTecnologia().pipe(
      tap((response) => {
        this.tecnologias = response
      })
    ).subscribe();
  }

  getRegistrador() {
    this.modeloService.getRegistrador().pipe(
      tap((response) => {
        this.registradores = response
      })
    ).subscribe();
  }

  getUnidadMedidaConstante() {
    this.modeloService.getUnidadMedidaConstante().pipe(
      tap((response) => {
        this.unidadMedidaConstantes = response
      })
    ).subscribe();
  }

  store() {
    if(this.dataModelo.length == 0) {
      alert('Debe agregar medidas')
    }else {
      const usuario = this.usuarioService.usuarioStorage;
      const id_modelo = (this.idUrl > 0) ? this.idUrl : 0;
      const form = {
        "id_usuario_registro" : usuario?.id,
        "id_modelo": id_modelo,
        ...this.form.value,
        ...{
          'medidas' : this.dataModelo
        }
      }
      this.modeloService.postModelo(form).subscribe();
    }
  }

  getCodigoMarca(marcaId : any) {
    this.codigo_marca = this.marcas.filter(function(marca : Marca) {
      return marca.id == marcaId
    });
  }

  openModal(marca?: any): void {
    const dialogRef = this.dialog.open(MedidaCrearComponent, {
      data: ''
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result.id_medida != '') {
        this.dataModelo.push(result);
        this.modeloData = new MatTableDataSource<Medida>(this.dataModelo);
      }
    });
  }

  remove(i: number) {
    this.dataModelo.splice(i, 1);
    this.modeloData = new MatTableDataSource<Medida>(this.dataModelo);
  }
}
