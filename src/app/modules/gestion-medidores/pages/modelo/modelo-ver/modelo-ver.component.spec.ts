import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModeloVerComponent } from './modelo-ver.component';

describe('ModeloVerComponent', () => {
  let component: ModeloVerComponent;
  let fixture: ComponentFixture<ModeloVerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModeloVerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModeloVerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
