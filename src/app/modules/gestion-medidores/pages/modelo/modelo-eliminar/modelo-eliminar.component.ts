import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { tap } from 'rxjs';
import { ModeloService } from '../../../services/modelo.service';

@Component({
  selector: 'app-modelo-eliminar',
  templateUrl: './modelo-eliminar.component.html',
  styleUrls: ['./modelo-eliminar.component.sass']
})
export class ModeloEliminarComponent {

  constructor(
    public dialogRef: MatDialogRef<ModeloEliminarComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public modeloService: ModeloService
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  
  confirmDelete(): void {
    this.modeloService.deleteModelo(this.data.id).pipe(
      tap((response) => !!response),
      tap((response) => this.dialogRef.close())
    ).subscribe();
    this.dialogRef.close();
  }

}
