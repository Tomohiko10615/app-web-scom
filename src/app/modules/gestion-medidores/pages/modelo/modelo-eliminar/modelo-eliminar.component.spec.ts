import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModeloEliminarComponent } from './modelo-eliminar.component';

describe('ModeloEliminarComponent', () => {
  let component: ModeloEliminarComponent;
  let fixture: ComponentFixture<ModeloEliminarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModeloEliminarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModeloEliminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
