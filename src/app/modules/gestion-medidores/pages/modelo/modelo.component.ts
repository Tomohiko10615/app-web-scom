import { DatePipe } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { tap } from 'rxjs';
import { errorMessages } from 'src/app/core/error/error.constant';
/* import { Modelo } from 'src/app/core/models/modelo.model'; */
import { Modelo } from 'src/app/modules/gestion-medidores/models/modelo.model';
import { ModeloService } from '../../services/modelo.service';
import { ModeloEliminarComponent } from './modelo-eliminar/modelo-eliminar.component';

@Component({
  selector: 'app-modelo',
  templateUrl: './modelo.component.html',
  styleUrls: ['./modelo.component.sass']
})
export class ModeloComponent implements OnInit {

  breadscrums = [
    {
      title: 'Modelos de Medidores',
      items: ['Home'],
      active: 'Modelos de Medidores'
    }
  ];

  
  filterToggle = false;
  displayedColumns = [
    'id',
    'codModelo',
    'desModelo',
    'marca',
    'fechaCreacion',
    'usuario',
    'actions'
  ];

  marcas: Modelo[];
  marcaPaginacion: MatTableDataSource<Modelo> = new MatTableDataSource();
  form: FormGroup;

  totalPages: Array<number>;
  totalPage: number;
  totalElements: number;
  errorMessage = errorMessages;

  page = 0;
  size = 5;
  order = "id";
  asc = false;

  isFirst = false;
  isLast = false;

  codModelo: string = '';
  desModelo: string = '';
  usuario: string = '';
  fechaInicio: any = '';
  fechaFin: any = '';
  idMarca: number = 0;

  fechaInicioActual = new Date();
  fechaFinActual = new Date();

  maxDate = new Date();

  constructor(
    public dialog: MatDialog,
    public modeloService: ModeloService,
    private formBuilder: FormBuilder,
    private router: Router,
    private datePipe: DatePipe,
    private snackBar: MatSnackBar
  ) {
    
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;
  @ViewChild(MatMenuTrigger)
  contextMenu: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };

  ngOnInit() {
    this.initForm();
    this.enviarValoresPorDefecto();
    this.form.value.fechaInicio = this.transformDate(this.form.value.fechaInicio.toString());
    this.form.value.fechaFin = this.transformDate(this.form.value.fechaFin.toString());
    this.getModeloPaginacion();
    this.getMarca();
  }

  initForm() {
    this.form = this.formBuilder.group({
      codModelo: ['', Validators.pattern('[a-zA-Z0-9 ]*')],
      desModelo: ['', Validators.pattern('[a-zA-Z0-9 ]*')],
      usuario: ['', Validators.pattern('[a-zA-Z0-9 ]*')],
      fechaInicio: ['', ''],
      fechaFin: ['', ''],
      idMarca: ['', '']
    });
  }

  enviarValoresPorDefecto(): void {
    this.form.get("fechaInicio").setValue(this.fechaInicioActual);
    this.form.get("fechaFin").setValue(this.fechaFinActual);
  }

  dias(fecha, dias){
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
  }

  transformDate(date: string): string{
    const dateTransform = this.datePipe.transform(date, 'yyyy-MM-dd');
    return dateTransform;
  }

  redirectEditar(id) {
    this.router.navigate(['/gestion-medidores/modelos/editar', id]);
  }

  getModeloPaginacion() {
    if(this.form.value.fechaInicio == '' && this.form.value.fechaFin != '') {
      this.snackBar.open('La fecha inicio no debe estar vacio.','Close');
      return
    }

    if(this.form.value.fechaFin == '' && this.form.value.fechaInicio != '') {
      this.snackBar.open('La fecha fin no debe estar vacio.','Close');
      return
    }

    this.codModelo = this.form.value.codModelo;
    this.desModelo = this.form.value.desModelo;
    this.usuario = this.form.value.usuario;
    this.fechaInicio = (!this.form.value.fechaInicio) ? '1900-01-01' : this.datePipe.transform(this.form.value.fechaInicio, 'yyyy-MM-dd'); // CHR 20-07-23
    this.fechaFin = (!this.form.value.fechaFin) ? '2999-01-01' : this.datePipe.transform(this.form.value.fechaFin, 'yyyy-MM-dd'); // CHR 20-07-23
    this.idMarca = ((this.form.value.idMarca == undefined) || (this.form.value.idMarca == '')) ? 0 : this.form.value.idMarca;
    this.modeloService.getModeloPaginacion(this.page, this.size, this.order, this.asc, this.codModelo, this.desModelo, this.usuario, this.fechaInicio, this.fechaFin, this.idMarca).pipe(
      tap((response) => {       
        this.marcaPaginacion = new MatTableDataSource<Modelo>(response.content);
        this.isFirst = response.first;
        this.isLast = response.last;
        this.totalPages = new Array(response.totalPages);
        this.totalPage = response.totalPages;
        this.totalElements = response.totalElements;
      })
    ).subscribe();
  }

  dialogEliminarMarca(row) {
    const dialogRef = this.dialog.open(ModeloEliminarComponent, { data: row ? row : '', disableClose: true });
    dialogRef.afterClosed().subscribe(result => {
      this.getModeloPaginacion()
    });
  }

  pageChanged(event: PageEvent) {
    this.size = event.pageSize;
    this.page = event.pageIndex;
    this.getModeloPaginacion();
  }

  ngAfterViewInit() {
    this.marcaPaginacion.paginator = this.paginator;
  }

  getMarca() {
    this.modeloService.getMarca().pipe(
      tap(response => {
        this.marcas = response;
      })
    ).subscribe();
  }
}
