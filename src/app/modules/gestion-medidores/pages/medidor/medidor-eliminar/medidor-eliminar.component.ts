import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { tap } from 'rxjs';
import { MedidorService } from '../../../services/medidor.service';

@Component({
  selector: 'app-medidor-eliminar',
  templateUrl: './medidor-eliminar.component.html',
  styleUrls: ['./medidor-eliminar.component.sass']
})
export class MedidorEliminarComponent {

  constructor(
    public dialogRef: MatDialogRef<MedidorEliminarComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public medidorService: MedidorService
  ) {}
  onNoClick(): void {
    this.dialogRef.close();
  }
  confirmDelete(): void {
    this.medidorService.deleteMedidor(this.data.id).pipe(
      tap((response) => !!response),
      tap((response) => this.dialogRef.close())
    ).subscribe();
  }

}
