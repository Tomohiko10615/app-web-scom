import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedidorEliminarComponent } from './medidor-eliminar.component';

describe('MedidorEliminarComponent', () => {
  let component: MedidorEliminarComponent;
  let fixture: ComponentFixture<MedidorEliminarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedidorEliminarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MedidorEliminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
