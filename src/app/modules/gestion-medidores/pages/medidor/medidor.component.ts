import { DatePipe, formatDate } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NumberValueAccessor, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { tap } from 'rxjs';
import { errorMessages } from 'src/app/core/error/error.constant';
import { Medidor } from 'src/app/core/models/medidor.model';
import { MedidorService } from '../../services/medidor.service';
import { MedidorEliminarComponent } from './medidor-eliminar/medidor-eliminar.component';
// R.I. REQSCOM09 07/08/2023 INICIO
import { map } from 'rxjs/operators';
import { ExcelExportService } from 'src/app/core/service/excel-export.service';
import { ParametroService } from 'src/app/core/service/parametro.service';
// R.I. REQSCOM09 07/08/2023 FIN

@Component({
  selector: 'app-medidor',
  templateUrl: './medidor.component.html',
  styleUrls: ['./medidor.component.css','./../../../../app.component.scss']
})

export class MedidorComponent implements OnInit {
  rangoDiasFechaBusqueda: number;
  filterToggle = false;
  displayedColumns = [
    'id',
    'nroMedidor',
    'marca',
    'modelo',
    'estado',
    'usuario',
    'fechaEstado',
    'fechaCreacion',
    'actions'
  ];

  breadscrums = [
    {
      title: 'Gestión de Medidores',
      items: ['Home'],
      active: 'Gestión de Medidores'
    }
  ];

  marcas: Medidor[];
  medidorPaginacion: MatTableDataSource<Medidor> = new MatTableDataSource();
  form: FormGroup;

  totalPages: Array<number>;
  totalPage: number;
  totalElements: number;
  errorMessage = errorMessages;

  page = 0;
  size = 5;
  order = "id";
  asc = false;

  isFirst = false;
  isLast = false;

  nroMedidor: string = '';
  idMarca: number = 0;
  idModelo: number = 0;
  usuario: string = '';
  fechaInicio: any = '';
  fechaFin: any = '';
  fechaInicioEstado: any = '';
  fechaFinEstado: any = '';
  idEstado: number = 0;

  estados: any[];
  modelos: any[];
  codigo_marca: any[];
  codigo_modelo: any[];

  fechaInicioActual = new Date();
  fechaFinActual = new Date();

  maxDate = new Date();

  valor: number = 0;
  // R.I. REQSCOM09 07/08/2023 INICIO
  exportDataExcel: any[];
  exportReport = { fileName:`reporte-medidores-${this.datePipe.transform(new Date, 'dd.MM.yyyy')}`, sheet: 'Hoja1' }

  // R.I. REQSCOM09 07/08/2023 FIN
  constructor(
    public dialog: MatDialog,
    public medidorService: MedidorService,
    // R.I. Corrección 20/10/2023 INICIO
    private parametroService: ParametroService,
    // R.I. Corrección 20/10/2023 FIN
    private formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private snackBar: MatSnackBar,
    // R.I. REQSCOM09 07/08/2023 INICIO
    private excelExportService: ExcelExportService,
    // R.I. REQSCOM09 07/08/2023 FIN
  ) {
    
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;
  @ViewChild(MatMenuTrigger)
  contextMenu: MatMenuTrigger;
  contextMenuPosition = { x: '0px', y: '0px' };

  // R.I. Corrección 20/10/2023 INICIO
  limiteDias: number = 0;
  // R.I. Corrección 20/10/2023 FIN

  ngOnInit() {
    this.initForm();
    this.enviarValoresPorDefecto();
    this.form.value.fechaInicio = this.transformDate(this.form.value.fechaInicio.toString());
    this.form.value.fechaFin = this.transformDate(this.form.value.fechaFin.toString());
    this.form.value.fechaInicioEstado = this.transformDate(this.form.value.fechaInicioEstado.toString());
    this.form.value.fechaFinEstado = this.transformDate(this.form.value.fechaFinEstado.toString());
    // R.I. Corrección 20/10/2023 INICIO
    if (this.limiteDias == 0) {
      this.getLimiteDias();
    }
    // R.I. Corrección 20/10/2023 FIN
    this.getMedidor();
    this.getMarca();
    this.getEstados();
  }

  // R.I. Corrección 20/10/2023 INICIO
  getLimiteDias() {
    this.parametroService.getLimiteDias().pipe(
      tap(response => {
        this.limiteDias = response.valorNum;
      })
    ).subscribe();
  }
  // R.I. Corrección 20/10/2023 FIN

  enviarValoresPorDefecto(): void {
    this.form.get("fechaInicio").setValue(this.fechaInicioActual);
    this.form.get("fechaFin").setValue(this.fechaFinActual);
  }

  dias(fecha, dias){
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
  }

  transformDate(date: string): string{
    const dateTransform = this.datePipe.transform(date, 'yyyy-MM-dd');
    return dateTransform;
  }

  initForm() {
    this.form = this.formBuilder.group({
      nroMedidor: ['', Validators.pattern('[0-9]*')],
      idMarca: ['', ''],
      idModelo: ['', ''],
      usuario: ['', Validators.pattern('[a-zA-Z0-9 ]*')],
      fechaInicio: ['', ''],
      fechaFin: ['', ''],
      fechaInicioEstado: ['', ''],
      fechaFinEstado: ['', ''],
      idEstado: ['', '']
    });
  }

  getMedidor() {

  //     //2023.06.26
  // if(((this.form.value.fechaFin == '' || this.form.value.fechaFin == null) || (this.form.value.fechaInicio == '' || this.form.value.fechaInicio == null))){
  //   this.snackBar.open('Favor de completar los campos fecha de creación desde y hasta.','Close',{
  //     duration: 1500
  //   });
  //   return
  // }
//  //2023.06.26
//  if(((this.form.value.fechaInicioEstado == '' || this.form.value.fechaInicioEstado == null) || (this.form.value.fechaFinEstado == '' || this.form.value.fechaFinEstado == null))){
//   this.snackBar.open('Favor de completar los campos fecha de creación Estado desde y hasta.','Close');
//   return
// }

  // if(this.form.value.fechaFin != '' && (this.form.value.fechaInicio == '' || this.form.value.fechaInicio == null) ) {
  //   this.snackBar.open('La fecha inicio no debe estar vacio.','Close');
  //   return
  // }

  // if(this.form.value.fechaInicio != '' && (this.form.value.fechaFin == '' || this.form.value.fechaFin == null)) {
  //   this.snackBar.open('La fecha fin no debe estar vacio.','Close');
  //   this.form.value.fechaInicio.
  //   return
  // }

  // if(this.form.value.fechaFinEstado != '' && (this.form.value.fechaInicioEstado == '' || this.form.value.fechaInicioEstado == null)) {
  //   this.snackBar.open('La fecha inicio estado no debe estar vacio.','Close');
  //   return
  // }

  // if(this.form.value.fechaInicioEstado != '' && (this.form.value.fechaFinEstado=='' || this.form.value.fechaFinEstado ==null)) {
  //   this.snackBar.open('La fecha fin estado no debe estar vacio.','Close');
  //   return
  // }

//2023.06.26
  // if(Math.round((this.form.value.fechaFin - this.form.value.fechaInicio)/(1000*60*60*24)) >= 6){
  //   this.snackBar.open('El rango para filtrar por Fecha Creación NO debe ser mayor a 6 dias','Close');
  //   return
  // }

//  //2023.06.26
//  if(Math.round((this.form.value.fechaFinEstado - this.form.value.fechaInicioEstado)/(1000*60*60*24)) >= 6){
//   this.snackBar.open('El rango para filtrar el reporte NO debe ser mayor a 6 dias','Close');
//   return
// }

console.log(this.limiteDias)

    if (
      (this.form.value.nroMedidor.replace(/\s/g, "") == '' || this.form.value.nroMedidor.replace(/\s/g, "") == null) &&
      (this.form.value.usuario.replace(/\s/g, "") == '' || this.form.value.usuario.replace(/\s/g, "") == null) ||
      (this.form.value.fechaInicio !== '' || this.form.value.fechaFin !== '' || this.form.value.fechaInicio !== null || this.form.value.fechaFin !== null)
    )
    {
      // R.I. Corrección 20/10/2023 INICIO
      if(((this.form.value.fechaFin !== '' && this.form.value.fechaFin !== null) || (this.form.value.fechaInicio !== '' && this.form.value.fechaInicio !== null))){
        //this.snackBar.open('Favor de completar los campos fecha de creación desde y hasta.','Close');
        //return
      //}
      //else {
        //this.rangoDiasFechaBusqueda = Number.parseInt(JSON.parse(sessionStorage.getItem('rangoFechaBusqueda')).valorNum);
        
        if (
          this.form.value.fechaInicio === '' ||
          this.form.value.fechaFin === '' ||
          this.form.value.fechaInicio === null ||
          this.form.value.fechaFin === null
        ) {
          this.snackBar.open('Favor de completar ambas fechas de creación.','Close');
          return;
        }
        if(Math.round((this.form.value.fechaFin - this.form.value.fechaInicio)/(1000*60*60*24)) >= this.limiteDias){
          console.log(this.limiteDias)
          this.snackBar.open('El reporte no debe ser mayor a ' + this.limiteDias + ' dias','Close');
          return
        }
        // R.I. Corrección 20/10/2023 FIN
      }
    }

    if ((this.form.value.fechaInicio == '' || this.form.value.fechaFin == '' || this.form.value.fechaInicio == null || this.form.value.fechaFin == null) && (this.form.value.nroMedidor.replace(/\s/g, "") == '' || this.form.value.nroMedidor.replace(/\s/g, "") == null) && (this.form.value.idMarca === '' || this.form.value.idMarca === null || this.form.value.idMarca === undefined) ) {
      this.snackBar.open('Favor de completar los campos fecha de creación desde y hasta.','Close');
        return
    }

    this.nroMedidor = this.form.value.nroMedidor;
    this.idMarca = ((this.form.value.idMarca == undefined) || (this.form.value.idMarca == '')) ? 0 : this.form.value.idMarca;
    this.idModelo = ((this.form.value.idModelo == undefined) || (this.form.value.idModelo == '')) ? 0 : this.form.value.idModelo;
    this.usuario = this.form.value.usuario;
    this.fechaInicio = (!this.form.value.fechaInicio) ? '1900-01-01' : this.datePipe.transform(this.form.value.fechaInicio, 'yyyy-MM-dd');
    this.fechaFin = (!this.form.value.fechaFin) ? '2999-01-01' : this.datePipe.transform(this.form.value.fechaFin, 'yyyy-MM-dd');
    this.fechaInicioEstado = (!this.form.value.fechaInicioEstado) ? '' : this.datePipe.transform(this.form.value.fechaInicioEstado, 'yyyy-MM-dd');
    this.fechaFinEstado = (!this.form.value.fechaFinEstado) ? '' : this.datePipe.transform(this.form.value.fechaFinEstado, 'yyyy-MM-dd');
    this.idEstado = ((this.form.value.idEstado == undefined) || (this.form.value.idEstado == '')) ? 0 : this.form.value.idEstado;
    this.medidorService.getMedidorPaginacion(this.page, this.size, this.order, this.asc, this.nroMedidor, this.idMarca, this.idModelo, this.usuario, this.fechaInicio, this.fechaFin, this.fechaInicioEstado, this.fechaFinEstado, this.idEstado).pipe(
      tap((response) => {
        this.medidorPaginacion = new MatTableDataSource<Medidor>(response.content);
        this.isFirst = response.first;
        this.isLast = response.last;
        this.totalPages = new Array(response.totalPages);
        this.totalPage = response.totalPages;
        this.totalElements = response.totalElements;
      })
    ).subscribe();
  }

  dialogEliminarMedidor(row) {
    const dialogRef = this.dialog.open(MedidorEliminarComponent, { data: row ? row : '', disableClose: true });
    dialogRef.afterClosed().subscribe(result => {
      this.getMedidor()
    });
  }

  pageChanged(event: PageEvent) {
    this.size = event.pageSize;
    this.page = event.pageIndex;
    this.getMedidor();
  }

  ngAfterViewInit() {
    this.medidorPaginacion.paginator = this.paginator;
  }

  getMarca() {
    this.medidorService.getMarcas().pipe(
      tap(response => {
        this.marcas = response;
      })
    ).subscribe();
  }

  getEstados() {
    this.medidorService.getEstados().pipe(
      tap(response => {
        this.estados = response;
      })
    ).subscribe();
  }

  getModelos(id: any) {
    if(id == undefined) {
      this.valor = 0;
    }else {
      this.valor = id;
    }
    this.form.value.idModelo = undefined; // R.I. Corrección 23/10/2023
    this.medidorService.getModelos(this.valor).pipe(
      tap(response => {
        this.modelos = response;
        if(id > 0) {
          this.codigo_marca = this.marcas.filter(function(marca : any) {
            return marca.id == id;
          });
        }
      })
    ).subscribe();
  }

  // R.I. REQSCOM09 07/08/2023 INICIO
  exportFile() {
    if (this.totalElements == 0) {
      this.snackBar.open('No hay datos de Resultado de búsqueda para exportar, seleccione filtrar para realizar nueva búsqueda', 'Close');
      return;
    }
  
    this.medidorService.getMedidorPaginacion(this.page, this.totalElements, this.order, this.asc, this.nroMedidor, this.idMarca, this.idModelo, this.usuario, this.fechaInicio, this.fechaFin, this.fechaInicioEstado, this.fechaFinEstado, this.idEstado).pipe(
      map((response: any) => {
        const modifiedContent = response.content.map((item: any) => {
          const { id, ...itemWithoutId } = item;
          itemWithoutId.fecha_creacion = formatDate(itemWithoutId.fecha_creacion, 'dd-MM-yyyy HH:mm:ss', 'en-US');
          itemWithoutId.fecha_estado = formatDate(itemWithoutId.fecha_estado, 'dd-MM-yyyy HH:mm:ss', 'en-US');
          return itemWithoutId;
        });
        response.content = modifiedContent;
        return response;
      }),
      tap((response: any) => {
        this.exportDataExcel = response.content;
      }),
      tap((response: any) => !!response),
      tap(() => this.excelExportService.exportAsExcelFile(this.exportDataExcel, this.exportReport.fileName))
    ).subscribe();
  }
  // R.I. REQSCOM09 07/08/2023 FIN
}
