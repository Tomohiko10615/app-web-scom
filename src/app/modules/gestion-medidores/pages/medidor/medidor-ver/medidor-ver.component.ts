import { Component, OnInit } from '@angular/core';
import { tap } from 'rxjs/operators';
import { UnsubscribeOnDestroyAdapter } from 'src/app/shared/UnsubscribeOnDestroyAdapter';
import { Medida } from '../../../models/medida.model';
import { Marca } from '../../../models/marca.model';
import { Ubicacion } from 'src/app/core/models/ubicacion.model';
import { errorMessages } from 'src/app/core/error/error.constant';
import { MedidorEditar } from 'src/app/core/models/medidorEditar.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MedidorService } from '../../../services/medidor.service';
import { ActivatedRoute } from '@angular/router';
import { UsuarioService } from 'src/app/core/service/usuario.service';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-medidor-ver',
  templateUrl: './medidor-ver.component.html',
  styleUrls: ['./medidor-ver.component.sass']
})

export class MedidorVerComponent extends UnsubscribeOnDestroyAdapter implements OnInit {

  breadscrums = [
    {
      title: 'Ver de Medidor',
      items: ['Home'],
      active: 'Ver de Medidor'
    }
  ];

  filterToggle = false;
  displayedColumns = [
    'id',
    'cant_ent',
    'cant_dec',
    'factor_codigo',
    'factor_descripcion',
    'factor_valor'
  ];

  marcas: Marca[];
  ubicaciones: Ubicacion[];
  modelos: any[];
  medidas: any[];
  medida: any;
  errorMessage = errorMessages;

  dataModelo = new Array();
  modeloData: MatTableDataSource<Medida> = new MatTableDataSource();

  medidor = new MedidorEditar();

  form: FormGroup;

  idUrl: number;

  ver : boolean = false;

  codigo_marca: any[];

  codigo_modelo: any[];

  titulo: string = "Creación de Medidor";

  boton: string = "Crear Medidor";
  constructor(private medidorService : MedidorService, private formBuilder: FormBuilder, 
              private usuarioService: UsuarioService, private route : ActivatedRoute) {
                super();
               }

  ngOnInit() {
    this.idUrl = this.route.snapshot.params['id'];


    this.getMarca();
    this.getUbicaciones();

    this.initForm();

    this.form.get('id_marca')?.setValue(0);
    this.form.get('id_modelo')?.setValue(0);
    this.form.get('id_ubicacion')?.setValue(0);

    if(this.idUrl) {
      this.getData(this.idUrl) 
      this.titulo = "Edición de Medidor";
      this.boton = "Editar Medidor";
      this.form.controls['nro_medidor'].disable();
      this.form.controls['cod_modelo'].disable();
    }

    if(document.location.href.search('ver') != -1) {
      this.ver = true;
      this.titulo = "Ver Medidor";
    }
  }

  initForm() {
    this.form = this.formBuilder.group({
      id_marca: [{ value : this.medidor ? this.medidor.id_marca : '', disabled: true}, Validators.required],
      id_ubicacion: [{ value : this.medidor ? this.medidor.id_ubicacion : '', disabled: true}, Validators.required],
      id_modelo: [{ value : this.medidor ? this.medidor.id_modelo : '', disabled: true}, Validators.required],
      serie: [{ value : this.medidor ? this.medidor.serie : '', disabled: true}, Validators.required],
      nro_medidor: [{ value : this.medidor ? this.medidor.numero : '', disabled: true}, Validators.required],
      cod_modelo: [{ value : this.medidor ? this.medidor.cod_modelo : '', disabled: true}, Validators.required]
    });
  }

  getData(id: number) {
    this.medidorService.getData(id).pipe(
      tap((response: any) => {
        const medidor = response
        this.medidor = medidor
        this.initForm()
        this.getModelos(this.medidor.id_marca)
        this.getMedidas(this.medidor.id_modelo);
        response.medidas.forEach((element: any) => {
          const dataResponse = 
                        { 
                          'id_medida': element.id_medida,
                          'medida_descripcion' : element.medida_descripcion,
                          'id_ent_dec': element.id_ent_dec,
                          'cant_enteros': element.cant_enteros,
                          'cant_decimales': element.cant_decimales, // CHR 07-09-2023 INC000115648937
                          'id_factor': element.id_factor,
                          'factor_codigo': element.factor_codigo,
                          'factor_descripcion': element.factor_descripcion,
                          'factor_valor': element.factor_valor
                        };
          this.dataModelo.push(dataResponse)
          this.modeloData = new MatTableDataSource<Medida>(this.dataModelo);
        });
      }) 
    ).subscribe();
  }

  getMarca() {
    this.medidorService.getMarcas().pipe(
      tap(response => {
        this.marcas = response;
      })
    ).subscribe();
  }

  getUbicaciones() {
    this.medidorService.getUbicaciones().pipe(
      tap(response => {
        this.ubicaciones = response;
      })
    ).subscribe();
  }

  getModelos(id: any) {
    this.medidorService.getModelos(id).pipe(
      tap(response => {
        this.modelos = response;
        if(id > 0) {
          this.codigo_marca = this.marcas.filter(function(marca : Marca) {
            return marca.id == id;
          });
        }
      })
    ).subscribe();
  }

  getMedidas(id: any) {
    this.medidorService.getMedidas(id).pipe(
      tap(response => {
        this.medidas = response;
        console.log(id)
        if(id > 0) {
          this.codigo_modelo = this.modelos.filter(function(marca : Marca) {
            return marca.id == id;
          });
        }
      })
    ).subscribe();
  }

  store() {
    if(this.dataModelo.length == 0) {
      alert('Debe agregar medidas')
    }else {
      const usuario = this.usuarioService.usuarioStorage;
      const id_medidor = (this.idUrl > 0) ? this.idUrl : 0;
      const form = {
        "id_usuario" : usuario?.id,
        "id_medidor" : id_medidor,
        ...this.form.value,
        ...{
          'medidas' : this.dataModelo
        }
      }
      this.medidorService.postMedidor(form).subscribe();
    }
  }

  selectMedida(medida: any) {
    this.medida = medida;
  }

  addMedida() {
    const medida = this.medida.split('-');
    const dataResponse = 
                        { 
                          'id_medida': medida[0],
                          'medida_descripcion' : medida[1],
                          'id_ent_dec': medida[2],
                          'cant_enteros': medida[3],
                          'cant_decimales': medida[4],
                          'id_factor': medida[5],
                          'factor_codigo': medida[6],
                          'factor_descripcion': medida[7],
                          'factor_valor': medida[8]
                        };
    this.dataModelo.push(dataResponse);
    this.modeloData = new MatTableDataSource<Medida>(this.dataModelo);
  }

  remove(i: number) {
    this.dataModelo.splice(i, 1);
    this.modeloData = new MatTableDataSource<Medida>(this.dataModelo);
  }

  

}
