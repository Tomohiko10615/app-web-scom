import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedidorVerComponent } from './medidor-ver.component';

describe('MedidorVerComponent', () => {
  let component: MedidorVerComponent;
  let fixture: ComponentFixture<MedidorVerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedidorVerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MedidorVerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
