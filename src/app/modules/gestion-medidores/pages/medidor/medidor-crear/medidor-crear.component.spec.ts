import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedidorCrearComponent } from './medidor-crear.component';

describe('MedidorCrearComponent', () => {
  let component: MedidorCrearComponent;
  let fixture: ComponentFixture<MedidorCrearComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedidorCrearComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MedidorCrearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
