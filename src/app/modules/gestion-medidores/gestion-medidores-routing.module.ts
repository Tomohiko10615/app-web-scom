import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "src/app/core/guard/auth.guard";
import { PermissionGuard } from "src/app/core/guard/permission.guard";
import { MarcaComponent } from "./pages/marca/marca.component";
import { MedidorCrearComponent } from "./pages/medidor/medidor-crear/medidor-crear.component";
import { MedidorVerComponent } from "./pages/medidor/medidor-ver/medidor-ver.component";
import { MedidorComponent } from "./pages/medidor/medidor.component";
import { ModeloCrearComponent } from "./pages/modelo/modelo-crear/modelo-crear.component";
import { ModeloVerComponent } from "./pages/modelo/modelo-ver/modelo-ver.component";
import { ModeloComponent } from "./pages/modelo/modelo.component";
import { ReporteMedidoresComponent } from "./pages/reporte-medidores/reporte-medidores.component";

const routes: Routes = [
    {
        path: 'marca',
        canActivate: [PermissionGuard, AuthGuard],
        component: MarcaComponent,
        data: { permission: 'ROL_MARCAS' }
    },
    {
        path: 'medidores',
        component: MedidorComponent, 
        canActivate: [PermissionGuard, AuthGuard],
        data: { permission: 'ROL_MEDIDORES' }
    },
    {
        path: 'medidores/crear',
        component: MedidorCrearComponent,
        canActivate: [PermissionGuard, AuthGuard],
        data: { permission: 'ROL_MEDIDORES' }
    },
    {
        path: 'medidores/editar/:id',
        component: MedidorCrearComponent,
        canActivate: [PermissionGuard, AuthGuard],
        data: { permission: 'ROL_MEDIDORES' }
    },
    {
        path: 'medidores/ver/:id',
        component: MedidorVerComponent,
        canActivate: [PermissionGuard, AuthGuard],
        data: { permission: 'ROL_MEDIDORES' }
    },
    {
        path: 'modelos',
        component: ModeloComponent,
        canActivate: [PermissionGuard, AuthGuard],
        data: { permission: 'ROL_MODELOS' }
    },
    {
        path: 'modelos/crear',
        component: ModeloCrearComponent,
        canActivate: [PermissionGuard, AuthGuard],
        data: { permission: 'ROL_MODELOS' }
    },
    {
        path: 'modelos/editar/:id',
        component: ModeloCrearComponent,
        canActivate: [PermissionGuard, AuthGuard],
        data: { permission: 'ROL_MODELOS' }
    },
    {
        path: 'modelos/ver/:id',
        component: ModeloVerComponent,
        canActivate: [PermissionGuard, AuthGuard],
        data: { permission: 'ROL_MODELOS' }
    },
    {
        path: 'reporte-medidores',
        component: ReporteMedidoresComponent,
        canActivate: [PermissionGuard, AuthGuard],
        data: { permission: 'ROL_REPORTE_MEDIDORES' }
        
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GestionMedidoresRoutingModule { }