import { formatDate } from '@angular/common';
export class Marca {
  //id: number;
  id: string; //CHR 20-07-23
  nro: string;
  codMarca: string;
  desMarca: string;
  fechaCreacion: string;
  usuarioCreador: string;

  constructor(marca) {
    {
      this.id = marca.id || this.getRandomID();
      this.nro = marca.nro || '';
      this.codMarca = marca.codMarca || '';
      this.desMarca = marca.desMarca || '';
      this.fechaCreacion = formatDate(new Date(), 'yyyy-MM-dd', 'en') || '';
      this.usuarioCreador = marca.usuarioCreador || '';
    }
  }
  public getRandomID(): string {
    const S4 = () => {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return S4() + S4();
  }
}
