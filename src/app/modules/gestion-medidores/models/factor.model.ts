export class Factor {
    id: number;
	desFactor: string;
	codFactor: string;
	valFactor: string;

	constructor(factor) {
        {
            this.id = factor.id || '';
            this.desFactor = factor.desFactor || '';
            this.codFactor = factor.codFactor || '';
            this.valFactor = factor.valFactor || '';
        }
    }
}