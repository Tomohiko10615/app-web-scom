import { formatDate } from '@angular/common';
export class Medidor {
  id: number;
  nro: string;
  codMarca: string;
  desMarca: string;
  fechaCreacion: string;
  usuarioCreador: string;

  constructor(medidor) {
    {
      this.id = medidor.id || this.getRandomID();
      this.nro = medidor.nro || '';
      this.codMarca = medidor.codMarca || '';
      this.desMarca = medidor.desMarca || '';
      this.fechaCreacion = formatDate(new Date(), 'yyyy-MM-dd', 'en') || '';
      this.usuarioCreador = medidor.usuarioCreador || '';
    }
  }
  public getRandomID(): string {
    const S4 = () => {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return S4() + S4();
  }
}
