import { formatDate } from '@angular/common';
export class Modelo {
  id: string;
  nro: string;
  codModelo: string;
  desModelo: string;
  marca: string;
  fechaCreacion: string;
  usuarioCreador: string;

  constructor(modelo) {
    {
      this.id = modelo.id || this.getRandomID();
      this.nro = modelo.nro || '';
      this.codModelo = modelo.codModelo || '';
      this.desModelo = modelo.desModelo || '';
      this.marca = modelo.marca || '';
      this.fechaCreacion = formatDate(new Date(), 'dd-MM-yyyy HH:mm:ss', 'en') || '';
      this.usuarioCreador = modelo.usuarioCreador || '';
    }
  }
  public getRandomID(): string {
    const S4 = () => {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return S4() + S4();
  }
}
