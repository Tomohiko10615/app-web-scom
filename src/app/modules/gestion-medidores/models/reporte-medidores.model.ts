export class ReporteMedidores {

    tipoComponente:string;
    marca: string;
    modelo: string;
    nroMedidor: string;
    estado: string;
    fechaDesde: string;
    fechaHasta: string;
    ubicacion: string;
    username: string;
    nroCuenta: string;
    codPrimarioApe: string;         //INICIO - REQ # 7 - JEGALARZA 
    codSecundarioApe: string;
    codPrimarioCaja: string;
    codSecundarioCaja: string;
    codMedidor: string;             //FIN - REQ # 7 - JEGALARZA 
}