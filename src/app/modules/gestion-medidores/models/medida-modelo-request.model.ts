export class MedidaModeloRequest {
    idModelo: number;
    idMedida: number;
    idEntDec: number;
    idTipoCalculo: number; /* CHR 04-07-23 */
}