import { formatDate } from '@angular/common';
export class Medida {
    id_medida: number;
    medida_descripcion: string;
    id_ent_dec: string;
    cant_enteros    : string;
    cant_decimales: string;
    id_medida_modelo: number;
    des_tip_calculo: string;

    constructor(medida) {
        {
            this.id_medida = medida.id_medida || '';
            this.medida_descripcion = medida.medida_descripcion || '';
            this.id_ent_dec = medida.id_ent_dec || '';
            this.cant_enteros = medida.cant_enteros || '';
            this.cant_decimales = medida.cant_decimales || '';
            this.des_tip_calculo = medida.des_tip_calculo || '';
            this.id_medida_modelo = medida.id_medida_modelo || '';
        }
    }
    public getRandomID(): string {
        const S4 = () => {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return S4() + S4();
    }
}
