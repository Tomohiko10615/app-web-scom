import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/service/auth.service';
import { errorMessages } from 'src/app/core/error/error.constant';
import { UsuarioService } from 'src/app/core/service/usuario.service';
import { Usuario } from 'src/app/core/models/usuario.model';
import { tap } from 'rxjs/operators';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { MenuService } from 'src/app/core/service/menu.service';
import { PerfilToken, UsuarioPerfil, UsuarioToken } from 'src/app/core/interfaces/core.interface';
import { Parametro } from 'src/app/core/models/Parametro.model';
import { ParametroService } from 'src/app/core/service/parametro.service';
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  pathProd = environment.pathProd;
  form: FormGroup;
  formLogin: FormGroup;
  usuario: Usuario;
  errorServerMessage: string;
  errorMessage = errorMessages;
  menus: any[];
  perfilesToken: PerfilToken[] = [];
  usuarioToken: UsuarioToken = {id: 0, username: '', nombrePerfil: '', id_perfil: 0, cod_perfil:'', authorities: []};
  usuarioPerfil : UsuarioPerfil = {idPerfil: 0, username: '', nombrePerfil:'', paramNombre: ''};
  showButtonValidarPerfil: boolean = false;
  showButtonIngresar: boolean = true;

  constructor(
    private router: Router,
    private authService: AuthService,
    private usuarioService: UsuarioService,
    private formBuilder: FormBuilder,
    private menuService: MenuService,
    private parametroService: ParametroService
  ) {}
  ngOnInit() {
    this.initForm();
  }


  initForm(): void {
    this.form = this.formBuilder.group({
      username: ['', [Validators.required, Validators.maxLength(25)]],
      password: ['', [Validators.required,  Validators.maxLength(100)]]
    });
  }
  onSubmit() {
    this.form.addControl('idPerfil', new FormControl('', Validators.required));
    //this.form.get('idPerfil').setValue(-1);

    const usuario = this.form.value;
    usuario.username = usuario.username.toLowerCase();
    this.authService.logout();
    this.authService.saveToken(null);
    this.authService.login(usuario).pipe(
     tap((response)=> {
      this.authService.saveToken(response.access_token);
      this.showButtonValidarPerfil = true;
      this.showButtonIngresar = false;
      this.perfilesToken= response.access_perfil;      ;
      this.usuarioToken = this.usuarioService.saveUsuarioTokenSessionStorage(response);
     }),
     tap(() =>{
      this.updateUltimoAccesoUsuario(this.usuarioToken.username)
      this.form.controls['idPerfil'].setValidators([Validators.required])
      this.form.get('idPerfil').updateValueAndValidity();
     }), 
    ).
    subscribe();
  }

  guardarMenusStorage(idPerfil: number){
    this.menuService.getMenuByIdPerfil(idPerfil).subscribe((response: any) => {
      this.router.navigate([`${response[0].nombre_modulo}`])
      sessionStorage.setItem('menu',JSON.stringify(response));
    }); 
  }

  updateUltimoAccesoUsuario(username: string) :void {
    this.usuarioService.updateUsuario(username).subscribe();
  }

  onValidarPerfil(): void {
    
    const id = this.form.get("idPerfil").value;
    
    sessionStorage.removeItem('menu');
    sessionStorage.removeItem("usuarioPerfil")
    sessionStorage.removeItem("usuarioRoles");

    this.authService.obtenerAccesosUsuario(this.usuarioToken.username,id).pipe(
      tap((reponse) => !!reponse),
      tap((response) => {
        this.usuarioPerfil.idPerfil = response?.idPerfil;
        this.usuarioPerfil.username =  response?.username;
        this.usuarioPerfil.nombrePerfil = response?.nombrePerfil;
        this.usuarioPerfil.paramNombre = response?.paramNombre;
        sessionStorage.setItem('usuarioPerfil', JSON.stringify(this.usuarioPerfil));
        sessionStorage.setItem('usuarioRoles', JSON.stringify(response.roles));
      }),
      tap(()=> this.guardarMenusStorage(id))
    ).subscribe();

    this.form.clearValidators();
    this.obtenerRangoFechaBusqueda();
  }

  obtenerRangoFechaBusqueda(): void {
    this.parametroService.obtenerRangoFechaBusqueda().pipe(
      tap((response: Parametro) => {
        sessionStorage.setItem('rangoFechaBusqueda', JSON.stringify(response));
      })
    ).subscribe();
  }

  
}
