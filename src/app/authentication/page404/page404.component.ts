import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/service/auth.service';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-page404',
  templateUrl: './page404.component.html',
  styleUrls: ['./page404.component.scss']
})
export class Page404Component implements OnInit {

  pathProd = environment.pathProd;
  constructor(private router: Router,
    private authService: AuthService) {}
  ngOnInit() {}

  submit() {
    this.router.navigate(['/authentication/signin']);
  }
  
  irLogin() {
    this.authService.logout();
    this.router.navigate(['/authentication/signin']);

  }
}
