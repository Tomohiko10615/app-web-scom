import { Injectable } from "@angular/core";
import { MatPaginatorIntl } from "@angular/material/paginator";
import { TranslateParser, TranslateService } from '@ngx-translate/core';

@Injectable()
export class CustomMatPaginatorIntl extends MatPaginatorIntl {

    OF_LABEL = 'de';

    itemsPerPageLabel: string;
    nextPageLabel: string;
    previousPageLabel: string;
    firstPageLabel: string;
    lastPageLabel: string;

    constructor(private translateService: TranslateService, private translateParser: TranslateParser) {
        super();
    }
    
    getRangeLabel = (page: number, pageSize: number, length: number) => {
        if (length === 0 || pageSize === 0) {
            return `0 ${this.OF_LABEL} ${length}`;
          }
          length = Math.max(length, 0);
          const startIndex = page * pageSize;
          const endIndex =
            startIndex < length
              ? Math.min(startIndex + pageSize, length)
              : startIndex + pageSize;
          return `${startIndex + 1} - ${endIndex} ${
            this.OF_LABEL
          } ${length}`;
        
    };
    
    private getTranslations() {
        this.itemsPerPageLabel = this.translateService.instant('Items por página');
        this.nextPageLabel = this.translateService.instant('Siguiente página');
        this.previousPageLabel = this.translateService.instant('Anterior página');
        this.firstPageLabel = this.translateService.instant('Primera página');
        this.lastPageLabel = this.translateService.instant('Última página');
    }
  }