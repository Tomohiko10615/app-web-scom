import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../models/user';
import { environment } from 'src/environments/environment';
import { UsuarioService } from './usuario.service';
import { Usuario } from '../models/usuario.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private urlEndPoint = `${environment.apiUrl}/oauth/token`;
  private urlEndPointGestion = `${environment.apiUrlGestion}`;
  private _token: string | null = null;

  constructor(private http: HttpClient,
    private usuarioService: UsuarioService) {
  }


  // SCOM METHODS

  
  login(usuario: Usuario): Observable<any> {
    const params = new URLSearchParams(); 
    params.set('grant_type', 'password');
    params.set('username', usuario.username);
    params.set('password', usuario.password);
    return this.http.post<any>(this.urlEndPoint, params.toString());

  }

  saveToken(accessToken: string | null): void {
    if (accessToken) {
      sessionStorage.setItem('token', accessToken);
    }
  }

  obtenerAccesosUsuario(username: string, idPerfil: number): Observable<any> {
    return this.http.post<any>(this.urlEndPointGestion+'/api/usuario/update-perfil/'+username +'/'+idPerfil, {});
  }

  public get token(): string | null {

    if(sessionStorage.getItem('token') != null) {
      this._token = sessionStorage.getItem('token');
      return this._token;
    }else {
      return null;
    }
  }

  logout(): void {
    sessionStorage.clear();
    localStorage.clear();
    sessionStorage.removeItem('usuario');
    sessionStorage.removeItem('menu');
    sessionStorage.removeItem('token');
    sessionStorage.removeItem("filtro");
    sessionStorage.removeItem("filtroOrdenScom");
    sessionStorage.removeItem('usuarioToken');
    sessionStorage.removeItem("usuarioPerfil")
    sessionStorage.removeItem("usuarioRoles");
  }

  isAuthenticated(): boolean {
    const payload = this.usuarioService.obtenerDatosToken(this.token);
    if (payload != null && payload.username) {
      return true;
    } else {
    return false;
    }
  }
}
