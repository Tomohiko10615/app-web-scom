import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { AuthoritiesToken, UsuarioPerfil, UsuarioRol, UsuarioToken } from "../interfaces/core.interface";
import { Menu } from "../models/menu.model";
import { Usuario } from "../models/usuario.model";
import { MenuService } from "./menu.service";

@Injectable({
    providedIn: 'root'
})
export class UsuarioService {
    private urlEndPoint = `${environment.apiUrlGestion}/api/usuario`;
    usuarioToken: UsuarioToken = {id: 0, username: '', nombrePerfil: '', id_perfil: 0, cod_perfil:'', authorities: []};
    usuario: UsuarioToken = {id: 0, username: '', nombrePerfil: '', id_perfil: 0, cod_perfil:'', authorities: []};
    usuarioSession : UsuarioToken;
    usuarioPerfilSession : UsuarioPerfil;
    usuarioRol: UsuarioRol;
    menus: Menu[] = [];
    menuSession: Menu[] = [];

    constructor(private http: HttpClient) { }

    //Obtener información de usuario del sesion storage
    public get usuarioStorage(): UsuarioToken | null {
        if (this.usuarioSession != null) {
            return this.usuarioSession;
        } else if (this.usuarioSession == null && sessionStorage.getItem('usuarioToken') != null) {
            this.usuarioSession = JSON.parse(sessionStorage.getItem('usuarioToken') || '');
            return this.usuarioSession;
        }
        return null;
    }

    public get usuarioPerfilStorage(): UsuarioPerfil | null {
        if (this.usuarioPerfilSession != null) {
            return this.usuarioPerfilSession;
        } else if (this.usuarioPerfilSession == null && sessionStorage.getItem('usuarioPerfil') != null) {
            this.usuarioPerfilSession = JSON.parse(sessionStorage.getItem('usuarioPerfil') || '');
            return this.usuarioPerfilSession;
        }
        return null;
    }

    public get usuarioRolesStorage(): UsuarioRol | null {
        if (this.usuarioRol != null) {
            return this.usuarioRol;
        } else if (this.usuarioRol == null && sessionStorage.getItem('usuarioRoles') != null) {
            this.usuarioRol = JSON.parse(sessionStorage.getItem('usuarioRoles') || '');
            return this.usuarioRol;
        }
        return null;
    }

    obtenerDatosToken(accessToken: any): any {
        if (accessToken != null) {
            return JSON.parse(atob(accessToken.split('.')[1]));
        }
        return null;
    }


    obtenerUserNameToken(response: any): any {
        const payload = this.obtenerDatosToken(response.access_token);
        return payload.user_name;
    }


    saveUsuarioTokenSessionStorage(response: any): any {
        const payload = this.obtenerDatosToken(response.access_token);
        this.usuarioToken.id = payload.id;
        this.usuarioToken.username = payload.user_name;
      //  this.usuarioToken.nombrePerfil = payload.perfil;
       // this.usuarioToken.id_perfil = payload.id_perfil;
       // this.usuarioToken.cod_perfil = payload.cod_perfil;
        payload.authorities.forEach((perfil: any)=> {
            let per : AuthoritiesToken = {authority: perfil.authority};
            this.usuarioToken.authorities.push(per);
          });
        sessionStorage.setItem('usuarioToken', JSON.stringify(this.usuarioToken));
        return this.usuarioToken;
    }

    hasRoleGuard(component: any): boolean {
        const usuarioRol: any = this.usuarioRol;
        if(usuarioRol) {
            if (usuarioRol.nombre.find((rol: any) => component?.data?.permission.includes(rol.nombre))) {
                return false;
              }
              return true;
        }
      }


      hasRole(nombreRol: string): boolean {
        const usuarioRol: any = this.usuarioRol;
        if (usuarioRol.nombre.find((rol: any) => rol.nombre === nombreRol)) {
            return true;
        }else {
            return false;
        }
      }

    getUsuarioById(id: number): Observable<Usuario[]> {
        const params = new URLSearchParams();
        params.set('id', id.toString());
        return this.http.get<Usuario[]>(this.urlEndPoint + '/findById/' + params.toString());
    }

    updateUsuario(username:string){
        return this.http.put<any>(this.urlEndPoint+ '/update-usuario/'+ username, {} );
      }
}