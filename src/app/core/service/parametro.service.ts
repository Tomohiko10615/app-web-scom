import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
  })
  export class ParametroService {
    private urlEndPoint = `${environment.apiUrlGestion}/api/parametro`;
    constructor(private http: HttpClient) { }
  
    //Tipos de proceso de las ordenes
    getProcesos(): Observable<any> {
      return this.http.get<any[]>(this.urlEndPoint + '/tipo_proceso');
    }
    //Estados de las ordenes de trabajo
    getEstados(): Observable<any> {
      return this.http.get<any[]>(this.urlEndPoint + '/estados');
    }
    //Estados de la transferencias
    getEstadosTransferencia(): Observable<any> {
      return this.http.get<any[]>(this.urlEndPoint + '/estadosTransfer');
    }

    getTiposUbicacionMedidor(): Observable<any> {
      return this.http.get<any[]>(this.urlEndPoint + '/tiposUbicacionMed');
    }

    obtenerRangoFechaBusqueda(): Observable<any> {
      return this.http.get<any>(this.urlEndPoint + '/rangoFechaBusqueda');
    }

    getLimiteDias(): Observable<any> {
      return this.http.get<any>(this.urlEndPoint + '/rangoFechaBusqueda?rango=RANGODIASM');
    }
  }
  