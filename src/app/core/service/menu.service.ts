import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { Menu } from "../models/menu.model";
import { Usuario } from "../models/usuario.model";
import { tap } from 'rxjs';
import { UsuarioService } from "./usuario.service";

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  menus: Menu[] = [];
  private urlEndPoint = `${environment.apiUrlGestion}/api/menu`;
  
  constructor(private http: HttpClient,
              private usuarioService: UsuarioService) { }
  
  //obtener roles del usuario logueado segun su perfil
  getMenuByIdPerfil(idPerfil: number): Observable<Menu[]> {
    return this.http.get<Menu[]>(this.urlEndPoint + '/menuPerfil/' + idPerfil);
  }
}