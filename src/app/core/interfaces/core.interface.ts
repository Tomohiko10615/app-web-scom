export interface UsuarioToken {
    id: number;
    username: string;
    nombrePerfil: string;
    id_perfil: number;
    cod_perfil: string;
    authorities: AuthoritiesToken[];
}

export interface AuthoritiesToken {
    authority: string;
}

export interface ChartBarReport {
    name: string;
    value: number;
}

export interface PerfilToken {
    id: number;
    nombre: string;
    paramNombre: string;
}


export interface UsuarioPerfil {
    idPerfil: number;
    username: string;
    nombrePerfil: string;
    paramNombre: string;
}

export interface UsuarioRol {
    nombre: string
}