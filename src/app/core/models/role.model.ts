export class Role {
    codRol: number;
    nombre: string;
    codPerfil: number;
    descripcion: string;
    menu: string;
    nombreMenu: string;
}