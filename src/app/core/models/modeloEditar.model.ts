export class ModeloEditar {
    id?: number;
    cod_marca: string;
    des_marca: string;
    cod_modelo: string;
    des_modelo: string;
    cant_anos_vida: string;
    cant_anos_almacen: string;
    nro_sellos_medidor: string;
    nro_sellos_bornera: string;
    nro_registrador: string;
    es_reacondicionador: string;
    es_reseteado: string;
    es_patron: string;
    es_totalizador: string;
    clase_medidor: string;
    constante: string;
    cant_hilos: string;
    id_marca: number;
    id_fase: number;
    id_amperaje: number;
    id_tipo_medicion: number;
    id_voltaje: number;
    id_tension: number;
    id_tecnologia: number;
    id_registrador: number;
    id_unidad_medida_constante: number;
}
