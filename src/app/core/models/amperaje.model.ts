export class Amperaje {
    id ?: number;
    codAmperaje: string;
    desAmperaje: string;
    valAmperaje: string;
    activo: boolean;
}
