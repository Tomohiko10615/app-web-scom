import { Usuario } from "./usuario.model"

export class Marca {
    id?: number;
    codigo: string
    descripcion: string
    usuario?: Usuario
}
