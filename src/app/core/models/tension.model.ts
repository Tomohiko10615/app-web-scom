export class Tension {
    id ?: number;
    codTension: string;
    desTension: string;
    codInterno: string;
    activo: boolean;
}
