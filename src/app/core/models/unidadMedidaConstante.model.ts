export class UnidadMedidaConstante {
    id ?: number;
    codUniMedConst: string;
    desUniMedConst: string;
    activo: boolean;
}
