export class Fase {
    id ?: number;
    codFase: string;
    desFase: string;
    codInterno: string;
    activo: boolean;
}
