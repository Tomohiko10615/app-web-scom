import { Role } from "./role.model";

export class Perfil {
    id: number;
    nombre: string;
    activo: string;
    roles: Role[] = [];

}