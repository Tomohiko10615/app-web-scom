import { MedEntDec } from "./medentdec.model";
import { Medida } from "./medida.model";
import { Modelo } from "./modelo.model";

export class MedidaModelo {
    medida: Medida;
    modelo: Modelo;
    medEntDec: MedEntDec;
}
