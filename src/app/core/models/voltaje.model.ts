export class Voltaje {
    id ?: number;
    codVoltaje: string;
    desVoltaje: string;
    valVoltaje: string;
    activo: boolean;
}
