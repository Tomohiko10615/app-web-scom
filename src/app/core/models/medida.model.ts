export class Medida {
    id ?: number;
    codMedida: string;
    desMedida: string;
    codInterno: string;
    activo ?: boolean;
}
