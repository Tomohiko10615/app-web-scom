import { Marca } from "./marca.model";

export class MarcaPaginacion {
    content: Marca[] = [];
    pageable: [
        sort: [
            empty: boolean,
            sorted: boolean,
            unsorted: boolean
        ],
        offset: number,
        pageSize: number,
        pageNumber: number,
        unpaged: number,
        paged: number
    ];
    last: boolean;
    totalPages: number;
    totalElements: number;
    size: number;
    number: number;
    sort: [
        empty: boolean,
        sorted: boolean,
        unsorted: boolean
    ];
    first: boolean;
    numberOfElements: number;
    empty: boolean;
}
