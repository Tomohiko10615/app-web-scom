export class Menu {
    nombre: string;
    descripcion: string;
    icono: string;
    url: string;
    id_padre: number;
    param_nombre: string;
    nombre_modulo: string;
    clase: string;
    titulo_grupo: boolean;
    sub_items: Menu[] = [];
}