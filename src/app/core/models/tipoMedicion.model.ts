export class TipoMedicion {
    id ?: number;
    descripcion: string;
    activo: boolean;
}
