export class Ubicacion {
    id ?: number;
    codigo: string;
    descripcion: string;
    activo: boolean;
}
