export class Registrador {
    id ?: number;
    codRegistrador: string;
    desRegistrador: string;
    activo: boolean;
}
