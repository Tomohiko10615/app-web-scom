export class MedidorEditar {
    id ?: number;
    id_marca : number;
    id_modelo: number;
    id_descripcion: number;
    numero : string;
    id_ubicacion: number;
    serie: string;
    cod_modelo: string;
}
