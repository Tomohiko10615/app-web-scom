import { Factor } from "./factor.model";
import { Medida } from "./medida.model";
import { Medidor } from "./medidor.model";

export class MedidaMedidor {
    cantEnteros: string;
    cantDecimales: string;
    medida: Medida;
    medidor: Medidor;
    factor: Factor;
}
