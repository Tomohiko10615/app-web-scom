export class Parametro {
    id: number;
	codigo: string;
	descripcion: string;
	valorAlf: string;
	valorNum: number;
	rol: string;
}
