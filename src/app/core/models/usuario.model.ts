import { Perfil } from "./perfil.model";

export class Usuario {
    id: number;
    nombre: string;
    username: string;
    ape_materno: string;
    ape_paterno: string;
    perfil: Perfil; 
    password: string;
}