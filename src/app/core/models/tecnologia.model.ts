export class Tecnologia {
    id ?: number;
    codTecnologia: string;
    desTecnologia: string;
    codInterno: string;
    activo: boolean;
}
