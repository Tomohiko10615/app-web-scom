import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";
import { AutoCompleteModel } from "src/app/modules/gestion-ordenes/models/auto-complete.model";

// Leverage TypeScript type guards to check to see if we have a Character type selected
function instanceOfCharacter(character: any): character is AutoCompleteModel {
    return !!character // truthy
    && typeof character !== 'string' // Not just string input in the autocomplete
    && 'codigo' in character; // Has some qualifying property of Character type
}

export const CharacterSelectionRequiredValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null =>
		!instanceOfCharacter(control?.value) ? { matchRequired: true } : null;
