import { Injectable } from '@angular/core';
import {
  HttpRequest, HttpHandler,
  HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ServerErrorInterceptor implements HttpInterceptor {
  constructor(private snackBar: MatSnackBar,
    private router: Router) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {

    return next.handle(request).pipe(
      catchError(error => {
        if (error instanceof HttpErrorResponse) {
          switch ((error as HttpErrorResponse).status) {
            case 0:
              return this.showError('Servicio no activo.');
            case 400:
              return this.errorAuthentication(error?.error);
            case 401:
              return this.showError('Error en la autenticación');
            case 403:
              return this.showError(error.error.message);
            case 409:
              return this.showError(error.error.message);
            case 500:
              console.log("Interceptor error=",error.error);
              return this.showError(error.error.message);
            case 422:
              return null
            default:
              return throwError(error.error.message);
          }

        } else {
          return throwError(error);
        }
      })
    );
  }

  showError(message: string): any {
    //this.snackBar.open(message, 'CERRAR');
      console.error("App Interceptor SCOM: Ocurrio un error, message = "+message);
      return message;
  }

  handle500Error () {
    this.router.navigate(['page500']);
  }

  errorAuthentication(message: any): any {
    if(message?.error === 'invalid_grant') {
      this.showError('Su usuario o contraseña son incorrectos.');
      return;
    } 
    this.showError('Ha ocurrido un error al procesar la solicitud.');
  }
}