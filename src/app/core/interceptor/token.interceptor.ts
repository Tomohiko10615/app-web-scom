import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AuthService } from "../service/auth.service";

@Injectable({
    providedIn: 'root'
  })
export class TokenInterceptor implements HttpInterceptor{
    
    constructor(private authService: AuthService) {
    
    }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = this.authService.token;
        if (token) {
            const authreq = req.clone({
              headers: req.headers.set('Authorization', `Bearer ${token}`)
            });

            return next.handle(authreq);
        }
        return next.handle(req);
    } 
}