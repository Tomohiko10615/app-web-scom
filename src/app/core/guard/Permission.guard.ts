import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { Observable } from "rxjs";
import { UsuarioService } from "../service/usuario.service";

@Injectable()
export class PermissionGuard implements CanActivate {
    constructor(private usuarioService: UsuarioService,
        private router: Router){}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
       if(this.usuarioService.hasRoleGuard(route)){
        this.router.navigate(['/**']) ;
        return false;
       }else {
        return true;
       } 
    }
 
}