export const errorMessages = {
    login: {
        username: {
            errors: [
                { type: 'required', message: 'El usuario es requerido.'},
                { type: 'maxlength', message: 'Solo se permiten maximo 25 caracteres.'}
            ]
        },
        password: {
            errors: [
                { type: 'required', message: 'La constraseña es requerido.'},
                { type: 'maxlength', message: 'Solo se permiten maximo 100 caracteres.'}
            ]
        },   
        idPerfil: {
            errors: [
                { type: 'required', message: 'Seleccione un perfil para ingresar.'},
            ]
        }   
    },
    marca: {
        errors :[
            { type: 'required', message: 'El campo es requerido.'},
        ]
    },
    medidor: {
        errors :[
            { type: 'required', message: 'El campo es requerido.'},
        ]
    },
    modelo: {
        errors :[
            { type: 'required', message: 'El campo es requerido.'},
        ]
    },
    modelo_medida: {
        errors :[
            { type: 'required', message: 'El campo es requerido.'},
        ]
    },
    orden_trabajo: {
        errors :[
            { type: 'pattern', message: 'Solo se permiten números.'},
        ]
    },
    reporte_medidores: {
        errors :[
            { type: 'pattern', message: 'Solo se permiten números.'},
        ]
    },

    activar_suministro: {
        cadena: {
            errors:[
                { type: 'matchRequired', message: 'Debe seleccionar una opción de la lista.'},
                { type: 'required', message: 'El campo es requerido.'},
            ]
        },
        sector: {
            errors:[
                { type: 'minlength', message: 'El sector debe tener 2 dígitos.'},
                { type: 'maxlength', message: 'El sector debe tener 2 dígitos.'},
                { type: 'pattern', message: 'Solo se permiten números.'},
                { type: 'required', message: 'El campo es requerido.'},
            ]
        },
        zona: {
            errors:[
                { type: 'minlength', message: 'La zona debe tener 3 dígitos.'},
                { type: 'maxlength', message: 'La zona debe tener 3 dígitos.'},
                { type: 'pattern', message: 'Solo se permiten números.'},
                { type: 'required', message: 'El campo es requerido.'},
            ]
            
        },
        correlativo: {
            errors:[
                { type: 'minlength', message: 'El correlativo debe tener 4 dígitos.'},
                { type: 'maxlength', message: 'El correlativo debe tener 4 dígitos.'},
                { type: 'pattern', message: 'Solo se permiten números.'},
                { type: 'required', message: 'El campo es requerido.'},
            ]
        }
    },
    reporte_novedades: {
        errors :[
            { type: 'pattern', message: 'Solo se permiten números.'},
        ]
    },
    actualizar_datos_ord_insp_cnr: {
        numericos_decimales: {
            errors:[
                { type: 'min', message: 'El valor ingresado debe ser mayor a Cero (0.00).'},
                { type: 'minlength', message: 'largo minimo incorrecto'},
                { type: 'maxlength', message: 'largo maximo sobrepasado'},
                { type: 'pattern', message: 'Solo se permiten números con 2 decimales'},
                { type: 'required', message: 'El campo es requerido.'},
            ]
        },
        numericos: {
            errors:[
                { type: 'min', message: 'El valor ingresado debe ser mayor a Cero (0).'},
                { type: 'minlength', message: 'largo minimo incorrecto'},
                { type: 'maxlength', message: 'largo maximo sobrepasado'},
                { type: 'pattern', message: 'Solo se permiten números sin decimales'},
                { type: 'required', message: 'El campo es requerido.'},
            ]
        },
        string_comentario: {
            errors:[
                { type: 'minlength', message: 'largo minimo incorrecto'},
                { type: 'maxlength', message: 'Largo maximo de 500 caracteres sobrepasado'},
                { type: 'pattern', message: 'Solo se permiten números sin decimales'},
                { type: 'required', message: 'El campo es requerido.'},
            ]
        },
    },
};
