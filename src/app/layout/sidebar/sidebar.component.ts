import { Router, NavigationEnd } from "@angular/router";
import { DOCUMENT } from '@angular/common';
import {
  Component,
  Inject,
  ElementRef,
  OnInit,
  Renderer2,
  HostListener,
  OnDestroy,
  AfterViewInit
} from '@angular/core';
import { ROUTES } from './sidebar-items';
import { AuthService } from 'src/app/core/service/auth.service';
import { MenuService } from 'src/app/core/service/menu.service';
import { UsuarioService } from 'src/app/core/service/usuario.service';
import { Subject, tap } from 'rxjs';
import { Menu } from 'src/app/core/models/menu.model';
import { UsuarioPerfil, UsuarioToken } from 'src/app/core/interfaces/core.interface';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, OnDestroy {
  public sidebarItems: any[];
  level1Menu = '';
  level2Menu = '';
  level3Menu = '';
  public innerHeight: any;
  public bodyTag: any;
  listMaxHeight: string;
  listMaxWidth: string;
  headerHeight = 60;
  routerObj = null;
  componentDestroyed$: Subject<boolean> = new Subject()

  //scom globa values
  menus :any[] = [];
  usuarioToken: UsuarioToken = {id: 0, username: '', nombrePerfil: '', id_perfil: 0, cod_perfil:'',  authorities: []};
  usuarioPerfilSession : UsuarioPerfil = { idPerfil:0 , username: '', nombrePerfil:'', paramNombre:''};

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2,
    public elementRef: ElementRef,
    private authService: AuthService,
    private router: Router,
    private menuService: MenuService,
    private usuarioService: UsuarioService
  ) {
    this.routerObj = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // logic for select active menu in dropdown
        const currenturl = event.url.split('?')[0];
        this.level1Menu = currenturl.split('/')[1];
        this.level2Menu = currenturl.split('/')[2];

        // close sidebar on mobile screen after menu select
        this.renderer.removeClass(this.document.body, 'overlay-open');
      }
    });
  }

  @HostListener('window:resize', ['$event'])
  windowResizecall(event) {
    this.setMenuHeight();
    this.checkStatuForResize(false);
  }
  @HostListener('document:mousedown', ['$event'])
  onGlobalClick(event): void {
    if (!this.elementRef.nativeElement.contains(event.target)) {
      this.renderer.removeClass(this.document.body, 'overlay-open');
    }
  }
  callLevel1Toggle(event: any, element: any) {
    if (element === this.level1Menu) {
      this.level1Menu = '0';
    } else {
      this.level1Menu = element;
    }
    const hasClass = event.target.classList.contains('toggled');
    if (hasClass) {
      this.renderer.removeClass(event.target, 'toggled');
    } else {
      this.renderer.addClass(event.target, 'toggled');
    }
  }
  callLevel2Toggle(event: any, element: any) {
    if (element === this.level2Menu) {
      this.level2Menu = '0';
    } else {
      this.level2Menu = element;
    }
  }
  callLevel3Toggle(event: any, element: any) {
    if (element === this.level3Menu) {
      this.level3Menu = '0';
    } else {
      this.level3Menu = element;
    }
  }
  ngOnInit(): void {
  
 
  
     
      

   // if (this.authService.currentUserValue) {
   /*   

     this.sidebarItems = ROUTES.filter((sidebarItem) => sidebarItem);
     console.log(this.sidebarItems[0].moduleName);
     console.log(this.sidebarItems[0].submenu.length);
    //}
    console.log(this.menus);  */
    this.initLeftSidebar();
    this.bodyTag = this.document.body;
    this.obtenerMenuItems();

    
  }

  obtenerMenuItems(): void {

    if(sessionStorage.getItem("usuarioPerfil") != null) {
      this.usuarioPerfilSession = JSON.parse(sessionStorage.getItem("usuarioPerfil"));
    }
    

    console.log(this.usuarioPerfilSession?.idPerfil);
      if(this.usuarioPerfilSession) {
        this.menuService.getMenuByIdPerfil(this.usuarioPerfilSession?.idPerfil).pipe(
          tap((response) => this.menus = response)
        ).subscribe(); 
      } 

  }
  ngOnDestroy() {
    this.routerObj.unsubscribe();
  }
  initLeftSidebar() {
    const _this = this;
    // Set menu height
    _this.setMenuHeight();
    _this.checkStatuForResize(true);
  }
  setMenuHeight() {
    this.innerHeight = window.innerHeight;
    const height = this.innerHeight - this.headerHeight;
    this.listMaxHeight = height + '';
    this.listMaxWidth = '500px';
  }
  isOpen() {
    return this.bodyTag.classList.contains('overlay-open');
  }
  checkStatuForResize(firstTime) {
    if (window.innerWidth < 1170) {
      this.renderer.addClass(this.document.body, 'ls-closed');
    } else {
      this.renderer.removeClass(this.document.body, 'ls-closed');
    }
  }
  mouseHover(e) {
    const body = this.elementRef.nativeElement.closest('body');
    if (body.classList.contains('submenu-closed')) {
      this.renderer.addClass(this.document.body, 'side-closed-hover');
      this.renderer.removeClass(this.document.body, 'submenu-closed');
    }
  }
  mouseOut(e) {
    const body = this.elementRef.nativeElement.closest('body');
    if (body.classList.contains('side-closed-hover')) {
      this.renderer.removeClass(this.document.body, 'side-closed-hover');
      this.renderer.addClass(this.document.body, 'submenu-closed');
    }
  }
}
